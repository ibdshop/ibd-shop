var droppableObject = false;

(function ($) {

	function msieversion()
	{
		var ua = window.navigator.userAgent;
		var msie = ua.indexOf("MSIE ");

		if (msie > 0)      // If Internet Explorer, return version number
			return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
		else                 // If another browser, return 0
			return false;
	}

	if (msieversion() != false) {
		document.documentElement.className += ' ie_x';
	}


	function AttrItem(htmlItem, mainContainer, container)
	{
		var $this = this
			, $htmlItem = $(htmlItem)
			, $childrenContainer = $htmlItem.find('ul')
			, $childInput = $htmlItem.find('input.attr_group')
			, $enableInput = $htmlItem.find('input.attr_enable')

			, isChild = false
			, isGroup = false
			, isEnabled = false
			, parentObject = false

			, tmpGroupLabel = '';

		$this.code = $htmlItem.data('value');

		// bind htmlItem
		$htmlItem
			.data('fn-attr-item', $this)
			.droppable({
				accept: ".attr_item",
				hoverClass: "ui-state-hover",
				drop: function( event, ui ) {
					// An item that already is child cannot contains another items
					if (isChild) {
						return false;
					}

					//$this.addChild(ui.draggable, '');
					droppableObject = $this;
				},
				over: function() {
					$(container).find('.ui-sortable-placeholder').addClass('bg-white');
				},
				out: function () {
					$(container).find('.ui-sortable-placeholder').removeClass('bg-white');
				}
			})
			.disableSelection();

		this.changeStatus = function (en) {
			isEnabled = en;
			$enableInput.val((isEnabled)? $this.code: '');
		}

		this.append = function () {
			// return if exists
			if (isEnabled) {
				return false;
			}

			// return if no selected
			if (! $htmlItem.hasClass('active')) {
				return false;
			}

			$this.changeStatus(true);
			$htmlItem.appendTo(container);
			$htmlItem.removeClass("active");
		}
		//this.append();

		this.remove = function () {
			// return if exists
			if (! isEnabled) {
				return false;
			}

			if (isGroup) {
				$childrenContainer.find('> li').each(function () {
					var itemObj = $(this).data('fn-attr-item');
					if (itemObj) {
						itemObj.cancelChild(mainContainer);
						//itemObj.changeStatus(false);
						itemObj.remove();
					}
				});
			}
			/*
			if (isChild) {
				$this.cancelChild(false);
			}*/
			//isChild = false;
			isGroup = false;
			//parentObject = false;
			//$childInput.val('');
			$this.changeStatus(false);
			$htmlItem.removeClass("active");
		} 

		this.addChild = function($item, group_label) {
			if (!isEnabled) {
				return false;
			}
			$this.makeGroup(group_label);

			// should be <ul
			var itemObj = $item.data('fn-attr-item');
			if (itemObj) {
				itemObj.makeChild($this, $childrenContainer);
			}
			$item.css({'top': '0', 'left': 0});
		}

		this.makeChild = function (parentObj, parentItem) {
			if (parentItem) {
				$htmlItem.appendTo(parentItem);
			}
			if (parentObject) {
				parentObject.childMoveOut();
			}
			parentObject = parentObj;
			$childInput.val(parentObj? parentObj.code: '');
			isChild = true;

			// if it is group
			if (isGroup && parentObj) {
				$childrenContainer.find('> li').each(function () {
					var itemObj = $(this).data('fn-attr-item');
					if (itemObj) {
						itemObj.makeChild(parentObj, parentItem);
					}
				});
			}
		}

		this.cancelChild = function (cont) {
			this.makeChild(false, cont);
			isChild = false;
		}

		this.makeGroup = function(group_label) {
			if (!isGroup) {
				$this.tmpGroupLabel = group_label? group_label: $htmlItem.find('label').text();

				var $div = $('<div></div>')
					.addClass('group_name')
					.prependTo($htmlItem);

				var $input = $('<input />')
					.attr('type', 'text')
					.attr('name', 'groups[attributes][fields][name][value][' + $this.code + ']')
					.attr('placeholder', 'Name of group')
					.val($this.tmpGroupLabel + ' (Group)')
					.focus(function() {
						$div.find('.ok,.cancel').show();
						$div.find('.edit').hide();
						$input.val($this.tmpGroupLabel);
					})
					.keypress(function(e) {
						if(e.which == 13) {
							$this.tmpGroupLabel = $input.val();
							$input.blur();
						}
					})
					.blur(function(event) {
						$div.find('.ok,.cancel').hide();
						$div.find('.edit').show();
						$input.val($this.tmpGroupLabel + ' (Group)');
					})
					.appendTo($div);

				$('<span></span>')
					.addClass("cancel")
					.attr('title', "cancel")
					.click(function() {
						$input.val($this.tmpGroupLabel + ' (Group)').blur();
					})
					.appendTo($div);

				$('<span></span>')
					.addClass("ok")
					.attr('title', "save")
					.click(function() {
						$this.tmpGroupLabel = $input.val();
						$input.blur();
					})
					.appendTo($div);

				$('<span></span>')
					.addClass("edit")
					.attr('title', "edit")
					.click(function() {
						$input.focus();
					})
					.appendTo($div);

				isGroup = true;
				$htmlItem./*draggable("disable").*/addClass('group');
			}
		}

		this.childMoveOut = function() {
			// clear prev parent's ul and text if empty
			if ($childrenContainer && $childrenContainer.find('li').length == 0) {
				isGroup = false;
				$htmlItem
					.removeClass('group')
					//.draggable("enable")
					.find('.group_name')
						.remove();
			}
		}
	}

	$.fn.attrItemPlugin = function(fields) {
		var settings = {
			'main_list': '#productsfilter_attributes_enable',
			'linked_list': '#productsfilter_attributes_group',
			'data_attr_name': 'fn-attr-item'
		};

		// fix html columns (created by Magento)
		$(settings.linked_list).parent().removeClass('scope-label').next().addClass('scope-label').find('div').show();

		// create objects
		this.find('li').each(function() {
			var $th = $(this);
			$th.data(settings.data_attr_name, new AttrItem(this, settings.main_list, settings.linked_list));
			$th.attr('id', 'attr_item_' + $th.data('value'));
		});

		// create childs
		var getItemAndObjectFromCode = function (code, callback) {
			var $item = $('#attr_item_' + code);
			if ($item.length > 0) {
				var itemObj = $item.data(settings.data_attr_name);

				if (itemObj) {
					callback($item, itemObj);
				}
			}
		}

		for (var i = 0, _len = enabledAttributesCodes.length; i < _len; i++) {
			getItemAndObjectFromCode(enabledAttributesCodes[i], function($htmlItem, itemObj) {
				itemObj.append();
			});
		}

		for (parentCode in fields) {
			getItemAndObjectFromCode(parentCode, function ($parentItem, parentObj) {
				for (var i = 0, len = fields[parentCode]['children'].length; i < len; i++) {
					getItemAndObjectFromCode(fields[parentCode]['children'][i], function ($childItem, childObj) {
						parentObj.addChild($childItem, fields[parentCode]['label']);
					});
				}
			});
		}

		// add classes
		$(settings.linked_list).addClass('sortable');
		$(settings.main_list).addClass('sortable');

		// make sortable list
		$('.sortable').sortable({
			connectWith: '.sortable',
			/*
			sort: function(event, ui) {
				if (ui.item.hasClass("group")) {
					return false;
				}
			},*/
			stop: function(event, ui) {
				if (droppableObject) {
					droppableObject.addChild(ui.item, '');
				} else {
					var itemObj = ui.item.data('fn-attr-item');
					if (itemObj) {
						itemObj.cancelChild(false);
					}
				}
				droppableObject = false;
			},
			receive: function(e, ui) {
				var itemObj = ui.item.data('fn-attr-item');
				if (itemObj) {
					if (ui.item.closest('ul').hasClass('list_enabled')) {
						itemObj.changeStatus(true);
					} else {
						itemObj.remove();
					}
				}
			}
		})
		$('.sortable li').click(function(event) {
			$('.sortable li').removeClass('active');
			$(this).toggleClass('active');
			//event.stopPropagation();
		});

		return this;
	};
}(pjQuery_1_9));
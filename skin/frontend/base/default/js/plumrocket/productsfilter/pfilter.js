/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Product_Filter-v1.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/


pjQuery_1_9(document).ready(initProductFilterFields);

productFilterBusy = false;
var ATTRIBUTE_DELIMITER	= '--';
var OPTION_DELIMITER		= '-';

function initProductFilterFields()
{
	var $ = pjQuery_1_9;
	var $toolbar = $('.product-filter').parent();
	var $systemToolbar = $('.toolbar');
	var touchMove = false;

	if (($(window).width() < 480) && ($toolbar.find('.product-filter-param .filter_options > *').length > 0)) {
		$('.reset-filter-mobile').show();
	}

	// hide selector
	$toolbar.find('.selector').mouseleave(function() {
		var $this = $(this);
		$this.find('.dropdowner').css('display', 'none');
		$this.find('.border-hidder').css('display', 'none');
	});

	$toolbar.find('.selector .hld').on('touchmove', function() {
		touchMove = true;
	});

	// open an attribute select with options
	$toolbar.find('.selector .hld').on('touchend click', function() {
		if (touchMove) {
			touchMove = false;
			return false;
		}
		touchMove = false;
		
		var $selector = $(this).parent();
		var $dropdowner = $selector.find('.dropdowner');
		// save state before hide all selecters
		var dropdowner_is_hidden = ! $dropdowner.is(':visible');

		// hide all
		$toolbar.find('.dropdowner').css('display', 'none');
		$toolbar.find('.border-hidder').css('display', 'none');
		
		if (dropdowner_is_hidden) {
			$dropdowner.css('display', 'block');
			$selector.find('.border-hidder').css('display', 'block');
		}
		return false;
	});

	var _sendUrl = function(url, clearPage) {
		if (url && (url != '#')) {
			setProductFilterLocation(url, clearPage);
		}
		return false;
	}

	$systemToolbar.find('.sorter a, .pager a, .limiter a').click(function() {
		return _sendUrl($(this).attr('href'), false);
	});
	$toolbar.find('.product-filter a, .product-filter-param a').click(function() {
		$this = $(this);
		if ($this.hasClass('disabled') && !$this.hasClass('active-list')) {
			return false;
		}
		if ((productFilterSendMode == 'package') && !$this.hasClass('delete_option')) {
			$toolbar.find('.filter_send').show();
			if ($this.hasClass('list')) {
				var itemClassNode = /pf([a-z0-9A-Z\_]+)/.exec($this.attr('class'))
				if (itemClassNode && itemClassNode[0]) {
					var $element = $('.' + itemClassNode[0]);
					if ($element.hasClass('active-list')) {
						$element.removeClass('active-list');
					} else {
						$element.addClass('active-list');
					}
				}
			}
		} else {
			_sendUrl($this.attr('href'), true);
		}
		return false;
	});

	$toolbar.find('.filter_send').click(function() {
		var activeItems = {}

		$toolbar.find('.product-filter a.active-list').each(function() {
			var $a = $(this);
			var attr = $a.data('attribute');
			var option = $a.data('option');

			if (! (attr in activeItems)) {
				activeItems[ attr ] = {};
			}
			activeItems[attr][option] = 1;
		});

		var nodes = [];
		for (var attr in activeItems) {
			nodes.push( attr + OPTION_DELIMITER + Object.keys(activeItems[attr]).join(OPTION_DELIMITER) );
		}
		var result = nodes.join(ATTRIBUTE_DELIMITER);
		if (result) {
			if (productFilterCatalogUseFuffix) {
				result = '?filter=' + result;
			} else {
				var prefix = '';
				if (productFilterCleartUrl[ productFilterCleartUrl.length - 1 ] != '/') {
					prefix = '/';
				}
				result = prefix + 'filter/' + result + '/';
			}
		} else {
			result = '';
		}
		return _sendUrl(productFilterCleartUrl + result, true);
	});

	$systemToolbar.find('select').attr('onchange', 'setProductFilterLocation(this.value, false);');
}

function setProductFilterLocation(url, clearPage)
{
	if (productFilterBusy) {
		return false;
	}
	var $toolbar = pjQuery_1_9('.product-filter').parent();
	var $systemToolbar = pjQuery_1_9('.toolbar');

	$toolbar.find('.filter_send').hide();
	if (pjQuery_1_9('.productsfilter-main-container').length > 0) {
		$toolbar.find('.product-filter-lock').show();	
	} else {
		$toolbar.find('.product-filter-loader').show();
	}
	
	productFilterBusy = true;

	if (clearPage) {
		url = url.replace(/(\?|&){1}p=[0-9]+&/g, '$1')
			.replace(/[\?|&]{1}p=[0-9]+$/g, '');
	}

	pjQuery_1_9.post(url, {'ajax': 1}, function(data) {
		/*
		var response = pjQuery_1_9.parseJSON(data);
		if (typeof response =='object') {
			if ('productlist' in response) {
				data = response.productlist;
			}
		}*/

		var data = JSON.parse(data);

		jQuery(document).unbind('product-media-loaded');

		//$systemToolbar.parent().replaceWith(data.productlist);
		$systemToolbar.parent().parent().html(data.category_view);
		if (data.navigation_bar) {
			$toolbar.replaceWith(data.navigation_bar);
		}
		pjQuery_1_9('.toolbar').parent().parent().find('script').each(function() {
			eval( pjQuery_1_9(this).text() );
		});
		
		//var $toolbar = pjQuery_1_9('.product-filter').parent();
		initProductFilterFields();
		$toolbar.find('.product-filter-loader').hide();
		$toolbar.find('.product-filter-lock').hide();
		productFilterBusy = false;

		// init color swatch
		if (typeof(ProductMediaManager) !== 'undefined') {
			jQuery(document).trigger('product-media-loaded', ProductMediaManager);
		}
		// init Ajax Cart
		if (window.productQuickViewPopup) {
			productQuickViewPopup.run();
		}

		if (window.ajaxCart) {
			ajaxCart.onDocumentReady();
		}
	});

	return false;
}
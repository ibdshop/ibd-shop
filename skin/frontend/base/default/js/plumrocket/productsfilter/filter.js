/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Product_Filter-v1.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/


pjQuery_1_9(document).ready(initProductFilterFields);

productFilterBusy = false;

function initProductFilterFields()
{
	var $ = pjQuery_1_9;
	var $toolbar = $('.product-filter').parent();

	if (($(window).width() < 480) && ($toolbar.find('.product-filter-param .filter_options > *').length > 0)) {
		$('.reset-filter-mobile').show();
	}

	var appendOrDelete = function() {
		$this = $(this);
		$this.toggleClass('active-list');

		var node = 'f' + $this.data('attribute') + '[' + $this.data('option') + ']';

		productFilterCurrentUrl = productFilterCurrentUrl
			.replace('&' + node, '')
			.replace(node, '')
			.replace('?&', '?');

		if ($this.hasClass('active-list')) {
			var prefix = (productFilterCurrentUrl.indexOf("?") > -1)? '&' : '?';
			productFilterCurrentUrl += prefix + node;
		}
	};

	$toolbar.find('.dropdowner > div.list').click(function() {
		var $this = $(this);
		if ($this.hasClass('disabled') && !$this.hasClass('active-list')) {
			return false;
		}
		if (productFilterBusy) {
			return false;
		}
		appendOrDelete.call(this);

		if (productFilterSendMode == 'package') {
			$toolbar.find('.filter_send').show();
		} else if (productFilterSendMode == 'one') {
			setProductFilterLocation(productFilterCurrentUrl, true);
		}
		return false;
	});

	$toolbar.find('.filter_send').click(function() {
		setProductFilterLocation(productFilterCurrentUrl, true);
	});

	$toolbar.find('.product-filter-param .close-fo').click(function() {
		var $this = $(this);
		if (productFilterBusy) {
			return false;
		}
		var $param = $this.parent();

		$('.dropdowner > div.filter_group_' + $param.data('attribute'))
			.filter('div[data-option="' + $param.data('option') + '"]:first')
			.click();
		$param.addClass('deleted');
		$this.remove();
		return false;
	});

	$toolbar.find('.reset-filter, .reset-filter-mobile').click(function() {
		if (productFilterBusy) {
			return false;
		}
		$toolbar.find('.dropdowner > div.active-list').each(appendOrDelete);
		setProductFilterLocation(productFilterCurrentUrl, true);
		return false;
	});

	$toolbar.find('.selector').mouseleave(function() {
		var $this = $(this);
		$this.find('.dropdowner').css('display', 'none');
		$this.find('.border-hidder').css('display', 'none');
	});

	$toolbar.find('.selector .hld').click(function() {
		var $this = $(this);
		if ($this.parent().hasClass('product-filter-disabled')) {
			return false;
		}
		var $dropdowner = $this.parent().find('.dropdowner');
		var dropdowner_is_hidden = ! $dropdowner.is(':visible');

		$toolbar.find('.dropdowner').css('display', 'none');
		$toolbar.find('.border-hidder').css('display', 'none');
		
		if (dropdowner_is_hidden) {
			$dropdowner.css('display', 'block');
			$this.parent().find('.border-hidder').css('display', 'block');
		}
		return false;
	});

	$toolbar.find('.sorter a, .pager a, .limiter a').click(function() {
		var url = $(this).attr('href');
		if (url && (url != '#')) {
			setProductFilterLocation(url, false);
		}
		return false;
	});

	$toolbar.find('select').attr('onchange', 'setProductFilterLocation(this.value, false);');
}

function setProductFilterLocation(url, clearPage)
{
	if (productFilterBusy) {
		return false;
	}
	var $toolbar = pjQuery_1_9('.product-filter').parent();

	$toolbar.find('.filter_send').hide();
	$toolbar.find('.product-filter-loader').show();
	productFilterBusy = true;

	if (clearPage) {
		url = url.replace(/(\?|&){1}p=[0-9]+&/g, '$1')
			.replace(/[\?|&]{1}p=[0-9]+$/g, '');
	}

	pjQuery_1_9.post(url, {'ajax': 1}, function(data) {
		/*
		var response = pjQuery_1_9.parseJSON(data);
		if (typeof response =='object') {
			if ('productlist' in response) {
				data = response.productlist;
			}
		}*/

		$toolbar.parent().replaceWith(data);
		
		//var $toolbar = pjQuery_1_9('.product-filter').parent();
		initProductFilterFields();
		$toolbar.find('.product-filter-loader').hide();
		productFilterBusy = false;

		window.scrollTo(0, 0);
	});

	return false;
}

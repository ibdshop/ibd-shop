/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2011-2014 Webcomm Pty Ltd
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
(function($,window,document,undefined) {
    'use strict';
    var _ = undefined, // local lodash (wait until we're attaching to grab it)
        opacity = 'opacity',
        disabledProp = 'disabled',
        hidePageFloatCookie = 'hidePageFloat',
        pageFloatUpdateTimeCookie = 'pageFloatUpdateTime';

    // constructor function
    function Site(settings) {

        this.windowLoaded = false;

    }

    Site.prototype = {
        constructor: Site, // assign the constructor

        start: function() {

            this.attach();

        },

        attach: function() {
            this.renumberCheckoutSteps();
            this.attachBootstrapPrototypeCompatibility();
            this.attachMedia();
            this.attachSearchBox();
            this.attachSidebar();
            this.setupMenuQR();
            this.attachAd();
            this.narrowWidth();
            this.initHorizontalScroll();
            _ = IBD_lodash || undefined;
            if(_) {
                _ = IBD_lodash;
                _.templateSettings = {
                    interpolate: /\{\{(.+?)\}\}/g,
                    evaluate: /\{\[(.+?)\]\}/g,
                    escape: /\{\{-(.+?)\}\}/g
                };
                this.processTextColors();
                this.attachMaps(window.ibdMapLocations);
                this.attachThumbnailGalleries();
            }
            this.setupJSPagerHellip();
            this.addDownloadLinkGlyphs();
            this.attachBrandFilters();
            this.setupBrandFilterMobileSupport();
            this.setupMasonry();
            this.ajaxifyQuantity(this);
            this.animateCart();
            this.animatePageFloat( document.cookie.indexOf(hidePageFloatCookie) >= 0 );
            this.setupCheckoutFapiao();
        },

        ajaxifyQuantity: function() {

            function saveCart(liElement, quantity) {
                console.log('saving ' + liElement.data('id') + ': ' + quantity);
                return $.ajax({
                    url: '/ibd/ajax/modifyCart',
                    method: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify({
                        productId: liElement.data('id'),
                        quantity: quantity
                    }),
                    dataType: 'json'
                });
            };

            function respondUpdateQuantity(quantityInput, data) {
                console.log('success response ' + JSON.stringify(data));
                if( data.error ){
                    quantityInput.val(data.quantity);
                }
                else{
                    $('#cart-subtotal').html(data.subtotal);
                    $('#cart-button span.tag').html(data.count);
                    if(data.count == 0){
                        $('#cart-button span.tag').hide();
                    }
                }
            };

            var quantityWrapperFinder = '.quantity-wrapper',
                quantityWrapper = $(quantityWrapperFinder),
                inputQuantityLocator = 'input.quantity',
                minusButtonLocator = 'button.minus',
                liItemLocator = 'li.item',
                skipSave = 'skipSave',
                timeout = null,
                saveCartTimeoutDuration = 600,
                quantityError = 'There was an error updating the cart quantity, please try again. '+
                                'If this issue persists, please contact support: ';

            quantityWrapper.find('button.remove').click(function(){

                if( !confirm('您确定吗？') ) return;

                var button = $(this),
                    liElement = button.closest(liItemLocator);

                button.prop(disabledProp, true);
                liElement.css(opacity, 0.5);

                if( !button.closest(quantityWrapperFinder).find(inputQuantityLocator).data(skipSave) ) {
                    saveCart(liElement, 0).done(function(data, textStatus, jqXHR){
                        liElement.remove();
                        respondUpdateQuantity(null, data);
                    })
                    .fail(function(jqXHR, textStatus, errorThrown){
                        console.log('error removing');
                        button.prop(disabledProp, false);
                        liElement.css(opacity, 1);
                        alert(quantityError + textStatus + ' ' + errorThrown);
                    });
                }
            });

            quantityWrapper.find('button.plus').click(function(){

                var quantityInput = $(this).siblings(inputQuantityLocator),
                    quantity = quantityInput.val();

                clearTimeout(timeout);

                if( isNaN(quantity) ){
                    quantity = 0;
                }

                quantityInput.val(++quantity);
                timeout = setTimeout(function() {
                    if( !quantityInput.data(skipSave) ){
                        saveCart(quantityInput.closest(liItemLocator), quantity).done(function(data, textStatus, jqXHR){
                            respondUpdateQuantity(quantityInput, data);
                        })
                        .fail(function(jqXHR, textStatus, errorThrown){
                            alert(quantityError + textStatus + ' ' + errorThrown);
                        });
                    }
                }, saveCartTimeoutDuration);

                $(this).siblings(minusButtonLocator).prop(disabledProp, false);
            });

            quantityWrapper.find(minusButtonLocator).click(function(){

                var quantityInput = $(this).siblings(inputQuantityLocator),
                    quantity = quantityInput.val();

                clearTimeout(timeout);

                if( isNaN(quantity) || quantity == 0) {
                    quantity = 2;
                }

                quantityInput.val(--quantity);
                timeout = setTimeout(function() {
                    if( !quantityInput.data(skipSave) ){
                        saveCart(quantityInput.closest(liItemLocator), quantity).done(function(data, textStatus, jqXHR){
                            respondUpdateQuantity(quantityInput, data);
                        })
                        .fail(function(jqXHR, textStatus, errorThrown){
                            alert(quantityError + textStatus + ' ' + errorThrown);
                        });
                    }
                }, saveCartTimeoutDuration);

                if( quantity == 1 ){
                    $(this).prop(disabledProp, true);
                }
            });

            var quantityInputs = quantityWrapper.find(inputQuantityLocator);
            quantityInputs.each(function(){
                if(this.value == 1){
                    $(this).siblings(minusButtonLocator).prop(disabledProp, true);
                }
            });
            quantityInputs.change(function(){

                var quantityInput = $(this);

                clearTimeout(timeout);

                if( isNaN(quantityInput.val()) || quantityInput.val() == '' || quantityInput.val() == 'e' ) {
                    quantityInput.val(1);
                    quantityInput.siblings(minusButtonLocator).prop(disabledProp);
                }

                if( !quantityInput.data(skipSave) ){
                    saveCart(quantityInput.closest(liItemLocator), quantityInput.val()).done(function(data, textStatus, jqXHR){
                        respondUpdateQuantity(quantityInput, data);
                    })
                    .fail(function(jqXHR, textStatus, errorThrown){
                        alert(quantityError + textStatus + ' ' + errorThrown);
                    });
                }
            });
        },

        animateCart: function() {
            var showClass = 'show',
                cart = $('#slide-cart'),
                cartButton = $('#cart-button'),
                blackoutDiv = $('#blackout');

            function closeCart(){
                cart.removeClass(showClass);
                blackoutDiv.removeClass(showClass);
            };

            cartButton.click(function(){
                
                if( !$('#is-xs').is(':hidden') ){
                    cart.offset({ top: 0 });
                }
                else{
                    cart.offset({ top: cartButton.offset().top + cartButton.height() + 20 });
                }

                cart.css( 'max-height', ($(window).height() - cart.offset().top) + 'px' );
                cart.toggleClass(showClass);
                blackoutDiv.toggleClass(showClass);
            });

            $('#close-cart').click(closeCart);
            blackoutDiv.click(closeCart)
        },
        
        initHorizontalScroll: function() {
            $(".horizontal-scroll").mCustomScrollbar({
                axis: "x",
                theme: "dark",
                scrollInertia: 10
            }); 
        },

        attachBootstrapPrototypeCompatibility: function() {

            // Bootstrap and Prototype don't play nice, in the sense that
            // prototype is a really wacky horrible library. It'll
            // hard-code CSS to hide an element when a hide() event
            // is fired. See http://stackoverflow.com/q/19139063
            // To overcome this with dropdowns that are both
            // toggle style and hover style, we'll add a CSS
            // class which has "display: block !important"
            $('*').on('show.bs.dropdown show.bs.collapse', function(e) {
                $(e.target).addClass('bs-prototype-override');
            });

            $('*').on('hidden.bs.collapse', function(e) {
                $(e.target).removeClass('bs-prototype-override');
            });
        },

        attachMedia: function() {
            var $links = $('[data-toggle="media"]');
            if ( ! $links.length) return;

            // When somebody clicks on a link, slide the
            // carousel to the slide which matches the
            // image index and show the modal
            $links.on('click', function(e) {
                e.preventDefault();

                var $link = $(this),
                   $modal = $($link.attr('href')),
                $carousel = $modal.find('.carousel'),
                    index = parseInt($link.data('index'));

                $carousel.carousel(index);
                $modal.modal('show');

                return false;
            });
        },

        narrowWidth: function() {
            $('.std').addClass(' col-lg-10 col-lg-offset-1');
        },

        processTextColors: function() {
            _.forEach($('.auto-text-color'), function(obj) {
                var explicitColor = obj.style.color;
                if(!explicitColor || explicitColor == '#'){
                    var jobj = $(obj);

                    while(!jobj[0].style.backgroundColor){
                        if(!jobj) return;
                        jobj = jobj.parent();
                    }
                    var bkgRegEx = jobj[0].style.backgroundColor.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
                    var r = bkgRegEx[1];
                    var g = bkgRegEx[2];
                    var b = bkgRegEx[3];
                    var yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;
                    obj.style.color = '#' + (yiq >= 128 ? '333333' : 'F6F6F5');
                }
            });
        },

        attachSidebar: function() {
            var filter = $('.sidebar-filter > .collapse');

            var collapseSidebar = function() {
                var viewport = $(window).width();

                if ( viewport < 768 ) {
                    filter.removeClass('in');
                    filter.siblings('.panel-heading').addClass('collapsed');
                } 
                else {
                    if( ! filter.hasClass('in') ) filter.addClass('in');
                    
                }
            };

            collapseSidebar();

            $(window).resize(function() {
                collapseSidebar();
            });
        },

        attachAd: function(){
            var bannerAd = $('.banner-ad'),
                brandShowCase = $('.brand-showcase-bkg');

            var heightInit = function() {
                var wjnWidth = $(window).width(),
                    originHeight = brandShowCase.height();

                if ( wjnWidth > 767 ) {
                    bannerAd.height(originHeight);
                }

            };

            heightInit();

            $(window).resize(function(event) {
                heightInit();
            });
        },

        attachSearchBox: function() {
            // Travelsuite Search
            var search = $('#travelsuite-search'),
                divider = $('#nav-divider'),
                navBarForm = $('.navbar-main-collapse .navbar-form'),
                delay = 300;

            search.val("");

            // the search-box animation
            $('#travelsuite-search-button').click(function(e){
                navBarForm.toggleClass('search-open');
                if(navBarForm.hasClass('search-open')) {
                    search.focus();
                }
            });

            // Product Search
            var productSearch = $('#search'),
                divider = $('#nav-divider'),
                productNavBarForm = $('.menu-search.navbar-form'),
                delay = 300;

            productSearch.val("");

            // the search-box animation
            $('#search-button').click(function(e){
                productNavBarForm.toggleClass('search-open');
                if(productNavBarForm.hasClass('search-open')) {
                    productSearch.focus();
                }
            });
        },

        attachMaps: function(mapLocations) {

            if(!mapLocations) return;

            if(typeof(google) === 'undefined')
            {   
                $('.map').html('无法加载地图');
                return;
            }

            var me = this,
                windowTemplate = _.template($('#mapWindowTemplate').html()),
                maps = {};

            var openInfoWindow = function(map, marker, infoWindow) {
                return function(){
                    infoWindow.open(map,marker);
                };
            };

            var resizeMap = function(map) {
                if(!map) return;
                var center = map.getCenter();
                google.maps.event.trigger(map, 'resize');
                map.setCenter(center);
            };

            _.forEach(mapLocations, function(info,mapId){
                var center = new google.maps.LatLng(info.latLng.lat, info.latLng.lng);
                var mapLocator = '#' + mapId;

                var map = new google.maps.Map($(mapLocator)[0], {
                    zoom : 16,
                    center: center,
                    panControl: false,
                    zoomControl: true,
                    scrollwheel: false,
                    mapTypeControl: true,
                    scaleControl: false,
                    streetViewControl: true,
                    overviewMapControl: true
                });

                var marker = new google.maps.Marker({
                    position: center,
                    map: map,
                    title: info.title,
                    draggable:false,
                });

                var infoWindow = new google.maps.InfoWindow({
                    content: windowTemplate(info),
                    maxWidth: 300
                });

                google.maps.event.addListener(marker, 'click', openInfoWindow(map, marker, infoWindow));

                maps[mapLocator] = map;
            });

            // re-load the map when window resize
            $(window).resize(function(){
                // oo that's nice, we can just pass the resize function (forEach function gets the array value [the
                // map] first and the array index second)
                _.forEach(maps, resizeMap);
            });

            $('*').on('show.bs.collapse', function(e){
                
                var targjt = $(e.target);

                resizeMap(maps[ '#' + targjt.find('.map').attr('id') ]);

                targjt.prev().addClass('active');
            });

            $('*').on('hide.bs.collapse', function(e){
                $(e.target).prev().removeClass('active');
            });
        },

        attachThumbnailGalleries: function(){
            var mainImages = $('.main-image'),
                imageContainer = mainImages.parent(),
                hideClassName = 'hidden',
                selectedClassName = 'opening',
                duration = 200,
                openImage = null;

            var wrapperWidth = imageContainer.width();

            $('.image-thumb-box').click(function(e){
                var thumb = $(e.currentTarget).find('img') 
                if(thumb.length > 0)
                {
                    var newImage = mainImages.parent().find('#main-image-'+thumb[0].dataset['imgNum']);
                    if(newImage.length > 0 && newImage[0].id != openImage[0].id)
                    {
                        newImage.addClass(selectedClassName);
                        newImage.removeClass(hideClassName);
                        openImage.removeClass(selectedClassName);
                        newImage.width(imageContainer.width());
                        newImage.animate({opacity:1.0}, duration, function(){
                            openImage.removeClass(selectedClassName);
                            openImage.addClass(hideClassName);
                            openImage.css(opacity, '0.0');
                            openImage = newImage;
                            openImage.removeClass(selectedClassName);
                            openImage.css('width', '100%');
                        });
                    }
                }
            });

            if(mainImages.length > 0)
            {
                var jmg = $(mainImages[0]);
                jmg.removeClass(hideClassName);
                jmg.css(opacity, '1.0');
                openImage = jmg;
            }
        },

        setupJSPagerHellip: function(){

            if(!window.ibdPaging) return;

            var maxPages = 7; // maximum number of pages to show (should be odd so we have a center)
            var pagerDiv = $('#js-pager'),
                limit = window.ibdPaging['limit'],
                count = window.ibdPaging['count'],
                offset = window.ibdPaging['offset'],
                params = window.ibdPaging['params'],
                pagerTemplate = _.template(
                    '<a href="?offset={{ offset }}&limit={{ limit }}&{{ params }}"><div>{{ text }}</div></a>'),
                hellipText = '<div class="hellip">&hellip;</div>';

            // fewer results than the limit (zero limit means displaying all), no need for paging
            if(count <= limit || limit < 1) return;

            // some more things to track now that we know we're building this thing
            var totalPages = Math.ceil(count / limit),
                currentPage = offset / limit + 1,
                pagesBeforeCurrent = 0,
                pagesAfterCurrent = 0;

            // find out how many pages in either direction from the center we need (this algorithm
            // only works if the maxPages is odd)
            while(pagesBeforeCurrent + pagesAfterCurrent < maxPages - 1 &&
                    pagesBeforeCurrent + pagesAfterCurrent + 1 < totalPages)
            {
                // do we have any space before the current page?
                if(currentPage - pagesBeforeCurrent > 1)
                {
                    pagesBeforeCurrent++;
                }

                // do we have any space after the current page?
                if(currentPage + pagesAfterCurrent < totalPages)
                {
                    pagesAfterCurrent++;
                }
            }

            // add the first page link
            if(currentPage > pagesBeforeCurrent + 1)
            {
                pagerDiv.append(pagerTemplate({
                    text : '1',
                    offset : 0,
                    limit : limit,
                    params : params
                }));
            }

            // add the ellipses
            if(currentPage > pagesBeforeCurrent + 2)
            {
                pagerDiv.append(hellipText);
            }

            // add our pages
            for (var page = 1; page <= totalPages; page++)
            {
                if(page == currentPage) pagerDiv.append('<a class="current"><div>' + page + '</div></a>');
                else if(page >= currentPage - pagesBeforeCurrent && page <= currentPage + pagesAfterCurrent)
                {
                    pagerDiv.append(pagerTemplate({
                        text : page,
                        offset : limit * (page - 1),
                        limit : limit,
                        params : params
                    }));
                }
            }

            // add another ellipses
            if(currentPage + 1 < totalPages - pagesAfterCurrent)
            {
                pagerDiv.append(hellipText);
            }

            // add the last page
            if(currentPage < totalPages - pagesAfterCurrent)
            {
                pagerDiv.append(pagerTemplate({
                    text : totalPages,
                    offset : (totalPages - 1) * limit,
                    limit : limit,
                    params : params
                }));
            }
        },

        attachBrandFilters: function(){

            // return if there is no brand data
            if(!window.ibdFullBrandData) return;

            // grab all the necessary items
            var defaultOptionValue = 'default',
                alphaSelectedClass = 'active',
                alphaLists = $('ul.alpha'),
                alphaSelects = $('select.alpha'),
                productFilter = $('select.product'),
                cityFilter = $('select.city'),
                destinationFilter = $('select.destination'),
                showcaseContainer = $('#brand-showcase-container'),
                directoryContainer = $('#brand-directory-container'),
                searchInput = $('input#brand-search'),
                alphaParents = $('.alpha'),
                showcaseTemplate = _.template($('#brand-showcase-template').html()),
                directoryTemplate = _.template($('#brand-directory-template').html()),
                finalShowcaseTemplate = $('#final-showcase-template').html(),
                optionTemplate = _.template('<option value="{{ value }}"{[ if(title){ ]} title="{{ title }}"{[ } ]}>{{ text }}</option>'),

            // declare/initialize filter values
            alpha=null, search=null, city=null, destination=null, product=null, searchTimeout=null,
            featured = true, // initialize featured to true since that's what we should start with

            // function for determining if a sub array exists in its entirety in the super array
            superBag = function(sub, sup){
                // requires the arrays to be sorted, we are assuming that has been done...
                var i, j;
                for(i=0,j=0; i<sup.length && j<sub.length;){
                    if(sup[i] < sub[j]){
                        i++;
                    }
                    else if(sup[i] == sub[j]){
                        i++; j++;
                    }
                    else return false;
                }
                return j == sub.length;
            },

            // add a brand to the display
            showBrand = function(brand){
                directoryContainer.append(directoryTemplate(brand));
                if(brand.showcase) showcaseContainer.append(showcaseTemplate(brand));
            },

            // clear the containers to get ready for a new set of filters...
            clearBrands = function(){
                directoryContainer.html('');
                showcaseContainer.html('');
            },

            goToBrandDirectory = function(){
                $('ul.nav a[href="#brand-directory"]').tab('show');
                $('a.tab-anchor').css('display', 'block'); // this again...
            },

            getFilteredBrands = function(){
                var results = [];

                _.forEach(window.ibdFullBrandData.brands, function(brand){
                    
                    // attempt to apply each existing filter, if one doenst work, we don't want it!

                    if(window.ibdBrandKey && !(brand.key == window.ibdBrandKey)){ return true; }

                    if(window.ibdBrandKey){
                        // ok we matched a key, we should be on the initial one, let's
                        // switch a few things and rerun the labeling...
                        featured = false;
                        alpha = brand.name.charAt(0).toLowerCase();
                        updateAlphaActiveClass();

                        searchInput.val(window.ibdBrandKey.replace(/-/g, ' '));

                        if(!brand.showcase){
                            goToBrandDirectory();
                        }
                    }

                    // search filter else alpha filter
                    if(search && brand.name.toLowerCase().indexOf(search.toLowerCase()) == -1){ return true; }
                    else if(alpha){
                        var charOne = brand.name.charAt(0).toLowerCase();
                        if(alpha == 'hash'){
                            if(charOne.match(/[a-z]/)) { return true; }
                        }
                        else{
                            if(charOne != alpha) { return true; }
                        }
                    }

                    // featured filter
                    if(featured && brand.featured == "0"){ return true; }

                    // destination (group) filter else city (region) filter
                    if(destination || city){
                        var brandGroups = window.ibdFullBrandData.brandGroups[brand.id];
                        if(!brandGroups){ return true; }

                        var destinations = destination ? [destination] : window.ibdFullBrandData.regions[city].groupIds;
                        var found = false;
                        _.forEach(destinations, function(destination){
                            found = _.indexOf(brandGroups, destination) >= 0;
                            if(found) { return false; }
                        });
                        if(!found){ return true; }
                    }

                    if(product && !(superBag(product, window.ibdFullBrandData.brandTags[brand.id]))){ return true; }

                    // all the filters match, we've got a winner
                    results.push(brand);

                    if(window.ibdBrandKey){
                        // if we have a key and we found a result, we are done!
                        return false;
                    }
                });

                return results;
            },

            // clear, filter, and display the brands
            displayBrands = function(){
                clearBrands();
                updateAlphaActiveClass();
                var brands = getFilteredBrands();
                _.forEach(brands, showBrand);
                var allBrandCount = directoryContainer.children().length;
                var showcaseCount = showcaseContainer.children().length;
                if(showcaseCount == 0 && allBrandCount == 0){
                    showcaseContainer.append(window.ibdNoResultsText);
                    directoryContainer.append(window.ibdNoResultsText);
                }
                else if(showcaseCount == 0){
                    goToBrandDirectory();
                }
                else if(allBrandCount > showcaseCount){
                    showcaseContainer.append($(finalShowcaseTemplate).click(goToBrandDirectory));
                }
            },

            updateAlphaActiveClass = function(){
                alphaParents.children().removeClass(alphaSelectedClass);
                if(featured){
                    alphaParents.children('.featured').addClass(alphaSelectedClass);
                }
                if(alpha){
                    alphaParents.children('.normal.'+alpha).addClass(alphaSelectedClass);
                }
                else if(!featured){
                    alphaParents.children('.all').addClass(alphaSelectedClass);
                }
            },

            setupAlpha = function(newAlpha){
                if(newAlpha == 'featured'){
                    // if they click the featured button, toggle featured
                    featured = !featured;
                }
                else{

                    // set the filter value
                    alpha = (newAlpha == alpha ? null : newAlpha);

                    // fix it if it's set to all
                    if(alpha == 'all'){
                        featured = false;
                        alpha = null;
                    }
                    else{
                        // clear the search box
                        search = null;
                        searchInput.val('');
                    }
                }

                // do the search
                displayBrands();
            };

            var activeTagIds = [];
            _.forEach(window.ibdFullBrandData.brands, function(brand){

                activeTagIds = _.union(activeTagIds, window.ibdFullBrandData.brandTags[brand.id]);

                if(!brand.addresses)
                {
                    brand.addresses = {ready:[]};
                    return true
                };
                brand.addresses.ready = [];
                if(brand.addresses.sorted){
                    _.forEach(brand.addresses.sorted, function(keys, order){
                        if(order == 0) return true;

                        _.forEach(keys, function(key){
                            if(brand.addresses[key].address)
                                brand.addresses.ready.push(brand.addresses[key].address);
                        });
                    });
                }
                else{
                    _.forEach(brand.addresses, function(address){
                        if(address.address) brand.addresses.ready.push(address.address);
                    });
                }
            });

            // initialized the alphabetic filters
            var alphaInitArray = 'abcdefghijklmnopqrstuvwxyz'.split('');
            _.forEach(alphaInitArray, function(care){
                var base = '<a href="#" data-alpha="'+care+'">'+care.toUpperCase()+'</a>';
                alphaLists.append('<li role="presentation" class="normal '+care+'">'+base+'</li>');
                alphaSelects.append('<option class="normal '+care+'" value="'+care+'">'+base+'</option>');
            });
            // add one final one with the hash tag for numbers and non-letter characters
            alphaInitArray = '<a href="#" data-alpha="hash">#</a>';
            alphaLists.append('<li role="presentation" class="normal hash">'+alphaInitArray+'</li>');
            alphaSelects.append('<option class="normal hash" value="hash">'+alphaInitArray+'</option>');

            // initialize city dropdown
            cityFilter.append(optionTemplate({
                text : cityFilter[0].dataset['all'],
                value : defaultOptionValue,
                title : cityFilter.attr('title')
            }));
            _.forEach(window.ibdFullBrandData.regions, function(city, cityId){
                if(!city.name || !cityId) return true;
                cityFilter.append(optionTemplate({
                    text : city.name,
                    value : cityId,
                    title : null
                }));
            });

            // initialize product filter
            _.forEach(activeTagIds, function(tagId){
                productFilter.append(optionTemplate({
                    text : window.ibdFullBrandData.tags[tagId],
                    value : tagId,
                    title : null
                }));
            });

            searchInput.keyup(function(){
                // clear the alphabet
                if(alpha != 'featured'){
                    alpha = null;
                }

                // do the search
                if(search != searchInput.val()){
                    // set an unoticable timeout for quickly struck keys
                    clearTimeout(searchTimeout);
                    searchTimeout = setTimeout(displayBrands, 200);
                    search = searchInput.val();
                }
            });

            // setup alpha buttons click
            alphaParents.click(function(e){
                e.preventDefault();
                setupAlpha(e.target.dataset['alpha']);
            });

            alphaParents.change(function(e){
                featured = false;
                alpha = null;
                setupAlpha(e.target.value);
            });

            // setup the change event on the city filter
            cityFilter.change(function(){
                destination = null; // clear any previously chosen destination
                destinationFilter.html('');
                var noCity = (cityFilter.val() == defaultOptionValue);

                destinationFilter.append(optionTemplate({
                    text : destinationFilter[0].dataset['all'],
                    title : noCity ? destinationFilter.attr('title') : destinationFilter[0].dataset['altTitle'],
                    value : defaultOptionValue
                }));

                destinationFilter.prop('disabled', noCity);

                if(!noCity){ // not no city means we have a city!
                    _.forEach(window.ibdFullBrandData.regions[cityFilter.val()].groupIds, function(groupId){
                        destinationFilter.append(optionTemplate({
                            text : window.ibdFullBrandData.groups[groupId].name,
                            value : groupId,
                            title : null
                        }));
                    });
                    city = cityFilter.val();
                }
                else city = null;

                destinationFilter.selectpicker('refresh');
                if(!noCity) destinationFilter.selectpicker('val', defaultOptionValue);
                displayBrands();
            });

            // setup on destination filter change
            destinationFilter.change(function(){
                destination = (destinationFilter.val() == defaultOptionValue ? null : destinationFilter.val());
                displayBrands();
            });

            // setup on product filter change
            productFilter.change(function(){
                product = productFilter.val();
                if(product) { product.sort(); }
                displayBrands();
            });

            if( window.ibdSearchQuery ) {
                goToBrandDirectory();
                search = window.ibdSearchQuery;
                searchInput.val(search);
                featured = false;
            }
            displayBrands(); // run this the first time...

            if( window.ibdBrandKey ){

                // two final things: we don't want this to get in the way again.
                window.ibdBrandKey = null;

                // and let's open the modal if this is not a showcase brand
                if( showcaseContainer.children().length == 0 ) {
                    directoryContainer.find('a[data-toggle="modal"]').click();
                }
            }

            // this is fucked up right here. somewhere, some script is hiding the tabs when you click the other
            // tab... this little code forces them both to show when one is clicked.
            $(document).delegate('.where-to-shop>.nav-tabs', 'click', function(){
                $('a.tab-anchor').css('display', 'block');
            });

            _.forEach(window.ibdFullBrandData.brandTags, function(tagIds){
                tagIds.sort();
            });
        },

        setupBrandFilterMobileSupport: function(){
            var isXsDiv = $('#is-xs');
            var selectPickers = null;
            $(document).delegate('.brand-selectpickers button.dropdown-toggle', 'click', function(){

                if(!selectPickers)
                {
                    // these get added to the page after it loads, so wait until we've had an
                    // interaction to grab them (but once we have, hold on to them)
                    selectPickers = $('.where-to-shop .brand-selectpickers div.dropdown-menu');
                }

                if(isXsDiv.is(':hidden')){
                    // if isXsDiv is hidden, then we're not xs and we don't need to do this stuff
                    // (setting to '' unsets what we may have done on smaller screens)
                    selectPickers.css('width', '').css('margin-left', '');
                    return;
                }

                // grab the section width, then loop through the select pickers and
                // set their left margins
                var width = selectPickers.closest('.brand-selectpickers').width();
                selectPickers.each(function(){
                    var el = $(this);
                    el.width(width);
                    el.css('margin-left', 0-el.offset().left);
                });
            });
        },

        addDownloadLinkGlyphs: function(){
            var fileTypes = [
                'avi',
                'doc',
                'docx',
                'flv',
                'gzip',
                'mov',
                'mp3',
                'mp4',
                'pdf',
                'ppt',
                'pptx',
                'rm',
                'swf',
                'tar',
                'txt',
                'wav',
                'wmv',
                'xls',
                'xlsx',
                'zip'
            ];
            var locatorTemplate = _.template('a[href$=\'.{{ type }}\'],');
            var locator = '';
            _.forEach(fileTypes, function(type){
                locator += locatorTemplate({ type: type });
            });
            $(locator.slice(0, -1)).append(' <i class="glyphicon glyphicon-download-alt"></i>');
        },

        setupMenuQR: function(){
            $('.top-links a.wechat-qr').click(function(e){
                e.preventDefault();
            }).popover({
                template: '<div class="popover" role="tooltip"><div class="arrow"></div><div class="popover-content"></div><h3 class="popover-title"></h3></div>',
                trigger: 'hover',
                html: true,
                placement: 'bottom',
            });
        },

        animatePageFloat: function(hide){

            var duration = 400,
                pageFloat = $('#page-float'),
                pfContent = pageFloat.find('.content'),
                pfMinimized = pageFloat.find('.minimized'),
                pfUpdateTime = pageFloat.data('updateTime'),
                pfCookieUpdateTime = decodeURIComponent(document.cookie.replace(/(?:(?:^|.*;\s*)pageFloatUpdateTime\s*\=\s*([^;]*).*$)|^.*$/, "$1"));

                // console.log(pfUpdateTime);
                // console.log(pfCookieUpdateTime);


            function toggleFloat(){
                pfContent.slideToggle();
                pfMinimized.slideToggle();
            }

            // * TODO: Store HTML in cookie
            //TODO: Create function to grab HTML of #page_float's last updated date
            //TODO: Compare HTML to stored in Cookie on load
            //TODO: If New HTML != stored HTML, set the cookie to show the page float (and toggle open if already closed)

            pageFloat.find('button.minimize').click(function(e){
                e.preventDefault();
                toggleFloat();
                // set cookie to automatically hide the page float on page reload
                document.cookie = hidePageFloatCookie+'=1';
                document.cookie = pageFloatUpdateTimeCookie+'='+encodeURIComponent(pfUpdateTime);
            });

            pageFloat.find('button.maximize').click(function(e){
                e.preventDefault();
                toggleFloat();
                // expire the cookie to stop hiding...
                document.cookie = hidePageFloatCookie+'=; expires=Thu, 01 Jan 1970 00:00:00 UTC';
                document.cookie = pageFloatUpdateTimeCookie+'=; expires=Thu, 01 Jan 1970 00:00:00 UTC';
            });

            pageFloat.find('button.go-to-top').click(function(e){
                e.preventDefault();

                $('html, body').animate({ scrollTop: 0 }, duration);
            });

            // Show the page float if it's been updated since the last time the user closed it
            if ( (hide) && !(pfCookieUpdateTime.length > 0) ) hide = false;
            if ( (hide) && (pfUpdateTime > pfCookieUpdateTime) ) hide = false;
            if(hide) toggleFloat();
        },

        setupMasonry: function(){
            $('.masonry').each(function(){
                var container = $(this),
                    itemSelector = container.data('itemSelector'),
                    gutterWidth = container.data('gutterWidth'),
                    containerWidth = container.width(),
                    colCount = container.data('colCount');

                // wait for the images to load, then setup their widths and the masonry
                container.imagesLoaded(function(){

                    // width of one column
                    // whole container, minus the gutters (one fewer than number of columns), divided by columns
                    var columnWidth = (containerWidth - gutterWidth * (colCount - 1)) / colCount;

                    // set the item widths. since they could vary in size, set them to the closest
                    // half column width, starting at 1.5.
                    _.forEach(container.find(itemSelector), function(item){

                        var jtem = $(item),
                            itemWidth = jtem.outerWidth(true),
                            itemWidthDiff = itemWidth - jtem.outerWidth(),
                            columnSpan = 1;

                        if(!jtem.hasClass('no-span')) return true; // don't add manual widths if there is a span

                        while( columnSpan <= colCount )
                        {

                            if ( itemWidth < columnWidth * columnSpan + columnWidth / 2 ){
                                // looks like it'll fit here!
                                // column width times the column span, plus the number of gutters (one less than the
                                // column span), and subtracing the different in width from what we can measure and what
                                // we have to set
                                jtem.outerWidth( columnWidth * columnSpan + gutterWidth * (columnSpan - 1) - itemWidthDiff );
                                console.log('setting masonry image width (' + ( columnWidth * columnSpan + gutterWidth * (columnSpan - 1) - itemWidthDiff ) + ') at column ' + columnSpan);
                                break;
                            }
                            columnSpan++; // don't forget to increment!

                            if( columnSpan > colCount ) console.log('image larger than container, getting full width');
                        }
                    });

                    // finally, setup the masonry!
                    container.masonry({
                        itemSelector : itemSelector,
                        columnWidth : columnWidth,
                        gutter : gutterWidth
                    });
                });
            });
        },

        setupCheckoutFapiao: function() {
            var parentLi = $('#checkout-fapiao');
            if(parentLi.length == 0 || !Payment) {
                return;
            }

            // fix the payment tab init method so that it doesn't disable the fapiao inputs
            Payment.prototype.init = function () {
                this.beforeInit();
                var elements = Form.getElements(this.form);
                if ($(this.form)) {
                    $(this.form).observe('submit', function(event){this.save();Event.stop(event);}.bind(this));
                }
                var method = null;
                for (var i=0; i<elements.length; i++) {
                    if(elements[i].name.indexOf('fapiao') >= 0){ // this if is the new bit
                        continue;                                //
                    }                                            //
                    if (elements[i].name=='payment[method]') {
                        if (elements[i].checked) {
                            method = elements[i].value;
                        }
                    } else {
                        elements[i].disabled = true;
                    }
                    elements[i].setAttribute('autocomplete','off');
                }
                if (method) this.switchMethod(method);
                this.afterInit();
            };

            var neededDiv = parentLi.find('div#fapiao-needed');
            var isNeededCB = parentLi.find('input#is-fapiao-needed');
            var geRenCB = parentLi.find('input#fapiao-ge-ren');
            var fapiaoTitleText = parentLi.find('input#fapiao-title');

            // $('li#opc-billing').on('change', '#billing:company', function(e){
            //     fapiaoTitleText.val($(e.target).val());
            // });

            isNeededCB.change(function(){
                isNeededCB.prop('checked') ? neededDiv.removeClass('hide') : neededDiv.addClass('hide');
            });

            geRenCB.change(function(){
                geRenCB.prop('checked') ? fapiaoTitleText.addClass('hide') : fapiaoTitleText.removeClass('hide');
            });

            $('#fapiao-tnc').popover({
                trigger: 'hover',
                html: true
            });
        },

        renumberCheckoutSteps: function(){
            var count = 1;
            $('#checkoutSteps .step-title span.number').each(function (i){
                var span = $(this);
                if(span.closest('li').css('display') == 'none') return;

                span.html(count++);
            });
        }
    };

    $(document).ready(function($) {
        // don't add code here, add it to the site (see start/attach methods)
        var site = new Site();
        site.start();
    });

})(jQuery.noConflict(),window,document);

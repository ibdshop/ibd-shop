<?php
// Can Create blocks following the format below if needed.  The NameHere part of "NameSpace_ModuleName_Block_NameHere" should match the blocktype called from your XML layout file.
// class IBD_MerchantSignup_Block_MerchantSignup extends Mage_Core_Block_Template
// {
// 	public function methodblock() 
// 	{
// 		return 'information about my block';
// 	}
// }
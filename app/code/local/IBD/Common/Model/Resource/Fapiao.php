<?php
/**
 * IBD_Common extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_Common
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Fapiao resource model
 *
 * @category    IBD
 * @package     IBD_Common
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_Common_Model_Resource_Fapiao extends Mage_Core_Model_Resource_Db_Abstract
{

    /**
     * constructor
     *
     * @access public
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function _construct()
    {
        $this->_init('ibd_common/fapiao', 'order_id');
        $this->_isPkAutoIncrement = false;
    }
}
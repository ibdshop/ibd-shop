<?php
/**
 * IBD_Common extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_Common
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Fapiao model
 *
 * @category    IBD
 * @package     IBD_Common
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_Common_Model_Fapiao extends Mage_Core_Model_Abstract
{
    /**
     * Entity code.
     * Can be used as part of method name for entity processing
     */
    const ENTITY    = 'ibd_common_fapiao';
    const CACHE_TAG = 'ibd_common_fapiao';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'ibd_common_fapiao';

    /**
     * Parameter name in event
     *
     * @var string
     */
    protected $_eventObject = 'fapiao';

    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('ibd_common/fapiao');
    }

    public function getFapiaoNameForOrder($order)
    {
        if(!$order) return null;

        if(is_object($order)){
            $order = $order->getId();
        }

        $fapiao = $this->load($order);

        if($fapiao){
            return $fapiao->getData('name');
        }

        return null;
    }

    public function saveFapiaoForOrder($fapiaoName, $order)
    {
        if(!$order) return null;

        if(is_object($order)){
            //Mage::log('order is an object, getting ID');
            $order = $order->getId();
        }

        //Mage::log('upserting fapiao for order ID: '.$order);
        $fapiao = $fapiao = $this->load($order, 'order_id');
        //Mage::log('found fapiao with order ID: '.$fapiao->getId());
        if(!$fapiao->getId()){
            //Mage::log('could not get existing fapiao');
            $fapiao = new IBD_Common_Model_Fapiao();
            $fapiao->setData('order_id', $order);
        }

        $fapiao->setData('name', $fapiaoName);

        $fapiao->save();
    }
}

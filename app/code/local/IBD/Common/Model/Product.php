<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    IBD
 * @package     IBD_Common
 * @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Catalog product model
 *
 * @category    IBD
 * @package     IBD_Common
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_Common_Model_Product extends Mage_Catalog_Model_Product
{
    /**
     * Get product name
     *
     * @return string
     */
    public function getName()
    {
        $name = Mage::getResourceModel('catalog/product')
                ->getAttributeRawValue($this->getId(), 'chinese_name', Mage::app()->getStore()->getId());

        return $name ? $name : parent::getName();
    }

    // override save method to add the first brand as an EAV...
    public function save(){
        $ret = parent::save();
        //Mage::log('Hit IBD Common Model Product save method! Saving Product brand EAVs');
        $brands = Mage::getResourceModel('ibd_travelsuite/brand_collection')->addProductFilter($this);
        $action = Mage::getModel('catalog/resource_product_action');
        $updated = false;

        foreach ( $brands as $brand ) {
            $action->updateAttributes(
                array( $this->getId() ),
                array(
                    'product_brand' => $brand->getName(),
                    'product_brand_id' => $brand->getId(),
                ),
                Mage::app()->getStore()->getId());

            $updated = true;
            break; // only add the first one
        }

        if( !$updated ) {
            $action->updateAttributes(
                array( $this->getId() ),
                array( 'product_brand' => null, 'product_brand_id' => null ),
                Mage::app()->getStore()->getId());
        }
        return $ret;
    }
}

<?php 
class IBD_Common_Model_Observer
{
	private function getFapiaoFromPost($postFapiao)
	{
		// from the controller, get the fapiao data from the POST
		//Mage::log('Post fapiao: '.json_encode($postFapiao));

		if(isset($postFapiao['needed'])) {

			//Mage::log('fapiao is needed');

			if(isset($postFapiao['geren'])) {
				//Mage::log('getting geren fapiao');
				return $postFapiao['geren'];
			}
			elseif (isset($postFapiao['title'])) {
				//Mage::log('getting company fapiao');
				return $postFapiao['title'];
			}
		}

		return false;
	}

	public function setOrderFapiaoSession(Varien_Event_Observer $observer){

		//Mage::log('ibd_common controller_action_predispatch_checkout_onepage_savePayment observer');

		// get the controller
		$controller = $observer->getEvent()->getControllerAction();

		// from the controller, get the onepage -> quote -> order; make sure we have each one
		$onePage = $controller->getOnepage();
		if(!$onePage){
			//Mage::log('no onePage, can\'t save fapiao.');
			return $this;
		}

		$quote = $onePage->getQuote();
		if(!$quote){
			//Mage::log('no quote, can\'t save fapiao.');
			return $this;
		}

		$order = Mage::getModel('sales/order')->load($quote->getId(), 'quote_id');
		if(!$order){
			//Mage::log('no order, can\'t save fapiao.');
			return $this;
		}

		$fapiao = $this->getFapiaoFromPost($controller->getRequest()->getPost('fapiao'));

		Mage::getSingleton('core/session')->setFapiao($fapiao);
		if($order->getId()){
			//Mage::log('saving fapiao for order: '.$order->getId().', '.$fapiao);
			Mage::getSingleton('ibd_common/fapiao')->saveFapiaoForOrder($fapiao, $order);
		}

		return $this;
	}

	public function saveOrderFapiao(Varien_Event_Observer $observer)
	{
		//Mage::log('ibd_common '.$observer->getEvent()->getName().' observer');

		$fapiao = Mage::getSingleton('core/session')->getFapiao();
		if(!$fapiao === null){

			$postFapiao = isset($_POST['fapiao']) ? $_POST['fapiao'] : array();

			$fapiao = $this->getFapiaoFromPost($postFapiao);
		}

		$order = $observer->getEvent()->getOrder();
		if(!$order){
			//Mage::log('no order, can\'t update fapiao');
			return $this;
		}

		//Mage::log('saving fapiao for order: '.$order->getId().', '.$fapiao);
		Mage::getSingleton('ibd_common/fapiao')->saveFapiaoForOrder($fapiao, $order);

		return $this;
	}
}
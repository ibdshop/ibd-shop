<?php
/**
 * IBD_Common extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_Common
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * AJAX front contrller
 *
 * @category    IBD
 * @package     IBD_Common
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_Common_AjaxController extends Mage_Core_Controller_Front_Action
{
	public function indexAction($data=null)
	{
		$this->getResponse()->setHeader('Content-type', 'application/json');
		if( !$data )
		{
			$this->getResponse()->setHeader('HTTP/1.1', '405', true);
			$data = array( 'error' => 1, 'message' => 'nothing to do' );
		}

		$this->getResponse()->setBody( json_encode($data) );
	}

	public function modifyCartAction()
	{
		$data = null;
		try
		{
			$data = json_decode( file_get_contents('php://input') );
		}
		catch (Exception $e) {}

		if( !$data || !isset($data->productId) || !isset($data->quantity) )
		{
			$this->getResponse()->setHeader('HTTP/1.1', '400', true);
			return $this->indexAction(array(
				'error' => 2,
				'message' => 'Invalid POST JSON. POST body requires productId and quantity'
			));
		}

		$response = array( 'error' => 0, 'items' => array(), 'subtotal' => 0, 'count' => 0 );

		$session = Mage::getSingleton('core/session', array('name'=>'frontend'));
		$cartHelper = Mage::helper('checkout/cart');
		$cart = $cartHelper->getCart();
		$updated = false;

		foreach ($cart->getItems() as $item)
		{
			if($item->getId() == $data->productId)
			{
				$updated = true;
				if( $data->quantity < 1 )
				{
					// zero quantity means we're removing the item completely
					$cart->removeItem($data->productId);
				}
				else
				{
					$item->setQty($data->quantity);
				}
			}

			$response['items'][$item->getId()] = $item->getQty();
		}
		
		if( $updated )
		{
			$cart->save();
		}
		else
		{
			$response['error'] = '1';
		}

		$response['subtotal'] = Mage::helper('checkout')->formatPrice($cart->getQuote()->getSubtotal());
		$response['count'] = $cartHelper->getSummaryCount();

		return $this->indexAction($response);
	}
}
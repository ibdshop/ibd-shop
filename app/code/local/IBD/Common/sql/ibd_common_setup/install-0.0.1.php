<?php

$installer = $this;
$installer->startSetup();

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$entityTypeId     = $setup->getEntityTypeId('customer');
$attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

$installer->addAttribute('customer', 'customer_mobile', array(
	'label' => 'Mobile Number',
	'type' => 'varchar',
	'input' => 'text',
	'visible' => true,
	'required' => false,
	'position' => 9999,
));

$setup->addAttributeToGroup(
	$entityTypeId,
	$attributeSetId,
	$attributeGroupId,
	'customattribute',
	'999'  //sort_order
);

$attribute   = Mage::getSingleton('eav/config')->getAttribute('customer', 'customer_mobile');
$attribute->setData("used_in_forms", array(
	"adminhtml_customer",
	"checkout_register",
	"customer_account_create",
	"customer_account_edit",
	"adminhtml_checkout"
))
	->setData("is_used_for_customer_segment", true)
	->setData("is_system", 0)
	->setData("is_user_defined", 1)
	->setData("is_visible", 1)
	->setData("sort_order", 100);

$attribute->save();

$installer->endSetup();
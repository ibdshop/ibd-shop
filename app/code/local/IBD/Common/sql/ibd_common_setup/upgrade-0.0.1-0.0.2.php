<?php

$installer = $this;
$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('ibd_common/fapiao'))
    ->addColumn(
        'name',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable'  => false,
        ),
        'Name'
    )
    ->addColumn(
        'order_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'auto_increment'=> false,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true
        ),
        'Order ID'
    )
    ->addIndex(
        $installer->getIdxName(
            'ibd_common/fapiao',
            array('order_id')
        ),
        array('order_id')
    )
    ->addForeignKey(
        $installer->getFkName(
            'ibd_common/fapiao',
            'order_id',
            'sales/order',
            'entity_id'
        ),
        'order_id',
        $installer->getTable('sales/order'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_NO_ACTION
    )
    ->setComment('Fapiaos for orders');

$this->getConnection()->createTable($table);
$this->endSetup();
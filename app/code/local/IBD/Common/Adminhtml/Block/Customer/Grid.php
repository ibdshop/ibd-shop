<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    IBD
 * @package     IBD_Common
 * @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Adminhtml customer grid block
 *
 * @category    IBD
 * @package     IBD_Common
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_Common_Adminhtml_Block_Customer_Grid extends Mage_Adminhtml_Block_Customer_Grid
{
	protected function _prepareCollection()
	{
		$collection = Mage::getResourceModel('customer/customer_collection')
			->addAttributeToSelect('lastname')
			->addAttributeToSelect('email')
			->addAttributeToSelect('created_at')
			->addAttributeToSelect('group_id')
			->addAttributeToSelect('customer_mobile')
			->joinAttribute('billing_postcode', 'customer_address/postcode', 'default_billing', null, 'left')
			->joinAttribute('billing_city', 'customer_address/city', 'default_billing', null, 'left')
			->joinAttribute('billing_region', 'customer_address/region', 'default_billing', null, 'left')
			->joinAttribute('billing_country_id', 'customer_address/country_id', 'default_billing', null, 'left');

		$this->setCollection($collection);

		return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
	}

    protected function _prepareColumns()
    {
    	return parent::_prepareColumns()

	        ->removeColumn('name')
	        ->removeColumn('Telephone')

	        ->addColumnAfter('lastname', array(
	            'header'    => Mage::helper('customer')->__('Name'),
	            'index'     => 'lastname'
	        ), 'entity_id')
    	
	        ->addColumnAfter('customer_mobile', array(
	            'header'    => Mage::helper('customer')->__('Mobile'),
	            'index'     => 'customer_mobile'
	        ), 'group')

	        ->sortColumnsByOrder();
    }
}
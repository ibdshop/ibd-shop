<?php
/**
 * IBD_HomepageBanner extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_HomepageBanner
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Advertisement brand collection resource model
 *
 * @category    IBD
 * @package     IBD_HomepageBanner
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_HomepageBanner_Model_Resource_Advertisement_Brand_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected $_joinedFields = array();

    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('ibd_travelsuite/brand');
    }

    /**
     * get advertisements as array
     *
     * @access protected
     * @param string $valueField
     * @param string $labelField
     * @param array $additional
     * @return array
     * @author Ultimate Module Creator
     */
    protected function _toOptionArray($valueField='entity_id', $labelField='name', $additional=array())
    {
        return parent::_toOptionArray($valueField, $labelField, $additional);
    }

    /**
     * get options hash
     *
     * @access protected
     * @param string $valueField
     * @param string $labelField
     * @return array
     * @author Ultimate Module Creator
     */
    protected function _toOptionHash($valueField='entity_id', $labelField='name')
    {
        return parent::_toOptionHash($valueField, $labelField);
    }

    /**
     * Get SQL for get record count.
     * Extra GROUP BY strip added.
     *
     * @access public
     * @return Varien_Db_Select
     * @author Ultimate Module Creator
     */
    public function getSelectCountSql()
    {
        $countSelect = parent::getSelectCountSql();
        $countSelect->reset(Zend_Db_Select::GROUP);
        return $countSelect;
    }

    /**
     * Add filter by relation
     *
     * @access public
     * @param int
     * @param string
     * @return IBD_HomepageBanner_Model_Resource_Advertisement_Brand_Collection
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function addRelationFilter($advertisement, $status=-1)
    {
        if ( !isset($this->_joinedFields['relation']) ) {

            if($advertisement instanceof IBD_HomepageBanner_Model_Advertisement)
            {
                $advertisement = $advertisement->getId();
            }
            
            $this->getSelect()->join(
                array('relation' => $this->getTable('ibd_homepagebanner/advertisement_relation')),
                'relation.relation_id = main_table.entity_id',
                array());

            $this->getSelect()
                ->where('relation.advertisement_id = ?', $advertisement)
                ->where('relation.relation_type = ?', 'ibd_brand');

            if($status < 0)
            {
                $this->getSelect()->where('relation.relation_id > 0');
            }
            else
            {
                $this->getSelect()->where('relation.relation_id = ?', $status);
            }

            $this->_joinedFields['relation'] = true;
        }
        return $this;
    }
}

<?php
/**
 * IBD_HomepageBanner extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_HomepageBanner
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Advertisement resource model
 *
 * @category    IBD
 * @package     IBD_HomepageBanner
 * @author      Ultimate Module Creator
 */
class IBD_HomepageBanner_Model_Resource_Advertisement_Relation extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * constructor
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function _construct()
    {
        $this->_init('ibd_homepagebanner/advertisement_relation', 'entity_id');
    }
}

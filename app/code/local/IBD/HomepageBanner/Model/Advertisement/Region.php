<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Advertisement region model
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_HomepageBanner_Model_Advertisement_Region extends IBD_TravelSuite_Model_Region
{
    /**
     * get regions for advertisement
     *
     * @access public
     * @param IBD_TravelSuite_Model_Advertisement $advertisement
     * @return IBD_TravelSuite_Model_Resource_Advertisement_Region_Collection
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function getAdvertisementAdCollection($advertisement)
    {
        $collection = Mage::getResourceModel('ibd_homepagebanner/advertisement_region_collection')
                                                                ->addRelationFilter($advertisement);
        return $collection;
    }
}

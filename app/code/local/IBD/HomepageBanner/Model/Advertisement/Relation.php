<?php
/**
 * IBD_HomepageBanner extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_HomepageBanner
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Advertisement model
 *
 * @category    IBD
 * @package     IBD_HomepageBanner
 * @author      Ultimate Module Creator
 */
class IBD_HomepageBanner_Model_Advertisement_Relation extends Mage_Core_Model_Abstract
{
    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'ibd_homepagebanner_advertisement_relation';

    /**
     * Parameter name in event
     *
     * @var string
     */
    protected $_eventObject = 'advertisement_relation';

    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('ibd_homepagebanner/advertisement_relation');
    }

    /**
     * upserts all the advertisement IDs for an entity
     *
     * @access public
     * @param int
     * @param string
     * @param array (int)
     * @return void
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function upsertEntityRelations($relationId, $relationType, $newAdIds)
    {
        $existingRelations = $this->getEntityRelations($relationId, $relationType);

        // delete existing ads that are no longer referenced
        // remove IDs that exist (we don't need to add them)
        foreach ($existingRelations as $relation)
        {
            $index = array_search($relation->getAdvertisementId(), $newAdIds);
            if( $index === false )
            {
                $relation->delete();
            }
            else
            {
                if($relation->getOrder() != $index + 1)
                {
                    $relation->setOrder($index+1);
                    $relation->save();
                }
                $newAdIds[$index] = false; // we already handled this ID, set it to false for creating
            }
        }

        // for any ad IDs left, create relations
        foreach ($newAdIds as $index => $adId)
        {
            if($adId === false) continue; // we set this one to false since it already exists, continue

            Mage::log('saving new: '.$adId);
            $relation = new IBD_HomepageBanner_Model_Advertisement_Relation();
            $relation->setRelationId($relationId);
            $relation->setRelationType($relationType);
            $relation->setAdvertisementId($adId);
            $relation->setOrder($index + 1); // keep the order that they entered them in?
            $relation->save();
        }
    }

    /**
     * gets the advertisement relations for an entity
     *
     * @access public
     * @param int
     * @param string
     * @return IBD_HomepageBanner_Model_Resource_Advertisement_Relation_Collection
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function getEntityRelations($relationId, $relationType)
    {
        return $this->getCollection()
                            ->addFieldToFilter('main_table.relation_id', $relationId)
                            ->addFieldToFilter('main_table.relation_type', $relationType);
    }

    /**
     * upserts the entity IDs for an advertisement
     *
     * @access public
     * @param int
     * @param string
     * @param array (int)
     * @return void
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function upsertAdvertisementRelations($advertisementId, $relationType, $newRelationIds)
    {
        $existingRelations = $this->getAdvertisementRelations($advertisementId, $relationType);

        // delete existing ads that are no longer referenced
        // remove IDs that exist (we don't need to add them)
        foreach ($existingRelations as $relation)
        {
            $idIndex = array_search($relation->getRelationId(), $newRelationIds);
            if( $idIndex === false )
            {
                $relation->delete();
            }
            else
            {
                $newRelationIds[$idIndex] = false; // we already handled this ID, set it to false for creating
            }
        }

        // for any ad IDs left, create relations
        foreach ($newRelationIds as $relationId)
        {
            if($relationId === false) continue; // we set this one to false since it already exists, continue

            Mage::log('saving new: '.$relationId);
            $relation = new IBD_HomepageBanner_Model_Advertisement_Relation();
            $relation->setRelationId($relationId);
            $relation->setRelationType($relationType);
            $relation->setAdvertisementId($advertisementId);
            $relation->setOrder(0); // default of 0 if we are adding from the advertisement?
            $relation->save();
        }
    }

    /**
     * gets the advertisement relations for an entity
     *
     * @access public
     * @param int
     * @param string
     * @return IBD_HomepageBanner_Model_Resource_Advertisement_Relation_Collection
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function getAdvertisementRelations($advertisementId, $relationType)
    {
        return $this->getCollection()
                            ->addFieldToFilter('main_table.advertisement_id', $advertisementId)
                            ->addFieldToFilter('main_table.relation_type', $relationType);
    }
}

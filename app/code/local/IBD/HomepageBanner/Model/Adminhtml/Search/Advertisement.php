<?php
/**
 * IBD_HomepageBanner extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_HomepageBanner
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Admin search model
 *
 * @category    IBD
 * @package     IBD_HomepageBanner
 * @author      Ultimate Module Creator
 */
class IBD_HomepageBanner_Model_Adminhtml_Search_Advertisement extends Varien_Object
{
    /**
     * Load search results
     *
     * @access public
     * @return IBD_HomepageBanner_Model_Adminhtml_Search_Advertisement
     * @author Ultimate Module Creator
     */
    public function load()
    {
        $arr = array();
        if (!$this->hasStart() || !$this->hasLimit() || !$this->hasQuery()) {
            $this->setResults($arr);
            return $this;
        }
        $collection = Mage::getResourceModel('ibd_homepagebanner/advertisement_collection')
            ->addFieldToFilter('name', array('like' => $this->getQuery().'%'))
            ->setCurPage($this->getStart())
            ->setPageSize($this->getLimit())
            ->load();
        foreach ($collection->getItems() as $advertisement) {
            $arr[] = array(
                'id'          => 'advertisement/1/'.$advertisement->getId(),
                'type'        => Mage::helper('ibd_homepagebanner')->__('Content Image'),
                'name'        => $advertisement->getName(),
                'description' => $advertisement->getName(),
                'url' => Mage::helper('adminhtml')->getUrl(
                    '*/homepagebanner_advertisement/edit',
                    array('id'=>$advertisement->getId())
                ),
            );
        }
        $this->setResults($arr);
        return $this;
    }
}

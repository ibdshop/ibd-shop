<?php
/**
 * IBD_HomepageBanner extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_HomepageBanner
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Admin search model
 *
 * @category    IBD
 * @package     IBD_HomepageBanner
 * @author      Ultimate Module Creator
 */
class IBD_HomepageBanner_Model_Adminhtml_Search_Bannertile extends Varien_Object
{
    /**
     * Load search results
     *
     * @access public
     * @return IBD_HomepageBanner_Model_Adminhtml_Search_Bannertile
     * @author Ultimate Module Creator
     */
    public function load()
    {
        $arr = array();
        if (!$this->hasStart() || !$this->hasLimit() || !$this->hasQuery()) {
            $this->setResults($arr);
            return $this;
        }
        $collection = Mage::getResourceModel('ibd_homepagebanner/bannertile_collection')
            ->addFieldToFilter('title', array('like' => $this->getQuery().'%'))
            ->setCurPage($this->getStart())
            ->setPageSize($this->getLimit())
            ->load();
        foreach ($collection->getItems() as $bannertile) {
            $arr[] = array(
                'id'          => 'bannertile/1/'.$bannertile->getId(),
                'type'        => Mage::helper('ibd_homepagebanner')->__('Banner Tile'),
                'name'        => $bannertile->getTitle(),
                'description' => $bannertile->getTitle(),
                'url' => Mage::helper('adminhtml')->getUrl(
                    '*/homepagebanner_bannertile/edit',
                    array('id'=>$bannertile->getId())
                ),
            );
        }
        $this->setResults($arr);
        return $this;
    }
}

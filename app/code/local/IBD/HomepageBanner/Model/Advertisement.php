<?php
/**
 * IBD_HomepageBanner extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_HomepageBanner
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Advertisement model
 *
 * @category    IBD
 * @package     IBD_HomepageBanner
 * @author      Ultimate Module Creator
 */
class IBD_HomepageBanner_Model_Advertisement extends Mage_Core_Model_Abstract
{
    const DISABLED      = '0';
    const CAROUSEL      = '1';
    const FULLWIDTH     = '2';
    const PAGE          = '3';
    const SHOWCASE      = '4';
    const QUICKLINKS    = '5';
    const BANNERS       = '6';
    const SIDEBAR       = '7';
    const TRAVEL        = '8';

    /**
     * Entity code.
     * Can be used as part of method name for entity processing
     */
    const ENTITY    = 'ibd_homepagebanner_advertisement';
    const CACHE_TAG = 'ibd_homepagebanner_advertisement';

    public function getAvailableLocations(){
        return array(
            self::CAROUSEL      => Mage::helper('ibd_homepagebanner')->__('Carousel'),
            self::QUICKLINKS    => Mage::helper('ibd_homepagebanner')->__('Quick-link'),
            self::SHOWCASE      => Mage::helper('ibd_homepagebanner')->__('Homepage Showcase'),
            self::TRAVEL        => Mage::helper('ibd_homepagebanner')->__('Travel Showcase'),
            self::FULLWIDTH     => Mage::helper('ibd_homepagebanner')->__('Full Width'),
            self::BANNERS       => Mage::helper('ibd_homepagebanner')->__('Banner'),
            self::SIDEBAR       => Mage::helper('ibd_homepagebanner')->__('Sidebar Ad'),
            self::PAGE          => Mage::helper('ibd_homepagebanner')->__('Related'),
            self::DISABLED      => Mage::helper('ibd_homepagebanner')->__('Disabled'),
        );
    }

    public function getAvailableLocationOptions()
    {
        $locations = $this->getAvailableLocations();
        $values = array();
        foreach ($locations as $value => $text) {
            $values[] = array(
                'value' => $value,
                'label' => $text,
            );
        }
        return $values;
    }

    public function getAds($count, $location = -1)
    {
        $collection = $this->getCollection();
        if($location < 0)
        {
            $collection->getSelect()->where('location > 0');
        }
        else
        {
            $collection->getSelect()->where('location = ?', $location);
        }
        if($count > 0) $collection->getSelect()->limit($count);
        return $collection;
    }

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'ibd_homepagebanner_advertisement';

    /**
     * Parameter name in event
     *
     * @var string
     */
    protected $_eventObject = 'advertisement';

    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('ibd_homepagebanner/advertisement');
    }

    /**
     * before save advertisement
     *
     * @access protected
     * @return IBD_HomepageBanner_Model_Advertisement
     * @author Ultimate Module Creator
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        $now = Mage::getSingleton('core/date')->gmtDate();
        if ($this->isObjectNew()) {
            $this->setCreatedAt($now);
        }
        $this->setUpdatedAt($now);
        return $this;
    }

    /**
     * save advertisement relation
     *
     * @access public
     * @return IBD_HomepageBanner_Model_Advertisement
     * @author Ultimate Module Creator
     */
    protected function _afterSave()
    {
        return parent::_afterSave();
    }

    /**
     * get default values
     *
     * @access public
     * @return array
     * @author Ultimate Module Creator
     */
    public function getDefaultValues()
    {
        $values = array();
        $values['status'] = 1;
        return $values;
    }

    /**
     * upsert the region ids
     *
     * @access public
     * @param array (int)
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function upsertAdvertisementRelations($relationType, $entityIds)
    {
        Mage::getSingleton('ibd_homepagebanner/advertisement_relation')
                    ->upsertAdvertisementRelations($this->getId(), $relationType, $entityIds);
    }

    /**
     * get an advertisement collection filtered by status (content image location) and ordered by sort_order
     *
     * @access public
     * @param int (status ID as string or int)
     * @return IBD_HomepageBanner_Model_Resource_Advertisement_Collection
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function getCollectionByLocation($location)
    {
        $collection = $this->getCollection();
        $collection->getSelect()->where('location = ?', $location)->order('sort_order ASC');
        return $collection;
    }
}

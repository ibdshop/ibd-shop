<?php
/**
 * IBD_HomepageBanner extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_HomepageBanner
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Advertisement admin controller
 *
 * @category    IBD
 * @package     IBD_HomepageBanner
 * @author      Ultimate Module Creator
 */
class IBD_HomepageBanner_Adminhtml_Homepagebanner_AdvertisementController extends IBD_HomepageBanner_Controller_Adminhtml_HomepageBanner
{
    /**
     * init the advertisement
     *
     * @access protected
     * @return IBD_HomepageBanner_Model_Advertisement
     */
    protected function _initAdvertisement()
    {
        $advertisementId  = (int) $this->getRequest()->getParam('id');
        $advertisement    = Mage::getModel('ibd_homepagebanner/advertisement');
        if ($advertisementId) {
            $advertisement->load($advertisementId);
        }
        Mage::register('current_advertisement', $advertisement);
        return $advertisement;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('ibd_homepagebanner')->__('Homepage Banner'))
             ->_title(Mage::helper('ibd_homepagebanner')->__('Advertisements'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit advertisement - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function editAction()
    {
        $advertisementId    = $this->getRequest()->getParam('id');
        $advertisement      = $this->_initAdvertisement();
        if ($advertisementId && !$advertisement->getId()) {
            $this->_getSession()->addError(
                Mage::helper('ibd_homepagebanner')->__('This content image no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getAdvertisementData(true);
        if (!empty($data)) {
            $advertisement->setData($data);
        }
        Mage::register('advertisement_data', $advertisement);
        $this->loadLayout();
        $this->_title(Mage::helper('ibd_homepagebanner')->__('Homepage'))
             ->_title(Mage::helper('ibd_homepagebanner')->__('Content Images'));
        if ($advertisement->getId()) {
            $this->_title($advertisement->getName());
        } else {
            $this->_title(Mage::helper('ibd_homepagebanner')->__('Add content image'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new advertisement action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save advertisement - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('advertisement')) {
            try {
                if(strpos($data['url'], 'www') === 0){
                    $data['url'] = 'http://'.$data['url'];
                }
                $advertisement = $this->_initAdvertisement();
                $advertisement->addData($data);
                $imageName = $this->_uploadAndGetName(
                    'image',
                    Mage::helper('ibd_homepagebanner/advertisement_image')->getImageBaseDir(),
                    $data
                );
                $advertisement->setData('image', $imageName);
                $advertisement->save();
                if(isset($data['regionids'])) $advertisement->upsertAdvertisementRelations('ibd_region', $data['regionids']);
                if(isset($data['groupids'])) $advertisement->upsertAdvertisementRelations('ibd_group', $data['groupids']);
                if(isset($data['brandids'])) $advertisement->upsertAdvertisementRelations('ibd_brand', $data['brandids']);
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ibd_homepagebanner')->__('Content image was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $advertisement->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                if (isset($data['image']['value'])) {
                    $data['image'] = $data['image']['value'];
                }
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setAdvertisementData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                if (isset($data['image']['value'])) {
                    $data['image'] = $data['image']['value'];
                }
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_homepagebanner')->__('There was a problem saving the content image.')
                );
                Mage::getSingleton('adminhtml/session')->setAdvertisementData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('ibd_homepagebanner')->__('Unable to find content image to save.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * delete advertisement - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function deleteAction()
    {
        if ( $this->getRequest()->getParam('id') > 0) {
            try {
                $advertisement = Mage::getModel('ibd_homepagebanner/advertisement');
                $advertisement->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ibd_homepagebanner')->__('Content image was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_homepagebanner')->__('There was an error deleting content image.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('ibd_homepagebanner')->__('Could not find content image to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete advertisement - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeleteAction()
    {
        $advertisementIds = $this->getRequest()->getParam('advertisement');
        if (!is_array($advertisementIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('ibd_homepagebanner')->__('Please select content images to delete.')
            );
        } else {
            try {
                foreach ($advertisementIds as $advertisementId) {
                    $advertisement = Mage::getModel('ibd_homepagebanner/advertisement');
                    $advertisement->setId($advertisementId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ibd_homepagebanner')->__('Total of %d content images were successfully deleted.', count($advertisementIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_homepagebanner')->__('There was an error deleting content images.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massStatusAction()
    {
        $advertisementIds = $this->getRequest()->getParam('advertisement');
        if (!is_array($advertisementIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('ibd_homepagebanner')->__('Please select content images.')
            );
        } else {
            try {
                foreach ($advertisementIds as $advertisementId) {
                $advertisement = Mage::getSingleton('ibd_homepagebanner/advertisement')->load($advertisementId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d content images were successfully updated.', count($advertisementIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_homepagebanner')->__('There was an error updating content images.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportCsvAction()
    {
        $fileName   = 'contentimage.csv';
        $content    = $this->getLayout()->createBlock('ibd_homepagebanner/adminhtml_advertisement_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportExcelAction()
    {
        $fileName   = 'contentimage.xls';
        $content    = $this->getLayout()->createBlock('ibd_homepagebanner/adminhtml_advertisement_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportXmlAction()
    {
        $fileName   = 'contentimage.xml';
        $content    = $this->getLayout()->createBlock('ibd_homepagebanner/adminhtml_advertisement_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Ultimate Module Creator
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('cms/ibd_homepagebanner/advertisement');
    }
}

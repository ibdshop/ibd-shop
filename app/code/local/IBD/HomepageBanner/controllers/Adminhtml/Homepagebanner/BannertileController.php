<?php
/**
 * IBD_HomepageBanner extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_HomepageBanner
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Banner Tile admin controller
 *
 * @category    IBD
 * @package     IBD_HomepageBanner
 * @author      Ultimate Module Creator
 */
class IBD_HomepageBanner_Adminhtml_Homepagebanner_BannertileController extends IBD_HomepageBanner_Controller_Adminhtml_HomepageBanner
{
    /**
     * init the banner tile
     *
     * @access protected
     * @return IBD_HomepageBanner_Model_Bannertile
     */
    protected function _initBannertile()
    {
        $bannertileId  = (int) $this->getRequest()->getParam('id');
        $bannertile    = Mage::getModel('ibd_homepagebanner/bannertile');
        if ($bannertileId) {
            $bannertile->load($bannertileId);
        }
        Mage::register('current_bannertile', $bannertile);
        return $bannertile;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('ibd_homepagebanner')->__('Homepage'))
             ->_title(Mage::helper('ibd_homepagebanner')->__('Banner Tiles'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit banner tile - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function editAction()
    {
        $bannertileId    = $this->getRequest()->getParam('id');
        $bannertile      = $this->_initBannertile();
        if ($bannertileId && !$bannertile->getId()) {
            $this->_getSession()->addError(
                Mage::helper('ibd_homepagebanner')->__('This banner tile no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getBannertileData(true);
        if (!empty($data)) {
            $bannertile->setData($data);
        }
        Mage::register('bannertile_data', $bannertile);
        $this->loadLayout();
        $this->_title(Mage::helper('ibd_homepagebanner')->__('Homepage Banner'))
             ->_title(Mage::helper('ibd_homepagebanner')->__('Banner Tiles'));
        if ($bannertile->getId()) {
            $this->_title($bannertile->getTitle());
        } else {
            $this->_title(Mage::helper('ibd_homepagebanner')->__('Add banner tile'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new banner tile action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save banner tile - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('bannertile')) {
            try {
                $bannertile = $this->_initBannertile();
                $bannertile->addData($data);
                $iconName = $this->_uploadAndGetName(
                    'icon',
                    Mage::helper('ibd_homepagebanner/bannertile_image')->getImageBaseDir(),
                    $data
                );
                $bannertile->setData('icon', $iconName);
                $bannertile->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ibd_homepagebanner')->__('Banner Tile was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $bannertile->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                if (isset($data['icon']['value'])) {
                    $data['icon'] = $data['icon']['value'];
                }
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setBannertileData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                if (isset($data['icon']['value'])) {
                    $data['icon'] = $data['icon']['value'];
                }
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_homepagebanner')->__('There was a problem saving the banner tile.')
                );
                Mage::getSingleton('adminhtml/session')->setBannertileData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('ibd_homepagebanner')->__('Unable to find banner tile to save.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * delete banner tile - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function deleteAction()
    {
        if ( $this->getRequest()->getParam('id') > 0) {
            try {
                $bannertile = Mage::getModel('ibd_homepagebanner/bannertile');
                $bannertile->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ibd_homepagebanner')->__('Banner Tile was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_homepagebanner')->__('There was an error deleting banner tile.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('ibd_homepagebanner')->__('Could not find banner tile to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete banner tile - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeleteAction()
    {
        $bannertileIds = $this->getRequest()->getParam('bannertile');
        if (!is_array($bannertileIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('ibd_homepagebanner')->__('Please select banner tiles to delete.')
            );
        } else {
            try {
                foreach ($bannertileIds as $bannertileId) {
                    $bannertile = Mage::getModel('ibd_homepagebanner/bannertile');
                    $bannertile->setId($bannertileId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ibd_homepagebanner')->__('Total of %d banner tiles were successfully deleted.', count($bannertileIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_homepagebanner')->__('There was an error deleting banner tiles.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massStatusAction()
    {
        $bannertileIds = $this->getRequest()->getParam('bannertile');
        if (!is_array($bannertileIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('ibd_homepagebanner')->__('Please select banner tiles.')
            );
        } else {
            try {
                foreach ($bannertileIds as $bannertileId) {
                $bannertile = Mage::getSingleton('ibd_homepagebanner/bannertile')->load($bannertileId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d banner tiles were successfully updated.', count($bannertileIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_homepagebanner')->__('There was an error updating banner tiles.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass Coming Soon change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massComingsoonAction()
    {
        $bannertileIds = $this->getRequest()->getParam('bannertile');
        if (!is_array($bannertileIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('ibd_homepagebanner')->__('Please select banner tiles.')
            );
        } else {
            try {
                foreach ($bannertileIds as $bannertileId) {
                $bannertile = Mage::getSingleton('ibd_homepagebanner/bannertile')->load($bannertileId)
                    ->setComingsoon($this->getRequest()->getParam('flag_coming_soon'))
                    ->setIsMassupdate(true)
                    ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d banner tiles were successfully updated.', count($bannertileIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_homepagebanner')->__('There was an error updating banner tiles.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportCsvAction()
    {
        $fileName   = 'bannertile.csv';
        $content    = $this->getLayout()->createBlock('ibd_homepagebanner/adminhtml_bannertile_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportExcelAction()
    {
        $fileName   = 'bannertile.xls';
        $content    = $this->getLayout()->createBlock('ibd_homepagebanner/adminhtml_bannertile_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportXmlAction()
    {
        $fileName   = 'bannertile.xml';
        $content    = $this->getLayout()->createBlock('ibd_homepagebanner/adminhtml_bannertile_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Ultimate Module Creator
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('cms/ibd_homepagebanner/bannertile');
    }
}

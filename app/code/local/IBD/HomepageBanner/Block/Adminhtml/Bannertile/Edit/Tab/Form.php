<?php
/**
 * IBD_HomepageBanner extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_HomepageBanner
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Banner Tile edit form tab
 *
 * @category    IBD
 * @package     IBD_HomepageBanner
 * @author      Ultimate Module Creator
 */
class IBD_HomepageBanner_Block_Adminhtml_Bannertile_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return IBD_HomepageBanner_Block_Adminhtml_Bannertile_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('bannertile_');
        $form->setFieldNameSuffix('bannertile');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'bannertile_form',
            array('legend' => Mage::helper('ibd_homepagebanner')->__('Banner Tile'))
        );
        $fieldset->addType(
            'image',
            Mage::getConfig()->getBlockClassName('ibd_homepagebanner/adminhtml_bannertile_helper_image')
        );

        $fieldset->addField(
            'title',
            'text',
            array(
                'label' => Mage::helper('ibd_homepagebanner')->__('Title'),
                'name'  => 'title',
                'note'	=> $this->__('The title of the banner tile'),
                'required'  => true,
                'class' => 'required-entry',
            )
        );

        $fieldset->addField(
            'url',
            'text',
            array(
                'label' => Mage::helper('ibd_homepagebanner')->__('Link URL'),
                'name'  => 'url',
                'note'	=> $this->__('The destination link URL for the tile'),
                'required'  => true,
                'class' => 'required-entry',
            )
        );

        $fieldset->addField(
            'order',
            'text',
            array(
                'label' => Mage::helper('ibd_homepagebanner')->__('Order'),
                'name'  => 'order',
                'note'  => $this->__('1-4, lower values display higher on the page'),
                'required'  => true,
                'class' => 'required-entry',
           )
        );

        $fieldset->addField(
            'new_window',
            'select',
            array(
                'label' => Mage::helper('ibd_homepagebanner')->__('Open in new window'),
                'name'  => 'new_window',
                'note'  => $this->__('The link will open in a new window'),
                'required'  => true,
                'class' => 'required-entry',

                'values'=> array(
                    array(
                        'value' => 1,
                        'label' => Mage::helper('ibd_homepagebanner')->__('Yes'),
                    ),
                    array(
                        'value' => 0,
                        'label' => Mage::helper('ibd_homepagebanner')->__('No'),
                    ),
                ),
            )
        );

        $fieldset->addField(
            'text',
            'text',
            array(
                'label' => Mage::helper('ibd_homepagebanner')->__('Text'),
                'name'  => 'text',
                'note'	=> $this->__('The smaller text on the tile'),
            )
        );

        $fieldset->addField(
            'icon',
            'image',
            array(
                'label' => Mage::helper('ibd_homepagebanner')->__('Icon'),
                'name'  => 'icon',
                'note'	=> $this->__('The icon image on the tile'),
            )
        );

        $fieldset->addField(
            'coming_soon',
            'select',
            array(
                'label' => Mage::helper('ibd_homepagebanner')->__('Coming Soon'),
                'name'  => 'coming_soon',
                'note'	=> $this->__('The tile will be dimmed and will show the Coming Soon ribbon'),
                'required'  => true,
                'class' => 'required-entry',

                'values'=> array(
                    array(
                        'value' => 1,
                        'label' => Mage::helper('ibd_homepagebanner')->__('Yes'),
                    ),
                    array(
                        'value' => 0,
                        'label' => Mage::helper('ibd_homepagebanner')->__('No'),
                    ),
                ),
            )
        );
        $fieldset->addField(
            'status',
            'select',
            array(
                'label'  => Mage::helper('ibd_homepagebanner')->__('Status'),
                'name'   => 'status',
                'values' => array(
                    array(
                        'value' => 1,
                        'label' => Mage::helper('ibd_homepagebanner')->__('Enabled'),
                    ),
                    array(
                        'value' => 0,
                        'label' => Mage::helper('ibd_homepagebanner')->__('Disabled'),
                    ),
                ),
            )
        );
        if (Mage::app()->isSingleStoreMode()) {
            $fieldset->addField(
                'store_id',
                'hidden',
                array(
                    'name'      => 'stores[]',
                    'value'     => Mage::app()->getStore(true)->getId()
                )
            );
            Mage::registry('current_bannertile')->setStoreId(Mage::app()->getStore(true)->getId());
        }
        $formValues = Mage::registry('current_bannertile')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getBannertileData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getBannertileData());
            Mage::getSingleton('adminhtml/session')->setBannertileData(null);
        } elseif (Mage::registry('current_bannertile')) {
            $formValues = array_merge($formValues, Mage::registry('current_bannertile')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}

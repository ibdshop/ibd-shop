<?php
/**
 * IBD_HomepageBanner extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_HomepageBanner
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Banner Tile admin edit tabs
 *
 * @category    IBD
 * @package     IBD_HomepageBanner
 * @author      Ultimate Module Creator
 */
class IBD_HomepageBanner_Block_Adminhtml_Bannertile_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('bannertile_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('ibd_homepagebanner')->__('Banner Tile'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return IBD_HomepageBanner_Block_Adminhtml_Bannertile_Edit_Tabs
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_bannertile',
            array(
                'label'   => Mage::helper('ibd_homepagebanner')->__('Banner Tile'),
                'title'   => Mage::helper('ibd_homepagebanner')->__('Banner Tile'),
                'content' => $this->getLayout()->createBlock(
                    'ibd_homepagebanner/adminhtml_bannertile_edit_tab_form'
                )
                ->toHtml(),
            )
        );
        $this->addTab(
            'form_colors_bannertile',
            array(
                'label'   => Mage::helper('ibd_homepagebanner')->__('Colors'),
                'title'   => Mage::helper('ibd_homepagebanner')->__('Colors'),
                'content' => $this->getLayout()->createBlock(
                    'ibd_homepagebanner/adminhtml_bannertile_edit_tab_colors'
                )
                ->toHtml(),
            )
        );
        if (!Mage::app()->isSingleStoreMode()) {
            $this->addTab(
                'form_store_bannertile',
                array(
                    'label'   => Mage::helper('ibd_homepagebanner')->__('Store views'),
                    'title'   => Mage::helper('ibd_homepagebanner')->__('Store views'),
                    'content' => $this->getLayout()->createBlock(
                        'ibd_homepagebanner/adminhtml_bannertile_edit_tab_stores'
                    )
                    ->toHtml(),
                )
            );
        }
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve banner tile entity
     *
     * @access public
     * @return IBD_HomepageBanner_Model_Bannertile
     * @author Ultimate Module Creator
     */
    public function getBannertile()
    {
        return Mage::registry('current_bannertile');
    }
}

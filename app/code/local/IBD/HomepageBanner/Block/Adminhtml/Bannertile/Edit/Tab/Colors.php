<?php
/**
 * IBD_HomepageBanner extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_HomepageBanner
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Bannertile edit form tab
 *
 * @category    IBD
 * @package     IBD_HomepageBanner
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_HomepageBanner_Block_Adminhtml_Bannertile_Edit_Tab_Colors extends Mage_Adminhtml_Block_Widget_Form
{
    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('ibd_shared/color_tab.phtml');
    }

    /**
     * prepare the form
     *
     * @access protected
     * @return IBD_HomepageBanner_Block_Adminhtml_Bannertile_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setFieldNameSuffix('bannertile');
        $this->setForm($form);

        $fieldset = $form->addFieldset(
            'bannertile_color_form',
            array('legend' => Mage::helper('ibd_homepagebanner')->__('Tile Color'))
        );
        $fieldset->addField(
            'fore_color',
            'text',
            array(
                'label' => Mage::helper('ibd_homepagebanner')->__('Text Color'),
                'name'  => 'fore_color',
                'note'  => $this->__('If empty, default will be white or gray depending on the background color'),
                'required'  => true,
                'class' => 'required-entry',
            )
        );
        $fieldset->addField(
            'background_color',
            'text',
            array(
                'label' => Mage::helper('ibd_homepagebanner')->__('Background Color'),
                'name'  => 'background_color',
                'note'  => $this->__('The color of the tile'),
                'required'  => true,
                'class' => 'required-entry',
            )
        );

        $form->addValues(Mage::registry('current_bannertile')->getData());
        return parent::_prepareForm();
    }
}

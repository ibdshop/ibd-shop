<?php
/**
 * IBD_HomepageBanner extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_HomepageBanner
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Advertisement admin edit form
 *
 * @category    IBD
 * @package     IBD_HomepageBanner
 * @author      Ultimate Module Creator
 */
class IBD_HomepageBanner_Block_Adminhtml_Advertisement_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'ibd_homepagebanner';
        $this->_controller = 'adminhtml_advertisement';
        $this->_updateButton(
            'save',
            'label',
            Mage::helper('ibd_homepagebanner')->__('Save Content Image')
        );
        $this->_updateButton(
            'delete',
            'label',
            Mage::helper('ibd_homepagebanner')->__('Delete Content Image')
        );
        $this->_addButton(
            'saveandcontinue',
            array(
                'label'   => Mage::helper('ibd_homepagebanner')->__('Save And Continue Edit'),
                'onclick' => 'saveAndContinueEdit()',
                'class'   => 'save',
            ),
            -100
        );
        $this->_formScripts[] = "
            function saveAndContinueEdit() {
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * get the edit form header
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getHeaderText()
    {
        if (Mage::registry('current_advertisement') && Mage::registry('current_advertisement')->getId()) {
            return Mage::helper('ibd_homepagebanner')->__(
                "Edit Content Image '%s'",
                $this->escapeHtml(Mage::registry('current_advertisement')->getName())
            );
        } else {
            return Mage::helper('ibd_homepagebanner')->__('Add Content Image');
        }
    }
}

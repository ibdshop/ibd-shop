<?php
/**
 * IBD_HomepageBanner extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_HomepageBanner
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Advertisement edit form tab
 *
 * @category    IBD
 * @package     IBD_HomepageBanner
 * @author      Ultimate Module Creator
 */
class IBD_HomepageBanner_Block_Adminhtml_Advertisement_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected $_helper = null;

    protected function ibdHelper()
    {
        if(!$this->_helper)
        {
            $this->_helper = Mage::helper('ibd_homepagebanner');
        }
        return $this->_helper;
    }

    /**
     * prepare the form
     *
     * @access protected
     * @return IBD_HomepageBanner_Block_Adminhtml_Advertisement_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('advertisement_');
        $form->setFieldNameSuffix('advertisement');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'advertisement_form',
            array('legend' => $this->ibdHelper()->__('Content Image'))
        );
        $fieldset->addType(
            'image',
            Mage::getConfig()->getBlockClassName('ibd_homepagebanner/adminhtml_advertisement_helper_image')
        );

        $fieldset->addField(
            'location',
            'select',
            array(
                'label'  => $this->ibdHelper()->__('Location'),
                'name'   => 'location',
                'values' => Mage::getSingleton('ibd_homepagebanner/advertisement')->getAvailableLocationOptions(),
            )
        );

        $fieldset->addField(
            'sort_order',
            'text',
            array(
                'label'  => $this->ibdHelper()->__('Display Order'),
                'name'   => 'sort_order',
                'note'   => $this->ibdHelper()->__('Displayed in ascending order'),
            )
        );

        $fieldset->addField(
            'name',
            'text',
            array(
                'label' => $this->ibdHelper()->__('Name'),
                'name'  => 'name',
                'note'	=> $this->ibdHelper()->__('Admin name (not displayed on page)'),
                'required'  => true,
                'class' => 'required-entry',
           )
        );

        $fieldset->addField(
            'new_window',
            'select',
            array(
                'label' => $this->ibdHelper()->__('Open in new window'),
                'name'  => 'new_window',
                'note'  => $this->ibdHelper()->__('The link will open in a new window'),
                'required'  => true,
                'class' => 'required-entry',

                'values'=> array(
                    array(
                        'value' => 1,
                        'label' => $this->ibdHelper()->__('Yes'),
                    ),
                    array(
                        'value' => 0,
                        'label' => $this->ibdHelper()->__('No'),
                    ),
                ),
           )
        );

        $fieldset->addField(
            'image',
            'image',
            array(
                'label' => $this->ibdHelper()->__('Image'),
                'name'  => 'image',
                'note'	=> $this->ibdHelper()->__('The advertisement image'),
           )
        );

        $fieldset->addField(
            'url',
            'text',
            array(
                'label' => $this->ibdHelper()->__('Link URL'),
                'name'  => 'url',
                'note'	=> $this->ibdHelper()->__('The destination link URL for the advertisement'),
           )
        );

        $fieldset->addField(
            'img_alt_text',
            'text',
            array(
                'label' => $this->ibdHelper()->__('Image Alt Text'),
                'name'  => 'img_alt_text',
                'note'  => $this->ibdHelper()->__('This text is displayed if the image does not load. It is highly recommended for SEO and usability.').
                            '<br/><a href="http://www.searchenginejournal.com/image-alt-text-vs-image-title-whats-the-difference/" target="_blank">Alt Text vs Title: What\'s the Difference?</a>',
           )
        );

        $fieldset->addField(
            'img_title',
            'text',
            array(
                'label' => $this->ibdHelper()->__('Image Title'),
                'name'  => 'img_title',
                'note'  => $this->ibdHelper()->__('This text is displayed when the user hovers over the image. If not supplied, the image alt text will be used.').
                            '<br/><a href="http://www.searchenginejournal.com/image-alt-text-vs-image-title-whats-the-difference/" target="_blank">Alt Text vs Title: What\'s the Difference?</a>',
           )
        );

        if (Mage::app()->isSingleStoreMode()) {
            $fieldset->addField(
                'store_id',
                'hidden',
                array(
                    'name'      => 'stores[]',
                    'value'     => Mage::app()->getStore(true)->getId()
                )
            );
            Mage::registry('current_advertisement')->setStoreId(Mage::app()->getStore(true)->getId());
        }
        $formValues = Mage::registry('current_advertisement')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getAdvertisementData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getAdvertisementData());
            Mage::getSingleton('adminhtml/session')->setAdvertisementData(null);
        } elseif (Mage::registry('current_advertisement')) {
            $formValues = array_merge($formValues, Mage::registry('current_advertisement')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}

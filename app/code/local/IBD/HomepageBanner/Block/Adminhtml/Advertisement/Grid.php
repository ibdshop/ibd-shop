<?php
/**
 * IBD_HomepageBanner extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_HomepageBanner
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Advertisement admin grid block
 *
 * @category    IBD
 * @package     IBD_HomepageBanner
 * @author      Ultimate Module Creator
 */
class IBD_HomepageBanner_Block_Adminhtml_Advertisement_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * constructor
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('advertisementGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * prepare collection
     *
     * @access protected
     * @return IBD_HomepageBanner_Block_Adminhtml_Advertisement_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('ibd_homepagebanner/advertisement')
            ->getCollection();
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * prepare grid collection
     *
     * @access protected
     * @return IBD_HomepageBanner_Block_Adminhtml_Advertisement_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'entity_id',
            array(
                'header' => Mage::helper('ibd_homepagebanner')->__('Id'),
                'index'  => 'entity_id',
                'type'   => 'number'
            )
        );
        $this->addColumn(
            'name',
            array(
                'header'    => Mage::helper('ibd_homepagebanner')->__('Name'),
                'align'     => 'left',
                'index'     => 'name',
            )
        );
        
        $this->addColumn(
            'sort_order',
            array(
                'header'  => Mage::helper('ibd_homepagebanner')->__('Order'),
                'index'   => 'sort_order',
                'type'    => 'number'
            )
        );
        
        $this->addColumn(
            'location',
            array(
                'header'  => Mage::helper('ibd_homepagebanner')->__('Content Image Location'),
                'index'   => 'location',
                'type'    => 'options',
                'options' => Mage::getSingleton('ibd_homepagebanner/advertisement')->getAvailableLocations(),
            )
        );

        if (!Mage::app()->isSingleStoreMode() && !$this->_isExport) {
            $this->addColumn(
                'store_id',
                array(
                    'header'     => Mage::helper('ibd_homepagebanner')->__('Store Views'),
                    'index'      => 'store_id',
                    'type'       => 'store',
                    'store_all'  => true,
                    'store_view' => true,
                    'sortable'   => false,
                    'filter_condition_callback'=> array($this, '_filterStoreCondition'),
                )
            );
        }
        $this->addColumn(
            'created_at',
            array(
                'header' => Mage::helper('ibd_homepagebanner')->__('Created at'),
                'index'  => 'created_at',
                'width'  => '120px',
                'type'   => 'datetime',
            )
        );
        $this->addColumn(
            'updated_at',
            array(
                'header'    => Mage::helper('ibd_homepagebanner')->__('Updated at'),
                'index'     => 'updated_at',
                'width'     => '120px',
                'type'      => 'datetime',
            )
        );
        $this->addColumn(
            'action',
            array(
                'header'  =>  Mage::helper('ibd_homepagebanner')->__('Action'),
                'width'   => '100',
                'type'    => 'action',
                'getter'  => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('ibd_homepagebanner')->__('Edit'),
                        'url'     => array('base'=> '*/*/edit'),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'is_system' => true,
                'sortable'  => false,
            )
        );
        $this->addExportType('*/*/exportCsv', Mage::helper('ibd_homepagebanner')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('ibd_homepagebanner')->__('Excel'));
        $this->addExportType('*/*/exportXml', Mage::helper('ibd_homepagebanner')->__('XML'));
        return parent::_prepareColumns();
    }

    /**
     * prepare mass action
     *
     * @access protected
     * @return IBD_HomepageBanner_Block_Adminhtml_Advertisement_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('advertisement');
        $this->getMassactionBlock()->addItem(
            'delete',
            array(
                'label'=> Mage::helper('ibd_homepagebanner')->__('Delete'),
                'url'  => $this->getUrl('*/*/massDelete'),
                'confirm'  => Mage::helper('ibd_homepagebanner')->__('Are you sure?')
            )
        );
        $this->getMassactionBlock()->addItem(
            'location',
            array(
                'label'      => Mage::helper('ibd_homepagebanner')->__('Change location'),
                'url'        => $this->getUrl('*/*/massStatus', array('_current'=>true)),
                'additional' => array(
                    'location' => array(
                        'name'   => 'location',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('ibd_homepagebanner')->__('Status'),
                        'values' => Mage::getSingleton('ibd_homepagebanner/advertisement')->getAvailableLocations(),
                    )
                )
            )
        );
        return $this;
    }

    /**
     * get the row url
     *
     * @access public
     * @param IBD_HomepageBanner_Model_Advertisement
     * @return string
     * @author Ultimate Module Creator
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * get the grid url
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    /**
     * after collection load
     *
     * @access protected
     * @return IBD_HomepageBanner_Block_Adminhtml_Advertisement_Grid
     * @author Ultimate Module Creator
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }

    /**
     * filter store column
     *
     * @access protected
     * @param IBD_HomepageBanner_Model_Resource_Advertisement_Collection $collection
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     * @return IBD_HomepageBanner_Block_Adminhtml_Advertisement_Grid
     * @author Ultimate Module Creator
     */
    protected function _filterStoreCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }
        $collection->addStoreFilter($value);
        return $this;
    }
}

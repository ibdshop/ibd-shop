<?php
/**
 * IBD_HomepageBanner extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_HomepageBanner
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Advertisement admin edit tabs
 *
 * @category    IBD
 * @package     IBD_HomepageBanner
 * @author      Ultimate Module Creator
 */
class IBD_HomepageBanner_Block_Adminhtml_Advertisement_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('advertisement_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('ibd_homepagebanner')->__('Content Image'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return IBD_HomepageBanner_Block_Adminhtml_Advertisement_Edit_Tabs
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_advertisement',
            array(
                'label'   => Mage::helper('ibd_homepagebanner')->__('Content Image'),
                'title'   => Mage::helper('ibd_homepagebanner')->__('Content Image'),
                'content' => $this->getLayout()->createBlock(
                    'ibd_homepagebanner/adminhtml_advertisement_edit_tab_form'
                )
                ->toHtml(),
            )
        );
        $this->addTab(
            'form_regions_advertisement',
            array(
                'label'   => Mage::helper('ibd_homepagebanner')->__('Location Relations'),
                'title'   => Mage::helper('ibd_homepagebanner')->__('Location Relations'),
                'content' => $this->getLayout()->createBlock(
                    'ibd_homepagebanner/adminhtml_advertisement_edit_tab_regions'
                )
                ->toHtml(),
            )
        );
        if (!Mage::app()->isSingleStoreMode()) {
            $this->addTab(
                'form_store_advertisement',
                array(
                    'label'   => Mage::helper('ibd_homepagebanner')->__('Store views'),
                    'title'   => Mage::helper('ibd_homepagebanner')->__('Store views'),
                    'content' => $this->getLayout()->createBlock(
                        'ibd_homepagebanner/adminhtml_advertisement_edit_tab_stores'
                    )
                    ->toHtml(),
                )
            );
        }
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve advertisement entity
     *
     * @access public
     * @return IBD_HomepageBanner_Model_Advertisement
     * @author Ultimate Module Creator
     */
    public function getAdvertisement()
    {
        return Mage::registry('current_advertisement');
    }
}

<?php
/**
 * IBD_HomepageBanner extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_HomepageBanner
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Advertisement edit form tab
 *
 * @category    IBD
 * @package     IBD_HomepageBanner
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_HomepageBanner_Block_Adminhtml_Advertisement_Edit_Tab_Regions extends Mage_Adminhtml_Block_Widget_Form
{
    protected $currentAd;

    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('ibd_shared/regions_tab.phtml');
        $this->currentAd = Mage::registry('current_advertisement');
    }

    protected function getAllTagsJSON($type)
    {
        return $this->getCollectionJSON(Mage::getSingleton('ibd_travelsuite/'.$type)->getCollection());
    }

    public function getChosenTagsJSON($type)
    {
        return $this->getCollectionJSON(
            Mage::getSingleton('ibd_homepagebanner/advertisement_'.$type)
                ->getAdvertisementAdCollection($this->currentAd, IBD_HomepageBanner_Model_Advertisement::PAGE));
    }

    protected function getCollectionJSON($collection)
    {
        $json = array();
        foreach ($collection as $region)
        {
            $json[] = array(
                'id' => $region->getId(),
                'name' => $region->getName(),
            );
        }
        return json_encode($json);
    }
}

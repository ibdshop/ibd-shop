<?php
/**
 * IBD_HomepageBanner extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_HomepageBanner
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Advertisement model
 *
 * @category    IBD
 * @package     IBD_HomepageBanner
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_HomepageBanner_Block_Tiles extends Mage_Core_Block_Template
{
	public function getTiles($count = 4)
	{
		return Mage::getModel('ibd_homepagebanner/bannertile')->getTiles($count);
	}
}
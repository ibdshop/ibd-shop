<?php
/**
 * IBD_HomepageBanner extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_HomepageBanner
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * HomepageBanner module install script
 *
 * @category    IBD
 * @package     IBD_HomepageBanner
 * @author      Ultimate Module Creator
 */
$this->startSetup();
$table = $this->getConnection()
    ->newTable($this->getTable('ibd_homepagebanner/advertisement'))
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'Advertisement ID'
    )
    ->addColumn(
        'name',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable'  => false,
        ),
        'Name'
    )
    ->addColumn(
        'new_window',
        Varien_Db_Ddl_Table::TYPE_SMALLINT, 1,
        array(
            'nullable'  => false,
        ),
        'Opens link in a new window'
    )
    ->addColumn(
        'image',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(),
        'Image'
    )
    ->addColumn(
        'url',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(),
        'Advertisement Link URL'
    )
    ->addColumn(
        'location',
        Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
        array(),
        'Enabled'
    )
    ->addColumn(
        'updated_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Advertisement Modification Time'
    )
    ->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Advertisement Creation Time'
    ) 
    ->setComment('Advertisement Table');
$this->getConnection()->createTable($table);
$table = $this->getConnection()
    ->newTable($this->getTable('ibd_homepagebanner/bannertile'))
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'Banner Tile ID'
    )
    ->addColumn(
        'title',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable'  => false,
        ),
        'Title'
    )
    ->addColumn(
        'url',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable'  => false,
        ),
        'Link URL'
    )
    ->addColumn(
        'background_color',
        Varien_Db_Ddl_Table::TYPE_TEXT, 10,
        array(
            'nullable'  => false,
            'default' => 'C5C1B5',
        ),
        'Background Color'
    )
    ->addColumn(
        'order',
        Varien_Db_Ddl_Table::TYPE_SMALLINT, 2,
        array(
            'nullable'  => false,
            'unsigned' => true,
        ),
        'Homepage display order'
    )
    ->addColumn(
        'new_window',
        Varien_Db_Ddl_Table::TYPE_SMALLINT, 1,
        array(
            'nullable'  => false,
        ),
        'Opens link in a new window'
    )
    ->addColumn(
        'text',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(),
        'Text'
    )
    ->addColumn(
        'icon',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(),
        'Icon'
    )
    ->addColumn(
        'coming_soon',
        Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
        array(
            'nullable'  => false,
        ),
        'Coming Soon'
    )
    ->addColumn(
        'status',
        Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
        array(),
        'Enabled'
    )
    ->addColumn(
        'updated_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Banner Tile Modification Time'
    )
    ->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Banner Tile Creation Time'
    ) 
    ->setComment('Banner Tile Table');
$this->getConnection()->createTable($table);
$table = $this->getConnection()
    ->newTable($this->getTable('ibd_homepagebanner/advertisement_store'))
    ->addColumn(
        'advertisement_id',
        Varien_Db_Ddl_Table::TYPE_SMALLINT,
        null,
        array(
            'nullable'  => false,
            'primary'   => true,
        ),
        'Advertisement ID'
    )
    ->addColumn(
        'store_id',
        Varien_Db_Ddl_Table::TYPE_SMALLINT,
        null,
        array(
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'Store ID'
    )
    ->addIndex(
        $this->getIdxName(
            'ibd_homepagebanner/advertisement_store',
            array('store_id')
        ),
        array('store_id')
    )
    ->addForeignKey(
        $this->getFkName(
            'ibd_homepagebanner/advertisement_store',
            'advertisement_id',
            'ibd_homepagebanner/advertisement',
            'entity_id'
        ),
        'advertisement_id',
        $this->getTable('ibd_homepagebanner/advertisement'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $this->getFkName(
            'ibd_homepagebanner/advertisement_store',
            'store_id',
            'core/store',
            'store_id'
        ),
        'store_id',
        $this->getTable('core/store'),
        'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->setComment('Advertisements To Store Linkage Table');
$this->getConnection()->createTable($table);
$table = $this->getConnection()
    ->newTable($this->getTable('ibd_homepagebanner/bannertile_store'))
    ->addColumn(
        'bannertile_id',
        Varien_Db_Ddl_Table::TYPE_SMALLINT,
        null,
        array(
            'nullable'  => false,
            'primary'   => true,
        ),
        'Banner Tile ID'
    )
    ->addColumn(
        'store_id',
        Varien_Db_Ddl_Table::TYPE_SMALLINT,
        null,
        array(
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'Store ID'
    )
    ->addIndex(
        $this->getIdxName(
            'ibd_homepagebanner/bannertile_store',
            array('store_id')
        ),
        array('store_id')
    )
    ->addForeignKey(
        $this->getFkName(
            'ibd_homepagebanner/bannertile_store',
            'bannertile_id',
            'ibd_homepagebanner/bannertile',
            'entity_id'
        ),
        'bannertile_id',
        $this->getTable('ibd_homepagebanner/bannertile'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $this->getFkName(
            'ibd_homepagebanner/bannertile_store',
            'store_id',
            'core/store',
            'store_id'
        ),
        'store_id',
        $this->getTable('core/store'),
        'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->setComment('Banner Tiles To Store Linkage Table');
$this->getConnection()->createTable($table);
$this->endSetup();

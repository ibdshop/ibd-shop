<?php
/**
 * IBD_HomepageBanner extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_HomepageBanner
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * HomepageBanner module install script
 *
 * @category    IBD
 * @package     IBD_HomepageBanner
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */

// adding alt text to advertisements
$this->startSetup();

$conn = $this->getConnection();
$conn->addColumn($this->getTable('ibd_homepagebanner_advertisement'),
    'sort_order',
    'int(8) unsigned default 0 comment \'The content image sort order\''
);

$this->endSetup();

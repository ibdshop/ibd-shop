<?php
/**
 * IBD_HomepageBanner extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_HomepageBanner
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * HomepageBanner module install script
 *
 * @category    IBD
 * @package     IBD_HomepageBanner
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
$this->startSetup();
$table = $this->getConnection()
    ->newTable($this->getTable('ibd_homepagebanner/advertisement_relation'))
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'identity'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'Entity ID'
    )
    ->addColumn(
        'advertisement_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'nullable'  => false,
            'unsigned' => true,
        ),
        'The ID of the linked advertisement'
    )
    ->addColumn(
        'relation_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'nullable'  => false,
            'unsigned' => true,
        ),
        'The ID of the linked relation'
    )
    ->addColumn(
        'relation_type',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable'  => false,
        ),
        'The type of the linked relation'
    )
    ->addColumn(
        'order',
        Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'default' => 0,
            'unsigned' => true,
        ),
        'The display order for the advertisement'
    )
    ->addIndex(
        $this->getIdxName(
            'ibd_homepagebanner/advertisement_relation',
            array('advertisement_id', 'relation_id', 'relation_type'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('advertisement_id', 'relation_id', 'relation_type'),
        array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE)
    )
    ->addIndex(
        $this->getIdxName(
            'ibd_homepagebanner/advertisement_relation',
            array('order')
        ),
        array('order')
    )
    ->addForeignKey(
        $this->getFkName(
            'ibd_homepagebanner/advertisement_relation',
            'advertisement_id',
            'ibd_homepagebanner/advertisement',
            'entity_id'
        ),
        'advertisement_id',
        $this->getTable('ibd_homepagebanner/advertisement'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->setComment('Advertisement Relation Table');
$this->getConnection()->createTable($table);
$this->endSetup();

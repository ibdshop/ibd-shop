-- they used absolute URLs when setting up certain things on their
-- dev site that we migrated to their production site. these should
-- be migrated to use relative URLs.

UPDATE primary_ibd_homepagebanner_advertisement
SET url = REPLACE(url, 'http://ibd-shop.robotjumpstart.com', '')
WHERE url LIKE 'http://ibd-shop.robotjumpstart.com%';

UPDATE primary_ibd_homepagebanner_bannertile
SET url = REPLACE(url, 'http://ibd-shop.robotjumpstart.com', '')
WHERE url LIKE 'http://ibd-shop.robotjumpstart.com%';

UPDATE primary_cms_page
SET ibd_topsection_url = REPLACE(ibd_topsection_url, 'http://ibd-shop.robotjumpstart.com', '')
WHERE ibd_topsection_url LIKE 'http://ibd-shop.robotjumpstart.com%';
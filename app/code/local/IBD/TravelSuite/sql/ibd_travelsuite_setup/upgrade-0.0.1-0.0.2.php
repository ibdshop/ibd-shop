<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * TravelSuite module install script
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
$this->startSetup();
$table = $this->getConnection()
    ->newTable($this->getTable('ibd_travelsuite/brand_group'))
    ->addColumn(
        'brand_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'unsigned'  => true,
            'nullable'  => false,
            'default'   => '0',
        ),
        'Brand ID'
    )
    ->addColumn(
        'group_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'unsigned'  => true,
            'nullable'  => false,
            'default'   => '0',
        ),
        'Group ID'
    )
    ->addIndex(
        $this->getIdxName(
            'ibd_travelsuite/brand_group',
            array('group_id')
        ),
        array('group_id')
    )
    ->addIndex(
        $this->getIdxName(
            'ibd_travelsuite/brand_group',
            array('brand_id')
        ),
        array('brand_id')
    )
    ->addForeignKey(
        $this->getFkName(
            'ibd_travelsuite/brand_group',
            'brand_id',
            'ibd_travelsuite/brand',
            'entity_id'
        ),
        'brand_id',
        $this->getTable('ibd_travelsuite/brand'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $this->getFkName(
            'ibd_travelsuite/brand_group',
            'group_id',
            'ibd_travelsuite/group',
            'entity_id'
        ),
        'group_id',
        $this->getTable('ibd_travelsuite/group'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addIndex(
        $this->getIdxName(
            'ibd_travelsuite/brand_group',
            array('brand_id', 'group_id'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('brand_id', 'group_id'),
        array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE)
    )
    ->setComment('Brand to Group Linkage Table');

$this->getConnection()->createTable($table);
$this->endSetup();

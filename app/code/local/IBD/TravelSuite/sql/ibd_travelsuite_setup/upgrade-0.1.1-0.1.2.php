<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */

/**
 * TravelSuite module install script
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */

// add banner icon and position fields to cms page
$this->startSetup();

$conn = $this->getConnection();
$table = $this->getTable('cms_page');

$conn->addColumn($table,
    'ibd_banner_icon',
    'nvarchar(255) comment \'cms page banner icon\''
);
$conn->addColumn($table,
    'ibd_banner_align',
    'nvarchar(5) comment \'cms page banner alignment\''
);
$conn->addColumn($table,
    'ibd_banner_icon_position',
    'nvarchar(6) comment \'cms page banner icon position\''
);

$this->endSetup();

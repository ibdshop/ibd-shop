<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * TravelSuite module update script
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */

// adding product image to brands so they don't have to manage actual products yet (and other images)
$this->startSetup();

$conn = $this->getConnection();
$conn->addColumn($this->getTable('ibd_travelsuite/brand'),
    'logo',
    'nvarchar(255) comment \'brand logo image\''
);
$conn->addColumn($this->getTable('ibd_travelsuite/brand'),
    'wechat_qr',
    'nvarchar(255) comment \'wechat QR code image\''
);
$conn->addColumn($this->getTable('ibd_travelsuite/brand'),
    'product_image',
    'nvarchar(255) comment \'Sample product image for showcase\''
);

$this->endSetup();
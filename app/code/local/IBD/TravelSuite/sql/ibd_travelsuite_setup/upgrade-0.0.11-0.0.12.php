<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * TravelSuite module install script
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */

// add tag table
$this->startSetup();
$tagTable = $this->getConnection()
    ->newTable($this->getTable('ibd_travelsuite/tag'))
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'Gallery Image ID'
    )
    ->addColumn(
        'tag',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        255,
        array( 'nullable'  => false ),
        'The tag text itself'
    )
    ->addIndex(
        $this->getIdxName(
            'ibd_travelsuite/gallery',
            array('tag'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('tag'),
        array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE)
    )
    ->setComment('Text tags');

$this->getConnection()->createTable($tagTable);


// add tag relation table
$tagRelationTable = $this->getConnection()
    ->newTable($this->getTable('ibd_travelsuite/tag_relation'))
    ->addColumn(
        'tag_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'nullable'  => false,
            'unsigned'   => true,
        ),
        'The ID of the tag'
    )
    ->addColumn(
        'relation_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'nullable'  => false,
            'unsigned'   => true,
        ),
        'The ID of the relation'
    )
    ->addColumn(
        'relation_type',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        255,
        array( 'nullable'  => false ),
        'The type of the relation'
    )
    ->addForeignKey(
        $this->getFkName(
            'ibd_travelsuite/tag_relation',
            'tag_id',
            'ibd_travelsuite/tag',
            'entity_id'
        ),
        'tag_id',
        $this->getTable('ibd_travelsuite/tag'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addIndex(
        $this->getIdxName(
            'ibd_travelsuite/gallery',
            array('tag_id', 'relation_id', 'relation_type'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('tag_id', 'relation_id', 'relation_type'),
        array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE)
    )
    ->setComment('Text tags');

$this->getConnection()->createTable($tagRelationTable);
$this->endSetup();

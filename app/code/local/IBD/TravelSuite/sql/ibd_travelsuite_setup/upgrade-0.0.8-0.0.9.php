<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * TravelSuite module install script
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */

// add gallery table
$this->startSetup();
$table = $this->getConnection()
    ->newTable($this->getTable('ibd_travelsuite/gallery'))
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'Gallery Image ID'
    )
    ->addColumn(
        'relation_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'unsigned'  => true,
            'nullable'  => false,
        ),
        'The ID of the relation that this image is for'
    )
    ->addColumn(
        'relation_type',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        20,
        array( 'nullable'  => false ),
        'The type of the relation that this image is for'
    )
    ->addColumn(
        'image_path',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        255,
        array( 'nullable'  => false ),
        'The image path'
    )
    ->addColumn(
        'order',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'unsigned' => true,
            'nullable'  => false,
            'default' => 0,
        ),
        'The order of the image'
    )
    ->addColumn(
        'updated_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Gallery Image Modification Time'
    )
    ->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Gallery Image Creation Time'
    )
    ->addIndex(
        $this->getIdxName(
            'ibd_travelsuite/gallery',
            array('relation_id')
        ),
        array('relation_id')
    )
    ->addIndex(
        $this->getIdxName(
            'ibd_travelsuite/gallery',
            array('relation_type')
        ),
        array('relation_type')
    )
    ->addIndex(
        $this->getIdxName(
            'ibd_travelsuite/gallery',
            array('order')
        ),
        array('order')
    )
    ->setComment('Gallery linkage table to any type');

$this->getConnection()->createTable($table);
$this->endSetup();

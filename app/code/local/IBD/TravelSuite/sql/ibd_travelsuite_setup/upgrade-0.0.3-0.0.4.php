<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * TravelSuite module install script
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */

// add fore color columns
$this->startSetup();

$conn = $this->getConnection();

// add fore_color columns
$conn->addColumn($this->getTable('ibd_travelsuite_region'),
	'fore_color',
	'varchar(10)'
);
$conn->addColumn($this->getTable('ibd_travelsuite_group'),
	'fore_color',
	'varchar(10)'
);
$conn->addColumn($this->getTable('ibd_travelsuite_brand'),
	'fore_color',
	'varchar(10)'
);

$this->endSetup();

<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * TravelSuite module install script
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */

// add more columns to groups
$this->startSetup();

$conn = $this->getConnection();
$table = $this->getTable('ibd_travelsuite_group');

// adding more columns to group table
$conn->addColumn($table,
	'address',
	'varchar(255) comment \'The written out physical address\''
);
$conn->addColumn($table,
	'directions',
	'text comment \'JSON object of directions e.g. walking, driving\''
);
$conn->addColumn($table,
	'official_url',
	'varchar(255) comment \'Official Group URL\''
);
$conn->addColumn($table,
	'social',
	'text comment \'JSON object of social media links e.g. twitter, facebook\''
);

$this->endSetup();

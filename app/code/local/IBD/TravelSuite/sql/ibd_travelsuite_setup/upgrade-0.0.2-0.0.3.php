<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * TravelSuite module install script
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */

// add header columns to the page table
$this->startSetup();

$conn = $this->getConnection();
$table = $this->getTable('cms_page');
$conn->addColumn($table,
    'ibd_header_title',
    'varchar(255)'
);
$conn->addColumn($table,
    'ibd_header_icon',
    'varchar(255)'
);
$conn->addColumn($table,
    'ibd_header_color',
    'varchar(10)'
);
$conn->addColumn($table,
    'ibd_header_text',
    'varchar(255)'
);

$this->endSetup();

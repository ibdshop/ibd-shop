<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * TravelSuite module install script
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */

// remove top section and header stuff
$this->startSetup();
$conn = $this->getConnection();
$table = $this->getTable('cms_page');

// drop top section cols
$conn->dropColumn($table, 'ibd_topsection_title');
$conn->dropColumn($table, 'ibd_topsection_image');
$conn->dropColumn($table, 'ibd_topsection_fore_color');
$conn->dropColumn($table, 'ibd_topsection_color');
$conn->dropColumn($table, 'ibd_topsection_tab_text');
$conn->dropColumn($table, 'ibd_topsection_url');

// drop header cols
$conn->dropColumn($table, 'ibd_header_title');
$conn->dropColumn($table, 'ibd_header_icon');
$conn->dropColumn($table, 'ibd_header_color');
$conn->dropColumn($table, 'ibd_header_text');
$conn->dropColumn($table, 'ibd_header_fore_color');

$this->endSetup();
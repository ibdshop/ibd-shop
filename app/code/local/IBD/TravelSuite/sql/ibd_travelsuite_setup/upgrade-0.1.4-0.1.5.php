<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */

/**
 * TravelSuite module install script
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */

// add a meta JSON field to gallery images for storing URLs, descriptions, etc
$this->startSetup();

$conn = $this->getConnection();
$table = $this->getTable('cms_page');

$conn->addColumn($table,
    'ibd_text_color',
    'nvarchar(10) not null default \'f6f6f6\' comment \'title text color\''
);

$conn->addColumn($table,
    'ibd_background_color',
    'nvarchar(10) not null default \'000\' comment \'title background color\''
);

$this->endSetup();
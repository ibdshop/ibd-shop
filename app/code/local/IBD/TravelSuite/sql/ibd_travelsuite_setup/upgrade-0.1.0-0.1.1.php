<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * TravelSuite module update script
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */

// adding product image to brands so they don't have to manage actual products yet (and other images)
$this->startSetup();

$conn = $this->getConnection();
$conn->addColumn($this->getTable('ibd_travelsuite/brand'),
    'banner_icon',
    'nvarchar(255) comment \'brand banner icon\''
);
$conn->addColumn($this->getTable('ibd_travelsuite/group'),
    'banner_icon',
    'nvarchar(255) comment \'group banner icon\''
);
$conn->addColumn($this->getTable('ibd_travelsuite/region'),
    'banner_icon',
    'nvarchar(255) comment \'region banner icon\''
);

$conn->addColumn($this->getTable('ibd_travelsuite/brand'),
    'banner_align',
    'nvarchar(5) comment \'brand banner alignment\''
);
$conn->addColumn($this->getTable('ibd_travelsuite/group'),
    'banner_align',
    'nvarchar(5) comment \'group banner alignment\''
);
$conn->addColumn($this->getTable('ibd_travelsuite/region'),
    'banner_align',
    'nvarchar(5) comment \'region banner alignment\''
);

$conn->addColumn($this->getTable('ibd_travelsuite/brand'),
    'banner_icon_position',
    'nvarchar(6) comment \'brand banner icon position\''
);
$conn->addColumn($this->getTable('ibd_travelsuite/group'),
    'banner_icon_position',
    'nvarchar(6) comment \'group banner icon position\''
);
$conn->addColumn($this->getTable('ibd_travelsuite/region'),
    'banner_icon_position',
    'nvarchar(6) comment \'region banner icon position\''
);

$this->endSetup();
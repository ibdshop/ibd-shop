-- phpMyAdmin SQL Dump
-- version 2.8.0.1
-- http://www.phpmyadmin.net
-- 
-- Host: custsql-ipg54.eigbox.net
-- Generation Time: May 29, 2015 at 04:01 AM
-- Server version: 5.5.32
-- PHP Version: 4.4.9
-- 
-- Database: `ibd`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `primary_ibd_homepagebanner_advertisement`
-- 

-- CREATE TABLE `primary_ibd_homepagebanner_advertisement` (
--   `entity_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Advertisement ID',
--   `name` varchar(255) NOT NULL COMMENT 'Name',
--   `new_window` smallint(6) NOT NULL COMMENT 'Opens link in a new window',
--   `image` varchar(255) DEFAULT NULL COMMENT 'Image',
--   `url` varchar(255) DEFAULT NULL COMMENT 'Advertisement Link URL',
--   `location` smallint(6) DEFAULT NULL COMMENT 'Enabled',
--   `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Advertisement Modification Time',
--   `created_at` timestamp NULL DEFAULT NULL COMMENT 'Advertisement Creation Time',
--   PRIMARY KEY (`entity_id`)
-- ) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Advertisement Table' AUTO_INCREMENT=6 ;

-- 
-- Dumping data for table `primary_ibd_homepagebanner_advertisement`
-- 

INSERT INTO `primary_ibd_homepagebanner_advertisement` VALUES (1, 'Homepage Left Ad', 1, '/s/p/space-for-ads-sample.jpg', 'http://www.google.com/ncr', 1, '2015-05-25 04:57:04', '2015-05-20 01:09:37');
INSERT INTO `primary_ibd_homepagebanner_advertisement` VALUES (2, 'GB Tax Free', 1, '/g/b/gb-tax-free.jpg', 'http://ibd-shop.robotjumpstart.com/travel/group/view/id/5/', 3, '2015-05-20 02:54:09', '2015-05-20 02:38:04');
INSERT INTO `primary_ibd_homepagebanner_advertisement` VALUES (4, 'liberty banner', 1, '/l/i/liberty-banners-02.jpg', 'http://ibd-shop.robotjumpstart.com/travel/brand/view/id/4/', 2, '2015-05-29 03:37:50', '2015-05-28 03:35:15');

-- --------------------------------------------------------

-- 
-- Table structure for table `primary_ibd_homepagebanner_advertisement_relation`
-- 

-- CREATE TABLE `primary_ibd_homepagebanner_advertisement_relation` (
--   `entity_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
--   `advertisement_id` int(11) NOT NULL COMMENT 'The ID of the linked advertisement',
--   `relation_id` int(10) unsigned NOT NULL COMMENT 'The ID of the linked relation',
--   `relation_type` varchar(255) NOT NULL COMMENT 'The type of the linked relation',
--   `order` int(10) unsigned DEFAULT '0' COMMENT 'The display order for the advertisement',
--   PRIMARY KEY (`entity_id`),
--   UNIQUE KEY `B0E190BDF4260AC812FCF624DA840DEE` (`advertisement_id`,`relation_id`,`relation_type`),
--   KEY `IDX_IBD_IBD_HOMEPAGEBANNER_ADVERTISEMENT_RELATION_ORDER` (`order`)
-- ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Advertisement Relation Table' AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `primary_ibd_homepagebanner_advertisement_relation`
-- 

INSERT INTO `primary_ibd_homepagebanner_advertisement_relation` VALUES (1, 2, 4, 'ibd_region', 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `primary_ibd_homepagebanner_advertisement_store`
-- 

-- CREATE TABLE `primary_ibd_homepagebanner_advertisement_store` (
--   `advertisement_id` int(11) NOT NULL COMMENT 'Advertisement ID',
--   `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
--   PRIMARY KEY (`advertisement_id`,`store_id`),
--   KEY `IDX_IBD_IBD_HOMEPAGEBANNER_ADVERTISEMENT_STORE_STORE_ID` (`store_id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Advertisements To Store Linkage Table';

-- 
-- Dumping data for table `primary_ibd_homepagebanner_advertisement_store`
-- 

INSERT INTO `primary_ibd_homepagebanner_advertisement_store` VALUES (1, 1);
INSERT INTO `primary_ibd_homepagebanner_advertisement_store` VALUES (2, 1);
INSERT INTO `primary_ibd_homepagebanner_advertisement_store` VALUES (4, 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `primary_ibd_homepagebanner_bannertile`
-- 

-- CREATE TABLE `primary_ibd_homepagebanner_bannertile` (
--   `entity_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Banner Tile ID',
--   `title` varchar(255) NOT NULL COMMENT 'Title',
--   `url` varchar(255) NOT NULL COMMENT 'Link URL',
--   `background_color` varchar(10) NOT NULL DEFAULT 'C5C1B5' COMMENT 'Background Color',
--   `order` smallint(5) unsigned NOT NULL COMMENT 'Homepage display order',
--   `new_window` smallint(6) NOT NULL COMMENT 'Opens link in a new window',
--   `text` varchar(255) DEFAULT NULL COMMENT 'Text',
--   `icon` varchar(255) DEFAULT NULL COMMENT 'Icon',
--   `coming_soon` smallint(6) NOT NULL COMMENT 'Coming Soon',
--   `status` smallint(6) DEFAULT NULL COMMENT 'Enabled',
--   `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Banner Tile Modification Time',
--   `created_at` timestamp NULL DEFAULT NULL COMMENT 'Banner Tile Creation Time',
--   `fore_color` varchar(10) NOT NULL DEFAULT 'F6F6F5' COMMENT 'The text fore color for the tile',
--   PRIMARY KEY (`entity_id`)
-- ) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Banner Tile Table' AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `primary_ibd_homepagebanner_bannertile`
-- 

INSERT INTO `primary_ibd_homepagebanner_bannertile` VALUES (1, '海外购物', 'http://www.ibdshop.com', 'FFF', 1, 1, '世界最前沿的潮流资讯', '/s/h/shop-abroad.png', 0, 1, '2015-05-22 02:43:09', '2015-05-13 02:35:08', '53c1c0');
INSERT INTO `primary_ibd_homepagebanner_bannertile` VALUES (2, '百货商店', 'http://www.ibdshop.com', 'FFF', 2, 1, '逛琳琅满目的百货公司', '/d/e/department-stores.png', 0, 1, '2015-05-28 03:23:27', '2015-05-13 02:42:07', 'f38840');
INSERT INTO `primary_ibd_homepagebanner_bannertile` VALUES (3, '免税购物', 'http://www.ibdshop.com', 'FFF', 3, 1, '网络最全品牌免税服务', '/t/a/tax-free-shopping.png', 0, 1, '2015-05-28 03:34:06', '2015-05-20 03:05:52', '56c4c3');
INSERT INTO `primary_ibd_homepagebanner_bannertile` VALUES (4, '酒店预览', 'http://www.ibdshop.com', 'FFF', 4, 1, '最方便的购物住宿选择', '/h/o/hotel.png', 0, 1, '2015-05-28 03:35:22', '2015-05-20 03:09:59', 'f38840');

-- --------------------------------------------------------

-- 
-- Table structure for table `primary_ibd_homepagebanner_bannertile_store`
-- 

-- CREATE TABLE `primary_ibd_homepagebanner_bannertile_store` (
--   `bannertile_id` int(11) NOT NULL COMMENT 'Banner Tile ID',
--   `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
--   PRIMARY KEY (`bannertile_id`,`store_id`),
--   KEY `IDX_IBD_IBD_HOMEPAGEBANNER_BANNERTILE_STORE_STORE_ID` (`store_id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Banner Tiles To Store Linkage Table';

-- 
-- Dumping data for table `primary_ibd_homepagebanner_bannertile_store`
-- 

INSERT INTO `primary_ibd_homepagebanner_bannertile_store` VALUES (1, 1);
INSERT INTO `primary_ibd_homepagebanner_bannertile_store` VALUES (2, 1);
INSERT INTO `primary_ibd_homepagebanner_bannertile_store` VALUES (3, 1);
INSERT INTO `primary_ibd_homepagebanner_bannertile_store` VALUES (4, 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `primary_ibd_travelsuite_brand`
-- 

-- CREATE TABLE `primary_ibd_travelsuite_brand` (
--   `entity_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Brand ID',
--   `name` varchar(255) NOT NULL COMMENT 'Name',
--   `summary` varchar(255) DEFAULT NULL COMMENT 'Summary',
--   `description` text COMMENT 'Description',
--   `image` varchar(255) DEFAULT NULL COMMENT 'Main Image',
--   `color` varchar(10) NOT NULL DEFAULT 'C5C1B5' COMMENT 'Highlight Color',
--   `status` smallint(6) DEFAULT NULL COMMENT 'Enabled',
--   `url_key` varchar(255) DEFAULT NULL COMMENT 'URL key',
--   `in_rss` smallint(6) DEFAULT NULL COMMENT 'In RSS',
--   `meta_title` varchar(255) DEFAULT NULL COMMENT 'Meta title',
--   `meta_keywords` text COMMENT 'Meta keywords',
--   `meta_description` text COMMENT 'Meta description',
--   `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Brand Modification Time',
--   `created_at` timestamp NULL DEFAULT NULL COMMENT 'Brand Creation Time',
--   `fore_color` varchar(10) NOT NULL DEFAULT '333333',
--   `official_url` varchar(255) DEFAULT NULL COMMENT 'Official Group URL',
--   `social` text COMMENT 'JSON object of social media links e.g. twitter, facebook',
--   `group_data` text COMMENT 'JSON object for group data',
--   `logo` varchar(255) DEFAULT NULL COMMENT 'brand logo image',
--   `wechat_qr` varchar(255) DEFAULT NULL COMMENT 'wechat QR code image',
--   `product_image` varchar(255) DEFAULT NULL COMMENT 'Sample product image for showcase',
--   PRIMARY KEY (`entity_id`)
-- ) ENGINE=InnoDB AUTO_INCREMENT=394 DEFAULT CHARSET=utf8 COMMENT='Brand Table' AUTO_INCREMENT=394 ;

-- 
-- Dumping data for table `primary_ibd_travelsuite_brand`
-- 

INSERT INTO `primary_ibd_travelsuite_brand` VALUES (1, 'Browns 布朗斯', NULL, '<p>布朗斯百货成立于1970年，由英国顶尖时尚零售教母Joan Burstein创建，是世界上最负盛名的时尚购物目的地之一。布朗斯以挖掘诸如亚历山大&bull;麦昆，约翰&bull;加里亚诺及克里斯托弗&bull;凯恩等时尚设计师而闻名，为顾客们提供了独一无二的新潮时尚元素，以及由顶尖设计师们所呈现的各类时尚精品，包括女装，男装，珠宝及配饰等。</p>', '/b/r/browns-shop-floor-front.jpg', '000000', 3, 'browns', 0, NULL, NULL, NULL, '2015-05-22 03:20:00', '2015-05-11 03:34:54', 'FFF', 'https://www.brownsfashion.com ', '{"twitter":"https:\\/\\/twitter.com\\/BrownsFashion","facebook":"https:\\/\\/www.facebook.com\\/brownsfashion","instagram":"https:\\/\\/instagram.com\\/brownsfashion\\/","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"24-27 Molton Street<br>\\r\\nLondon<br>\\r\\nW1K 5RD","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', '/0/9/093.jpg', NULL, '/s/l/sloane_1.jpg');
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (2, 'Jaeger 耶格', NULL, '<p><span style="font-size: 12px;">耶格成立于1884年，作为经典英式品牌的缩影，耶格融合了质量，文化底蕴和一丝狡黠的英式智慧。耶格的产品取料来源于质量最为上乘的动物皮料 &ndash; 其中包括骆驼毛发，羊驼及美利诺羊毛，保证了其经典款式的可穿性，耐用性，及持久的保存性，使顾客对其爱不释手。从千岛花纹，到Gostwyck 牧场出产的羊毛，耶格的经典款式于数十载赢得多位明星粉丝的心。其中包括影星玛丽莲 &bull;梦露，加里 &bull;格兰特，超模凯特 &bull;摩丝及朱迪&bull;基德等。这个春季，耶格女装从著名艺术家Barbara Hepworth 和Paul Klee的作品中汲取灵感，打造了雕塑般的剪裁和极致的印花。而男装设计则把上等羊绒布料用于典型的英式剪裁，将&ldquo;前卫的经典&rdquo;演绎得淋漓尽致。</span></p>', '/j/a/jaeger-21_final.jpg', '3F3A54', 3, 'jaeger', 0, '耶格品牌店铺信息IBDShop.com', '耶格伦敦商店购物信息地图地铁公交自驾停车', '耶格是一个经典的英式品牌，以皮毛产品为主。其风格简约，面料及设计精致，有极强的可穿性，耐用性及保存性。欢迎来到IBDShop.com了解更多关于耶格的品牌信息，及交通，地址，和相关购物指南。', '2015-05-25 04:32:44', '2015-05-13 03:17:22', 'FFF', 'http://www.jaeger.co.uk', '{"twitter":"https:\\/\\/twitter.com\\/JaegerOfficial","facebook":"https:\\/\\/www.facebook.com\\/Jaeger.co.uk","instagram":"https:\\/\\/instagram.com\\/jaegerofficial\\/","weibo":""}', '{"1":{"groupName":"\\u90a6\\u5fb7\\u8857","address":"\\u6444\\u653f\\u5927\\u8857200-206\\u53f7.  \\u90ae\\u7f16: W1B 5BN.","hours":"10am - 8pm","phone":"0845 051 0063","directions":{"driving":"\\u4e58\\u5750\\u51fa\\u79df\\u8f66\\u53ef\\u4eceFoubert''s Place\\u8857\\u5230\\u8fbe","ptrans":"\\u5730\\u94c1\\u7ebf\\u8def\\uff1aCentral, Victoria and Bakerloo Line. \\u4ece\\u725b\\u6d25\\u5706\\u73af\\u5e7f\\u573a\\u5730\\u94c1\\u7ad9\\u53ef\\u5feb\\u901f\\u65b9\\u4fbf\\u5230\\u8fbe"}},"2":{"groupName":"Regent Street \\u6444\\u653f\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', '/j/a/jaeger-logo_1.png', NULL, '/j/a/jaeger-product.jpg');
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (3, 'Prada 普拉达', NULL, NULL, NULL, '', 1, 'prada', 0, NULL, NULL, NULL, '2015-05-22 03:43:53', '2015-05-13 04:04:09', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"16 - 18 Old Bond Street, London. \\r\\nW1X 3DA","hours":"","phone":"020 7647 5000","directions":{"driving":"","ptrans":""}},"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"Store address Street\\r\\nLondon Store City\\r\\nUK 123455-hk","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (4, 'Liberty 利伯提百货', NULL, '<p>Liberty百货成立于1875年，坐落于伦敦市心脏地带，是为数不多的集创意和多样性设计为一体的大型百货公司。直到今天它仍然是深谙购物之道人士的首选地之一。Liberty传承了前沿及实验性设计的的丰厚底蕴，为顾客每一次的光临都创造出独一无二的惊喜和令人兴奋的购物体验。Liberty百货一共6层，提供了从时装，美容到家居以及Liberty自创生产的精致布料。Liberty不只是一个名字，更是其创始人Arthur Liberty这段传奇的延续，体现出其至诚、价值、质量、及美学设计的理念。</p>', '/g/r/great-marlborough-street.jpg', '3F3A54', 3, 'liberty', 0, 'Liberty百货品牌信息IBDShop.com', 'Liberty百货伦敦奢侈品购物地铁公交交通品牌信息攻略', 'Liberty百货是伦敦著名的百货公司，是奢侈品购物的绝佳地点，请点击查看更多品牌及交通信息。', '2015-05-15 03:15:56', '2015-05-14 09:29:18', 'F6F6F5', 'http://www.liberty.co.uk/', '{"twitter":"https:\\/\\/twitter.com\\/libertylondon","facebook":"https:\\/\\/www.facebook.com\\/libertylondon","instagram":"https:\\/\\/instagram.com\\/LibertyLondon\\/","weibo":""}', '{"2":{"groupName":"Regent Street \\u6444\\u653f\\u5927\\u8857","address":"\\u4f26\\u6566\\u6444\\u653f\\u5927\\u8857 \\u90ae\\u7f16: W1B 5AH","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\u4e3a10am-8pm \\u5468\\u65e5\\u4e3a12pm \\u2013 6pm","phone":"0207 734 1234","directions":{"driving":"\\u53ef\\u4ee5\\u544a\\u77e5\\u53f8\\u673aLiberty\\u90ae\\u7f16\\uff1aW1B 5AH. \\u6216\\u505c\\u5728Carnaby \\u5927\\u8857\\u5165\\u53e3\\u7684Great Marlborough\\u4ea4\\u6c47\\u5904","ptrans":"\\u5730\\u94c1\\r\\n \\r\\n\\u725b\\u6d25\\u5706\\u73af\\u5e7f\\u573a \\u2013 Central, Bakerloo and Victoria lines\\r\\n\\u76ae\\u5361\\u8fea\\u5229\\u5706\\u73af\\u5e7f\\u573a \\u2013 Piccadilly and Bakerloo lines\\r\\n\\r\\n\\r\\n\\u516c\\u4ea4\\u7ebf\\u8def\\r\\n\\r\\n3, 6, 12, 13, 15, 23, 53, X53, 88, 94, 139, 159 \\u4ee5\\u53ca C2\\r\\n"}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (5, 'Zara ', NULL, NULL, NULL, '', 1, 'zara', 0, NULL, NULL, NULL, '2015-05-20 01:36:50', '2015-05-20 01:36:50', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (6, 'TM LEWIN', NULL, NULL, NULL, '', 1, 'tm-lewin', 0, NULL, NULL, NULL, '2015-05-20 01:37:29', '2015-05-20 01:37:29', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (7, 'SAKARE', NULL, NULL, NULL, '', 1, 'sakare', 0, NULL, NULL, NULL, '2015-05-20 01:38:05', '2015-05-20 01:38:05', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (8, 'PRONOVIAS', NULL, NULL, NULL, '', 1, 'pronovias', 0, NULL, NULL, NULL, '2015-05-20 01:38:34', '2015-05-20 01:38:34', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (9, 'VICTORINOX', NULL, NULL, NULL, '', 1, 'victorinox', 0, NULL, NULL, NULL, '2015-05-20 01:39:03', '2015-05-20 01:39:03', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (10, 'BALLY', NULL, NULL, NULL, '', 1, 'bally', 0, NULL, NULL, NULL, '2015-05-20 01:39:40', '2015-05-20 01:39:40', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (11, 'ROYAL BANK OF SCOTLAND', NULL, NULL, NULL, '', 1, 'royal-bank-of-scotland', 0, NULL, NULL, NULL, '2015-05-20 01:39:54', '2015-05-20 01:39:54', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (12, 'WEMPE', NULL, NULL, NULL, '', 1, 'wempe', 0, NULL, NULL, NULL, '2015-05-20 01:40:12', '2015-05-20 01:40:12', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (13, 'BOUDI', NULL, NULL, NULL, '', 1, 'boudi', 0, NULL, NULL, NULL, '2015-05-20 01:40:14', '2015-05-20 01:40:14', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (14, 'COACH', NULL, NULL, NULL, '', 1, 'coach', 0, NULL, NULL, NULL, '2015-05-20 01:40:28', '2015-05-20 01:40:28', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (15, 'LORIBLU', NULL, NULL, NULL, '', 1, 'loriblu', 0, NULL, NULL, NULL, '2015-05-20 01:40:30', '2015-05-20 01:40:30', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (16, 'MEPHISTO', NULL, NULL, NULL, '', 1, 'mephisto', 0, NULL, NULL, NULL, '2015-05-20 01:40:51', '2015-05-20 01:40:51', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (17, 'SMYTHSON OF BOND STREET', NULL, NULL, NULL, '', 1, 'smythson-of-bond-street', 0, NULL, NULL, NULL, '2015-05-20 01:41:00', '2015-05-20 01:41:00', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (18, 'BONHAMS', NULL, NULL, NULL, '', 1, 'bonhams', 0, NULL, NULL, NULL, '2015-05-20 01:41:21', '2015-05-20 01:41:21', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (19, 'RM WILLIAMS', NULL, NULL, NULL, '', 1, 'rm-williams', 0, NULL, NULL, NULL, '2015-05-20 01:41:41', '2015-05-20 01:41:41', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (20, 'ERMENEGILDO ZEGNA', NULL, NULL, NULL, '', 1, 'ermenegildo-zegna', 0, NULL, NULL, NULL, '2015-05-20 01:41:48', '2015-05-20 01:41:48', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}},"2":{"groupName":"Regent Street \\u6444\\u653f\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (21, 'CHRISTIES', NULL, NULL, NULL, '', 1, 'christies', 0, NULL, NULL, NULL, '2015-05-20 01:41:59', '2015-05-20 01:41:59', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (22, 'DELVAUX', NULL, NULL, NULL, '', 1, 'delvaux', 0, NULL, NULL, NULL, '2015-05-20 01:42:14', '2015-05-20 01:42:14', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (23, 'CONTINI', NULL, NULL, NULL, '', 1, 'contini', 0, NULL, NULL, NULL, '2015-05-20 01:42:43', '2015-05-20 01:42:43', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (24, 'SOTHEBY''S', NULL, NULL, NULL, '', 1, 'sotheby-s', 0, NULL, NULL, NULL, '2015-05-20 01:43:02', '2015-05-20 01:43:02', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (25, 'KRONOMETRY', NULL, NULL, NULL, '', 1, 'kronometry', 0, NULL, NULL, NULL, '2015-05-20 01:43:08', '2015-05-20 01:43:08', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (26, 'HIGH', NULL, NULL, NULL, '', 1, 'high', 0, NULL, NULL, NULL, '2015-05-20 01:43:47', '2015-05-20 01:43:47', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (27, 'HIGH', NULL, NULL, NULL, '', 1, 'high', 0, NULL, NULL, NULL, '2015-05-20 01:43:51', '2015-05-20 01:43:51', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (28, 'FROST', NULL, NULL, NULL, '', 1, 'frost', 0, NULL, NULL, NULL, '2015-05-20 01:44:11', '2015-05-20 01:44:11', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (29, 'RUSSELL & BROMLEY', NULL, NULL, NULL, '', 1, 'russell-bromley', 0, NULL, NULL, NULL, '2015-05-20 01:44:42', '2015-05-20 01:44:42', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (30, 'RICHARD GREEN', NULL, NULL, NULL, '', 1, 'richard-green', 0, NULL, NULL, NULL, '2015-05-20 01:44:53', '2015-05-20 01:44:53', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (31, 'VICTORIA''S SECRET', NULL, NULL, NULL, '', 1, 'victoria-s-secret', 0, NULL, NULL, NULL, '2015-05-20 01:45:02', '2015-05-20 01:45:02', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (32, 'HUBLOT', NULL, NULL, NULL, '', 1, 'hublot', 0, NULL, NULL, NULL, '2015-05-20 01:45:18', '2015-05-20 01:45:18', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (33, 'ANYA HINDMARCH', NULL, NULL, NULL, '', 1, 'anya-hindmarch', 0, NULL, NULL, NULL, '2015-05-20 01:45:21', '2015-05-20 01:45:21', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (34, 'MONTBLANC', NULL, NULL, NULL, '', 1, 'montblanc', 0, NULL, NULL, NULL, '2015-05-20 01:45:44', '2015-05-20 01:45:44', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (35, 'ANNE FONTAINE 安妮芳汀', NULL, NULL, NULL, '000000', 1, 'anne-fontaine', 0, NULL, NULL, NULL, '2015-05-25 10:02:54', '2015-05-20 01:45:56', 'FFF', 'http://www.annefontaine.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"30 New Bond Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1S 2RN<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 4082280<br>\\r\\n<br>\\r\\n14 Sloane Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aSW1X 9NB<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 8389210<br>\\r\\n<br>\\r\\nBicester Village, 50 Pingle Drive, Bicester<br>\\r\\n\\u90ae\\u7f16\\uff1aOX26 6WD<br>\\r\\n\\u7535\\u8bdd\\uff1a01869 246758\\r\\n\\r\\n\\r\\n","hours":"","phone":"","directions":{"driving":"","ptrans":""}},"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (36, 'HUGO BOSS', NULL, NULL, NULL, '', 1, 'hugo-boss', 0, NULL, NULL, NULL, '2015-05-20 01:45:59', '2015-05-20 01:45:59', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (37, 'CAR SHOE', NULL, NULL, NULL, '', 1, 'car-shoe', 0, NULL, NULL, NULL, '2015-05-20 01:46:16', '2015-05-20 01:46:16', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (38, 'Z ZEGNA', NULL, NULL, NULL, '', 1, 'z-zegna', 0, NULL, NULL, NULL, '2015-05-20 01:46:32', '2015-05-20 01:46:32', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (39, 'HALCYON GALLERY', NULL, NULL, NULL, '', 1, 'halcyon-gallery', 0, NULL, NULL, NULL, '2015-05-20 01:46:34', '2015-05-20 01:46:34', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (40, 'PAL ZILERI', NULL, NULL, NULL, '', 1, 'pal-zileri', 0, NULL, NULL, NULL, '2015-05-20 01:46:53', '2015-05-20 01:46:53', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (41, 'LONGCHAMP', NULL, NULL, NULL, '', 1, 'longchamp', 0, NULL, NULL, NULL, '2015-05-20 01:47:02', '2015-05-20 01:47:02', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (42, 'CANALI', NULL, NULL, NULL, '', 1, 'canali', 0, NULL, NULL, NULL, '2015-05-20 01:47:13', '2015-05-20 01:47:13', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (43, 'JIMMY CHOO', NULL, NULL, NULL, '', 1, 'jimmy-choo', 0, NULL, NULL, NULL, '2015-05-20 01:47:24', '2015-05-20 01:47:24', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (44, 'HSBC', NULL, NULL, NULL, '', 1, 'hsbc', 0, NULL, NULL, NULL, '2015-05-20 01:47:27', '2015-05-20 01:47:27', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (45, 'NEXT', NULL, NULL, NULL, '', 1, 'next', 0, NULL, NULL, NULL, '2015-05-20 01:47:51', '2015-05-20 01:47:51', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (46, 'LUCIE CAMPBELL', NULL, NULL, NULL, '', 1, 'lucie-campbell', 0, NULL, NULL, NULL, '2015-05-20 01:47:52', '2015-05-20 01:47:52', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (47, 'HERMES', NULL, NULL, NULL, '', 1, 'hermes', 0, NULL, NULL, NULL, '2015-05-20 01:48:08', '2015-05-20 01:48:08', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (48, 'RUSSELL & BROMLEY', NULL, NULL, NULL, '', 1, 'russell-bromley', 0, NULL, NULL, NULL, '2015-05-20 01:48:22', '2015-05-20 01:48:22', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (49, 'BYFORD 佰富', NULL, NULL, NULL, '', 1, 'byford', 0, NULL, NULL, NULL, '2015-05-25 09:39:07', '2015-05-20 01:48:23', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}},"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (50, 'BATEEL', NULL, NULL, NULL, '', 1, 'bateel', 0, NULL, NULL, NULL, '2015-05-20 01:48:37', '2015-05-20 01:48:37', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (51, 'VINE VERA', NULL, NULL, NULL, '', 1, 'vine-vera', 0, NULL, NULL, NULL, '2015-05-20 01:48:57', '2015-05-20 01:48:57', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (52, 'BURBERRY', NULL, NULL, NULL, '', 1, 'burberry', 0, NULL, NULL, NULL, '2015-05-20 01:49:04', '2015-05-20 01:49:04', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (53, 'WATCHES OF BOND STREET', NULL, NULL, NULL, '', 1, 'watches-of-bond-street', 0, NULL, NULL, NULL, '2015-05-20 01:49:22', '2015-05-20 01:49:22', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (54, 'LOUIS VUITTON', NULL, NULL, NULL, '', 1, 'louis-vuitton', 0, NULL, NULL, NULL, '2015-05-20 01:49:24', '2015-05-20 01:49:24', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (55, 'BASLER CLOTHING', NULL, NULL, NULL, '', 1, 'basler-clothing', 0, NULL, NULL, NULL, '2015-05-20 01:49:40', '2015-05-20 01:49:40', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (56, 'WATCHES OF SWITZERLAND', NULL, NULL, NULL, '', 1, 'watches-of-switzerland', 0, NULL, NULL, NULL, '2015-05-20 01:49:54', '2015-05-20 01:49:54', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (57, 'SARAH PACINI', NULL, NULL, NULL, '', 1, 'sarah-pacini', 0, NULL, NULL, NULL, '2015-05-20 01:49:57', '2015-05-20 01:49:57', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (58, 'PATEK PHILLPPE', NULL, NULL, NULL, '', 1, 'patek-phillppe', 0, NULL, NULL, NULL, '2015-05-20 01:50:12', '2015-05-20 01:50:12', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (59, 'LUNGTA DE FANCY', NULL, NULL, NULL, '', 1, 'lungta-de-fancy', 0, NULL, NULL, NULL, '2015-05-20 01:50:15', '2015-05-20 01:50:15', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (60, 'GENEU', NULL, NULL, NULL, '', 1, 'geneu', 0, NULL, NULL, NULL, '2015-05-20 01:50:34', '2015-05-20 01:50:34', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (61, 'GEORG JENSEN', NULL, NULL, NULL, '', 1, 'georg-jensen', 0, NULL, NULL, NULL, '2015-05-20 01:50:37', '2015-05-20 01:50:37', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (62, 'FENWICK', NULL, NULL, NULL, '', 1, 'fenwick', 0, NULL, NULL, NULL, '2015-05-20 01:51:00', '2015-05-20 01:51:00', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (63, 'DE GRISOGONO', NULL, NULL, NULL, '', 1, 'de-grisogono', 0, NULL, NULL, NULL, '2015-05-20 01:51:03', '2015-05-20 01:51:03', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (64, 'DOLCE & GABBANA', NULL, NULL, NULL, '', 1, 'dolce-gabbana', 0, NULL, NULL, NULL, '2015-05-20 01:51:17', '2015-05-20 01:51:17', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (65, 'ADLER', NULL, NULL, NULL, '', 1, 'adler', 0, NULL, NULL, NULL, '2015-05-20 01:51:20', '2015-05-20 01:51:20', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (66, 'EMPORIO ARMANI', NULL, NULL, NULL, '', 1, 'emporio-armani', 0, NULL, NULL, NULL, '2015-05-20 01:51:37', '2015-05-20 01:51:37', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (67, 'CHOPARD', NULL, NULL, NULL, '', 1, 'chopard', 0, NULL, NULL, NULL, '2015-05-20 01:51:43', '2015-05-20 01:51:43', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (68, 'MULBERRY', NULL, NULL, NULL, '', 1, 'mulberry', 0, NULL, NULL, NULL, '2015-05-20 01:51:56', '2015-05-20 01:51:56', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (69, 'BLANCPAIN', NULL, NULL, NULL, '', 1, 'blancpain', 0, NULL, NULL, NULL, '2015-05-20 01:52:14', '2015-05-20 01:52:14', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (70, 'BREGUET', NULL, NULL, NULL, '', 1, 'breguet', 0, NULL, NULL, NULL, '2015-05-20 01:52:37', '2015-05-20 01:52:37', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (71, 'F.PINET', NULL, NULL, NULL, '', 1, 'f-pinet', 0, NULL, NULL, NULL, '2015-05-20 01:52:44', '2015-05-20 01:52:44', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (72, 'VAN CLEEF & ARPELS', NULL, NULL, NULL, '', 1, 'van-cleef-arpels', 0, NULL, NULL, NULL, '2015-05-20 01:53:13', '2015-05-20 01:53:13', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (73, 'GRAFF', NULL, NULL, NULL, '', 1, 'graff', 0, NULL, NULL, NULL, '2015-05-20 01:53:39', '2015-05-20 01:53:39', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (74, 'RALPH LAUREN', NULL, NULL, NULL, '', 1, 'ralph-lauren', 0, NULL, NULL, NULL, '2015-05-20 01:54:22', '2015-05-20 01:54:22', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (75, 'BREITLING', NULL, NULL, NULL, '', 1, 'breitling', 0, NULL, NULL, NULL, '2015-05-20 01:54:46', '2015-05-20 01:54:46', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (76, 'CORNELIANI', NULL, NULL, NULL, '', 1, 'corneliani', 0, NULL, NULL, NULL, '2015-05-20 01:55:06', '2015-05-20 01:55:06', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (77, 'CHURCH''S', NULL, NULL, NULL, '', 1, 'church-s', 0, NULL, NULL, NULL, '2015-05-20 01:55:23', '2015-05-20 01:55:23', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (78, 'OPERA GALLERY', NULL, NULL, NULL, '', 1, 'opera-gallery', 0, NULL, NULL, NULL, '2015-05-20 01:55:45', '2015-05-20 01:55:45', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (79, 'MIKIMOTO', NULL, NULL, NULL, '', 1, 'mikimoto', 0, NULL, NULL, NULL, '2015-05-20 01:56:02', '2015-05-20 01:56:02', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (80, 'HSBC', NULL, NULL, NULL, '', 1, 'hsbc', 0, NULL, NULL, NULL, '2015-05-20 01:56:07', '2015-05-20 01:56:07', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (81, 'BELSTAFF', NULL, NULL, NULL, '', 1, 'belstaff', 0, NULL, NULL, NULL, '2015-05-20 01:56:28', '2015-05-20 01:56:28', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (82, 'DAVID MORRIS INTERNATIONAL', NULL, NULL, NULL, '', 1, 'david-morris-international', 0, NULL, NULL, NULL, '2015-05-20 01:56:34', '2015-05-20 01:56:34', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (83, 'IWC', NULL, NULL, NULL, '', 1, 'iwc', 0, NULL, NULL, NULL, '2015-05-20 01:56:43', '2015-05-20 01:56:43', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (84, 'TIFFANY & CO.', NULL, NULL, NULL, '', 1, 'tiffany-co', 0, NULL, NULL, NULL, '2015-05-20 01:57:00', '2015-05-20 01:57:00', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (85, 'S.J. PHILLIPS', NULL, NULL, NULL, '', 1, 's-j-phillips', 0, NULL, NULL, NULL, '2015-05-20 01:57:06', '2015-05-20 01:57:06', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (86, 'CHANEL', NULL, NULL, NULL, '', 1, 'chanel', 0, NULL, NULL, NULL, '2015-05-20 01:57:26', '2015-05-20 01:57:26', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (87, 'ZILLI', NULL, NULL, NULL, '', 1, 'zilli', 0, NULL, NULL, NULL, '2015-05-20 01:57:28', '2015-05-20 01:57:28', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (88, 'DKNY', NULL, NULL, NULL, '', 1, 'dkny', 0, NULL, NULL, NULL, '2015-05-20 01:57:40', '2015-05-20 01:57:40', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (89, 'CAMPER', NULL, NULL, NULL, '', 1, 'camper', 0, NULL, NULL, NULL, '2015-05-20 01:58:00', '2015-05-20 01:58:00', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (90, 'THE FINE ART SOCIETY', NULL, NULL, NULL, '', 1, 'the-fine-art-society', 0, NULL, NULL, NULL, '2015-05-20 01:58:16', '2015-05-20 01:58:16', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (91, 'CHARBONNEL ET WALKER', NULL, NULL, NULL, '', 1, 'charbonnel-et-walker', 0, NULL, NULL, NULL, '2015-05-20 01:58:25', '2015-05-20 01:58:25', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (92, 'ROLEX SHOWROOM', NULL, NULL, NULL, '', 1, 'rolex-showroom', 0, NULL, NULL, NULL, '2015-05-20 01:58:42', '2015-05-20 01:58:42', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (93, 'LEVIEV', NULL, NULL, NULL, '', 1, 'leviev', 0, NULL, NULL, NULL, '2015-05-20 01:58:57', '2015-05-20 01:58:57', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (94, 'SAINT LAURENT', NULL, NULL, NULL, '', 1, 'saint-laurent', 0, NULL, NULL, NULL, '2015-05-20 01:59:23', '2015-05-20 01:59:23', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (95, 'FENDI', NULL, NULL, NULL, '', 1, 'fendi', 0, NULL, NULL, NULL, '2015-05-20 01:59:26', '2015-05-20 01:59:26', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (96, 'GUCCI', NULL, NULL, NULL, '', 1, 'gucci', 0, NULL, NULL, NULL, '2015-05-20 01:59:37', '2015-05-20 01:59:37', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (97, 'MIU MIU', NULL, NULL, NULL, '', 1, 'miu-miu', 0, NULL, NULL, NULL, '2015-05-20 01:59:45', '2015-05-20 01:59:45', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (98, 'VACHERON CONSTANTIN', NULL, NULL, NULL, '', 1, 'vacheron-constantin', 0, NULL, NULL, NULL, '2015-05-20 01:59:55', '2015-05-20 01:59:55', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (99, 'TORY BURCH', NULL, NULL, NULL, '', 1, 'tory-burch', 0, NULL, NULL, NULL, '2015-05-20 02:00:11', '2015-05-20 02:00:11', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (100, 'VERTU', NULL, NULL, NULL, '', 1, 'vertu', 0, NULL, NULL, NULL, '2015-05-20 02:00:15', '2015-05-20 02:00:15', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (101, 'TORY BURCH', NULL, NULL, NULL, '', 1, 'tory-burch', 0, NULL, NULL, NULL, '2015-05-20 02:00:17', '2015-05-20 02:00:17', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (102, 'CARTIER', NULL, NULL, NULL, '', 1, 'cartier', 0, NULL, NULL, NULL, '2015-05-20 02:00:35', '2015-05-20 02:00:35', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (103, 'HERMES', NULL, NULL, NULL, '', 1, 'hermes', 0, NULL, NULL, NULL, '2015-05-20 02:00:38', '2015-05-20 02:00:38', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (104, 'ETRO', NULL, NULL, NULL, '', 1, 'etro', 0, NULL, NULL, NULL, '2015-05-20 02:00:52', '2015-05-20 02:00:52', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (105, 'LORO PIANA', NULL, NULL, NULL, '', 1, 'loro-piana', 0, NULL, NULL, NULL, '2015-05-20 02:01:08', '2015-05-20 02:01:08', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (106, 'JEWELLERY THEATRE', NULL, NULL, NULL, '', 1, 'jewellery-theatre', 0, NULL, NULL, NULL, '2015-05-20 02:01:09', '2015-05-20 02:01:09', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (107, 'BOGH-ART', NULL, NULL, NULL, '', 1, 'bogh-art', 0, NULL, NULL, NULL, '2015-05-20 02:01:23', '2015-05-20 02:01:23', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (108, 'CHANEL', NULL, NULL, NULL, '', 1, 'chanel', 0, NULL, NULL, NULL, '2015-05-20 02:01:26', '2015-05-20 02:01:26', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (109, 'DE BEERS', NULL, NULL, NULL, '', 1, 'de-beers', 0, NULL, NULL, NULL, '2015-05-20 02:01:41', '2015-05-20 02:01:41', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (110, 'DIOR', NULL, NULL, NULL, '', 1, 'dior', 0, NULL, NULL, NULL, '2015-05-20 02:01:46', '2015-05-20 02:01:46', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (111, 'RALPH LAUREN', NULL, NULL, NULL, '', 1, 'ralph-lauren', 0, NULL, NULL, NULL, '2015-05-20 02:01:56', '2015-05-20 02:01:56', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (112, 'SALVATORE FERRAGAMO', NULL, NULL, NULL, '', 1, 'salvatore-ferragamo', 0, NULL, NULL, NULL, '2015-05-20 02:02:15', '2015-05-20 02:02:15', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (113, 'CHURCH''S LADIES COLLECTION', NULL, NULL, NULL, '', 1, 'church-s-ladies-collection', 0, NULL, NULL, NULL, '2015-05-20 02:02:16', '2015-05-20 02:02:16', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (114, 'JOSEPH', NULL, NULL, NULL, '', 1, 'joseph', 0, NULL, NULL, NULL, '2015-05-20 02:02:30', '2015-05-20 02:02:30', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (115, 'BOUCHERON', NULL, NULL, NULL, '', 1, 'boucheron', 0, NULL, NULL, NULL, '2015-05-20 02:02:37', '2015-05-20 02:02:37', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (116, 'CHATLIA', NULL, NULL, NULL, '', 1, 'chatlia', 0, NULL, NULL, NULL, '2015-05-20 02:02:47', '2015-05-20 02:02:47', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (117, 'ASPREY', NULL, NULL, NULL, '', 1, 'asprey', 0, NULL, NULL, NULL, '2015-05-20 02:02:58', '2015-05-20 02:02:58', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (118, 'MAX MARA', NULL, NULL, NULL, '', 1, 'max-mara', 0, NULL, NULL, NULL, '2015-05-20 02:03:03', '2015-05-20 02:03:03', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (119, 'BOTTEGA VENETA', NULL, NULL, NULL, '', 1, 'bottega-veneta', 0, NULL, NULL, NULL, '2015-05-20 02:03:22', '2015-05-20 02:03:22', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (120, 'JAEGER LE COULTRE', NULL, NULL, NULL, '', 1, 'jaeger-le-coultre', 0, NULL, NULL, NULL, '2015-05-20 02:03:42', '2015-05-20 02:03:42', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (121, 'OMEGA BOUTIQUE', NULL, NULL, NULL, '', 1, 'omega-boutique', 0, NULL, NULL, NULL, '2015-05-20 02:03:58', '2015-05-20 02:03:58', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (122, 'DAMIANI', NULL, NULL, NULL, '', 1, 'damiani', 0, NULL, NULL, NULL, '2015-05-20 02:04:11', '2015-05-20 02:04:11', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (123, 'DAKS', NULL, NULL, NULL, '', 1, 'daks', 0, NULL, NULL, NULL, '2015-05-20 02:04:26', '2015-05-20 02:04:26', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (124, 'BVLGARI', NULL, NULL, NULL, '', 1, 'bvlgari', 0, NULL, NULL, NULL, '2015-05-20 02:04:32', '2015-05-20 02:04:32', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (125, 'LA PERLA', NULL, NULL, NULL, '', 1, 'la-perla', 0, NULL, NULL, NULL, '2015-05-20 02:04:43', '2015-05-20 02:04:43', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (126, 'PIAGET', NULL, NULL, NULL, '', 1, 'piaget', 0, NULL, NULL, NULL, '2015-05-20 02:04:56', '2015-05-20 02:04:56', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (127, 'D&G', NULL, NULL, NULL, '', 1, 'd-g', 0, NULL, NULL, NULL, '2015-05-20 02:05:01', '2015-05-20 02:05:01', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (128, 'MARCUS', NULL, NULL, NULL, '', 1, 'marcus', 0, NULL, NULL, NULL, '2015-05-20 02:05:13', '2015-05-20 02:05:13', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (129, 'ALEXANDER MCQUEEN', NULL, NULL, NULL, '', 1, 'alexander-mcqueen', 0, NULL, NULL, NULL, '2015-05-20 02:05:21', '2015-05-20 02:05:21', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (130, 'TOD''S', NULL, NULL, NULL, '', 1, 'tod-s', 0, NULL, NULL, NULL, '2015-05-20 02:05:38', '2015-05-20 02:05:38', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (131, 'HARRY WINSTON', NULL, NULL, NULL, '', 1, 'harry-winston', 0, NULL, NULL, NULL, '2015-05-20 02:05:39', '2015-05-20 02:05:39', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (132, 'MAPPIN & WEBB', NULL, NULL, NULL, '', 1, 'mappin-webb', 0, NULL, NULL, NULL, '2015-05-20 02:05:55', '2015-05-20 02:05:55', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (133, 'MOUSSAIEFF', NULL, NULL, NULL, '', 1, 'moussaieff', 0, NULL, NULL, NULL, '2015-05-20 02:06:09', '2015-05-20 02:06:09', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (134, 'CHANEL FINE JEWELLERY', NULL, NULL, NULL, '', 1, 'chanel-fine-jewellery', 0, NULL, NULL, NULL, '2015-05-20 02:06:38', '2015-05-20 02:06:38', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (135, 'CHAUMET', NULL, NULL, NULL, '', 1, 'chaumet', 0, NULL, NULL, NULL, '2015-05-20 02:07:05', '2015-05-20 02:07:05', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (136, 'CARTIER', NULL, NULL, NULL, '', 1, 'cartier', 0, NULL, NULL, NULL, '2015-05-20 02:07:32', '2015-05-20 02:07:32', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (137, 'BOOOLES', NULL, NULL, NULL, '', 1, 'boooles', 0, NULL, NULL, NULL, '2015-05-20 02:07:56', '2015-05-20 02:07:56', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (138, 'Aspinal of London', NULL, NULL, NULL, '', 1, 'aspinal-of-london', 0, NULL, NULL, NULL, '2015-05-20 02:51:43', '2015-05-20 02:51:43', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (139, 'THE WHISKY EXCHANGE 威士忌商铺', NULL, NULL, NULL, '', 1, 'the-whisky-exchange', 0, NULL, NULL, NULL, '2015-05-25 09:10:41', '2015-05-21 08:58:29', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"Vinopolis,1 Bank End, London<br>\\r\\n\\u90ae\\u7f16\\uff1aSE1 9BU\\r\\n\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u56db 10am -7pm,\\u5468\\u4e9411am - 9pm, \\u5468\\u516d 10.30am - 8pm, \\u5468\\u65e512pm - 6pm","phone":"\\u7535\\u8bdd\\uff1a0207 4038688","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (140, 'DAVINA 戴维娜', NULL, NULL, NULL, '', 1, 'davina', 0, NULL, NULL, NULL, '2015-05-25 09:10:05', '2015-05-21 09:00:04', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"208 Edgware Road, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW2 1DH\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d10am - 7pm, \\u5468\\u65e5 11am - 5pm","phone":"\\u7535\\u8bdd\\uff1a0207 7247595","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (141, 'RACHEL COUTURE 蕾切尔', NULL, NULL, NULL, '', 1, 'rachel-couture', 0, NULL, NULL, NULL, '2015-05-25 09:09:41', '2015-05-21 09:04:40', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"65 Knightsbridge, London<br>\\r\\n\\u90ae\\u7f16\\uff1aSW1X 7RA\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d 10am - 6pm,  \\u5468\\u65e5\\u4e0d\\u8425\\u4e1a","phone":"\\u7535\\u8bdd\\uff1a0207 2353979","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (142, 'SOMETHING ELSE SHOES 它鞋', NULL, NULL, NULL, '', 1, 'something-else-shoes', 0, NULL, NULL, NULL, '2015-05-25 09:09:15', '2015-05-21 09:06:02', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"34-36 Wentworth Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aE1 7TF\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u4e94 9.30 - 6pm, \\u5468\\u516d10am - 4pm,\\u5468\\u65e59.30am - 4pm","phone":"\\u7535\\u8bdd\\uff1a0207 2473299","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (143, 'AIDA 阿依达', NULL, NULL, NULL, '', 1, 'aida', 0, NULL, NULL, NULL, '2015-05-25 09:08:44', '2015-05-21 09:07:09', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"133 Shoreditch High Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aE1 6JE\\r\\n","hours":" \\u5468\\u4e00\\u81f3\\u5468\\u516d10am - 7.30pm, \\u5468\\u65e512pm - 6pm","phone":"\\u7535\\u8bdd\\uff1a0207 7392811","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (144, 'HAMILTONS 汉密尔顿', NULL, NULL, NULL, '', 1, 'hamiltons', 0, NULL, NULL, NULL, '2015-05-25 09:08:13', '2015-05-21 09:08:04', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"102 Hatton Garden, London<br>\\r\\n\\u90ae\\u7f16\\uff1aEC1N 8LY\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d 10.30am - 5.30pm, \\u5468\\u65e511am - 4pm","phone":"\\u7535\\u8bdd\\uff1a0207 4302243","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (145, 'CELEB BOUTIQUE 名人精品', NULL, NULL, NULL, '', 1, 'celeb-boutique', 0, NULL, NULL, NULL, '2015-05-25 09:07:46', '2015-05-21 09:09:01', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"Westfield Shopping Centre, London<br>\\r\\n\\u90ae\\u7f16\\uff1aE20 1EN\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u4e94 10am - 9pm, \\u5468\\u516d 9am - 9pm, \\u5468\\u65e5 12pm - 6pm","phone":"\\u7535\\u8bdd\\uff1a0208 5343899","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (146, 'THE FINE GIFT COMPANY 高级礼物商店', NULL, NULL, NULL, '', 1, 'the-fine-gift-company', 0, NULL, NULL, NULL, '2015-05-25 09:07:06', '2015-05-21 09:10:13', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"3 Gees Court, St Christopher''s Place, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1U 1JB\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d 10am - 7pm, \\u5468\\u65e5 12pm - 6pm","phone":"\\u7535\\u8bdd\\uff1a0207 4093117","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (147, 'D HEDRAL 海德洛', NULL, NULL, NULL, '', 1, 'd-hedral', 0, NULL, NULL, NULL, '2015-05-25 09:06:31', '2015-05-21 09:11:38', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"38 Neal Street, Covent Garden, London<br>\\r\\n\\u90ae\\u7f16\\uff1aWC2H 9PS\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u4e8c 11am - 6pm, \\u5468\\u4e09\\u81f3\\u5468\\u516d 11am - 7pm, \\u5468\\u65e5 12pm - 6pm","phone":"\\u7535\\u8bdd\\uff1a0207 8369427","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (148, 'SOLITAIRE JEWELLERS 纸牌珠宝', NULL, NULL, NULL, '', 1, 'solitaire-jewellers', 0, NULL, NULL, NULL, '2015-05-25 09:05:57', '2015-05-21 09:12:30', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"8 Hatton Garden, London<br>\\r\\n\\u90ae\\u7f16\\uff1aEC1N 8AH\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d 10.30am - 5.30pm, \\u5468\\u65e5 11am - 4pm","phone":"\\u7535\\u8bdd\\uff1a0207 4057447","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (149, 'BEVERLEY HILLS JEWELLERS 比弗利山庄珠宝', NULL, NULL, NULL, '', 1, 'beverley-hills-jewellers', 0, NULL, NULL, NULL, '2015-05-25 09:03:01', '2015-05-21 09:13:19', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"108 Hatton Garden, London<br>\\r\\n\\u90ae\\u7f16\\uff1aEC1N 8LY\\r\\n","hours":"\\u5468\\u65e5\\u81f3\\u5468\\u516d10.30am - 5.30pm, \\u5468\\u65e511am - 4pm","phone":"\\u7535\\u8bdd\\uff1a0207 4054847","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (150, 'PAIN DE SUCRE 甜面包', NULL, NULL, NULL, '', 1, 'pain-de-sucre', 0, NULL, NULL, NULL, '2015-05-25 09:00:56', '2015-05-21 09:14:04', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"192 Kings Road, London<br>\\r\\n\\u90ae\\u7f16\\uff1aSW3 5XP\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d10am - 7pm,\\u5468\\u65e5 12pm - 6pm","phone":"\\u7535\\u8bdd\\uff1a0207 3490068","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (151, 'LARIZIA 拉丽莎', NULL, NULL, NULL, '', 1, 'larizia', 0, NULL, NULL, NULL, '2015-05-25 08:59:24', '2015-05-21 09:15:25', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"74 St Johns Wood High Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aNW8 7SH<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 7225999<br>\\r\\n<br>\\r\\n13 St Johns Wood High Street,London<br>\\r\\n\\u90ae\\u7f16\\uff1aNW8 7SH<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 5861110<br>\\r\\n<br>\\r\\n26 Temple Fortune Parade, London<br>\\r\\n\\u90ae\\u7f16\\uff1aNW11 0QS<br>\\r\\n\\u7535\\u8bdd\\uff1a0208 2090420\\r\\n\\r\\n","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (154, 'ALBAM CLOTHING 艾尔班', NULL, NULL, NULL, '', 1, 'albam-clothing', 0, NULL, NULL, NULL, '2015-05-25 08:58:28', '2015-05-21 09:17:28', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"23 Beak Street, Soho, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1F 9RS<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 3541424<br>\\r\\n<br>\\r\\n111a Commercial Street, Shoreditch, London<br>\\r\\n\\u90ae\\u7f16\\uff1aE1 6BG<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 2476254<br>\\r\\n<br>\\r\\n286 Upper Street, Islington, London<br>\\r\\n\\u90ae\\u7f16\\uff1aN1 2TZ<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 3541424","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (156, 'PAULE KA', NULL, NULL, NULL, 'FFF', 1, 'paule-ka', 0, NULL, NULL, NULL, '2015-05-22 03:26:52', '2015-05-21 09:18:25', '000000', 'http://www.pauleka.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"19 Mount Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1K 2RN<br>\\r\\n0207 4931812<br>\\r\\n<br>\\r\\n13A Grafton Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1S 4ES<br>\\r\\n0207 6474455<br>\\r\\n<br>\\r\\n161 Solane street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aSW1X 9BT<br>\\r\\n0207 8234180\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (158, 'FLY LONDON 飞虫', NULL, NULL, NULL, '', 1, 'fly-london', 0, NULL, NULL, NULL, '2015-05-25 09:17:54', '2015-05-21 09:19:22', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"37 Earlham Street , Covent Garden, London<br>\\r\\n\\u90ae\\u7f16\\uff1aWC2H 9LD<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 2404236<br>\\r\\n<br>\\r\\n5 St Christopher''s Place, Oxford Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1U 1NA<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 4863351<br>\\r\\n","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (160, 'SHOON 苏恩', NULL, NULL, NULL, '', 1, 'shoon', 0, NULL, NULL, NULL, '2015-05-25 09:24:15', '2015-05-21 09:20:39', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"11 Upper Borough Walls, Bath<br>\\r\\n\\u90ae\\u7f16\\uff1aBA1 1RG<br>\\r\\n\\u7535\\u54c8\\uff1a01225 469735<br>\\r\\n<br>\\r\\n29 New Canal, Salisbury<br>\\r\\n\\u90ae\\u7f16\\uff1aSP1 2AA<br>\\r\\n\\u7535\\u54c8\\uff1a07122 330932<br>\\r\\n<br>\\r\\n23-24 High Street, Winchester<br>\\r\\n\\u90ae\\u7f16\\uff1aSO23 9JX<br>\\r\\n\\u7535\\u54c8\\uff1a01962 820178\\r\\n\\r\\n","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (162, 'PAULE KA ', NULL, NULL, NULL, '000000', 1, 'paule-ka', 0, NULL, NULL, NULL, '2015-05-21 09:22:44', '2015-05-21 09:22:44', 'FFF', 'http://www.pauleka.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"13A Grafton Street, London  \\u90ae\\u7f16\\uff1aW1S 4ES\\r\\n","hours":"","phone":"0207 6474455","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (163, 'PAULE KA', NULL, NULL, NULL, '000000', 1, 'paule-ka', 0, NULL, NULL, NULL, '2015-05-21 09:27:05', '2015-05-21 09:27:05', 'FFF', 'http://www.pauleka.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"161 Sloane Street, London  \\u90ae\\u7f16\\uff1aSW1X 9BT\\r\\n","hours":"","phone":"0207 8234180","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (164, 'ROLEX BY THE WATCH GALLERY ', NULL, NULL, NULL, '000000', 1, 'rolex-by-the-watch-gallery', 0, NULL, NULL, NULL, '2015-05-21 09:43:18', '2015-05-21 09:43:18', 'FFF', 'http://www.thewatchgallery.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"One Hyde Park, 100 Knightsbridge, London\\t \\u90ae\\u7f16\\uff1aSW1X 7LJ\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d: 10am - 8pm \\u5468\\u65e5: 12pm - 6pm","phone":"0207 2920345","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (165, 'Dover Street Market', NULL, NULL, NULL, '000000', 1, 'dover-street-market', 0, NULL, NULL, NULL, '2015-05-22 01:32:51', '2015-05-22 01:32:51', 'FFF', 'http://www.doverstreetmarket.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"17-18 Dover Street, London  \\u90ae\\u7f16\\uff1aW1S 4LT\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u4e09\\uff1a11am - 6.30pm,  \\u5468\\u56db\\u81f3\\u5468\\u516d\\uff1a11am - 7pm,  \\u5468\\u65e5\\uff1a12pm - 5pm","phone":"0207 5180680","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (166, 'ERMANNO SCERVINO', NULL, NULL, NULL, '000000', 1, 'ermanno-scervino', 0, NULL, NULL, NULL, '2015-05-22 01:35:48', '2015-05-22 01:35:48', 'FFF', 'http://www.ermannoscervino.it/en', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"198-199 Sloane Street, London  \\u90ae\\u7f16\\uff1aSW1X 9QX\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a10am - 6pm,  \\u5468\\u4e09\\uff1a10am - 7pm,  \\u5468\\u65e5\\uff1a12pm - 6pm","phone":"0207 2350558","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (167, 'BOGHART', NULL, NULL, NULL, '', 1, 'boghart', 0, NULL, NULL, NULL, '2015-05-25 09:31:31', '2015-05-22 01:36:17', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"45a Old Bond Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1S 4QT\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d 10am - 6pm, \\u5468\\u65e5\\u4e0d\\u8425\\u4e1a","phone":"\\u7535\\u8bdd\\uff1a0207 4950885","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (168, 'CROCKETT AND JONES  克洛科琼斯手工鞋', NULL, NULL, NULL, '000000', 3, 'crockett-and-jones', 0, NULL, NULL, NULL, '2015-05-25 09:16:43', '2015-05-22 01:37:27', 'FFF', 'http://www.crockettandjones.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"92 Jermyn Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aSW1Y 6JE<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 8395239<br>\\r\\n<br>\\r\\n69 Jermyn Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aSW1Y 6PF<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 9762684<br>\\r\\n<br>\\r\\n20-21 Burlington Arcade, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1J 0PP<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 4996676<br>\\r\\n<br>\\r\\n155 Brompton Road, London<br>\\r\\n\\u90ae\\u7f16\\uff1aSW3 1QP<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 5812694<br>\\r\\n<br>\\r\\n25 Royal Exchange, City of London, London<br>\\r\\n\\u90ae\\u7f16\\uff1aEC3V 3LP<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 9292111<br>\\r\\n<br>\\r\\nCabot Place, Canary Wharf, London<br>\\t\\r\\n\\u90ae\\u7f16\\uff1aE14 4QS<br>\\r\\n\\u7535\\u8bdd\\uff1a020 7715 9210<br>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (169, 'BYFORD 佰富', NULL, NULL, NULL, '', 1, 'byford-1', 0, NULL, NULL, NULL, '2015-05-25 09:40:37', '2015-05-22 01:37:38', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"77 New Bond Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1a W1S 1RY\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d 10am - 6pm, \\u5468\\u65e5\\u4e0d\\u8425\\u4e1a","phone":"\\u7535\\u8bdd\\uff1a0207 4934158","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (174, 'FEDELI 菲德利', NULL, NULL, NULL, '', 1, 'fedeli', 0, NULL, NULL, NULL, '2015-05-25 09:47:13', '2015-05-22 01:43:24', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"108 Draycott Avenue, London<br>\\r\\n\\u90ae\\u7f16\\uff1aSW3 3AE\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d10am - 6pm, \\u5468\\u65e512pm - 6pm","phone":"\\u7535\\u8bdd\\uff1a0207 5844430","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (178, 'RUBINACCI 鲁比纳奇', NULL, NULL, NULL, '', 1, 'rubinacci', 0, NULL, NULL, NULL, '2015-05-25 09:49:15', '2015-05-22 01:48:52', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"96 Mount Street, Mayfair London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1K 2TB\\r\\n\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d10am - 6.30pm, \\u5468\\u65e5\\u4e0d\\u8425\\u4e1a","phone":"\\u7535\\u8bdd\\uff1a0207 4992299","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (180, 'INDUSTRIE 工业', NULL, NULL, NULL, '', 1, 'industrie', 0, NULL, NULL, NULL, '2015-05-25 09:51:26', '2015-05-22 01:49:56', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"19 Earlham Street, Covent Garden, London<br>\\r\\n\\u90ae\\u7f16\\uff1aWC2H 9LL\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d 11am - 7pm, \\u5468\\u65e5 12pm - 6pm","phone":"\\u7535\\u8bdd\\uff1a0207 2409240","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (181, 'FAT FACE 菲特菲斯', NULL, NULL, NULL, '', 1, 'fat-face', 0, NULL, NULL, NULL, '2015-05-25 09:59:24', '2015-05-22 01:50:46', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"Unit 11-13 Thomas Neal''s, Shorts Gardens, Covent Garden, London<br>\\r\\n\\u90ae\\u7f16\\uff1aWC2H 9LD<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 4976464<br>\\r\\n<br>\\r\\n126 Kings Road,Chelsea,London<br>\\r\\n\\u90ae\\u7f16\\uff1aSW3 4TR<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 5819380<br>\\r\\n<br>\\r\\nUnit 2111,Westfield Stratford City,London<br>\\r\\n\\u90ae\\u7f16\\uff1aW12 7GV<br>\\r\\n0208 7496707<br>\\r\\n<br>\\r\\n144-145 The Arcade, Ground Floor Level, Stratford City\\tLondon<br>\\r\\n\\u90ae\\u7f16\\uff1aE20 1EL<br>\\r\\n\\u7535\\u8bdd\\uff1a0208 5553604<br>\\r\\n<br>\\r\\nUnit 7, St Pancras Station, London<br>\\r\\n\\u90ae\\u7f16\\uff1aNW1 4QL<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 8334587<br>","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (182, 'CHOPARD 萧邦珠宝', NULL, NULL, NULL, '000000', 1, 'chopard-1', 0, NULL, NULL, NULL, '2015-05-25 10:06:00', '2015-05-22 01:51:35', 'FFF', 'http://www.chopard.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"12 New Bond Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1S 3SS\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a10am - 6pm,  \\u5468\\u65e5\\u4e0d\\u8425\\u4e1a","phone":"\\u7535\\u8bdd\\uff1a0207 4093140","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (186, 'FRED PERRY 弗莱德·派瑞', NULL, NULL, NULL, '000000', 1, 'fred-perry', 0, NULL, NULL, NULL, '2015-05-25 10:18:32', '2015-05-22 01:53:29', 'FFF', 'http://www.fredperry.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"14 The Piazza, Covent Garden, London<br>\\r\\n\\u90ae\\u7f16\\uff1aWC2E 8HD<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 8363327<br>\\r\\n<br>\\r\\n12 Newburgh Street, Carnaby Street, London<br>\\t\\u90ae\\u7f16\\uff1aW1F 7RP<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 7344494<br>\\r\\n<br>\\r\\nUnit 6 & 7 Thomas Neal''s, Seven Dials, Covent Garden, London<br>\\r\\n\\u90ae\\u7f16\\uff1aWC2H 9LD<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 8364513<br>\\r\\n<br>\\r\\nWestfield Shopping Centre, Level 40, East Central, Unit 1138, Ariel Way, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW12 7GD<br>\\r\\n\\u7535\\u8bdd\\uff1a0208 7438764<br>\\r\\n<br>\\r\\nWestfield Strafford City, Ground Floory, Unit 1081, London<br>\\r\\n\\u90ae\\u7f16\\uff1aE20 1EN<br>\\r\\n\\u7535\\u8bdd\\uff1a0208 5030379<br>\\r\\n<br>\\r\\n11 Police Street, Manchester<br>\\r\\n\\u90ae\\u7f16\\uff1aM2 7LQ<br>\\r\\n\\u7535\\u8bdd\\uff1a0161 8329874<br>\\r\\n<br>\\r\\nUpper Thames Walk, Bluewater<br>\\r\\n\\u90ae\\u7f16\\uff1aDA9 9AQ<br>\\r\\n\\u7535\\u8bdd\\uff1a01322 423980<br>\\r\\n<br>\\r\\n2 Dukes'' Lane, Brighton<br>\\r\\n\\u90ae\\u7f16\\uff1aBN1 1BG<br>\\r\\n\\u7535\\u8bdd\\uff1a01273 776023<br>\\r\\n<br>\\r\\n10 Quakers Friars, Unit SU8, Cabot Circus\\tBristol<br>\\r\\n\\u90ae\\u7f16\\uff1aBS1 3BU<br>\\r\\n\\u7535\\u8bdd\\uff1a0117 9277353<br>\\r\\n<br>\\r\\nUnit 13, Level 3, 22 Buchanan Galleries, Glasgow<br>\\r\\n\\u90ae\\u7f16\\uff1aG1 2SF<br>\\r\\n\\u7535\\u8bdd\\uff1a0141 3331851<br>\\r\\n<br>\\r\\nUnit 21, Liverpool One, 6 Manestys Lane, \\tLiverpool<br>\\r\\n\\u90ae\\u7f16\\uff1aL1 3DL<br>\\r\\n\\u7535\\u8bdd\\uff1a0151 7099591<br>\\r\\n<br>\\r\\n48-50 Bridlesmith Gate, Nottingham<br>\\r\\n\\u90ae\\u7f16\\uff1aNG1 2GP<br>\\r\\n\\u7535\\u8bdd\\uff1a0115 9506967<br>\\r\\n<br>\\r\\nUnit 76 McArthur Glen Designer Outlet, Ashford<br>\\r\\n\\u90ae\\u7f16\\uff1aTN24 0SE<br>\\r\\n\\u7535\\u8bdd\\uff1a01233 632727<br>\\r\\n<br>\\r\\nUnit 24, Cheshire Oaks Outlet Village, Kinsey Road, Ellesmere Port<br>\\r\\n\\u90ae\\u7f16\\uff1aCH65 9JJ<br>\\r\\n\\u7535\\u8bdd\\uff1a0151 3571383<br>\\r\\n<br>\\r\\nUnit 101, McArthur Glen Designer Outlet, Almondvale, Livingston<br>\\r\\n\\u90ae\\u7f16\\uff1aEH54 6QX<br>\\r\\n\\u7535\\u8bdd\\uff1a01506 418559<br>\\r\\n<br>\\r\\nUnit 70, Bicester Village, Bicester<br>\\r\\n\\u90ae\\u7f16\\uff1aOX26 6WD<br>\\r\\n\\u7535\\u8bdd\\uff1a01869 325504<br>\\r\\n<br>\\r\\nUnit 137a, York Desinger Outlet Village\\tSt. Nicholas Avenue, Fulford, York<br>\\r\\n\\u90ae\\u7f16\\uff1aYO19 4TA<br>\\r\\n\\u7535\\u8bdd\\uff1a01904 637090\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (188, 'H.STERN H.斯特恩', NULL, NULL, NULL, '', 1, 'h-stern', 0, NULL, NULL, NULL, '2015-05-25 10:02:45', '2015-05-22 01:54:49', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"6C Sloane Street\\tKnightsbridge, London<br>\\r\\n\\u90ae\\u7f16\\uff1aSW1X 9LE\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d10am - 6pm, \\u5468\\u65e5\\u4e0d\\u8425\\u4e1a","phone":"\\u7535\\u8bdd\\uff1a0203 1410233","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (190, 'COCCINELLE 可奇奈尔', NULL, NULL, NULL, '', 1, 'coccinelle', 0, NULL, NULL, NULL, '2015-05-25 10:30:53', '2015-05-22 01:56:01', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"Unit 85a, 50 Pingle Drive, Bicester Village, Bicester<br>\\r\\n\\u90ae\\u7f16\\uff1aOX26 6WD<br>\\r\\n\\u7535\\u8bdd\\uff1a01869 243695<br>\\r\\n<br>\\r\\n185 Brompton Road, London<br>\\r\\n\\u90ae\\u7f16\\uff1aSW3 1NE<br>\\t\\r\\n\\u7535\\u8bdd\\uff1a020 7581 5160<br>","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d 10am - 8pm, \\u5468\\u65e5 10pm - 7pm","phone":"\\u7535\\u8bdd\\uff1a01869 243695","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (191, 'FROST OF LONDON伦敦之霜', NULL, NULL, NULL, '', 1, 'frost-of-london', 0, NULL, NULL, NULL, '2015-05-25 10:06:17', '2015-05-22 01:57:03', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"108 New Bond Street\\t, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1S 1EF\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d10am - 6.30pm, \\u5468\\u65e5 12pm - 5pm","phone":"\\u7535\\u8bdd\\uff1a0203 3720108","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (192, 'Sample 例子', NULL, NULL, NULL, '', 3, 'sample1', 0, NULL, NULL, NULL, '2015-05-25 05:02:16', '2015-05-25 05:02:16', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', 'null', '/l/o/logo-sample.png', NULL, '/s/h/shoe-sample.jpg');
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (193, 'Sample 2號例子', NULL, NULL, NULL, '', 3, 'sample-2', 0, NULL, NULL, NULL, '2015-05-25 05:11:19', '2015-05-25 05:11:09', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', 'null', '/l/o/logo-sample2.png', NULL, '/b/o/bottle-sample.jpg');
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (194, 'Sample 3號例子', NULL, NULL, NULL, '', 3, 'sample-3', 0, NULL, NULL, NULL, '2015-05-25 05:15:17', '2015-05-25 05:15:17', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', 'null', '/l/o/logo-sample3.png', NULL, '/d/r/dress-sample.jpg');
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (195, 'ETON SHIRTS 伊顿', NULL, NULL, NULL, '', 1, 'eton-shirts', 0, NULL, NULL, NULL, '2015-05-25 10:09:04', '2015-05-25 10:09:04', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"53 South Molton Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1K 5SF<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 5699075<br>\\r\\n<br>\\r\\n65 Kingsway, London<br>\\r\\n\\u90ae\\u7f16\\uff1aSW14 7HN<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 4301433<br>\\r\\n","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (196, 'THE WATCH GALLERY 手表长廊', NULL, NULL, NULL, '', 1, 'the-watch-gallery', 0, NULL, NULL, NULL, '2015-05-25 10:11:40', '2015-05-25 10:11:40', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"The Village, Westfield, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW12 7GD\\t<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 2921245<br>\\r\\n<br>\\r\\n129 Fulham Road. London<br>\\r\\n\\u90ae\\u7f16\\uff1aSW3 6RT<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 9522731<br>\\r\\n","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (197, 'KATE SPADE 凯特·丝蓓', NULL, NULL, NULL, '', 1, 'kate-spade', 0, NULL, NULL, NULL, '2015-05-25 10:18:58', '2015-05-25 10:18:58', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"1-4 Langley Court, London<br>\\r\\n\\u90ae\\u7f16\\uff1aWC2E 9JY<br>\\t\\r\\n\\u7535\\u8bdd\\uff1a0207 8363988<br>\\r\\n<br>\\r\\n2 Symons Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aSW3 2TJ\\t<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 2590645<br>\\r\\n<br>\\r\\nAriel Way, Westfield, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW12 7DS<br>\\r\\n\\u7535\\u8bdd\\uff1a0208 7421728<br>\\r\\n<br>\\r\\nUnit 74, 50 Pingle Drive, Bicester Village, Bicester<br>\\r\\n\\u90ae\\u7f16\\uff1aOX26 6WD<br>\\t\\r\\n\\u7535\\u8bdd\\uff1a01869 241727<br>\\r\\n","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (198, 'N PEAL N.皮尔', NULL, NULL, NULL, '', 1, 'n-peal-n', 0, NULL, NULL, NULL, '2015-05-25 10:23:14', '2015-05-25 10:23:14', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"217 Piccadilly, St. James, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1J 9HW\\r\\n","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (199, 'BOSE 博士音响', NULL, NULL, NULL, '000000', 1, 'bose', 0, NULL, NULL, NULL, '2015-05-25 10:40:13', '2015-05-25 10:24:58', 'FFF', 'http://www.bose.co.uk', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"185 Regent Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1B 4JP<br>\\r\\n\\u7535\\u8bdd\\uff1a0208 0256888<br>\\r\\n<br>\\r\\nUnit 2020, Westfield Shopping Centre, Ariel Way, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW12 7GF<br>\\r\\n\\u7535\\u8bdd\\uff1a0208 2491130<br>\\r\\n<br>\\r\\nUnit 40, Bicester Village, 50 Pingle Drive, Bicester<br>\\r\\n\\u90ae\\u7f16\\uff1aOX26 6WD<br>\\r\\n\\u7535\\u8bdd\\uff1a0870 7414499<br>\\r\\n<br>\\r\\nWestfield Strafford City, 207 First Floor, The Gallery, Warton Road, London<br>\\r\\n\\u90ae\\u7f16\\uff1aE15 2EE<br>\\r\\n\\u7535\\u8bdd\\uff1a0208 5227150\\r\\n","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (200, 'SMYTHSON 斯迈森', NULL, NULL, NULL, '', 1, 'smythson', 0, NULL, NULL, NULL, '2015-05-25 10:27:01', '2015-05-25 10:27:01', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"40 New Bond Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1S 2DE<br>\\t\\r\\n\\u7535\\u8bdd\\uff1a0207 629 8558<br>\\r\\n<br>\\r\\n141-142 Sloane Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aSW1X 9AY\\t<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 730 5520<br>\\r\\n<br>\\r\\n214 Westbourne Grove, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW11 2RH<br>\\t\\r\\n\\u7535\\u8bdd\\uff1a0207 243 3527<br>\\r\\n<br>\\r\\nUnit 128B, 50 Pingle Drive, Bicester Village, Bicester\\t<br>\\r\\n\\u90ae\\u7f16\\uff1aOX26 6WD<br>\\t\\r\\n\\u7535\\u8bdd\\uff1a01869 242 660<br>\\r\\n<br>\\r\\n7 Royal Exchange, London<br>\\r\\n\\u90ae\\u7f16\\uff1aEC3V 3LL<br>\\t\\r\\n\\u7535\\u8bdd\\uff1a0207 621 1037<br>\\r\\n","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (201, 'JOHN VARVARTOS 约翰·瓦维托斯', NULL, NULL, NULL, '', 1, 'john-varvartos', 0, NULL, NULL, NULL, '2015-05-25 10:29:05', '2015-05-25 10:29:05', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"13 Conduit Street\\t, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1S 2XQ\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 3743 2230","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (202, 'GINA  吉娜', NULL, NULL, NULL, '', 1, 'gina', 0, NULL, NULL, NULL, '2015-05-25 10:33:23', '2015-05-25 10:33:23', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"189 Sloane Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aSW1X 9QR<br>\\t\\r\\n\\u7535\\u8bdd\\uff1a020 7235 2932<br>\\r\\n<br>\\r\\n119 Mount Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1K 3NL\\t<br>\\r\\n\\u7535\\u8bdd\\uff1a44 (0)20 7499 7539 <br>\\r\\n<br>\\r\\n50 Pingle Drive, Bicester<br>\\r\\n\\u90ae\\u7f16\\uff1aOX26 6WD<br>\\t\\r\\n\\u7535\\u8bdd\\uff1a01869 354 752<br>","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (203, 'OSCAR DEL A RENTA 奥斯卡·德拉伦塔', NULL, NULL, NULL, '', 1, 'oscar-del-a-renta', 0, NULL, NULL, NULL, '2015-05-25 10:35:36', '2015-05-25 10:35:36', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"50 Pingle Drive, Bicester<br>\\r\\n\\u90ae\\u7f16\\uff1aOX26 6WD<br>\\t\\r\\n\\u7535\\u8bdd\\uff1a01869 241 777<br>\\r\\n<br>\\r\\n130 Mount Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1K 3NY<br>\\t\\r\\n\\u7535\\u8bdd\\uff1a020 7493 0422<br>\\r\\n","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (204, 'J BARBOUR J.巴伯衫', NULL, NULL, NULL, '', 1, 'j-barbour-j', 0, NULL, NULL, NULL, '2015-05-25 10:38:20', '2015-05-25 10:38:20', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"50 Pingle Drive, Bicester<br>\\r\\n\\u90ae\\u7f16\\uff1aOX26 6WD\\t\\r\\n\\u7535\\u8bdd\\uff1a01869 366266\\r\\n\\r\\n29 Foubert''s Place London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1F 7QF\\t\\r\\n\\u7535\\u8bdd\\uff1a020 7434 3209\\r\\n\\r\\n134 Long Acre , London<br>\\r\\n\\u90ae\\u7f16\\uff1aWC2E 9AA\\t\\r\\n\\u7535\\u8bdd\\uff1a020 7240 5061\\r\\n","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (205, 'PANDORA 潘朵拉珠宝', NULL, NULL, NULL, '000000', 1, 'pandora', 0, NULL, NULL, NULL, '2015-05-25 10:39:01', '2015-05-25 10:39:01', 'FFF', 'http://www.pandora.net', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"Unit 1162, Ariel Way, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW12 7GD<br>\\r\\n\\u7535\\u8bdd\\uff1a0208 7434996<br>\\r\\n<br>\\r\\nUnit SU 1042, The Arcade\\tWestfield Stratford City, London<br>\\r\\n\\u90ae\\u7f16\\uff1aE20 1EL<br>\\r\\n\\u7535\\u8bdd\\uff1a0208 5346495<br>\\r\\n<br>\\r\\n257-259 Oxford Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1C 2DD<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 4092007<br>\\r\\n<br>\\r\\n23 Long Acre, Covent Garden, London<br>\\r\\n\\u90ae\\u7f16\\uff1aWC2E 9LZ<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 2405068<br>\\r\\n<br>\\r\\nCabot Place, 255\\tCanary Wharf, London<br>\\r\\n\\u90ae\\u7f16\\uff1aE14 4QT<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 9879801\\r\\n","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (206, 'ASPINAL OF LONDON 阿斯皮纳尔', NULL, NULL, NULL, '', 1, 'aspinal-of-london', 0, NULL, NULL, NULL, '2015-05-25 10:42:52', '2015-05-25 10:42:52', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"Westfield Shopping Centre, Ariel Way, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW12 7GD<br>\\t\\r\\n\\u7535\\u8bdd\\uff1a020 8743 0503<br>\\r\\n<br>\\r\\n46 Marylebone High Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1U 5HQ\\t<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 224 0413 <br>\\r\\n<br>\\r\\n25 Brook Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1K 4HB<br>\\t\\r\\n0207 493 9509 <br>\\r\\n<br>\\r\\n15 Cabot Place, London<br>\\r\\n\\u90ae\\u7f16\\uff1aE14 4QT<br>\\t\\r\\n\\u7535\\u8bdd\\uff1a0207 719 0727 <br>\\r\\n<br>\\r\\n34 Long Acre, London<br>\\r\\n\\u90ae\\u7f16\\uff1aWC2E 9LA\\t<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 836 2655 <br>\\r\\n<br>\\r\\nBluewater Unit L099, Lower Guild Hall Kent<br>\\r\\n\\u90ae\\u7f16\\uff1aDA9 9ST<br>\\t\\r\\n\\u7535\\u8bdd\\uff1a01322 387657 <br>","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (207, 'HEDONISM WINES 享乐主义酒肆', NULL, NULL, NULL, '000000', 1, 'hedonism-wines', 0, NULL, NULL, NULL, '2015-05-25 10:43:39', '2015-05-25 10:43:39', 'FFF', 'http://www.hedonism.co.uk', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"3-7 Davies Street\\t, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1K 3LD<br>\\r\\n\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a10am - 9pm, \\u5468\\u65e5\\uff1a12pm - 6pm","phone":"\\u7535\\u8bdd\\uff1a0207 2907870","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (208, 'CAMBRIDGE SATCHEL 剑桥包', NULL, NULL, NULL, '', 1, 'cambridge-satchel', 0, NULL, NULL, NULL, '2015-05-25 10:44:34', '2015-05-25 10:44:34', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"31 James Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aWC2E 8PA\\t\\r\\n\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 3077 1100","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (209, 'HARRYS OF LONDON 哈里斯伦敦', NULL, NULL, NULL, '000000', 1, 'harrys-of-london', 0, NULL, NULL, NULL, '2015-05-25 10:47:27', '2015-05-25 10:47:27', 'FFF', 'hhtp://www.harrysoflondon.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"59 South Audley Street, Mayfair, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1K 2QN<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 4097988<br>\\r\\n<br>\\r\\n68-69 Burlington Arcade, Mayfair, London<br>\\t\\u90ae\\u7f16\\uff1aW1U 0QU<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 4999320<br>\\r\\n<br>\\r\\n18 Royal Exchange, City of London, London<br>\\t\\u90ae\\u7f16\\uff1aEC3V 3LP<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 2834643\\r\\n","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (210, 'BERRYS 柏瑞思', NULL, NULL, NULL, '', 1, 'berrys', 0, NULL, NULL, NULL, '2015-05-25 10:49:04', '2015-05-25 10:49:04', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"62 Albion Street, Leeds<br>\\r\\n\\u90ae\\u7f16\\uff1aLS1 6AD<br>\\t\\r\\n\\u7535\\u8bdd\\uff1a0113 2454797<br>\\r\\n<br>\\r\\n19-21 Queen Victoria Street, Victoria Quarter, Leeds<br>\\r\\n\\u90ae\\u7f16\\uff1aLS1 6BD<br>\\t\\r\\n\\u7535\\u8bdd\\uff1a0113 2456100<br>\\r\\n<br>\\r\\n44 Commercial Street, Leeds<br>\\r\\n\\u90ae\\u7f16\\uff1aLS1 6EX<br>\\t\\r\\n\\u7535\\u8bdd\\uff1a0113 201 0500<br>\\r\\n<br>\\r\\n4 Queen Victoria Square, Hull<br>\\r\\n\\u90ae\\u7f16\\uff1aHU1 3RA \\t<br>\\r\\n\\u7535\\u8bdd\\uff1a01482 327505<br>\\r\\n<br>\\r\\n52 Stonegate, York<br>\\r\\n\\u90ae\\u7f16\\uff1aYO1 8AS<br>\\t\\r\\n01904 654104<br>\\r\\n<br>\\r\\n12 The Poultry, Nottingham<br>\\r\\n\\u90ae\\u7f16\\uff1aNG1 2HW\\t<br>\\r\\n\\u7535\\u8bdd\\uff1a0115 941 0666<br>\\r\\n<br>\\r\\n122 Grey Street, Newcastle<br>\\r\\n\\u90ae\\u7f16\\uff1aNE1 6JG\\t\\r\\n\\u7535\\u8bdd\\uff1a0191 221 0152\\r\\n<br>\\r\\n41 Peascod Street, Windsor<br>\\r\\n\\u90ae\\u7f16\\uff1aSL4 1EA<br>\\r\\n\\u7535\\u8bdd\\uff1a01753 840 930<br>\\r\\n","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (211, 'JADE JEWELLERS 玉珠宝', NULL, NULL, NULL, '000000', 1, 'jade-jewellers', 0, NULL, NULL, NULL, '2015-05-25 10:52:00', '2015-05-25 10:50:36', 'FFF', 'http://www.jadejewellers.weebly.com/', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"8 Grenville Street\\t, London<br>\\r\\n\\u90ae\\u7f16\\uff1aEC1N 8SB<br>\\r\\n\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a10.30am - 5.30pm, \\u5468\\u65e5\\uff1a11am - 4pm","phone":"\\u7535\\u8bdd\\uff1a0207 2425684","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (212, 'NICHOLAS KIRKWOOD 尼可拉斯·科克伍德', NULL, NULL, NULL, '', 1, 'nicholas-kirkwood', 0, NULL, NULL, NULL, '2015-05-25 11:10:58', '2015-05-25 10:50:53', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"5 Mount Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1K 3NE\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 7403 6303","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (213, 'GALERIA MELISSA  加莱里亚·梅丽莎', NULL, NULL, NULL, '', 1, 'galeria-melissa', 0, NULL, NULL, NULL, '2015-05-25 11:10:27', '2015-05-25 10:53:11', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"43 King Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aWC2E 8JY\\t\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 3096 1222","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (214, 'CHANNINGS 查宁珠宝', NULL, NULL, NULL, '000000', 1, 'channings-hatton-garden', 0, NULL, NULL, NULL, '2015-05-25 10:55:57', '2015-05-25 10:55:57', 'FFF', 'http://hatton-garden-london.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"96 Hatton Garden, London<br>\\r\\n\\u90ae\\u7f16\\uff1aEC1N 8NX<br>\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a10.30am - 5.30pm, \\u5468\\u65e5\\uff1a11am - 4pm","phone":"\\u7535\\u8bdd\\uff1a0207 4043133","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (215, 'FRENCH SOLE 唯一法兰西', NULL, NULL, NULL, '000000', 1, 'french-sole', 0, NULL, NULL, NULL, '2015-05-25 11:00:50', '2015-05-25 11:00:50', 'FFF', 'http://www.frenchsole.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"26 Brook Street, Mayfair, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1K 5DQ<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 4932678<br>\\r\\n<br>\\r\\n6 Ellis Street, Chelsea, London<br>\\r\\n\\u90ae\\u7f16\\uff1aSW1X 9AL<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 7303771<br>\\r\\n<br>\\r\\n323 Kings Road, London<br>\\r\\n\\u90ae\\u7f16\\uff1aSW3 5EP<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 3511634<br>\\r\\n<br>\\r\\n61 Marylebone Lane, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1U 2PA<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 4860021\\r\\n","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (216, 'BCBG MAX AZRIA 马克思·阿兹里亚', NULL, NULL, NULL, '000000', 1, 'bcbg-max-azria', 0, NULL, NULL, NULL, '2015-05-25 11:05:24', '2015-05-25 11:05:24', 'FFF', 'http://www.bcbg.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"23-25 Kings Road, London<br>\\r\\n\\u90ae\\u7f16\\uff1aSW3 4RP\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a10am - 7pm, \\u5468\\u65e5\\uff1a12pm - 6pm","phone":"\\u7535\\u8bdd\\uff1a0207 8249840","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (217, 'BEAUTY BASE', NULL, NULL, NULL, '000000', 1, 'beauty-base', 0, NULL, NULL, NULL, '2015-05-25 11:09:52', '2015-05-25 11:09:52', 'FFF', 'http://www.beautybase.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"Unit 1100-1101, Westfield Shopping Centre, Ariel Way, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW12 7SL<br>\\r\\n\\u7535\\u8bdd\\uff1a0208 7497448<br>\\r\\n<br>\\r\\nLower Ground Floor, 49 The Arcade, Westfield Stratford City, London<br>\\r\\n\\u90ae\\u7f16\\uff1aE20 1EH<br>\\r\\n\\u7535\\u8bdd\\uff1a0208 5554539\\r\\n","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (218, 'HIGH 嗨-高级定制女装', NULL, NULL, NULL, '000000', 1, 'high', 0, NULL, NULL, NULL, '2015-05-25 11:14:42', '2015-05-25 11:14:42', 'FFF', 'http://www.high-everydaycouture.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"107 New Bond Street, Mayfair, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1S 1ED<br>\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a10am - 7pm, \\u5468\\u65e5\\u4e0d\\u8425\\u4e1a","phone":"\\u7535\\u8bdd\\uff1a0207 4952595","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (219, 'LINDA FARROW 琳达·法罗眼镜', NULL, NULL, NULL, '000000', 1, 'linda-farrow', 0, NULL, NULL, NULL, '2015-05-25 11:17:08', '2015-05-25 11:17:08', 'FFF', 'http://www.lindafarrow.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"91 Mount Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1K 2SU\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a10.30am - 7pm (\\u5468\\u4e94\\uff1a11am - 7.30pm), \\u5468\\u65e5\\uff1a12pm - 5pm","phone":"\\u7535\\u8bdd\\uff1a0207 4996336","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (220, 'ELEVEN PARIS 巴黎十一', NULL, NULL, NULL, '000000', 1, 'eleven-paris', 0, NULL, NULL, NULL, '2015-05-25 11:19:32', '2015-05-25 11:19:32', 'FFF', 'http://www.elevenparis.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"46 Carnaby Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1F 9PS\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a10am - 7pm (\\u5468\\u56db\\uff1a10am - 8pm), \\u5468\\u65e5\\uff1a12pm - 6pm","phone":"\\u7535\\u8bdd\\uff1a0207 4341171","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (221, 'EMMETTE LONDON 艾梅特伦敦', NULL, NULL, NULL, '000000', 1, 'emmett-london', 0, NULL, NULL, NULL, '2015-05-25 11:24:23', '2015-05-25 11:24:23', 'FFF', 'http://www.emmettlondon.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"112 Jermyn Street, St. James, London<br>\\r\\n\\u90ae\\u7f16\\uff1aSW1Y 6LS<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 9251299<br>\\r\\n<br>\\r\\n380 Kings Road, Chelsea,\\tLondon<br>\\r\\n\\u90ae\\u7f16\\uff1aSW3 5UZ<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 3517529<br>\\r\\n<br>\\r\\n4 Eldon Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aEC2M 7LS<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 2471563\\r\\n","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (222, 'THE WATCH HUT 钟表之屋', NULL, NULL, NULL, '000000', 1, 'the-watch-hut', 0, NULL, NULL, NULL, '2015-05-25 11:27:19', '2015-05-25 11:27:19', 'FFF', 'http://www.thewatchhut.co.uk', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"128 Long Acre, Covent Garden, London<br>\\t\\u90ae\\u7f16\\uff1aWC2E 9AA\\t\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u4e09\\uff1a10am -7pm, \\u5468\\u56db\\u81f3\\u5468\\u516d\\uff1a10am - 8pm, \\u5468\\u65e5\\uff1a12pm - 6pm","phone":"\\u7535\\u8bdd\\uff1a0207 2920341","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (223, 'BOSIDENG 波司登', NULL, NULL, NULL, '000000', 1, 'bosideng', 0, NULL, NULL, NULL, '2015-05-25 11:29:29', '2015-05-25 11:29:29', 'FFF', 'http://www.bosidenglondon.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"28 South Molton Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1K 5RF\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a10am - 7pm (\\u5468\\u56db\\uff1a10am - 8pm), \\u5468\\u65e5\\uff1a12pm - 6pm","phone":"\\u7535\\u8bdd\\uff1a0207 2903170","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (224, 'UNITED NUDE 裸', NULL, NULL, NULL, '000000', 1, 'united-nude', 0, NULL, NULL, NULL, '2015-05-25 11:32:21', '2015-05-25 11:32:21', 'FFF', 'http://www.unitednude.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"13 Floral Street, Covent Garden, London<br>\\r\\n\\u90ae\\u7f16\\uff1aWC2E 9DH\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a10am - 7.30pm, \\u5468\\u65e5\\uff1a12pm - 6pm","phone":"\\u7535\\u8bdd\\uff1a0207 2407106","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (225, 'ALEXANDRE DE PARIS 亚历山大·巴黎发饰', NULL, NULL, NULL, '000000', 1, 'alexandre-de-paris', 0, NULL, NULL, NULL, '2015-05-27 09:58:03', '2015-05-25 11:37:41', 'FFF', 'http://www.alexandredeparis-accessories.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"3a Sloane Street, Knightsbridge, London<br>\\t\\u90ae\\u7f16\\uff1aSW1X 9LA\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a10am - 7pm, \\u5468\\u65e5\\uff1a12pm - 6pm","phone":"\\u7535\\u8bdd\\uff1a0207 2354481","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (226, 'NOURBEL & LE CAVELIER 帕拉伊巴碧玺', NULL, NULL, NULL, '000000', 1, 'nourbel-and-le-cavelier', 0, NULL, NULL, NULL, '2015-05-25 11:43:26', '2015-05-25 11:43:26', 'FFF', 'http://www.nourbel-lecavelier.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"23 Burlington Arcade, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1J 0PR\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u4e94\\uff1a10am - 6.30pm, \\u5468\\u516d\\uff1a10.30am - 6pm, \\u5468\\u65e5\\uff1a12pm - 5pm","phone":"\\u7535\\u8bdd\\uff1a0207 4090110","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (227, 'GAZIANO & GIRLIN 戈奇安奴&戈灵', NULL, NULL, NULL, '000000', 1, 'gaziano-and-girling', 0, NULL, NULL, NULL, '2015-05-25 11:56:42', '2015-05-25 11:49:45', 'FFF', 'http://www.gazianogirling.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"39 Savile Row, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1S 3QF\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u4e94\\uff1a10am - 6pm, \\u5468\\u516d\\uff1a9.30am - 6pm, \\u5468\\u65e5\\u4e0d\\u8425\\u4e1a","phone":"\\u7535\\u8bdd\\uff1a0207 4398717","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (228, 'NICK TENTIS 尼克·坦提斯', NULL, NULL, NULL, '000000', 1, 'nick-tentis', 0, NULL, NULL, NULL, '2015-05-25 11:57:18', '2015-05-25 11:52:17', 'FFF', 'http://www.nicktentis.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"37 Savile Row, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1S 3QD\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a10am - 6pm, \\u5468\\u65e5\\u4e0d\\u8425\\u4e1a","phone":"\\u7535\\u8bdd\\uff1a0207 2871966","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (229, 'WEBLEY JEWELLERS 维柏利珠宝', NULL, NULL, NULL, '000000', 1, 'webley-jewellers', 0, NULL, NULL, NULL, '2015-05-25 11:56:06', '2015-05-25 11:56:06', 'FFF', 'http://hatton-garden-london.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"County House, 14-15 Hatton Garden, London<br>\\t\\u90ae\\u7f16\\uff1aEC1N 8AT\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a10.30am - 5.30pm, \\u5468\\u65e5\\uff1a11am - 4pm","phone":"\\u7535\\u8bdd\\uff1a0207 4058107","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (230, 'FINE JEWELS 精致珠宝', NULL, NULL, NULL, '000000', 1, 'fine-jewels', 0, NULL, NULL, NULL, '2015-05-25 12:00:06', '2015-05-25 12:00:06', 'FFF', 'http://hatton-garden-london.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"88-90 Hatton Garden, London<br>\\r\\n\\u90ae\\u7f16\\uff1aEC1N 8PN\\t\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a10.30am - 5.30pm, \\u5468\\u65e5\\uff1a11am - 4pm","phone":"\\u7535\\u8bdd\\uff1a0207 4051117","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (231, 'RUGBY UNION WORLD CUP 世界杯橄榄球', NULL, NULL, NULL, '000000', 1, 'rugby-union-world-cup', 0, NULL, NULL, NULL, '2015-05-25 12:02:34', '2015-05-25 12:02:34', 'FFF', 'http://www.rwcshop.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"No 10 The Market, Covent Garden, London<br>\\r\\n\\u90ae\\u7f16\\uff1aWC2E 8RB\\t\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a10am - 8pm, \\u5468\\u65e5\\uff1a12pm - 6pm","phone":"\\u7535\\u8bdd\\uff1a0207 3790096","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (232, 'TRAFFIC PEOPLE 交通人类', NULL, NULL, NULL, '000000', 1, 'traffic-people', 0, NULL, NULL, NULL, '2015-05-25 12:05:39', '2015-05-25 12:05:39', 'FFF', 'http://www.trafficpeople.co.uk', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"61-63 Brushfield Street, Spitalfields Market, London<br>\\r\\n\\u90ae\\u7f16\\uff1aE1 6AA\\t\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a11am - 6pm","phone":"\\u7535\\u8bdd\\uff1a0207 2479377","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (233, 'TRACEY NEULS 特蕾西·尼尔斯', NULL, NULL, NULL, '000000', 1, 'tracey-neuls', 0, NULL, NULL, NULL, '2015-05-25 12:09:59', '2015-05-25 12:09:59', 'FFF', 'http://www.traceyneuls.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"29 Marylebone Lane, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1U 2NQ<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 9350039<br>\\r\\n<br>\\r\\n73 Redchurch Street, London<br>\\r\\n\\u90ae\\u7f16\\uff1aE2 7DJ<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 0180872\\r\\n","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (234, 'THE NATURAL SHOE STORE 自然之鞋', NULL, NULL, NULL, '000000', 1, 'the-natural-shoe-store', 0, NULL, NULL, NULL, '2015-05-25 12:12:50', '2015-05-25 12:12:50', 'FFF', 'http://www.thenaturalshoestore.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"13 Neal Street, Covent Garden, London<br>\\t\\u90ae\\u7f16\\uff1aWC2H 9PU<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 8365254<br>\\r\\n<br>\\r\\n325 Kings Road, London<br>\\r\\n\\u90ae\\u7f16\\uff1aSW3 5ES<br>\\r\\n\\u7535\\u8bdd\\uff1a0207 3513721\\r\\n","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (235, 'BIRKENSTOCK 勃肯', NULL, NULL, NULL, '000000', 1, 'birkenstock', 0, NULL, NULL, NULL, '2015-05-25 12:15:18', '2015-05-25 12:15:18', 'FFF', 'http://www.birkenstock.co.uk', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"24 Neal Street, Covent Garden, London<br>\\t\\u90ae\\u7f16\\uff1aWC2H 9QW\\t\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a10am - 7pm (\\u5468\\u56db\\uff1a10am - 8pm), \\u5468\\u65e5\\uff1a12pm - 6pm","phone":"\\u7535\\u8bdd\\uff1a0207 8368052","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (236, 'BILL AMBERG 比尔·安伯格', NULL, NULL, NULL, '000000', 1, 'bill-amberg', 0, NULL, NULL, NULL, '2015-05-25 12:17:57', '2015-05-25 12:17:57', 'FFF', 'http://www.billamberg.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"9 Shepherd Market, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1J 7PF\\t\\r\\n","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a10am - 6pm, \\u5468\\u65e5\\u4e0d\\u8425\\u4e1a","phone":"\\u7535\\u8bdd\\uff1a0207 4990962","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (237, 'CAMPO MARZIO 坎波·玛奇奥', NULL, NULL, NULL, '000000', 1, 'campo-marzio', 0, NULL, NULL, NULL, '2015-05-25 12:20:06', '2015-05-25 12:20:06', 'FFF', 'http://www.campomarzio.co.uk', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"166 Piccadilly, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1J 9EF\\t\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a0207 9072600","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (238, 'TGI FRIDAYS 星期五餐厅', NULL, NULL, NULL, '', 1, 'tgi-fridays', 0, NULL, NULL, NULL, '2015-05-26 09:02:09', '2015-05-26 09:02:09', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 1-3 , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0TG\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a0203 031 4901","directions":{"driving":"","ptrans":""}},"2":{"groupName":"Regent Street \\u6444\\u653f\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (239, 'ASICS  亚瑟士', NULL, NULL, NULL, '', 1, 'asics', 0, NULL, NULL, NULL, '2015-05-26 09:03:41', '2015-05-26 09:03:41', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 5B , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0TG\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8795 4904","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (240, 'BENCH 奔趣', NULL, NULL, NULL, '', 1, 'bench', 0, NULL, NULL, NULL, '2015-05-26 09:04:48', '2015-05-26 09:04:48', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 06 , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0TG\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8795 3019","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (241, 'TRESSPASS 特斯帕斯户外运动', NULL, NULL, NULL, '', 1, 'tresspass', 0, NULL, NULL, NULL, '2015-05-26 09:13:36', '2015-05-26 09:13:36', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 7, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0TG\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8795 4849","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (242, 'SKECHERS 斯凯奇鞋履', NULL, NULL, NULL, '', 1, 'skechers', 0, NULL, NULL, NULL, '2015-05-26 09:16:58', '2015-05-26 09:16:58', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 8, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0TG\\r\\n","hours":"","phone":"020 8782 8993","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (243, 'TOG 24 托格24户外运动', NULL, NULL, NULL, '', 1, 'tog-24-24', 0, NULL, NULL, NULL, '2015-05-26 09:18:48', '2015-05-26 09:18:48', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 9, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0TG\\r\\n","hours":"","phone":"01924 236 940","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (244, 'THE WORKS 沃克斯礼品店', NULL, NULL, NULL, '', 1, 'the-works', 0, NULL, NULL, NULL, '2015-05-26 09:22:50', '2015-05-26 09:22:50', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 10, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0TG\\r\\n","hours":"","phone":"020 8900 2216","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (245, 'BAGS ETC 包袋礼品店', NULL, NULL, NULL, '', 1, 'bags-etc', 0, NULL, NULL, NULL, '2015-05-26 09:24:38', '2015-05-26 09:24:38', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 11, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0TG\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8782 1551","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (246, 'CLAIRES 克莱尔礼品店', NULL, NULL, NULL, '', 1, 'claires', 0, NULL, NULL, NULL, '2015-05-26 09:25:41', '2015-05-26 09:25:41', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 12, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0TG\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8903 2308","directions":{"driving":"","ptrans":""}},"2":{"groupName":"Regent Street \\u6444\\u653f\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (247, 'ROCKPORT 美国乐步鞋履', NULL, NULL, NULL, '', 1, 'rockport', 0, NULL, NULL, NULL, '2015-05-26 09:27:05', '2015-05-26 09:27:05', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 13 , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0TG\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8795 3798","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (248, 'NEW BALANCE  新百伦运动', NULL, NULL, NULL, '', 1, 'new-balance', 0, NULL, NULL, NULL, '2015-05-26 09:29:43', '2015-05-26 09:29:43', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 14 , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0TG\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8903 5218","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (249, 'PAVERS 佩夫鞋履', NULL, NULL, NULL, '', 1, 'pavers', 0, NULL, NULL, NULL, '2015-05-26 09:34:03', '2015-05-26 09:34:03', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 15 , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0TG\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8795 1770","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (250, 'THE NORTH FACE 北面户外运动', NULL, NULL, NULL, '', 1, 'the-north-face', 0, NULL, NULL, NULL, '2015-05-26 09:36:00', '2015-05-26 09:36:00', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 16 , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0TG\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8903 7366","directions":{"driving":"","ptrans":""}},"2":{"groupName":"Regent Street \\u6444\\u653f\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (251, 'VODAFONE 沃达丰服务', NULL, NULL, NULL, '', 1, 'vodafone', 0, NULL, NULL, NULL, '2015-05-26 09:38:57', '2015-05-26 09:38:57', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 17, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0TG\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a07826 902 139","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (252, 'NEWSPOINT 新闻服务', NULL, NULL, NULL, '', 1, 'newspoint', 0, NULL, NULL, NULL, '2015-05-26 09:41:03', '2015-05-26 09:41:03', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 18 , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0TG\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 89037994","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (253, 'BHAVI BEAUTY 芭薇美容沙龙', NULL, NULL, NULL, '', 1, 'bhavi-beauty', 0, NULL, NULL, NULL, '2015-05-26 09:43:19', '2015-05-26 09:43:19', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 19, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0TG\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8900 0098","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (254, 'CHABOBA 查波巴餐厅', NULL, NULL, NULL, '', 1, 'chaboba', 0, NULL, NULL, NULL, '2015-05-26 09:45:12', '2015-05-26 09:45:12', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 20, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0TG\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 89020777","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (255, 'SAMSUNG 三星电子', NULL, NULL, NULL, '', 1, 'samsung', 0, NULL, NULL, NULL, '2015-05-26 09:46:59', '2015-05-26 09:46:59', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0TG\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8903 9864","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (256, 'WENZELS 温泽餐厅', NULL, NULL, NULL, '', 1, 'wenzels', 0, NULL, NULL, NULL, '2015-05-26 09:48:28', '2015-05-26 09:48:28', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 24 , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0TG\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8782 8514","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (257, 'SUBWAY 赛百味餐厅', NULL, NULL, NULL, '', 1, 'subway', 0, NULL, NULL, NULL, '2015-05-26 09:49:43', '2015-05-26 09:49:43', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 26, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0TG\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8903 2008","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (258, 'BROWN''S HOTEL 布朗斯酒店', NULL, '<p>布朗斯酒店由11排佐治亚式别墅组成，是一座有着悠久历史及丰富文化底蕴的酒店。下榻布朗斯酒店的客人囊括了总统，首相，皇室成员及著名作家。其中，最负盛名的当属阿加莎<span>&middot;</span>克里斯蒂 -- 她于1954年创作的惊悚故事中所描绘的&ldquo;柏特伦酒店&rdquo;，即由布朗斯酒店中汲取灵感而成。</p>', '/b/r/browns-front-door-day.jpg', '000000', 3, 'browns-hotel', 1, '布朗斯酒店', '布朗斯酒店, 伦敦, 英国, 旅游购物', '布朗斯酒店是伦敦久负盛名的高端酒店之一，坐落于伦敦市中心，交通及购物方便快捷，详情请于IBDShop.com咨询。', '2015-05-26 09:53:02', '2015-05-26 09:49:49', 'FFF', 'https://www.roccofortehotels.com/hotels-and-resorts/browns-hotel/', '{"twitter":"https:\\/\\/twitter.com\\/Browns_Hotel","facebook":"https:\\/\\/www.facebook.com\\/BrownsHotelLondon\\/timeline","instagram":"https:\\/\\/instagram.com\\/Browns_Hotel","weibo":""}', '{"1":{"groupName":"Bond Street \\u90a6\\u5fb7\\u5927\\u8857","address":"33 Albemarle Street, Mayfair, London<br>\\r\\n\\u90ae\\u7f16\\uff1aW1S 4BP","hours":"24\\u5c0f\\u65f6\\u8425\\u4e1a","phone":"020 7493 6020","directions":{"driving":"\\u9644\\u8fd1\\u7684\\u516c\\u5171\\u505c\\u8f66\\u573a\\u8d39\\u7528\\u4e3a50\\u82f1\\u9551\\u4e00\\u5929","ptrans":"\\u5730\\u94c1<br>\\r\\n\\u5e03\\u6717\\u65af\\u9152\\u5e97\\u8ddd\\u79bbGreen Park Station\\u5730\\u94c1\\u7ad9250\\u7c73\\u5de6\\u53f3\\uff0c\\u8fd0\\u8425\\u7684\\u5730\\u94c1\\u7ebf\\u4e3aJubilee Line, Piccadilly Line \\u4ee5\\u53caVictoria Line. \\r\\n\\r\\n\\u5df4\\u58eb<br>\\r\\n\\u9152\\u5e97\\u9644\\u8fd1\\u53ef\\u4e58\\u5750\\u5df4\\u58eb\\u7ebf\\u8def\\u5305\\u62ec\\uff1a9, 14, 19, 22, 38, C2, N9, N19, N22, N38, \\u53caN97,"}}}', '/b/r/browns-hotel-logo.jpg', NULL, '/b/r/browns-new-english-tea-room.jpg');
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (259, 'GUEST SERVICES 客户服务', NULL, NULL, NULL, '', 1, 'guest-services', 0, NULL, NULL, NULL, '2015-05-26 09:50:39', '2015-05-26 09:50:39', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 26A , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0TG\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8912 5210","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (260, 'M & S OUTLET 马莎时装', NULL, NULL, NULL, '', 1, 'm-s-outlet', 0, NULL, NULL, NULL, '2015-05-26 09:51:51', '2015-05-26 09:51:51', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 27, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0TG\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8795 1386","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (261, 'BJORN BORG 比约恩博格时装', NULL, NULL, NULL, '', 1, 'bjorn-borg', 0, NULL, NULL, NULL, '2015-05-26 09:53:58', '2015-05-26 09:53:58', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 36 , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0TG\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 3051 3342","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (262, 'ADIDAS OUTLET 阿迪达斯运动', NULL, NULL, NULL, '', 1, 'adidas-outlet', 0, NULL, NULL, NULL, '2015-05-26 09:54:58', '2015-05-26 09:54:58', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 37-39 , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0TG\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8795 0959","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (263, 'SOLE TRADER 丝特雷德鞋履', NULL, NULL, NULL, '', 1, 'sole-trader', 0, NULL, NULL, NULL, '2015-05-26 09:57:13', '2015-05-26 09:57:13', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 40, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0TG\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8902 2180","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (264, 'HELLY HANSEN 哈雷·汉森户外运动', NULL, NULL, NULL, '', 1, 'helly-hansen', 0, NULL, NULL, NULL, '2015-05-26 09:59:10', '2015-05-26 09:58:29', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 41, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0TG\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8795 3637","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (265, 'NIKE OUTLET 耐克运动', NULL, NULL, NULL, '', 1, 'nike-outlet', 0, NULL, NULL, NULL, '2015-05-26 10:01:25', '2015-05-26 10:01:25', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 42-45 , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0TG\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8903 1696","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (266, 'PRET A MANGER 餐厅', NULL, NULL, NULL, '', 1, 'pret-a-manger', 0, NULL, NULL, NULL, '2015-05-26 10:03:14', '2015-05-26 10:03:14', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 46-48, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"\\u7535\\u8bdd\\uff1a020 8902 1735","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (267, 'COSTA COFFEE 餐厅', NULL, NULL, NULL, '', 1, 'costa-coffee', 0, NULL, NULL, NULL, '2015-05-26 10:05:30', '2015-05-26 10:05:30', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 49 , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8903 9832","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (268, 'SUNGLASSES HUT 太阳眼镜屋', NULL, NULL, NULL, '', 1, 'sunglasses-hut', 0, NULL, NULL, NULL, '2015-05-26 10:07:08', '2015-05-26 10:07:08', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 50A, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8902 1561","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (269, 'SKOPES 时装', NULL, NULL, NULL, '', 1, 'skopes', 0, NULL, NULL, NULL, '2015-05-26 10:08:43', '2015-05-26 10:08:43', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 50, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8902 1327","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (270, 'HOLLAND & BARRETT 荷柏瑞保健品', NULL, NULL, NULL, '', 1, 'holland-barrett', 0, NULL, NULL, NULL, '2015-05-26 10:10:12', '2015-05-26 10:10:12', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 51, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 3774 2686","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (271, 'PRO COOK 厨房用品', NULL, NULL, NULL, '', 1, 'pro-cook', 0, NULL, NULL, NULL, '2015-05-26 10:11:40', '2015-05-26 10:11:40', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 52, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8902 0993","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (272, 'CHRISTY''S TOWEL 佳士得家具用品', NULL, NULL, NULL, '', 1, 'christy-s-towel', 0, NULL, NULL, NULL, '2015-05-26 10:12:46', '2015-05-26 10:12:46', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 53, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8903 1174","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (273, 'THE BODY SHOP 美体小铺化妆品', NULL, NULL, NULL, '', 1, 'the-body-shop', 0, NULL, NULL, NULL, '2015-05-26 10:14:13', '2015-05-26 10:14:13', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 54, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 7902 1048","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (274, 'CHAPELLE 夏佩尔礼品店', NULL, NULL, NULL, '', 1, 'chapelle', 0, NULL, NULL, NULL, '2015-05-26 10:15:33', '2015-05-26 10:15:33', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 57, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8903 7551","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (275, 'THE FRAGRANCE SHOP 香水铺', NULL, NULL, NULL, '', 1, 'the-fragrance-shop', 0, NULL, NULL, NULL, '2015-05-26 10:16:40', '2015-05-26 10:16:40', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 57, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8903 7551","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (276, 'GUESS 盖尔斯时装', NULL, NULL, NULL, '', 1, 'guess', 0, NULL, NULL, NULL, '2015-05-26 10:17:51', '2015-05-26 10:17:51', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 58-59 , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8795 5104 ","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (277, 'AFTERSHOCK 阿芙德萧克时装', NULL, NULL, NULL, '', 1, 'aftershock', 0, NULL, NULL, NULL, '2015-05-26 10:19:31', '2015-05-26 10:19:31', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 60, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8900 8555","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (278, 'LK BENNETT LK.班尼特时装', NULL, NULL, NULL, '', 1, 'lk-bennett-lk', 0, NULL, NULL, NULL, '2015-05-26 10:20:50', '2015-05-26 10:20:50', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 62 , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8795 4876","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (279, 'LUKE 77 卢克77时装', NULL, NULL, NULL, '', 1, 'luke-77-77', 0, NULL, NULL, NULL, '2015-05-26 10:22:07', '2015-05-26 10:21:46', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 63, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8782 2289","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (280, 'WITHOUT PREJUDICE 时装', NULL, NULL, NULL, '', 1, 'without-prejudice', 0, NULL, NULL, NULL, '2015-05-26 10:23:06', '2015-05-26 10:23:06', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 64, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a07951 835 555","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (281, 'PREZZO 餐厅', NULL, NULL, NULL, '', 1, 'prezzo', 0, NULL, NULL, NULL, '2015-05-26 10:24:08', '2015-05-26 10:24:08', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 66 , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8795 3377","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (282, 'CABANA 餐厅', NULL, NULL, NULL, '', 1, 'cabana', 0, NULL, NULL, NULL, '2015-05-26 10:25:25', '2015-05-26 10:25:25', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 67A, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8903 8747","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (283, 'PING PONG 餐厅', NULL, NULL, NULL, '', 1, 'ping-pong', 0, NULL, NULL, NULL, '2015-05-26 10:27:47', '2015-05-26 10:27:47', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 68-70, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 3696 0090","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (284, 'KURT GEIGER 库尔特·盖格鞋履', NULL, NULL, NULL, '', 1, 'kurt-geiger', 0, NULL, NULL, NULL, '2015-05-26 10:28:53', '2015-05-26 10:28:53', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 71, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8795 4091","directions":{"driving":"","ptrans":""}},"2":{"groupName":"Regent Street \\u6444\\u653f\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (285, 'DANIEL FOOTWEAR 丹尼尔鞋履', NULL, NULL, NULL, '', 1, 'daniel-footwear', 0, NULL, NULL, NULL, '2015-05-26 10:29:39', '2015-05-26 10:29:39', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 73, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8795 5219","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (286, 'PHASE 8 费斯8时装', NULL, NULL, NULL, '', 1, 'phase-8-8', 0, NULL, NULL, NULL, '2015-05-26 10:34:10', '2015-05-26 10:34:10', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 74, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8900 2210","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (287, 'REPLAY 时装', NULL, NULL, NULL, '', 1, 'replay', 0, NULL, NULL, NULL, '2015-05-26 10:35:59', '2015-05-26 10:35:59', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 78, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8903 9010","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (288, 'CAFÉ NERO 咖啡店', NULL, NULL, NULL, '', 1, 'cafe-nero', 0, NULL, NULL, NULL, '2015-05-26 10:37:07', '2015-05-26 10:37:07', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 79, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8902 0169","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (289, 'GAP 盖璞时装', NULL, NULL, NULL, '', 1, 'gap', 0, NULL, NULL, NULL, '2015-05-26 10:38:02', '2015-05-26 10:38:02', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 80-82 , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8902 9655","directions":{"driving":"","ptrans":""}},"2":{"groupName":"Regent Street \\u6444\\u653f\\u5927\\u8857","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (290, 'H&M 时装', NULL, NULL, NULL, '', 1, 'h-m', 0, NULL, NULL, NULL, '2015-05-26 10:38:57', '2015-05-26 10:38:57', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 83, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8795 2812","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (291, 'SUPERDRY 时装', NULL, NULL, NULL, '', 1, 'superdry', 0, NULL, NULL, NULL, '2015-05-26 10:39:46', '2015-05-26 10:39:46', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 84-85, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8902 0969","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (292, 'VILLEROY & BOCH 唯宝家居用品', NULL, NULL, NULL, '', 1, 'villeroy-boch', 0, NULL, NULL, NULL, '2015-05-26 10:40:43', '2015-05-26 10:40:43', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 86 , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8900 9013","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (293, 'DENBY 登比家居用品', NULL, NULL, NULL, '', 1, 'denby', 0, NULL, NULL, NULL, '2015-05-26 10:42:03', '2015-05-26 10:42:03', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 88, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8795 4414","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (294, 'HALLMARK 贺曼礼品店', NULL, NULL, NULL, '', 1, 'hallmark', 0, NULL, NULL, NULL, '2015-05-26 10:43:10', '2015-05-26 10:43:10', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 89 , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8902 8860","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (295, 'CLARKS 其乐鞋履', NULL, NULL, NULL, '', 1, 'clarks', 0, NULL, NULL, NULL, '2015-05-26 10:44:10', '2015-05-26 10:44:10', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 90, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8902 8323","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (296, 'LINDT  瑞士莲巧克力', NULL, NULL, NULL, '', 1, 'lindt', 0, NULL, NULL, NULL, '2015-05-26 10:45:07', '2015-05-26 10:45:07', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 91 , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8795 5025","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (297, 'NEXT  时装', NULL, NULL, NULL, '', 1, 'next', 0, NULL, NULL, NULL, '2015-05-26 10:46:06', '2015-05-26 10:46:06', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 92, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8453 5470","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (298, 'JIMMY''S 餐厅', NULL, NULL, NULL, '', 1, 'jimmy-s', 0, NULL, NULL, NULL, '2015-05-26 10:46:48', '2015-05-26 10:46:48', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 95 , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8795 5377","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (299, 'COAST TO COAST 餐厅', NULL, NULL, NULL, '', 1, 'coast-to-coast', 0, NULL, NULL, NULL, '2015-05-26 10:47:21', '2015-05-26 10:47:21', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 96, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8903 6360","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (300, 'LAS IGUANAS 餐厅', NULL, NULL, NULL, '', 1, 'las-iguanas', 0, NULL, NULL, NULL, '2015-05-26 10:47:58', '2015-05-26 10:47:58', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 97 , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8900 9373","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (301, 'PIZZA EXPRESS 餐厅', NULL, NULL, NULL, '', 1, 'pizza-express', 0, NULL, NULL, NULL, '2015-05-26 10:48:31', '2015-05-26 10:48:31', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 99 , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8795 4910","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (302, 'FRANKIE & BENNY''S 餐厅', NULL, NULL, NULL, '', 1, 'frankie-benny-s', 0, NULL, NULL, NULL, '2015-05-26 10:49:16', '2015-05-26 10:49:16', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 100 , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8903 7824","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (303, 'FRANKIE & BENNY''S 餐厅', NULL, NULL, NULL, '', 1, 'frankie-benny-s', 0, NULL, NULL, NULL, '2015-05-26 10:49:19', '2015-05-26 10:49:19', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 100 , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8903 7824","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (304, 'NANDO''S 餐厅', NULL, NULL, NULL, '', 1, 'nando-s', 0, NULL, NULL, NULL, '2015-05-26 10:50:06', '2015-05-26 10:50:06', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 101, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8902 7388","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (305, 'ZIZZI 餐厅', NULL, NULL, NULL, '', 1, 'zizzi', 0, NULL, NULL, NULL, '2015-05-26 10:51:05', '2015-05-26 10:51:05', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 102-103 , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8782 1535","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (306, 'WAGAMAMA''S 餐厅', NULL, NULL, NULL, '', 1, 'wagamama-s', 0, NULL, NULL, NULL, '2015-05-26 10:52:04', '2015-05-26 10:52:04', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 104 , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8795 2597","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (307, 'HANDMADE BURGER CO 餐厅', NULL, NULL, NULL, '', 1, 'handmade-burger-co', 0, NULL, NULL, NULL, '2015-05-26 10:52:53', '2015-05-26 10:52:53', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 105 , London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8902 7857","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (308, 'CINEWORLD 电影院', NULL, NULL, NULL, '', 1, 'cineworld', 0, NULL, NULL, NULL, '2015-05-26 10:53:34', '2015-05-26 10:53:34', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 107, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8900 5261","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (309, 'STARBUCKS 星巴克咖啡', NULL, NULL, NULL, '', 1, 'starbucks', 0, NULL, NULL, NULL, '2015-05-26 10:54:17', '2015-05-26 10:54:17', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"Unit 107, London Designer Outlet, Wembley Park Boulevard, Wembley<br>\\r\\n\\u90ae\\u7f16\\uff1aHA9 0FD\\r\\n","hours":"","phone":"\\u7535\\u8bdd\\uff1a020 8900 5260","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (310, 'GIVENCHY 纪梵希时装', NULL, NULL, NULL, '', 1, 'givenchy', 0, NULL, NULL, NULL, '2015-05-26 11:15:31', '2015-05-26 11:13:18', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"6":{"groupName":"BROWNS \\u5e03\\u6717\\u65af","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (311, 'VALENTINO 华伦天奴时装', NULL, NULL, NULL, '', 1, 'valentino', 0, NULL, NULL, NULL, '2015-05-26 11:15:06', '2015-05-26 11:15:06', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"6":{"groupName":"BROWNS \\u5e03\\u6717\\u65af","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (312, 'AZZEDINE ALAÏA 女装', NULL, NULL, NULL, '', 1, 'azzedine-alaia', 0, NULL, NULL, NULL, '2015-05-26 11:16:14', '2015-05-26 11:16:14', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"6":{"groupName":"BROWNS \\u5e03\\u6717\\u65af","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (313, 'DRIES VAN NOTEN 德赖斯·范诺顿时装', NULL, NULL, NULL, '', 1, 'dries-van-noten', 0, NULL, NULL, NULL, '2015-05-26 11:17:09', '2015-05-26 11:17:09', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"6":{"groupName":"BROWNS \\u5e03\\u6717\\u65af","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (314, 'BALENCIAGA 巴黎世家时装', NULL, NULL, NULL, '', 1, 'balenciaga', 0, NULL, NULL, NULL, '2015-05-26 11:17:38', '2015-05-26 11:17:38', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"6":{"groupName":"BROWNS \\u5e03\\u6717\\u65af","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (315, 'DOLCE & GABBANA 杜嘉班纳时装', NULL, NULL, NULL, '', 1, 'dolce-gabbana', 0, NULL, NULL, NULL, '2015-05-26 11:18:25', '2015-05-26 11:18:25', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"6":{"groupName":"BROWNS \\u5e03\\u6717\\u65af","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (316, 'CHRISTIAN LOUBOUTIN 克里斯提·鲁布托鞋履', NULL, NULL, NULL, '', 1, 'christian-louboutin', 0, NULL, NULL, NULL, '2015-05-26 11:19:25', '2015-05-26 11:19:25', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"6":{"groupName":"BROWNS \\u5e03\\u6717\\u65af","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (317, 'ASHISH 阿施施时装', NULL, NULL, NULL, '', 1, 'ashish', 0, NULL, NULL, NULL, '2015-05-26 11:20:04', '2015-05-26 11:20:04', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"6":{"groupName":"BROWNS \\u5e03\\u6717\\u65af","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (318, 'SOPHIA WEBSTER 索菲娅·韦伯斯特鞋履', NULL, NULL, NULL, '', 1, 'sophia-webster', 0, NULL, NULL, NULL, '2015-05-26 11:24:49', '2015-05-26 11:24:49', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"6":{"groupName":"BROWNS \\u5e03\\u6717\\u65af","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (319, 'LAURENCE DACADE 女鞋', NULL, NULL, NULL, '', 1, 'laurence-dacade', 0, NULL, NULL, NULL, '2015-05-26 11:26:47', '2015-05-26 11:26:47', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"6":{"groupName":"BROWNS \\u5e03\\u6717\\u65af","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (320, 'SAINT LAURENT 圣罗兰时装', NULL, NULL, NULL, '', 1, 'saint-laurent', 0, NULL, NULL, NULL, '2015-05-26 11:28:24', '2015-05-26 11:28:24', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"6":{"groupName":"BROWNS \\u5e03\\u6717\\u65af","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (321, 'LANVIN 浪凡时装', NULL, NULL, NULL, '', 1, 'lanvin', 0, NULL, NULL, NULL, '2015-05-26 11:29:56', '2015-05-26 11:29:56', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"6":{"groupName":"BROWNS \\u5e03\\u6717\\u65af","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (322, 'JAMES LONG 男装', NULL, NULL, NULL, '', 1, 'james-long', 0, NULL, NULL, NULL, '2015-05-26 11:33:10', '2015-05-26 11:33:10', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"6":{"groupName":"BROWNS \\u5e03\\u6717\\u65af","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (323, 'RICK OWENS 瑞克·欧文斯时装', NULL, NULL, NULL, '', 1, 'rick-owens', 0, NULL, NULL, NULL, '2015-05-26 11:33:45', '2015-05-26 11:33:45', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"6":{"groupName":"BROWNS \\u5e03\\u6717\\u65af","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (324, 'COMME DES GARÇONS 时装', NULL, NULL, NULL, '', 1, 'comme-des-garcons', 0, NULL, NULL, NULL, '2015-05-26 11:36:26', '2015-05-26 11:36:26', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"6":{"groupName":"BROWNS \\u5e03\\u6717\\u65af","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (325, 'ANN DEMEULEMEESTER :安-迪穆拉米斯特时装', NULL, NULL, NULL, '', 1, 'ann-demeulemeester', 0, NULL, NULL, NULL, '2015-05-26 11:38:18', '2015-05-26 11:38:18', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"6":{"groupName":"BROWNS \\u5e03\\u6717\\u65af","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (326, 'AGENT PROVOCATEUR 大内密探内衣', NULL, NULL, '/a/g/agent_300_1.png', '000000', 3, 'agent-provocateur', 0, NULL, NULL, NULL, '2015-05-27 09:30:44', '2015-05-27 09:13:44', 'FFF', 'http://www.agentprovocateur.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a10am - 7pm","phone":"\\u7535\\u8bdd\\uff1a+44 (0) 1869 250 762","directions":{"driving":"","ptrans":""}}}', '/a/g/agent_300_2.png', NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (327, 'ALEXANDER MCQUEEN 亚历山大·麦昆时装', NULL, NULL, '/a/l/alexander-mcqueen.png', '000000', 1, 'alexander-mcqueen-1', 0, NULL, NULL, NULL, '2015-05-27 09:57:34', '2015-05-27 09:41:28', 'FFF', 'http://www.alexandermcqueen.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a10am - 7pm","phone":"\\u7535\\u8bdd\\uff1a+44 (0) 1869 243 617","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (328, 'DUNHILL 登喜路时装', NULL, NULL, '/d/u/dunhill-logo-300x200.png', '000000', 1, 'dunhill', 0, NULL, NULL, NULL, '2015-05-27 09:59:01', '2015-05-27 09:45:53', 'FFF', 'http://www.dunhill.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a10am - 7pm","phone":"\\u7535\\u8bdd\\uff1a+44 (0) 1869 354 950","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (329, 'AMORINO 阿摩里诺冰淇淋店', NULL, NULL, '/a/m/amorino_logo_300.png', '000000', 1, 'amorino', 0, NULL, NULL, NULL, '2015-05-27 09:51:55', '2015-05-27 09:51:55', 'FFF', 'http://www.amorino.com/en/', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a10am - 7pm","phone":"\\u7535\\u8bdd\\uff1a+44 (0) 1869 366266","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (330, 'ANYA HINDMARCH 安雅·希德玛芝手袋', NULL, NULL, '/a/n/anya_300.png', '000000', 1, 'anya-hindmarch-1', 0, NULL, NULL, NULL, '2015-05-27 09:55:31', '2015-05-27 09:54:32', 'FFF', 'http://www.anyahindmarch.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a10am - 7pm","phone":"\\u7535\\u8bdd\\uff1a+44 (0) 1869 247 963","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (331, 'ARMANI OUTLET 阿玛尼时装折扣店', NULL, NULL, '/3/0/300x200_armani.png', '000000', 1, 'armani-outlet', 0, NULL, NULL, NULL, '2015-05-27 10:09:18', '2015-05-27 10:09:18', 'FFF', 'http://www.giorgioarmani.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a10am - 7pm","phone":"\\u7535\\u8bdd\\uff1a+44 (0) 1869 328 460","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (332, 'ANNE FONTAINE 安妮芳汀女装', NULL, NULL, '/a/n/annefontaine.png', '000000', 1, 'anne-fontaine', 0, NULL, NULL, NULL, '2015-05-27 10:11:52', '2015-05-27 10:11:52', 'FFF', 'http://www.annefontaine.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a10am - 7pm","phone":"+44 (0) 1869 246 758","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (333, 'ANNOUSHKA 阿诺仕卡珠宝', NULL, NULL, '/a/n/annoushka.png', '000000', 1, 'annoushka', 0, NULL, NULL, NULL, '2015-05-27 10:15:23', '2015-05-27 10:15:23', 'FFF', 'http://www.annoushka-jewellery.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a10am - 7pm","phone":"+44 (0) 1869 249 948","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (334, 'BARBOUR 巴伯尔时装', NULL, NULL, '/3/0/300x200-barbour-logo.png', '000000', 1, 'barbour', 0, NULL, NULL, NULL, '2015-05-27 10:24:08', '2015-05-27 10:24:08', 'FFF', 'http://www.barbour.com/uk', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a10am - 7pm","phone":"\\u7535\\u8bdd\\uff1a+44 (0) 1869 327 232","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (335, 'BELSTAFF 贝达弗男装', NULL, NULL, '/b/e/belstaff-logo.png', '000000', 1, 'belstaff', 0, NULL, NULL, NULL, '2015-05-27 10:28:08', '2015-05-27 10:28:08', 'FFF', 'http://www.belstaff.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a10am - 7pm","phone":"\\u7535\\u8bdd\\uff1a+44 (0) 1869 253 631","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (336, 'HUGO BOSS 雨果博斯时装', NULL, NULL, '/h/u/hugo-boss-logo.png', '000000', 1, 'hugo-boss', 0, NULL, NULL, NULL, '2015-05-27 10:32:29', '2015-05-27 10:32:29', 'FFF', 'http://www.hugoboss.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a\\u4e0a\\u534811\\u70b9\\u534a\\u5f00\\u95e8\\u53ef\\u53c2\\u89c2, 12\\u70b9\\u6b63\\u5f0f\\u8425\\u4e1a\\u81f3\\u665a\\u4e0a8\\u70b9","phone":"\\u7535\\u8bdd\\uff1a+44 (0)1869 324 816","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (337, 'BALLY 巴利皮具配件', NULL, NULL, '/b/a/ballylogo_300x200.jpg', '000000', 1, 'bally', 0, NULL, NULL, NULL, '2015-05-27 10:36:30', '2015-05-27 10:36:30', 'FFF', 'http://www.bally.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a10am - 7pm","phone":"\\u7535\\u8bdd\\uff1a+44 (0) 1869 354 886","directions":{"driving":"","ptrans":""}},"6":{"groupName":"BROWNS \\u5e03\\u6717\\u65af","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (338, 'BOTTEGA VENETA 宝缇嘉手袋', NULL, NULL, '/b/o/bottega-veneta-logo-vector-image.png', '000000', 1, 'bottega-veneta', 0, NULL, NULL, NULL, '2015-05-27 10:40:24', '2015-05-27 10:40:24', 'FFF', 'www.bottegaveneta.com/', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a10am - 7pm","phone":"\\u7535\\u8bdd\\uff1a+44 (0)1869 326 660","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (339, 'BRITISH DESIGNERS COLLECTIVE 英国设计师集合店', NULL, NULL, '/3/0/300-bdc-logo-2015.png', '000000', 1, 'british-designers-collective', 0, NULL, NULL, NULL, '2015-05-27 10:43:08', '2015-05-27 10:43:08', 'FFF', 'http://www.bicestervillage.com/en/brands/item/british-designers-collective', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a10am - 7pm","phone":"\\u7535\\u8bdd\\uff1a+44 (0) 1869 248679","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (340, 'BONPOINT 小樱桃儿童时装', NULL, NULL, '/b/o/bonpoint-logo-300x200.png', '000000', 1, 'bonpoint', 0, NULL, NULL, NULL, '2015-05-27 10:47:11', '2015-05-27 10:45:58', 'FFF', 'http://www.bonpoint.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a10am - 7pm","phone":"\\u7535\\u8bdd\\uff1a+44 (0) 1869 252 466","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (341, 'BOSE 博士音响电子产品', NULL, NULL, '/b/o/bose_300.png', '000000', 1, 'bose', 0, NULL, NULL, NULL, '2015-05-27 10:51:02', '2015-05-27 10:51:02', 'FFF', 'http://www.bose.co.uk', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a10am - 7pm","phone":"\\u7535\\u8bdd\\uff1a+44 (0) 8707 414 499","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (342, 'A BATHING APE 时装', NULL, NULL, NULL, '', 1, 'a-bathing-ape', 0, NULL, NULL, NULL, '2015-05-27 10:57:48', '2015-05-27 10:52:22', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (343, 'CAROLINA HERRERA 卡罗琳娜·埃莱拉时装', NULL, NULL, '/c/h/ch-logo-300x200-new.png', '000000', 1, 'carolina-herrera', 0, NULL, NULL, NULL, '2015-05-27 10:55:00', '2015-05-27 10:55:00', 'FFF', 'http://www.carolinaherrera.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a10am - 7pm","phone":"\\u7535\\u8bdd\\uff1a+44 (0) 1869 354 609","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (344, 'CALVIN KLEIN 卡文克莱内衣', NULL, NULL, '/c/a/calvinkleinunderwear-logo-2013.png', '000000', 1, 'calvin-klein', 0, NULL, NULL, NULL, '2015-05-27 10:58:26', '2015-05-27 10:58:26', 'FFF', 'http://www.cku.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a10am - 7pm","phone":"\\u7535\\u8bdd\\uff1a+44 (0) 1869 324 401","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (345, 'A F VANDEVORST 时装', NULL, NULL, NULL, '', 1, 'a-f-vandevorst', 0, NULL, NULL, NULL, '2015-05-27 10:59:55', '2015-05-27 10:59:55', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (346, 'Brooks Brothers 布克兄弟时装', NULL, NULL, '/3/0/300x200_bb.png', '000000', 1, 'brooks-brothers', 0, NULL, NULL, NULL, '2015-05-27 11:01:43', '2015-05-27 11:01:43', 'FFF', 'http://www.brooksbrothers.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a10am - 7pm","phone":"\\u7535\\u8bdd\\uff1a+44 (0) 1869 354 782","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (347, 'A MANO 酒业', NULL, NULL, NULL, '', 1, 'a-mano', 0, NULL, NULL, NULL, '2015-05-27 11:02:48', '2015-05-27 11:02:48', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (348, 'BURBERRY 博柏利时装', NULL, NULL, '/b/u/burberry-logo-300x200.png', '000000', 1, 'burberry', 0, NULL, NULL, NULL, '2015-05-27 11:03:40', '2015-05-27 11:03:40', 'FFF', 'http://www.burberry.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a\\u4e0a\\u534811\\u70b9\\u534a\\u5f00\\u95e8\\u53ef\\u53c2\\u89c2, 12\\u70b9\\u6b63\\u5f0f\\u8425\\u4e1a\\u81f3\\u665a\\u4e0a8\\u70b9","phone":"\\u7535\\u8bdd\\uff1a+44 (0) 1869 253 785","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (349, 'A-Z 酒业', NULL, NULL, NULL, '', 1, 'a-z', 0, NULL, NULL, NULL, '2015-05-27 11:05:32', '2015-05-27 11:05:32', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (350, 'CHARLES TYRWHITT 男装', NULL, NULL, '/3/0/300x200_ct_black.png', '000000', 1, 'charles-tyrwhitt', 0, NULL, NULL, NULL, '2015-05-27 11:06:14', '2015-05-27 11:06:14', 'FFF', 'http://www.ctshirts.co.uk', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a10am - 7pm","phone":"\\u7535\\u8bdd\\uff1a+44 (0) 1869 360 056","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (351, 'A.P.C. 时装', NULL, NULL, NULL, '', 1, 'a-p-c', 0, NULL, NULL, NULL, '2015-05-27 11:07:05', '2015-05-27 11:07:05', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (352, 'AAPE 时装', NULL, NULL, NULL, '', 1, 'aape', 0, NULL, NULL, NULL, '2015-05-27 11:09:00', '2015-05-27 11:09:00', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (353, 'CHLOE 蔻依女装', NULL, NULL, '/c/h/chlo___logo_.png', '000000', 1, 'chloe', 0, NULL, NULL, NULL, '2015-05-27 11:11:11', '2015-05-27 11:11:11', 'FFF', 'http://www.chloe.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a10am - 7pm","phone":"\\u7535\\u8bdd\\uff1a+44 (0) 1869 360 517","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (354, 'AB CREW 化妆品', NULL, NULL, NULL, '', 1, 'ab-crew', 0, NULL, NULL, NULL, '2015-05-27 11:12:09', '2015-05-27 11:12:09', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (355, 'ABELHA 酒业', NULL, NULL, NULL, '', 1, 'abelha', 0, NULL, NULL, NULL, '2015-05-27 11:12:45', '2015-05-27 11:12:45', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (356, 'ABERLOUR 亚伯乐威士忌', NULL, NULL, NULL, '', 1, 'aberlour', 0, NULL, NULL, NULL, '2015-05-27 11:13:41', '2015-05-27 11:13:41', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (357, 'CARLUCCIO RESTAURANT 卡露奇饭店', NULL, NULL, '/c/a/carluccios_closed.png', '000000', 1, 'carluccio-restaurant', 0, NULL, NULL, NULL, '2015-05-27 11:13:50', '2015-05-27 11:13:50', 'FFF', 'http://www.carluccios.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a10am - 7pm","phone":"\\u7535\\u8bdd\\uff1a+44 (0) 1869 247 651","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (358, 'CATH KIDSON 凯茜·绮丝敦女装', NULL, NULL, '/3/0/300x200_cath-kidston_logo.png', '000000', 1, 'cath-kidson', 0, NULL, NULL, NULL, '2015-05-27 11:16:11', '2015-05-27 11:16:11', 'FFF', 'http://www.cathkidston.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a10am - 7pm","phone":"\\u7535\\u8bdd\\uff1a+44 (0) 1869 247 358","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (359, 'ABIGAIL WARNER 家居用品', NULL, NULL, NULL, '', 1, 'abigail-warner', 0, NULL, NULL, NULL, '2015-05-27 11:17:12', '2015-05-27 11:17:12', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (360, 'ABSOLUT 绝对伏特加', NULL, NULL, NULL, '', 1, 'absolut', 0, NULL, NULL, NULL, '2015-05-27 11:17:46', '2015-05-27 11:17:46', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (361, 'COACH 蔻驰时装', NULL, NULL, '/c/o/coach_logo_300x200.png', '000000', 1, 'coach', 0, NULL, NULL, NULL, '2015-05-27 11:18:19', '2015-05-27 11:18:19', 'FFF', 'http://www.coach.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a10am - 7pm","phone":"\\u7535\\u8bdd\\uff1a+44 (0) 1869 248 069","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (362, 'ACE OF SPADES 酒业', NULL, NULL, NULL, '', 1, 'ace-of-spades', 0, NULL, NULL, NULL, '2015-05-27 11:18:33', '2015-05-27 11:18:33', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (363, 'ACNE STUDIOS 艾克妮时装', NULL, NULL, NULL, '', 1, 'acne-studios', 0, NULL, NULL, NULL, '2015-05-27 11:20:28', '2015-05-27 11:20:28', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (364, 'ACQUA DI PARMA 彭玛之源香氛', NULL, NULL, NULL, '', 1, 'acqua-di-parma', 0, NULL, NULL, NULL, '2015-05-27 11:21:46', '2015-05-27 11:21:46', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (365, 'COCCINELLE 可奇奈尔女装', NULL, NULL, '/c/o/coccinelle-logo.png', '000000', 1, 'coccinelle', 0, NULL, NULL, NULL, '2015-05-27 11:22:20', '2015-05-27 11:22:20', 'FFF', 'http://www.coccinelle.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a10am - 7pm","phone":"\\u7535\\u8bdd\\uff1a+44 (0) 1869 243 695","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (366, 'ADAMI 酒业', NULL, NULL, NULL, '', 1, 'adami', 0, NULL, NULL, NULL, '2015-05-27 11:23:32', '2015-05-27 11:23:32', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (367, 'ADDISON ROSS LONDON  家居用品', NULL, NULL, NULL, '', 1, 'addison-ross-london', 0, NULL, NULL, NULL, '2015-05-27 11:24:38', '2015-05-27 11:24:38', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (368, 'CHURCH''S OUTLET 教堂时装折扣店', NULL, NULL, '/c/h/church_logo-1024x690.jpg', '000000', 1, 'churchs-outlet', 0, NULL, NULL, NULL, '2015-05-27 11:25:00', '2015-05-27 11:25:00', 'FFF', 'http://www.church-footwear.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a10am - 7pm","phone":"\\u7535\\u8bdd\\uff1a+44 (0) 1869 252 245","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (369, 'ADEEN 阿登头饰', NULL, NULL, NULL, '', 1, 'adeen', 0, NULL, NULL, NULL, '2015-05-27 11:26:35', '2015-05-27 11:26:35', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (370, 'CLARIK''S 克拉克鞋', NULL, NULL, '/c/l/clarks-logo-300x200.png', '000000', 1, 'clarks', 0, NULL, NULL, NULL, '2015-05-27 11:27:20', '2015-05-27 11:27:20', 'FFF', 'http://www.clarks.co.uk', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a10am - 7pm","phone":"\\u7535\\u8bdd\\uff1a+44 (0) 1869 325 646","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (371, 'ADEN + ANAIS 婴儿用品', NULL, NULL, NULL, '', 1, 'aden-anais', 0, NULL, NULL, NULL, '2015-05-27 11:27:49', '2015-05-27 11:27:49', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (372, 'ADIDAS 阿迪达斯运动', NULL, NULL, NULL, '', 1, 'adidas', 0, NULL, NULL, NULL, '2015-05-27 11:28:30', '2015-05-27 11:28:30', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (373, 'ADIDAS BY STELLA MCCARTNEY 运动', NULL, NULL, NULL, '', 1, 'adidas-by-stella-mccartney', 0, NULL, NULL, NULL, '2015-05-27 11:30:17', '2015-05-27 11:30:17', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (374, 'ADIDAS X MARY KATRANTZOU 运动', NULL, NULL, NULL, '', 1, 'adidas-x-mary-katrantzou', 0, NULL, NULL, NULL, '2015-05-27 11:30:53', '2015-05-27 11:30:53', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (375, 'DKNY 唐娜·卡伦时装', NULL, NULL, '/d/k/dkny-logo-1.png', '000000', 1, 'dkny', 0, NULL, NULL, NULL, '2015-05-27 11:31:06', '2015-05-27 11:31:06', 'FFF', 'http://www.dkny.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a10am - 7pm","phone":"\\u7535\\u8bdd\\uff1a+44 (0) 1869 322 451","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (376, 'ADIDAS Y3 阿迪达斯Y3运动', NULL, NULL, NULL, '', 1, 'adidas-y3-y3', 0, NULL, NULL, NULL, '2015-05-27 11:32:02', '2015-05-27 11:32:02', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (377, 'ADUNA 茶品', NULL, NULL, NULL, '', 1, 'aduna', 0, NULL, NULL, NULL, '2015-05-27 11:33:27', '2015-05-27 11:33:27', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (378, 'DAVID CLULOW 大卫·克鲁洛时装', NULL, NULL, '/3/0/300x200_dc.png', '000000', 1, 'david-clulow', 0, NULL, NULL, NULL, '2015-05-27 11:33:40', '2015-05-27 11:33:40', 'FFF', 'http://www.davidclulow.com', '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"7":{"groupName":"Bicester Village \\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u5965\\u7279\\u83b1\\u65af","address":"","hours":"\\u5468\\u4e00\\u81f3\\u5468\\u516d\\uff1a9am - 8pm, \\u5468\\u65e5\\uff1a10am - 7pm","phone":"\\u7535\\u8bdd\\uff1a+44 (0) 1869 245 894","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (379, 'AE DOR 艾舵酒业', NULL, NULL, NULL, '', 1, 'ae-dor', 0, NULL, NULL, NULL, '2015-05-27 11:34:55', '2015-05-27 11:34:55', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (380, 'AEDLE 耳机', NULL, NULL, NULL, '', 1, 'aedle', 0, NULL, NULL, NULL, '2015-05-27 11:38:21', '2015-05-27 11:38:21', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (381, 'AERIN 化妆品', NULL, NULL, NULL, '', 1, 'aerin', 0, NULL, NULL, NULL, '2015-05-27 11:42:24', '2015-05-27 11:42:24', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (382, 'AEROBIE 家居用品', NULL, NULL, NULL, '', 1, 'aerobie', 0, NULL, NULL, NULL, '2015-05-27 11:45:45', '2015-05-27 11:45:45', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (383, 'AESOP 伊索化妆品', NULL, NULL, NULL, '', 1, 'aesop', 0, NULL, NULL, NULL, '2015-05-27 11:48:38', '2015-05-27 11:48:38', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}},"3":{"groupName":"LONDON DESIGNER OUTLET ","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (384, 'AETHER 音响设施', NULL, NULL, NULL, '', 1, 'aether', 0, NULL, NULL, NULL, '2015-05-27 11:51:38', '2015-05-27 11:51:38', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (385, 'AG 裤装', NULL, NULL, NULL, '', 1, 'ag', 0, NULL, NULL, NULL, '2015-05-27 11:53:30', '2015-05-27 11:53:30', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (386, 'AG JEANS 裤装', NULL, NULL, NULL, '', 1, 'ag-jeans', 0, NULL, NULL, NULL, '2015-05-27 11:54:59', '2015-05-27 11:54:59', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (387, 'AGONIST 香氛', NULL, NULL, NULL, '', 1, 'agonist', 0, NULL, NULL, NULL, '2015-05-27 11:55:54', '2015-05-27 11:55:54', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}},"5":{"groupName":"GB Tax Free \\u82f1\\u56fd\\u514d\\u7a0e","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (388, 'AGRAPART ET FILS 酒业', NULL, NULL, NULL, '', 1, 'agrapart-et-fils', 0, NULL, NULL, NULL, '2015-05-27 11:56:27', '2015-05-27 11:56:27', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (389, 'AGUA DE COCO 时装', NULL, NULL, NULL, '', 1, 'agua-de-coco', 0, NULL, NULL, NULL, '2015-05-27 12:03:45', '2015-05-27 12:03:45', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (390, 'AKASHI-TAI 酒业', NULL, NULL, NULL, '', 1, 'akashi-tai', 0, NULL, NULL, NULL, '2015-05-27 12:06:27', '2015-05-27 12:06:27', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (391, 'AKESSONS 巧克力', NULL, NULL, NULL, '', 1, 'akessons', 0, NULL, NULL, NULL, '2015-05-27 12:07:26', '2015-05-27 12:07:26', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (392, 'AKG 爱科技耳机', NULL, NULL, NULL, '', 1, 'akg', 0, NULL, NULL, NULL, '2015-05-27 12:10:06', '2015-05-27 12:10:06', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);
INSERT INTO `primary_ibd_travelsuite_brand` VALUES (393, 'AKID 童装', NULL, NULL, NULL, '', 1, 'akid', 0, NULL, NULL, NULL, '2015-05-27 12:14:13', '2015-05-27 12:14:13', '', NULL, '{"twitter":"","facebook":"","instagram":"","weibo":""}', '{"4":{"groupName":"Selfridges \\u585e\\u5c14\\u798f\\u91cc\\u5947","address":"","hours":"","phone":"","directions":{"driving":"","ptrans":""}}}', NULL, NULL, NULL);

-- --------------------------------------------------------

-- 
-- Table structure for table `primary_ibd_travelsuite_brand_group`
-- 

-- CREATE TABLE `primary_ibd_travelsuite_brand_group` (
--   `brand_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Brand ID',
--   `group_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Group ID',
--   UNIQUE KEY `UNQ_IBD_IBD_TRAVELSUITE_BRAND_GROUP_BRAND_ID_GROUP_ID` (`brand_id`,`group_id`),
--   KEY `IDX_IBD_IBD_TRAVELSUITE_BRAND_GROUP_GROUP_ID` (`group_id`),
--   KEY `IDX_IBD_IBD_TRAVELSUITE_BRAND_GROUP_BRAND_ID` (`brand_id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Brand to Group Linkage Table';

-- 
-- Dumping data for table `primary_ibd_travelsuite_group`
-- 

INSERT INTO `primary_ibd_travelsuite_group` VALUES (1, 4, 'Bond Street 邦德大街', NULL, '<p>邦德大街是伦敦上流社会的心脏地带，是世界上顶级设计师商店最集中的地区之一。街道遍布精品店、珠宝店、奢侈品牌和艺术古董店等。</p>\r\n<p>邦德大街VIP顾客服务</p>\r\n<p>&nbsp;</p>', '<ul>\r\n<li>伦敦最高档的时尚购物区之一</li>\r\n<li>全球顶尖奢侈品牌汇聚</li>\r\n</ul>\r\n<p>&nbsp;</p>', '000000', '/c/a/cartier-bond-street_1.jpg', 51.51423, -0.14649, 2, 'bond-street', 1, '邦德街bond street购物信息IBDShop.com', '邦德街bond street伦敦奢侈品牌购物旅游地铁公交自驾交通信息', '邦德街bond street是伦敦著名的奢侈品购物街区。点击进入查看地铁公交自驾交通以及品牌等信息。', '2015-05-28 05:00:25', '2015-05-11 03:28:54', 'FFF', '购物大街', NULL, '购物大道', 'New Bond St, Mayfair, London W1S（大不列颠）联合王国', '{"driving":"\\u81ea\\u9a7e\\r\\n\\r\\n\\u5728\\u90a6\\u5fb7\\u8857\\u5730\\u5e26\\u6709\\u5f88\\u591a\\u505c\\u8f66\\u573a\\u4ee5\\u53ca\\u505c\\u8f66\\u8ba1\\u65f6\\u5668\\u3002\\u5728\\u661f\\u671f\\u5929\\u5168\\u5929\\u4ee5\\u53ca\\u661f\\u671f\\u4e00\\u81f3\\u661f\\u671f\\u516d\\u665a\\u4e0a6\\uff1a30\\u4e4b\\u540e\\uff0c\\u90a6\\u5fb7\\u8857\\u7684\\u5355\\u9ec4\\u7ebf\\u5185\\u662f\\u5141\\u8bb8\\u505c\\u8f66\\u7684\\u3002\\r\\n\\r\\n\\u62e5\\u5835\\u8d39\\r\\n\\r\\n\\u9664\\u4e86\\u665a\\u4e0a\\u3001\\u5468\\u672b\\u3001\\u516c\\u5171\\u5047\\u65e5\\u4ee5\\u53ca\\u5723\\u8bde\\u8282\\u540e\\u7684\\u4e09\\u5929\\u662f\\u514d\\u8d39\\u7684\\uff0c\\u4f26\\u6566\\u5728\\u661f\\u671f\\u4e00\\u5230\\u661f\\u671f\\u4e94\\u7684\\u65e9\\u4e0a7\\u70b9\\u5230\\u665a\\u4e0a6\\u70b9\\u6536\\u53d6\\u62e5\\u5835\\u8d39\\u3002\\u5982\\u679c\\u4f60\\u5728\\u5f53\\u5929\\u5348\\u591c\\u652f\\u4ed8\\uff0c\\u8d39\\u7528\\u4e3a10\\u82f1\\u9551\\uff1b\\u5982\\u679c\\u4f60\\u5728\\u4e0b\\u4e00\\u4e2a\\u5348\\u591c\\u652f\\u4ed8\\uff0c\\u9700\\u989d\\u5916\\u652f\\u4ed82\\u82f1\\u9551\\u3002\\u62e5\\u5835\\u8d39\\u53ef\\u4ee5\\u6700\\u591a\\u63d0\\u524d90\\u5929\\u652f\\u4ed8\\u3002\\r\\n\\r\\n\\u8be6\\u60c5\\u8bf7\\u81f3\\uff1a0845 900 1234\\uff0c+44 20 7649 9122\\uff08\\u56fd\\u9645\\uff09\\r\\n","walking":"\\u5927\\u5df4\\u7ebf\\u8def\\r\\n\\r\\n 6, 7, 10, 13, 15, 23, 73, 94, 98, 137, 189  390\\r\\n9, 19, 22, 38 \\u5747\\u53ef\\u5230\\u8fbe\\r\\n\\r\\n\\u5730\\u94c1\\u7ebf\\u8def\\r\\n\\r\\nOxford Circus \\u4ee5\\u53ca Green Park stations\\u7ebf\\u5230Bond street\\u7ad9\\u4e0b\\uff0c\\u6b65\\u884c5\\u5206\\u949f\\u5230\\u8fbe\\r\\n\\r\\n"}', 'http://www.bondstreet.london', '{"twitter":"","facebook":"","instagram":"","weibo":""}');
INSERT INTO `primary_ibd_travelsuite_group` VALUES (2, 4, 'Regent Street 摄政大街', NULL, '<p>伦敦摄政大街包罗了众多的时尚品牌，食肆及高端酒店。他以国际化的形象吸引了世界的眼光，是众多标志旗舰店的安家之处：如柏博丽, 瑞士钟表和苹果。</p>\r\n<p>摄政大街发行了英国首张高街礼品卡及相关APP, 你能够及时收到关于摄政大街的新闻，优惠活动及秘密邀请。<br />摄政大街同时也举办了许多无交通活动，其中包括了夏日接到，摄政大街摩托秀，以及著名的圣诞节亮灯活动，吸引了来访者和当地居民前来购物，饮食和逛街。</p>', '<ul>\r\n<li>靠近摄政公园及唐人街 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</li>\r\n<li>最大玩具商店Hamley''s</li>\r\n<li>福尔摩斯原址</li>\r\n</ul>\r\n<p>&nbsp;</p>', '000000', '/r/e/regent_street_london_-_image_3_motor_show.jpg', 51.51503, -0.14183, 2, 'regent-street', 1, NULL, NULL, NULL, '2015-05-25 05:59:52', '2015-05-13 03:37:16', 'FFF', NULL, NULL, '0', 'Regent St, London, （大不列颠）联合王国', '{"driving":"\\u62e5\\u5835\\u8d39\\r\\n\\r\\n\\u6444\\u653f\\u5927\\u8857\\u5c5e\\u4e8e\\u4f26\\u6566\\u62e5\\u5835\\u8d39\\u6536\\u53d6\\u5730\\u533a\\u3002\\r\\n\\u5728\\u6307\\u5b9a\\u65f6\\u95f4\\u6cca\\u8f66\\u4e8e\\u5348\\u591c\\uff1a\\u6536\\u8d3910\\u82f1\\u9551\\uff0c\\u5982\\u7b2c\\u4e8c\\u5929\\u4ed8\\u5219\\u53e6\\u52a02\\u82f1\\u9551\\r\\n\\u8d39\\u7528\\u6536\\u53d6\\u65f6\\u95f4\\uff1a\\u5468\\u4e00\\u81f3\\u5468\\u4e947\\u70b9\\u81f318\\u70b9\\r\\n\\u4ee5\\u4e0b\\u65f6\\u95f4\\u6bb5\\u4e0d\\u6536\\u8d39\\uff1a\\r\\n\\u5728\\u5f53\\u665a18\\u70b9\\u81f3\\u7b2c\\u4e8c\\u5929\\u51cc\\u66687\\u70b9\\u4e4b\\u524d\\u6216\\u4e4b\\u540e\\r\\n\\u5468\\u672b\\uff0c\\u516c\\u5171\\u5047\\u671f\\uff0c\\u5723\\u8bde\\u65e5\\uff0812\\u670825\\u65e5\\uff09\\u53ca\\u65b0\\u5e74\\uff081\\u67081\\u65e5\\uff09\\r\\n\\r\\nOld Burlington Street\\r\\nQ-Park Burlington Street\\r\\n\\u5730\\u70b9\\uff1a 3-9 Old Burlington Street  W1S 3 AF\\r\\n24\\u5c0f\\u65f6\\u5f00\\u653e\\r\\n\\u8f66\\u9ad8\\u9650\\u5236\\uff1a 2.08M\\r\\n\\u7a7a\\u95f4\\u5bb9\\u91cf\\uff1a 331\\r\\n\\u989d\\u5916\\u8bbe\\u65bd\\uff1a \\u6b8b\\u75be\\u4eba\\u6cca\\u8f66\\uff0c\\u7535\\u52a8\\u8f66\\u8f86\\u6536\\u8d39\\uff0c\\u5395\\u6240\\uff0c\\u64e6\\u978b\\u53ca\\u4ee3\\u5ba2\\u6cca\\u8f66\\u670d\\u52a1\\r\\n\\u7535\\u8bdd\\uff1a 0207 494 3889\\r\\n\\r\\nQ-Park Soho\\r\\n\\u5730\\u70b9\\uff1a49 \\u2013 50 Poland Street W1F 7LZ\\r\\n24\\u5c0f\\u65f6\\u5f00\\u653e\\r\\n\\u8f66\\u9ad8\\u9650\\u5236\\uff1a 2.10M\\r\\n\\u7a7a\\u95f4\\u5bb9\\u91cf\\uff1a 196\\r\\n\\u989d\\u5916\\u8bbe\\u65bd\\uff1a \\u6b8b\\u75be\\u4eba\\u6cca\\u8f66\\uff0c\\u7535\\u52a8\\u8f66\\u8f86\\u6536\\u8d39\\uff0c\\u5395\\u6240\\uff0c\\u64e6\\u978b\\u53ca\\u4ee3\\u5ba2\\u6cca\\u8f66\\u670d\\u52a1\\r\\n\\u7535\\u8bdd\\uff1a 0207 437 7660\\r\\n\\r\\nQ-Park Oxford Street Cavendish Square\\r\\n\\u5730\\u70b9\\uff1aCavendish Square, W1G 0PG\\r\\n24\\u5c0f\\u65f6\\u5f00\\u653e\\r\\n\\u8f66\\u9ad8\\u9650\\u5236\\uff1a1.83M\\r\\n\\u7a7a\\u95f4\\u5bb9\\u91cf\\uff1a 395\\r\\n\\u989d\\u5916\\u8bbe\\u65bd\\uff1a\\u5395\\u6240\\uff0c\\u64e6\\u978b\\u53ca\\u4ee3\\u5ba2\\u6cca\\u8f66\\u670d\\u52a1\\uff0c\\u7167\\u770b\\u5a74\\u7ae5\\uff0c\\u96e8\\u4f1e\\u53ca\\u6469\\u6258\\u8f66\\uff0c\\u7535\\u52a8\\u8f66\\u8d39\\u7528\\r\\n\\u7535\\u8bdd\\uff1a 0207 491 2419\\r\\n\\r\\nQ-park Harley Street\\r\\n\\u5730\\u70b9\\uff1a Queen Anne Mews, W1G 9HF\\r\\n24\\u5c0f\\u65f6\\u5f00\\u653e\\r\\n\\u8f66\\u9ad8\\u9650\\u5236\\uff1a 1.82M\\r\\n\\u7a7a\\u95f4\\u5bb9\\u91cf\\uff1a 345\\r\\n\\u989d\\u5916\\u8bbe\\u65bd\\uff1a\\u7535\\u52a8\\u8f66\\u8f86\\u6536\\u8d39\\uff0c\\u5395\\u6240\\uff0c\\u64e6\\u978b\\u53ca\\u4ee3\\u5ba2\\u6cca\\u8f66\\u670d\\u52a1\\r\\n\\u7535\\u8bdd\\uff1a 0207 636 0838\\r\\n","walking":"\\u5730\\u94c1\\u7ebf\\u8def\\r\\n\\r\\n\\u6444\\u653f\\u5927\\u8857\\u4f4d\\u4e8e\\u4f26\\u6566\\u897f\\u7aef\\u4e2d\\u5fc3\\uff0c\\u4efb\\u4f55\\u5730\\u94c1\\u90fd\\u80fd\\u591f\\u65b9\\u4fbf\\u5230\\u8fbe\\r\\n\\r\\n\\u725b\\u6d25\\u5706\\u73af\\u5e7f\\u573a\\uff1aCentral, Bakerloo and Victoria lines \\r\\n\\r\\n\\u76ae\\u5361\\u8fea\\u5229\\u5706\\u73af\\u5e7f\\u573a\\uff1aBakerloo and Piccadilly lines\\u5730\\u94c1\\u7ad9\\u53ef\\u5feb\\u901f\\u65b9\\u4fbf\\u5230\\u8fbe\\r\\n"}', 'http://www.regentstreetonline.cn', '{"twitter":"twitter\\/parisprintemps","facebook":"","instagram":"","weibo":""}');
INSERT INTO `primary_ibd_travelsuite_group` VALUES (3, 4, 'LONDON DESIGNER OUTLET ', NULL, '<p>LDO拥有50多家店铺，包括独一无二的设计师品牌。不论是时装，配饰，鞋类，还是运动和休闲款式，甚至囊括了瓷器和巧克力，其限量版产品都融合了前卫与经典的元素！在伦敦仅有的设计师折扣店体验7至3折优惠零售价！</p>', '<ul>\r\n<li>伦敦首个折扣购物村</li>\r\n<li>邻近温布利球场</li>\r\n</ul>', '000000', '/l/d/ldo-night.jpg', 51.55952, -0.27539, 2, 'ldo', 1, 'LDO伦敦设计师奥特莱斯信息IBDShop.com', 'LDO伦敦设计师奥特莱斯London Designer Outlets品牌折扣购物村地铁自驾信息', 'LDO伦敦设计师奥特莱斯是伦敦首个折扣购物村，请点击查看地铁自驾等交通信息以及具体品牌信息等', '2015-05-20 03:46:28', '2015-05-13 08:14:36', 'FFF', NULL, NULL, '奥特莱斯', 'Wembley Park Boulevard, Wembley, Greater London HA9 0QL英国', '{"driving":"\\u63d0\\u4f9b3\\u4e2a\\u4ed8\\u8d39\\u505c\\u8f66\\u573a","walking":"\\u5730\\u94c1 \\r\\n\\r\\n\\u53ef\\u4e58\\u5750Metropolitan Line\\/Chiltern\\/Jubilee Lines \\u5230Wembley Park\\u7ad9\\u4e0b"}', 'http://www.londondesigneroutlet.com', '{"twitter":"https:\\/\\/twitter.com\\/londonoutlet","facebook":"https:\\/\\/www.facebook.com\\/pages\\/London-Designer-Outlet\\/136803903032109","instagram":"","weibo":""}');
INSERT INTO `primary_ibd_travelsuite_group` VALUES (4, 4, 'Selfridges 塞尔福里奇', NULL, '<p><span>Selfridges &amp; Co,&nbsp;</span><span>是英国标志性的零售巨头，他不仅是大型百货商场，更是社交中心。他的创新和个性化的服务，赢得了全球消费者们的<a>青睐</a>，来到这里你绝对不能错过无微不至的私人购物体验，不论是为了参加</span><span>VIP</span><span>活动准备特殊的装束，换季更新或是整理衣柜，我们的私人购物助理都会在这里与您分享他们的时尚秘诀。如今，</span><span>Selfridges</span><span>已成为唯一一家</span><span>&nbsp;3&nbsp;</span><span>次获</span><span>&ldquo;</span><span>世界最佳百货商场</span><span>&rdquo;</span><span>提名的百货店。</span><span>Selfridges&nbsp;</span><span>旗下的国际购物网站向全球</span><span>&nbsp;60&nbsp;</span><span>多个国家配送商品，其中也包括中国</span><span>。在这里您将感受到前所未有的购物体验。</span></p>\r\n<p>扫二维码关注Selfridges官方微信</p>\r\n<p>insertwechatqrcode</p>\r\n<p>insertwechatqrcode</p>\r\n<p>insertwechatqrcode</p>\r\n<p>insertwechatqrcode</p>', '<ul>\r\n<li>鲜艳的明黄色购物袋</li>\r\n<li>历史悠久，集购物与社交中心为一体</li>\r\n<li>于2010，2012，2014年荣获&ldquo;全球最佳百货商店&rdquo;</li>\r\n<li>VIP私人服务导购，享受至尊购物体验</li>\r\n</ul>', '000000', '/s/e/selfridges-oxford-street.jpg', 51.51465, -0.15291, 2, 'selfridges', 1, 'Selfridges IBDShop.com', 'Selfridges  伦敦 购物 牛津街 时尚 ', 'Selfridges是英国著名的百货公司，集购物，餐饮及社交于一体。坐落在伦敦市中心牛津街，是伦敦时尚地标之一。更多关于交通及营业时间信息，请点击ibdshop.com查看详细内容。', '2015-05-26 04:20:22', '2015-05-14 09:57:29', 'FFF', '百货商店', NULL, '购物中心', '400 Oxford Street, London W1A 1AB 英国', '{"driving":"\\u4f26\\u6566\\u5e97\\u94fa\\u6253\\u7684\\uff1a40 Duke Street, London, W1U 1AT\\r\\n\\r\\n","walking":"\\u6700\\u8fd1\\u5730\\u94c1\\u7ad9\\r\\n\\r\\n\\u4f26\\u6566\\u5e97\\u94fa\\uff1abond street\\r\\n\\u4f2f\\u660e\\u7ff0\\u5e97\\u94fa\\uff1aBirmingham moor street  \\r\\n\\u66fc\\u5f7b\\u65af\\u7279\\u4ea4\\u6613\\u5e7f\\u573a\\uff1aManchester Victoria\\r\\n"}', 'http://www.selfridges.com/', '{"twitter":"https:\\/\\/twitter.com\\/SELFRIDGES","facebook":"https:\\/\\/www.facebook.com\\/selfridges","instagram":"https:\\/\\/instagram.com\\/theofficialselfridges\\/","weibo":"http:\\/\\/weibo.com\\/selfridgesuk"}');
INSERT INTO `primary_ibd_travelsuite_group` VALUES (5, 4, 'GB Tax Free 英国免税', NULL, NULL, NULL, '', '/g/b/gb-tax-free.jpg', 0.00000, 0.00000, 1, 'gb-tax-free', 1, NULL, NULL, NULL, '2015-05-22 03:11:56', '2015-05-20 02:48:46', '', '免税购物', NULL, '免税购物', NULL, '{"driving":"","walking":""}', 'https://www.gbtaxfree.com/', '{"twitter":"","facebook":"","instagram":"","weibo":""}');
INSERT INTO `primary_ibd_travelsuite_group` VALUES (6, 4, 'BROWNS 布朗斯', NULL, '<p><span>布朗斯百货成立于1970年，由英国顶尖时尚零售教母Joan Burstein创建，是世界上最负盛名的时尚购物目的地之一。布朗斯以挖掘诸如亚历山大&bull;麦昆，约翰&bull;加里亚诺及克里斯托弗&bull;凯恩等时尚设计师而闻名，为顾客们提供了独一无二的新潮时尚元素，以及由顶尖设计师们所呈现的各类时尚精品，包括女装，男装，珠宝及配饰等。</span></p>', NULL, '', NULL, 0.00000, 0.00000, 1, 'browns', 1, NULL, NULL, NULL, '2015-05-26 11:12:20', '2015-05-26 11:12:20', '', NULL, NULL, '0', NULL, '{"driving":"","walking":""}', 'HTTPS://WWW.BROWNSFASHION.COM', '{"twitter":"","facebook":"","instagram":"","weibo":""}');
INSERT INTO `primary_ibd_travelsuite_group` VALUES (7, 5, 'Bicester Village 比斯特购物村奥特莱斯', NULL, '<p>比斯特购物村是一家隶属于Value Retail集团旗下Chic Outlet Shopping的奥特莱斯购物地点。Chic Outlet Shopping创立于1992年，是Value Retail所构造的一个独一无二的奢侈品奥特莱斯购物概念。 它拥有近1000家精品奥特莱斯店铺，囊括了各种尖端前沿的时尚及生活品牌。其中，The Collections系列更是为精益求精的购物者们呈现了无与伦比的奥特莱斯购物体验，对品质孜孜不倦的追求使Chic Outlet当之无愧地成为作为经典的购物地标。</p>\r\n<p>Chic Outlet Shopping 在苏州和上海也拥有奥特莱斯。详情请至Suzhou Village - 苏州奕欧来精品购物村官方网址：http://yioulai.com/。或Shanghai Village - 上海奕欧来精品购物村（即将在2016春季对外开放），官方网址：http://www.shanghaivillage.com/。</p>', '<p>英国人气最火的奥特莱斯</p>\r\n<p>众多设计师品牌云集</p>\r\n<p>零售价4折起的优惠价格</p>', '000000', '/b/i/bicester-village-logo.jpg', 51.89226, -1.15584, 2, 'bicester-village', 1, '比斯特购物村奥特莱斯', '比斯特, 比斯特购物村, 奥特莱斯, 奢侈品牌, 设计师品牌, 折扣品牌, 英国', '比斯特购物村是英国最大的奥特莱斯购物站之一，它拥有众多的奢侈品及设计师品牌，以及丰富的购物品类，是购物爱好者们不可错失的经典购物村。', '2015-05-28 03:09:33', '2015-05-27 07:25:42', 'FFF', '购物地标', NULL, '购物街区', '50 Pingle Dr, Bicester, Bicester, Oxfordshire OX26 6WD（大不列颠）联合王国', '{"driving":"\\u9a7e\\u8f66\\r\\n\\u4eceM40\\u516c\\u8def\\u76849\\u53f7\\u4ea4\\u53c9\\u8def\\u53e3\\u51fa\\u53d1\\uff0c\\u7531A41\\u8def\\u884c\\u9a762\\u82f1\\u91cc\\u5230\\u8fbe\\u6bd4\\u65af\\u7279\\u6751\\u3002\\u8def\\u4e0a\\u53ef\\u89c1\\u6bd4\\u65af\\u7279\\u8d2d\\u7269\\u6751\\u6307\\u793a\\u6807\\u5fd7\\u53ca\\u5730\\u5740\\u3002\\r\\n\\r\\n\\u51fa\\u79df\\u8f66\\r\\n\\u60a8\\u53ef\\u4ece\\u4ee5\\u4e0b\\u51e0\\u4e2a\\u7ad9\\u70b9\\u79df\\u5230\\u8f66\\r\\n\\r\\nQuick Cars Bicester\\r\\n\\u7535\\u8bdd\\uff1a+44 (0)7828 521 477\\r\\n\\u90ae\\u7bb1\\uff1aquickcarsbicester@yahoo.co.uk\\r\\n\\r\\nElite Cars of Bicester\\r\\n\\u7535\\u8bdd\\uff1a+44 (0)1869 240 000\\r\\n\\u90ae\\u7bb1\\uff1ahttp:\\/\\/www.elitecarsofbicester.co.uk\\r\\n\\r\\n5 Star Chauffeurs: Oxford\\r\\n\\u7535\\u8bdd\\uff1a+44 (0)1865 744 944\\r\\n\\u90ae\\u7bb1\\uff1ahttp:\\/\\/www.5starchauffeurs.com\\r\\n\\r\\n\\u5982\\u679c\\u60a8\\u4ece\\u4f26\\u6566\\u51fa\\u53d1\\uff0c\\u53ef\\u4e8e\\u6b64\\u7ad9\\u70b9\\u79df\\u8f66\\u524d\\u5f80\\r\\n\\r\\nClimate Cars\\r\\n\\u7535\\u8bdd\\uff1a+44 (0)2073 505960 \\r\\n\\u90ae\\u7f16\\uff1ahttp:\\/\\/www.climatecars.com\\r\\n\\r\\n\\u8bf7\\u4e8e\\u51fa\\u53d1\\u524d\\u8be2\\u95ee\\u65c5\\u7a0b\\u4ef7\\u683c\\u3002\\r\\n\\r\\n\\r\\n\\r\\n","walking":"\\u6bd4\\u65af\\u7279\\u4e13\\u8f66\\r\\n\\r\\n\\u53ef\\u9884\\u8ba2Shopping Express \\u548c Chauffeur Drive\\u5230\\u8fbe\\u3002\\r\\n\\r\\nShopping Express\\u7684\\u8fd0\\u884c\\u7ebf\\u8def\\u4e3a\\u4f26\\u6566\\u5e02\\u4e2d\\u5fc3\\u5230\\u6bd4\\u65af\\u7279\\u6751\\u3002\\u9884\\u7ea6\\u8be6\\u60c5\\u8bf7\\u8bbf\\u95ee\\u8be5\\u9875\\u9762\\uff1ahttps:\\/\\/www.bicestervillage.com\\/en\\/guest-services\\/chic-travel\\/shop\\/shopping-express\\r\\n\\r\\nChauffeur Drive\\u4e3aVIP\\u5ba2\\u6237\\u63d0\\u4f9b\\u63a5\\u9001\\u670d\\u52a1\\u3002\\u9884\\u8ba2\\u6700\\u9ad8\\u4eba\\u6570\\u4e3a10\\u4eba\\uff0c\\u63a5\\u9001\\u5730\\u70b9\\u53ef\\u81ea\\u7531\\u9009\\u62e9\\u3002\\u8be6\\u60c5\\u8bf7\\u81f3VIP\\u4e13\\u7ebf\\uff1a0844 809 9335\\u3002\\r\\n\\r\\n\\u4ef7\\u683c\\r\\n\\u5341\\u4f4d\\u4e58\\u5ba2\\u4ef7\\u683c500\\u82f1\\u9551\\u3002\\r\\n\\u56de\\u7a0b\\u8f66\\u8d39\\u4e3a\\u6bcf\\u4eba50\\u82f1\\u9551\\u3002\\r\\n\\u9700\\u63d0\\u524d\\u9884\\u8ba2\\u8f66\\u6b21\\u65f6\\u95f4\\u8868\\u3002\\r\\n\\r\\n\\u5982\\u9884\\u8ba2\\u4eba\\u6570\\u6700\\u9ad8\\u4e3a4\\u4eba\\r\\n\\u60a8\\u53ef\\u4eab\\u53d7Chauffeur Drive\\u7684\\u5962\\u4f88\\u4f53\\u9a8c\\uff0c\\u65e0\\u9700\\u62c5\\u5fe7\\u8d2d\\u7269\\u65f6\\u95f4\\uff0c\\u4e13\\u8f66\\u968f\\u65f6\\u606d\\u5019\\u3002\\r\\n\\u8d77\\u59cb\\u4ef7\\u683c\\u4e3a\\u5355\\u7a0b223\\u82f1\\u9551\\uff0c\\u7531\\u4f26\\u6566\\u897f\\u65af\\u7f57\\u673a\\u573a\\u53ca\\u4f2f\\u660e\\u7ff0\\u673a\\u573a\\u51fa\\u53d1\\u3002\\r\\n\\u5982\\u9700\\u9884\\u8ba2\\uff0c\\u8bf7\\u8bbf\\u95ee\\u5982\\u4e0b\\u9875\\u9762\\uff1ahttps:\\/\\/www.bicestervillage.com\\/en\\/guest-services\\/chic-travel\\/shop\\/chauffeur-drive\\r\\n\\r\\n\\u5730\\u94c1\\u7ad9\\r\\n\\r\\nStratford-upon-Avon\\r\\nBirmingham Snow Hill\\r\\nBicester North\\r\\n\\r\\nStagecoach \\u63d0\\u4f9b\\u4ece\\u725b\\u6d25\\u5230\\u6bd4\\u65af\\u7279\\u6751\\u7684\\u516c\\u4ea4\\u8f66\\uff1aS5\\u8def\\u548cX5\\u8def\\u3002\\u8f66\\u6b21\\u5927\\u7ea630\\u5206\\u949f\\u4e00\\u73ed\\uff0c\\u4e0a\\u8f66\\u5730\\u70b9\\u4f4d\\u4e8e\\u725b\\u6d25\\u7684Magdalen Street\\u3002 \\u8be6\\u7ec6\\u4fe1\\u606f\\u53ef\\u4ee5\\u8054\\u7cfbStagecoach\\u3002\\r\\n\\u7535\\u8bdd\\uff1a+44(0) 1865 772 250\\r\\n\\u90ae\\u7bb1\\uff1ahttp:\\/\\/www.stagecoachbus.com\\r\\n\\r\\n"}', 'http://www.bicestervillage.com/en/home/home', '{"twitter":"https:\\/\\/twitter.com\\/bicestervillage","facebook":"https:\\/\\/www.facebook.com\\/BicesterVillage\\/timeline","instagram":"https:\\/\\/instagram.com\\/bicestervillage\\/","weibo":""}');

-- 
-- Dumping data for table `primary_ibd_travelsuite_brand_group`
-- 

INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (1, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (2, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (3, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (5, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (6, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (7, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (8, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (9, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (10, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (11, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (12, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (13, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (14, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (15, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (16, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (17, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (18, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (19, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (20, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (21, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (22, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (23, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (24, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (25, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (26, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (27, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (28, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (29, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (30, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (31, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (32, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (33, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (34, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (36, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (37, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (38, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (39, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (40, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (41, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (42, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (43, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (44, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (45, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (46, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (47, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (48, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (49, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (50, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (51, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (52, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (53, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (54, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (55, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (56, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (57, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (58, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (59, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (60, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (61, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (62, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (63, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (64, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (65, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (66, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (67, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (68, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (69, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (70, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (71, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (72, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (73, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (74, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (75, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (76, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (77, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (78, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (79, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (80, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (81, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (82, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (83, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (84, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (85, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (86, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (87, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (88, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (89, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (90, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (91, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (92, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (93, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (94, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (95, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (96, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (97, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (98, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (99, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (100, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (101, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (102, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (103, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (104, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (105, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (106, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (107, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (108, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (109, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (110, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (111, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (112, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (113, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (114, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (115, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (116, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (117, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (118, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (119, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (120, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (121, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (122, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (123, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (124, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (125, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (126, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (127, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (128, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (129, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (130, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (131, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (132, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (133, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (134, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (135, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (136, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (137, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (258, 1);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (4, 2);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (238, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (239, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (240, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (241, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (242, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (243, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (244, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (245, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (246, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (247, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (248, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (249, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (250, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (251, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (252, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (253, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (254, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (255, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (256, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (257, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (259, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (260, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (261, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (262, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (263, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (264, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (265, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (266, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (267, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (268, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (269, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (270, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (271, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (272, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (273, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (274, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (275, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (276, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (277, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (278, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (279, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (280, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (281, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (282, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (283, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (284, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (285, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (286, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (287, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (288, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (289, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (290, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (291, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (292, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (293, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (294, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (295, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (296, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (297, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (298, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (299, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (300, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (301, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (302, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (303, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (304, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (305, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (306, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (307, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (308, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (309, 3);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (3, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (342, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (345, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (347, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (349, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (351, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (352, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (354, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (355, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (356, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (359, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (360, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (362, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (363, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (364, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (366, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (367, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (369, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (371, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (372, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (373, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (374, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (376, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (377, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (379, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (380, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (381, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (382, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (383, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (384, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (385, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (386, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (387, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (388, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (389, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (390, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (391, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (392, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (393, 4);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (35, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (138, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (139, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (140, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (141, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (142, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (143, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (144, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (145, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (146, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (147, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (148, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (149, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (150, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (151, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (154, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (156, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (158, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (160, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (162, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (163, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (164, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (165, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (166, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (167, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (168, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (169, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (174, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (178, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (180, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (181, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (182, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (186, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (188, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (190, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (191, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (195, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (196, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (197, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (198, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (199, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (200, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (201, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (202, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (203, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (204, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (205, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (206, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (207, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (208, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (209, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (210, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (211, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (212, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (213, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (214, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (215, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (216, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (217, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (218, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (219, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (220, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (221, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (222, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (223, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (224, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (225, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (226, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (227, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (228, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (229, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (230, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (231, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (232, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (233, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (234, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (235, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (236, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (237, 5);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (310, 6);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (311, 6);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (312, 6);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (313, 6);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (314, 6);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (315, 6);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (316, 6);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (317, 6);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (318, 6);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (319, 6);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (320, 6);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (321, 6);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (322, 6);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (323, 6);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (324, 6);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (325, 6);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (326, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (327, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (328, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (329, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (330, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (331, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (332, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (333, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (334, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (335, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (336, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (337, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (338, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (339, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (340, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (341, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (343, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (344, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (346, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (348, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (350, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (353, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (357, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (358, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (361, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (365, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (368, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (370, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (375, 7);
INSERT INTO `primary_ibd_travelsuite_brand_group` VALUES (378, 7);

-- --------------------------------------------------------

-- 
-- Table structure for table `primary_ibd_travelsuite_brand_product`
-- 

-- CREATE TABLE `primary_ibd_travelsuite_brand_product` (
--   `rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Relation ID',
--   `brand_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Brand ID',
--   `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
--   `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
--   PRIMARY KEY (`rel_id`),
--   UNIQUE KEY `UNQ_IBD_IBD_TRAVELSUITE_BRAND_PRODUCT_BRAND_ID_PRODUCT_ID` (`brand_id`,`product_id`),
--   KEY `IDX_IBD_IBD_TRAVELSUITE_BRAND_PRODUCT_PRODUCT_ID` (`product_id`)
-- ) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Brand to Product Linkage Table' AUTO_INCREMENT=6 ;

-- 
-- Dumping data for table `primary_ibd_travelsuite_brand_product`
-- 

INSERT INTO `primary_ibd_travelsuite_brand_product` VALUES (3, 1, 1, 1);
INSERT INTO `primary_ibd_travelsuite_brand_product` VALUES (4, 4, 2, 0);
INSERT INTO `primary_ibd_travelsuite_brand_product` VALUES (5, 3, 3, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `primary_ibd_travelsuite_brand_store`
-- 

-- CREATE TABLE `primary_ibd_travelsuite_brand_store` (
--   `brand_id` int(11) NOT NULL COMMENT 'Brand ID',
--   `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
--   PRIMARY KEY (`brand_id`,`store_id`),
--   KEY `IDX_IBD_IBD_TRAVELSUITE_BRAND_STORE_STORE_ID` (`store_id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Brands To Store Linkage Table';

-- 
-- Dumping data for table `primary_ibd_travelsuite_brand_store`
-- 

INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (1, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (2, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (3, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (4, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (5, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (6, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (7, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (8, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (9, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (10, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (11, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (12, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (13, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (14, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (15, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (16, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (17, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (18, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (19, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (20, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (21, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (22, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (23, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (24, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (25, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (26, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (27, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (28, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (29, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (30, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (31, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (32, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (33, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (34, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (35, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (36, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (37, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (38, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (39, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (40, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (41, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (42, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (43, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (44, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (45, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (46, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (47, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (48, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (49, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (50, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (51, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (52, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (53, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (54, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (55, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (56, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (57, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (58, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (59, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (60, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (61, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (62, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (63, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (64, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (65, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (66, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (67, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (68, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (69, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (70, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (71, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (72, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (73, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (74, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (75, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (76, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (77, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (78, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (79, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (80, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (81, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (82, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (83, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (84, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (85, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (86, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (87, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (88, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (89, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (90, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (91, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (92, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (93, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (94, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (95, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (96, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (97, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (98, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (99, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (100, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (101, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (102, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (103, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (104, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (105, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (106, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (107, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (108, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (109, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (110, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (111, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (112, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (113, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (114, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (115, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (116, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (117, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (118, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (119, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (120, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (121, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (122, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (123, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (124, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (125, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (126, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (127, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (128, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (129, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (130, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (131, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (132, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (133, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (134, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (135, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (136, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (137, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (138, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (139, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (140, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (141, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (142, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (143, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (144, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (145, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (146, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (147, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (148, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (149, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (150, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (151, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (154, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (156, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (158, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (160, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (162, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (163, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (164, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (165, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (166, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (167, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (168, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (169, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (174, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (178, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (180, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (181, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (182, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (186, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (188, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (190, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (191, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (192, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (193, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (194, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (195, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (196, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (197, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (198, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (199, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (200, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (201, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (202, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (203, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (204, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (205, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (206, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (207, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (208, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (209, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (210, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (211, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (212, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (213, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (214, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (215, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (216, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (217, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (218, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (219, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (220, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (221, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (222, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (223, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (224, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (225, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (226, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (227, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (228, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (229, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (230, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (231, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (232, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (233, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (234, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (235, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (236, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (237, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (238, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (239, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (240, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (241, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (242, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (243, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (244, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (245, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (246, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (247, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (248, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (249, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (250, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (251, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (252, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (253, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (254, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (255, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (256, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (257, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (258, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (259, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (260, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (261, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (262, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (263, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (264, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (265, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (266, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (267, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (268, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (269, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (270, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (271, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (272, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (273, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (274, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (275, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (276, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (277, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (278, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (279, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (280, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (281, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (282, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (283, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (284, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (285, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (286, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (287, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (288, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (289, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (290, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (291, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (292, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (293, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (294, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (295, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (296, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (297, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (298, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (299, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (300, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (301, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (302, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (303, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (304, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (305, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (306, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (307, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (308, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (309, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (310, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (311, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (312, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (313, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (314, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (315, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (316, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (317, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (318, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (319, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (320, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (321, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (322, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (323, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (324, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (325, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (326, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (327, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (328, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (329, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (330, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (331, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (332, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (333, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (334, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (335, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (336, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (337, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (338, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (339, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (340, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (341, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (342, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (343, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (344, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (345, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (346, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (347, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (348, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (349, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (350, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (351, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (352, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (353, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (354, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (355, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (356, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (357, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (358, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (359, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (360, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (361, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (362, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (363, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (364, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (365, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (366, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (367, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (368, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (369, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (370, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (371, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (372, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (373, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (374, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (375, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (376, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (377, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (378, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (379, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (380, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (381, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (382, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (383, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (384, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (385, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (386, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (387, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (388, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (389, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (390, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (391, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (392, 1);
INSERT INTO `primary_ibd_travelsuite_brand_store` VALUES (393, 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `primary_ibd_travelsuite_continent`
-- 

-- CREATE TABLE `primary_ibd_travelsuite_continent` (
--   `entity_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Continent ID',
--   `name` varchar(255) NOT NULL COMMENT 'Name',
--   `status` smallint(6) DEFAULT NULL COMMENT 'Enabled',
--   `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Continent Modification Time',
--   `created_at` timestamp NULL DEFAULT NULL COMMENT 'Continent Creation Time',
--   PRIMARY KEY (`entity_id`)
-- ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Continent Table' AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `primary_ibd_travelsuite_continent`
-- 

INSERT INTO `primary_ibd_travelsuite_continent` VALUES (2, '欧洲', 1, '2015-05-11 02:43:16', '2015-05-11 02:43:16');

-- --------------------------------------------------------

-- 
-- Table structure for table `primary_ibd_travelsuite_country`
-- 

-- CREATE TABLE `primary_ibd_travelsuite_country` (
--   `entity_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Country ID',
--   `continent_id` int(10) unsigned DEFAULT NULL COMMENT 'Continent ID',
--   `name` varchar(255) NOT NULL COMMENT 'Name',
--   `status` smallint(6) DEFAULT NULL COMMENT 'Enabled',
--   `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Country Modification Time',
--   `created_at` timestamp NULL DEFAULT NULL COMMENT 'Country Creation Time',
--   PRIMARY KEY (`entity_id`),
--   KEY `IDX_IBD_IBD_TRAVELSUITE_CONTINENT_CONTINENT_ID` (`continent_id`)
-- ) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='Country Table' AUTO_INCREMENT=8 ;

-- 
-- Dumping data for table `primary_ibd_travelsuite_country`
-- 

INSERT INTO `primary_ibd_travelsuite_country` VALUES (7, 2, '英国', 1, '2015-05-11 03:01:46', '2015-05-11 03:01:46');

-- --------------------------------------------------------

-- 
-- Table structure for table `primary_ibd_travelsuite_gallery_image`
-- 

-- CREATE TABLE `primary_ibd_travelsuite_gallery_image` (
--   `entity_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Gallery Image ID',
--   `relation_id` int(10) unsigned NOT NULL COMMENT 'The ID of the relation that this image is for',
--   `relation_type` varchar(20) NOT NULL COMMENT 'The type of the relation that this image is for',
--   `image_path` varchar(255) NOT NULL COMMENT 'The image path',
--   `order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The order of the image',
--   `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Gallery Image Modification Time',
--   `created_at` timestamp NULL DEFAULT NULL COMMENT 'Gallery Image Creation Time',
--   PRIMARY KEY (`entity_id`),
--   KEY `IDX_IBD_IBD_TRAVELSUITE_GALLERY_IMAGE_RELATION_ID` (`relation_id`),
--   KEY `IDX_IBD_IBD_TRAVELSUITE_GALLERY_IMAGE_RELATION_TYPE` (`relation_type`),
--   KEY `IDX_IBD_IBD_TRAVELSUITE_GALLERY_IMAGE_ORDER` (`order`)
-- ) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='Gallery linkage table to any type' AUTO_INCREMENT=26 ;

-- 
-- Dumping data for table `primary_ibd_travelsuite_gallery_image`
-- 

INSERT INTO `primary_ibd_travelsuite_gallery_image` VALUES (2, 1, 'ibd_brand', '/b/r/browns-south-molton-street-shoes.jpg', 2, '2015-05-22 03:20:00', '2015-05-11 03:41:58');
INSERT INTO `primary_ibd_travelsuite_gallery_image` VALUES (3, 1, 'ibd_brand', '/b/r/browns-ladieswear.jpg', 3, '2015-05-22 03:20:00', '2015-05-11 03:41:58');
INSERT INTO `primary_ibd_travelsuite_gallery_image` VALUES (5, 3, 'ibd_group', '/l/d/ldo-guess.jpg', 1, '2015-05-20 03:46:28', '2015-05-13 08:31:49');
INSERT INTO `primary_ibd_travelsuite_gallery_image` VALUES (6, 1, 'ibd_group', '/s/h/shoppers-and-beadle.jpg', 1, '2015-05-28 05:00:25', '2015-05-13 08:55:52');
INSERT INTO `primary_ibd_travelsuite_gallery_image` VALUES (7, 1, 'ibd_group', '/c/h/chinese-shoppers-in-bond-street-21_1.jpg', 2, '2015-05-28 05:00:25', '2015-05-13 08:55:52');
INSERT INTO `primary_ibd_travelsuite_gallery_image` VALUES (8, 4, 'ibd_group', '/s/e/selfridges-mens-personal-shopping.jpg', 1, '2015-05-26 04:20:22', '2015-05-14 10:04:08');
INSERT INTO `primary_ibd_travelsuite_gallery_image` VALUES (9, 4, 'ibd_group', '/s/e/selfridges-personal-shopping.jpg', 2, '2015-05-26 04:20:23', '2015-05-14 10:04:08');
INSERT INTO `primary_ibd_travelsuite_gallery_image` VALUES (10, 4, 'ibd_group', '/f/i/file.jpg', 3, '2015-05-26 04:20:23', '2015-05-14 10:04:08');
INSERT INTO `primary_ibd_travelsuite_gallery_image` VALUES (11, 2, 'ibd_group', '/r/e/regent-street-london.jpg', 1, '2015-05-25 05:59:52', '2015-05-15 03:27:47');
INSERT INTO `primary_ibd_travelsuite_gallery_image` VALUES (12, 2, 'ibd_group', '/r/e/regent-street-summer-streets.jpg', 2, '2015-05-25 05:59:52', '2015-05-15 03:27:47');
INSERT INTO `primary_ibd_travelsuite_gallery_image` VALUES (13, 2, 'ibd_group', '/r/e/regent-street-night.jpg', 3, '2015-05-25 05:59:52', '2015-05-15 03:27:47');
INSERT INTO `primary_ibd_travelsuite_gallery_image` VALUES (14, 1, 'ibd_group', '/t/i/tiffany-bond-street_1.jpg', 3, '2015-05-28 05:00:25', '2015-05-20 08:01:41');
INSERT INTO `primary_ibd_travelsuite_gallery_image` VALUES (18, 2, 'ibd_brand', '/j/a/jaeger-regent-street_6.jpg', 1, '2015-05-25 04:32:44', '2015-05-20 09:31:32');
INSERT INTO `primary_ibd_travelsuite_gallery_image` VALUES (19, 1, 'ibd_brand', '/b/r/browns-brides_1.jpg', 3, '2015-05-22 03:20:00', '2015-05-20 10:06:04');
INSERT INTO `primary_ibd_travelsuite_gallery_image` VALUES (20, 258, 'ibd_brand', '/b/r/browns-continental-for-children.jpg', 1, '2015-05-26 09:53:02', '2015-05-26 09:49:49');
INSERT INTO `primary_ibd_travelsuite_gallery_image` VALUES (21, 258, 'ibd_brand', '/b/r/browns-restaurant-interior.jpg', 2, '2015-05-26 09:53:02', '2015-05-26 09:49:49');
INSERT INTO `primary_ibd_travelsuite_gallery_image` VALUES (22, 258, 'ibd_brand', '/b/r/browns-wine-collection.jpg', 3, '2015-05-26 09:53:02', '2015-05-26 09:49:49');
INSERT INTO `primary_ibd_travelsuite_gallery_image` VALUES (23, 7, 'ibd_group', '/b/i/bicester-village-british-designer-collection-store.jpg', 1, '2015-05-28 03:09:33', '2015-05-27 08:06:19');
INSERT INTO `primary_ibd_travelsuite_gallery_image` VALUES (24, 7, 'ibd_group', '/b/i/bicester-village-iphone-case.jpg', 2, '2015-05-28 03:09:34', '2015-05-27 08:06:19');
INSERT INTO `primary_ibd_travelsuite_gallery_image` VALUES (25, 7, 'ibd_group', '/b/i/bicester-village-gift-card.jpg', 3, '2015-05-28 03:09:34', '2015-05-27 08:06:19');

-- --------------------------------------------------------

-- 
-- Table structure for table `primary_ibd_travelsuite_group`
-- 

-- CREATE TABLE `primary_ibd_travelsuite_group` (
--   `entity_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Group ID',
--   `region_id` int(10) unsigned DEFAULT NULL COMMENT 'Region ID',
--   `name` varchar(255) NOT NULL COMMENT 'Name',
--   `summary` varchar(255) DEFAULT NULL COMMENT 'Summary',
--   `description` text COMMENT 'Detailed Description',
--   `html` text COMMENT 'Extra HTML',
--   `color` varchar(10) NOT NULL DEFAULT 'C5C1B5' COMMENT 'Highlight Color',
--   `image` varchar(255) DEFAULT NULL COMMENT 'Main Image',
--   `lat` decimal(8,5) DEFAULT NULL COMMENT 'Latitude',
--   `lng` decimal(8,5) DEFAULT NULL COMMENT 'Longitude',
--   `status` smallint(6) DEFAULT NULL COMMENT 'Enabled',
--   `url_key` varchar(255) DEFAULT NULL COMMENT 'URL key',
--   `in_rss` smallint(6) DEFAULT NULL COMMENT 'In RSS',
--   `meta_title` varchar(255) DEFAULT NULL COMMENT 'Meta title',
--   `meta_keywords` text COMMENT 'Meta keywords',
--   `meta_description` text COMMENT 'Meta description',
--   `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Group Modification Time',
--   `created_at` timestamp NULL DEFAULT NULL COMMENT 'Group Creation Time',
--   `fore_color` varchar(10) NOT NULL DEFAULT '333333',
--   `badge_name` varchar(255) DEFAULT NULL COMMENT 'The name of the group badge',
--   `badge_icon` varchar(255) DEFAULT NULL COMMENT 'The icon for the group badge',
--   `category` varchar(10) DEFAULT NULL COMMENT 'The group category',
--   `address` varchar(255) DEFAULT NULL COMMENT 'The written out physical address',
--   `directions` text COMMENT 'JSON object of directions e.g. walking, driving',
--   `official_url` varchar(255) DEFAULT NULL COMMENT 'Official Group URL',
--   `social` text COMMENT 'JSON object of social media links e.g. twitter, facebook',
--   PRIMARY KEY (`entity_id`),
--   KEY `IDX_IBD_IBD_TRAVELSUITE_REGION_REGION_ID` (`region_id`)
-- ) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='Group Table' AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `primary_ibd_travelsuite_region`
-- 

-- CREATE TABLE `primary_ibd_travelsuite_region` (
--   `entity_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Region ID',
--   `country_id` int(10) unsigned DEFAULT NULL COMMENT 'Country ID',
--   `name` varchar(255) NOT NULL COMMENT 'Name',
--   `summary` varchar(255) DEFAULT NULL COMMENT 'Summary',
--   `description` text COMMENT 'Description',
--   `color` varchar(10) NOT NULL DEFAULT 'C5C1B5' COMMENT 'Highlight Color',
--   `cover` varchar(255) DEFAULT NULL COMMENT 'Main Image',
--   `status` smallint(6) DEFAULT NULL COMMENT 'Enabled',
--   `url_key` varchar(255) DEFAULT NULL COMMENT 'URL key',
--   `in_rss` smallint(6) DEFAULT NULL COMMENT 'In RSS',
--   `meta_title` varchar(255) DEFAULT NULL COMMENT 'Meta title',
--   `meta_keywords` text COMMENT 'Meta keywords',
--   `meta_description` text COMMENT 'Meta description',
--   `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Region Modification Time',
--   `created_at` timestamp NULL DEFAULT NULL COMMENT 'Region Creation Time',
--   `fore_color` varchar(10) NOT NULL DEFAULT '333333',
--   PRIMARY KEY (`entity_id`),
--   KEY `IDX_IBD_IBD_TRAVELSUITE_COUNTRY_COUNTRY_ID` (`country_id`)
-- ) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Region Table' AUTO_INCREMENT=6 ;

-- 
-- Dumping data for table `primary_ibd_travelsuite_region`
-- 

INSERT INTO `primary_ibd_travelsuite_region` VALUES (4, 7, '伦敦', NULL, '<p>欢迎来到伦敦购物！伦敦是世界上著名的集购物，旅游，度假，休闲及商务于一体的城市。它拥有迷人的天际线，其中包括了大本钟，伦敦塔桥，摩天轮等著名景点。</p>', '000000', '/s/h/shop-london.jpg', 1, 'london', 1, NULL, NULL, NULL, '2015-05-22 03:34:47', '2015-05-11 03:03:07', 'FFF');
INSERT INTO `primary_ibd_travelsuite_region` VALUES (5, 7, '比斯特', NULL, NULL, '000000', NULL, 1, 'bicester', 1, '比斯特', '比斯特购物村, 设计师奥特莱斯', NULL, '2015-05-27 06:27:45', '2015-05-27 06:27:45', 'FFF');

-- --------------------------------------------------------

-- 
-- Table structure for table `primary_ibd_travelsuite_tag`
-- 

-- CREATE TABLE `primary_ibd_travelsuite_tag` (
--   `entity_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Gallery Image ID',
--   `tag` varchar(255) NOT NULL COMMENT 'The tag text itself',
--   PRIMARY KEY (`entity_id`),
--   UNIQUE KEY `UNQ_IBD_IBD_TRAVELSUITE_GALLERY_IMAGE_TAG` (`tag`)
-- ) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='Text tags' AUTO_INCREMENT=26 ;

-- 
-- Dumping data for table `primary_ibd_travelsuite_tag`
-- 

INSERT INTO `primary_ibd_travelsuite_tag` VALUES (2, '买手店');
INSERT INTO `primary_ibd_travelsuite_tag` VALUES (25, '儿童');
INSERT INTO `primary_ibd_travelsuite_tag` VALUES (22, '其它');
INSERT INTO `primary_ibd_travelsuite_tag` VALUES (24, '内衣');
INSERT INTO `primary_ibd_travelsuite_tag` VALUES (16, '化妆品');
INSERT INTO `primary_ibd_travelsuite_tag` VALUES (7, '女士鞋履');
INSERT INTO `primary_ibd_travelsuite_tag` VALUES (1, '女装');
INSERT INTO `primary_ibd_travelsuite_tag` VALUES (15, '女鞋');
INSERT INTO `primary_ibd_travelsuite_tag` VALUES (9, '时装');
INSERT INTO `primary_ibd_travelsuite_tag` VALUES (8, '珠宝首饰');
INSERT INTO `primary_ibd_travelsuite_tag` VALUES (13, '电子产品');
INSERT INTO `primary_ibd_travelsuite_tag` VALUES (6, '男士鞋履');
INSERT INTO `primary_ibd_travelsuite_tag` VALUES (5, '男装');
INSERT INTO `primary_ibd_travelsuite_tag` VALUES (17, '眼镜');
INSERT INTO `primary_ibd_travelsuite_tag` VALUES (21, '礼品');
INSERT INTO `primary_ibd_travelsuite_tag` VALUES (3, '设计师品牌');
INSERT INTO `primary_ibd_travelsuite_tag` VALUES (19, '运动');
INSERT INTO `primary_ibd_travelsuite_tag` VALUES (14, '配件');
INSERT INTO `primary_ibd_travelsuite_tag` VALUES (18, '配饰');
INSERT INTO `primary_ibd_travelsuite_tag` VALUES (23, '酒店');
INSERT INTO `primary_ibd_travelsuite_tag` VALUES (10, '酒类');
INSERT INTO `primary_ibd_travelsuite_tag` VALUES (4, '钟表珠宝');
INSERT INTO `primary_ibd_travelsuite_tag` VALUES (11, '鞋履');
INSERT INTO `primary_ibd_travelsuite_tag` VALUES (20, '餐饮');
INSERT INTO `primary_ibd_travelsuite_tag` VALUES (12, '饰品');

-- --------------------------------------------------------

-- 
-- Table structure for table `primary_ibd_travelsuite_tag_relation`
-- 

-- CREATE TABLE `primary_ibd_travelsuite_tag_relation` (
--   `tag_id` int(11) NOT NULL COMMENT 'The ID of the tag',
--   `relation_id` int(10) unsigned NOT NULL COMMENT 'The ID of the relation',
--   `relation_type` varchar(255) NOT NULL COMMENT 'The type of the relation',
--   UNIQUE KEY `432C6D0B61BC9DAED908250F7FFC6B72` (`tag_id`,`relation_id`,`relation_type`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Text tags';

-- 
-- Dumping data for table `primary_ibd_travelsuite_tag_relation`
-- 

INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (1, 1, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (1, 141, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (1, 145, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (1, 150, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (1, 151, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (1, 156, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (1, 162, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (1, 163, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (1, 165, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (1, 166, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (1, 197, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (1, 203, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (1, 216, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (1, 218, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (1, 220, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (1, 232, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (1, 312, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (1, 332, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (1, 353, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (1, 358, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (1, 365, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (2, 1, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (3, 1, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (4, 164, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (4, 222, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (5, 49, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (5, 154, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (5, 165, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (5, 169, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (5, 178, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (5, 195, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (5, 201, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (5, 221, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (5, 228, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (5, 322, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (5, 328, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (5, 335, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (5, 350, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (6, 168, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (6, 170, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (6, 171, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (6, 172, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (6, 173, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (6, 175, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (6, 209, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (6, 227, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (7, 35, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (7, 176, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (7, 177, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (7, 179, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (7, 212, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (7, 213, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (7, 215, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (7, 224, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (7, 233, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (7, 319, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (8, 144, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (8, 146, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (8, 148, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (8, 149, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (8, 167, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (8, 182, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (8, 188, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (8, 191, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (8, 196, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (8, 205, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (8, 210, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (8, 211, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (8, 214, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (8, 226, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (8, 229, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (8, 230, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (8, 333, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 143, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 147, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 174, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 180, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 181, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 186, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 189, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 198, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 204, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 223, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 240, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 260, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 261, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 269, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 276, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 277, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 278, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 279, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 280, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 286, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 287, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 289, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 290, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 291, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 297, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 310, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 311, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 313, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 314, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 315, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 317, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 320, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 321, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 323, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 324, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 325, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 327, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 331, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 334, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 336, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 339, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 342, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 343, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 345, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 346, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 348, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 351, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 352, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 361, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 363, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 368, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 375, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 378, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 385, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 386, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (9, 389, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (10, 139, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (10, 207, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (11, 140, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (11, 142, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (11, 158, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (11, 160, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (11, 234, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (11, 235, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (11, 242, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (11, 247, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (11, 249, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (11, 263, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (11, 284, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (11, 285, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (11, 295, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (11, 316, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (11, 318, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (11, 370, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (12, 190, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (13, 199, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (13, 380, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (13, 384, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (13, 392, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (14, 200, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (14, 206, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (14, 208, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (14, 330, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (14, 337, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (14, 338, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (14, 369, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (15, 202, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (16, 217, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (16, 354, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (16, 364, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (16, 381, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (16, 383, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (16, 387, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (17, 219, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (18, 225, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (18, 236, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (18, 237, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (19, 231, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (19, 239, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (19, 241, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (19, 243, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (19, 248, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (19, 250, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (19, 262, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (19, 264, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (19, 265, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (19, 372, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (19, 373, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (19, 374, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (19, 376, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (20, 238, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (20, 254, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (20, 256, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (20, 257, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (20, 266, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (20, 267, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (20, 281, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (20, 282, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (20, 283, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (20, 288, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (20, 298, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (20, 299, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (20, 300, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (20, 301, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (20, 302, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (20, 303, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (20, 304, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (20, 305, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (20, 306, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (20, 307, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (20, 309, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (20, 329, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (20, 357, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (21, 244, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (21, 245, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (21, 246, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (21, 268, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (21, 274, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (21, 275, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (21, 294, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (21, 296, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 251, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 252, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 253, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 255, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 259, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 270, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 271, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 272, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 273, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 292, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 293, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 308, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 340, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 341, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 347, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 349, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 355, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 356, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 359, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 360, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 362, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 366, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 367, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 371, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 377, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 379, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 382, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 388, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 390, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 391, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (22, 393, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (23, 258, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (24, 326, 'ibd_brand');
INSERT INTO `primary_ibd_travelsuite_tag_relation` VALUES (24, 344, 'ibd_brand');

-- 
-- Constraints for dumped tables
-- 

-- 
-- Constraints for table `primary_ibd_homepagebanner_advertisement_relation`
-- 
-- ALTER TABLE `primary_ibd_homepagebanner_advertisement_relation`
--   ADD CONSTRAINT `FK_405260A3F4056FFE32E2191BA54D1027` FOREIGN KEY (`advertisement_id`) REFERENCES `primary_ibd_homepagebanner_advertisement` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `primary_ibd_homepagebanner_advertisement_store`
-- 
-- ALTER TABLE `primary_ibd_homepagebanner_advertisement_store`
--   ADD CONSTRAINT `FK_6BAEAD8E9652A1F138A7CAD262F38D84` FOREIGN KEY (`store_id`) REFERENCES `ibd_core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
--   ADD CONSTRAINT `FK_8C28109A32699C348AD0A7DE706DC57E` FOREIGN KEY (`advertisement_id`) REFERENCES `primary_ibd_homepagebanner_advertisement` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `primary_ibd_homepagebanner_bannertile_store`
-- 
-- ALTER TABLE `primary_ibd_homepagebanner_bannertile_store`
--   ADD CONSTRAINT `FK_1BC1001D652F67E2648F7A23E8753815` FOREIGN KEY (`store_id`) REFERENCES `ibd_core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
--   ADD CONSTRAINT `FK_4387A14D2EA3CE4D6D52EF5931C73704` FOREIGN KEY (`bannertile_id`) REFERENCES `primary_ibd_homepagebanner_bannertile` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `primary_ibd_travelsuite_brand_group`
-- 
-- ALTER TABLE `primary_ibd_travelsuite_brand_group`
--   ADD CONSTRAINT `FK_33E3690D98774F18DBAE3CA1898A5653` FOREIGN KEY (`brand_id`) REFERENCES `primary_ibd_travelsuite_brand` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
--   ADD CONSTRAINT `FK_51868B16922B9CB99B152B3DDF307A02` FOREIGN KEY (`group_id`) REFERENCES `primary_ibd_travelsuite_group` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `primary_ibd_travelsuite_brand_product`
-- 
-- ALTER TABLE `primary_ibd_travelsuite_brand_product`
--   ADD CONSTRAINT `FK_823C3505C5F983B2F98EA11EF4B41F7F` FOREIGN KEY (`brand_id`) REFERENCES `primary_ibd_travelsuite_brand` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
--   ADD CONSTRAINT `FK_IBD_IBD_TRAVELSUITE_BRAND_PRD_PRD_ID_IBD_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `ibd_catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `primary_ibd_travelsuite_brand_store`
-- 
-- ALTER TABLE `primary_ibd_travelsuite_brand_store`
--   ADD CONSTRAINT `FK_95578967B35CA60187CF99FF70491FAE` FOREIGN KEY (`brand_id`) REFERENCES `primary_ibd_travelsuite_brand` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
--   ADD CONSTRAINT `FK_BF017D697533D19879D10FA5269F85ED` FOREIGN KEY (`store_id`) REFERENCES `ibd_core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `primary_ibd_travelsuite_tag_relation`
-- 
-- ALTER TABLE `primary_ibd_travelsuite_tag_relation`
--   ADD CONSTRAINT `FK_D3C639E9C5616854248AFEC8D7C16BE1` FOREIGN KEY (`tag_id`) REFERENCES `primary_ibd_travelsuite_tag` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE;

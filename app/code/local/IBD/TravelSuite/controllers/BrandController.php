<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Brand front contrller
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_BrandController extends Mage_Core_Controller_Front_Action
{

    /**
      * default action
      *
      * @access public
      * @return void
      * @author Ultimate Module Creator
      */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('checkout/session');
        if (Mage::helper('ibd_travelsuite/brand')->getUseBreadcrumbs()) {
            if ($breadcrumbBlock = $this->getLayout()->getBlock('breadcrumbs')) {
                $breadcrumbBlock->addCrumb(
                    'home',
                    array(
                        'label' => Mage::helper('ibd_travelsuite')->__('Home'),
                        'link'  => Mage::getUrl(),
                    )
                );
                $breadcrumbBlock->addCrumb(
                    'brands',
                    array(
                        'label' => Mage::helper('ibd_travelsuite')->__('Brands'),
                        'link'  => '',
                    )
                );
            }
        }
        $headBlock = $this->getLayout()->getBlock('head');
        if ($headBlock) {
            $headBlock->addLinkRel('canonical', Mage::helper('ibd_travelsuite/brand')->getBrandsUrl());
        }
        if ($headBlock) {
            $headBlock->setTitle(Mage::getStoreConfig('ibd_travelsuite/brand/meta_title'));
            $headBlock->setKeywords(Mage::getStoreConfig('ibd_travelsuite/brand/meta_keywords'));
            $headBlock->setDescription(Mage::getStoreConfig('ibd_travelsuite/brand/meta_description'));
        }
        $this->renderLayout();
    }

    /**
     * init Brand
     *
     * @access protected
     * @return IBD_TravelSuite_Model_Brand
     * @author Ultimate Module Creator
     */
    protected function _initBrand()
    {
        $brandId   = $this->getRequest()->getParam('id', 0);
        $brand     = Mage::getModel('ibd_travelsuite/brand')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load($brandId);
        if (!$brand->getId()) {
            return false;
        }
        return $brand;
    }

    /**
     * view brand action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function viewAction()
    {
        $brand = $this->_initBrand();
        if (!$brand) {
            $this->_forward('no-route');
            return;
        }
        Mage::register('current_brand', $brand);
        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('checkout/session');
        if ($root = $this->getLayout()->getBlock('root')) {
            $root->addBodyClass('travelsuite-brand travelsuite-brand' . $brand->getId());
        }
        if (Mage::helper('ibd_travelsuite/brand')->getUseBreadcrumbs()) {
            if ($breadcrumbBlock = $this->getLayout()->getBlock('breadcrumbs')) {
                $breadcrumbBlock->addCrumb(
                    'home',
                    array(
                        'label'    => Mage::helper('ibd_travelsuite')->__('Home'),
                        'link'     => Mage::getUrl(),
                    )
                );
                $breadcrumbBlock->addCrumb(
                    'brands',
                    array(
                        'label' => Mage::helper('ibd_travelsuite')->__('Brands'),
                        'link'  => Mage::helper('ibd_travelsuite/brand')->getBrandsUrl(),
                    )
                );
                $breadcrumbBlock->addCrumb(
                    'brand',
                    array(
                        'label' => $brand->getName(),
                        'link'  => '',
                    )
                );
            }
        }
        $headBlock = $this->getLayout()->getBlock('head');
        if ($headBlock) {
            $headBlock->addLinkRel('canonical', $brand->getBrandUrl());
        }
        if ($headBlock) {
            if ($brand->getMetaTitle()) {
                $headBlock->setTitle($brand->getMetaTitle());
            } else {
                $headBlock->setTitle($brand->getName());
            }
            $headBlock->setKeywords($brand->getMetaKeywords());
            $headBlock->setDescription($brand->getMetaDescription());
        }
        $this->renderLayout();
    }

    /**
     * brands rss list action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function rssAction()
    {
        if (Mage::helper('ibd_travelsuite/brand')->isRssEnabled()) {
            $this->getResponse()->setHeader('Content-type', 'text/xml; charset=UTF-8');
            $this->loadLayout(false);
            $this->renderLayout();
        } else {
            $this->getResponse()->setHeader('HTTP/1.1', '404 Not Found');
            $this->getResponse()->setHeader('Status', '404 File not found');
            $this->_forward('nofeed', 'index', 'rss');
        }
    }
}

<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Brand front contrller
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_SearchController extends Mage_Core_Controller_Front_Action
{
    /**
      * default action
      *
      * @access public
      * @return void
      * @author Ultimate Module Creator
      */
    public function indexAction($type=null)
    {
        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('checkout/session');

        $headBlock = $this->getLayout()->getBlock('head');
        if ($headBlock) {
            $helper = Mage::helper('ibd_travelsuite');
            $headBlock->setTitle($helper->__('IBD Travel Search Results'));
            $headBlock->setKeywords($helper->__('Travel Search Brands'));
            $headBlock->setDescription($helper->__('Travel Search Results'));
        }

        $limit = $this->getRequest()->getParam('limit', 3);
        $offset = $this->getRequest()->getParam('offset', 0);

        Mage::helper('ibd_travelsuite/search')->filterResults($type, $this->getRequest(), $limit, $offset);
        $this->renderLayout();
    }

    /**
     * search brands action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function brandAction()
    {
        Mage::app()->getFrontController()->getResponse()->setRedirect('/brands?'.
            http_build_query($this->getRequest()->getParams()));
    }

    /**
     * search groups action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function groupAction()
    {
        $this->indexAction('group');
    }

    /**
     * search regions action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function regionAction()
    {
        $this->indexAction('region');
    }
}

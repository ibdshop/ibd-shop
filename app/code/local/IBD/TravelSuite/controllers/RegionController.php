<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Region front contrller
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_RegionController extends Mage_Core_Controller_Front_Action
{

    /**
      * default action
      *
      * @access public
      * @return void
      * @author Ultimate Module Creator
      */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('checkout/session');
        if (Mage::helper('ibd_travelsuite/region')->getUseBreadcrumbs()) {
            if ($breadcrumbBlock = $this->getLayout()->getBlock('breadcrumbs')) {
                $breadcrumbBlock->addCrumb(
                    'home',
                    array(
                        'label' => Mage::helper('ibd_travelsuite')->__('Home'),
                        'link'  => Mage::getUrl(),
                    )
                );
                $breadcrumbBlock->addCrumb(
                    'regions',
                    array(
                        'label' => Mage::helper('ibd_travelsuite')->__('Regions'),
                        'link'  => '',
                    )
                );
            }
        }
        $headBlock = $this->getLayout()->getBlock('head');
        if ($headBlock) {
            $headBlock->addLinkRel('canonical', Mage::helper('ibd_travelsuite/region')->getRegionsUrl());
        }
        if ($headBlock) {
            $headBlock->setTitle(Mage::getStoreConfig('ibd_travelsuite/region/meta_title'));
            $headBlock->setKeywords(Mage::getStoreConfig('ibd_travelsuite/region/meta_keywords'));
            $headBlock->setDescription(Mage::getStoreConfig('ibd_travelsuite/region/meta_description'));
        }
        $this->renderLayout();
    }

    /**
     * init Region
     *
     * @access protected
     * @return IBD_TravelSuite_Model_Region
     * @author Ultimate Module Creator
     */
    protected function _initRegion()
    {
        $regionId   = $this->getRequest()->getParam('id', 0);
        $region     = Mage::getModel('ibd_travelsuite/region')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load($regionId);
        if (!$region->getId()) {
            return false;
        }
        return $region;
    }

    /**
     * view region action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function viewAction()
    {
        $region = $this->_initRegion();
        if (!$region) {
            $this->_forward('no-route');
            return;
        }
        Mage::register('current_region', $region);
        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('checkout/session');
        if ($root = $this->getLayout()->getBlock('root')) {
            $root->addBodyClass('travelsuite-region travelsuite-region' . $region->getId());
        }
        if (Mage::helper('ibd_travelsuite/region')->getUseBreadcrumbs()) {
            if ($breadcrumbBlock = $this->getLayout()->getBlock('breadcrumbs')) {
                $breadcrumbBlock->addCrumb(
                    'home',
                    array(
                        'label'    => Mage::helper('ibd_travelsuite')->__('Home'),
                        'link'     => Mage::getUrl(),
                    )
                );
                $breadcrumbBlock->addCrumb(
                    'regions',
                    array(
                        'label' => Mage::helper('ibd_travelsuite')->__('Regions'),
                        'link'  => Mage::helper('ibd_travelsuite/region')->getRegionsUrl(),
                    )
                );
                $breadcrumbBlock->addCrumb(
                    'region',
                    array(
                        'label' => $region->getName(),
                        'link'  => '',
                    )
                );
            }
        }
        $headBlock = $this->getLayout()->getBlock('head');
        if ($headBlock) {
            $headBlock->addLinkRel('canonical', $region->getRegionUrl());
        }
        if ($headBlock) {
            if ($region->getMetaTitle()) {
                $headBlock->setTitle($region->getMetaTitle());
            } else {
                $headBlock->setTitle($region->getName());
            }
            $headBlock->setKeywords($region->getMetaKeywords());
            $headBlock->setDescription($region->getMetaDescription());
        }
        $this->renderLayout();
    }

    /**
     * regions rss list action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function rssAction()
    {
        if (Mage::helper('ibd_travelsuite/region')->isRssEnabled()) {
            $this->getResponse()->setHeader('Content-type', 'text/xml; charset=UTF-8');
            $this->loadLayout(false);
            $this->renderLayout();
        } else {
            $this->getResponse()->setHeader('HTTP/1.1', '404 Not Found');
            $this->getResponse()->setHeader('Status', '404 File not found');
            $this->_forward('nofeed', 'index', 'rss');
        }
    }
}

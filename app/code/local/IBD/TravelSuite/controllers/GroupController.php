<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Group front contrller
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_GroupController extends Mage_Core_Controller_Front_Action
{

    /**
     * init Group
     *
     * @access protected
     * @return IBD_TravelSuite_Model_Group
     * @author Ultimate Module Creator
     */
    protected function _initGroup()
    {
        $groupId   = $this->getRequest()->getParam('id', 0);
        $group     = Mage::getModel('ibd_travelsuite/group')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load($groupId);
        if (!$group->getId()) {
            return false;
        }
        return $group;
    }

    /**
     * view group action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function viewAction()
    {
        $group = $this->_initGroup();
        if (!$group) {
            $this->_forward('no-route');
            return;
        }
        Mage::register('current_group', $group);
        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('checkout/session');
        if ($root = $this->getLayout()->getBlock('root')) {
            $root->addBodyClass('travelsuite-group travelsuite-group' . $group->getId());
        }
        if (Mage::helper('ibd_travelsuite/group')->getUseBreadcrumbs()) {
            if ($breadcrumbBlock = $this->getLayout()->getBlock('breadcrumbs')) {
                $breadcrumbBlock->addCrumb(
                    'home',
                    array(
                        'label'    => Mage::helper('ibd_travelsuite')->__('Home'),
                        'link'     => Mage::getUrl(),
                    )
                );
                $breadcrumbBlock->addCrumb(
                    'group',
                    array(
                        'label' => $group->getName(),
                        'link'  => '',
                    )
                );
            }
        }
        $headBlock = $this->getLayout()->getBlock('head');
        if ($headBlock) {
            $headBlock->addLinkRel('canonical', $group->getGroupUrl());
        }
        if ($headBlock) {
            if ($group->getMetaTitle()) {
                $headBlock->setTitle($group->getMetaTitle());
            } else {
                $headBlock->setTitle($group->getName());
            }
            $headBlock->setKeywords($group->getMetaKeywords());
            $headBlock->setDescription($group->getMetaDescription());
        }
        $this->renderLayout();
    }

    /**
     * groups rss list action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function rssAction()
    {
        if (Mage::helper('ibd_travelsuite/group')->isRssEnabled()) {
            $this->getResponse()->setHeader('Content-type', 'text/xml; charset=UTF-8');
            $this->loadLayout(false);
            $this->renderLayout();
        } else {
            $this->getResponse()->setHeader('HTTP/1.1', '404 Not Found');
            $this->getResponse()->setHeader('Status', '404 File not found');
            $this->_forward('nofeed', 'index', 'rss');
        }
    }
}

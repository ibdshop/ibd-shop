<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Region admin controller
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Adminhtml_Travelsuite_RegionController extends IBD_TravelSuite_Controller_Adminhtml_TravelSuite
{
    /**
     * init the region
     *
     * @access protected
     * @return IBD_TravelSuite_Model_Region
     */
    protected function _initRegion()
    {
        $regionId  = (int) $this->getRequest()->getParam('id');
        $region    = Mage::getModel('ibd_travelsuite/region');
        if ($regionId) {
            $region->load($regionId);
        }
        Mage::register('current_region', $region);
        return $region;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('ibd_travelsuite')->__('Travel Suite'))
             ->_title(Mage::helper('ibd_travelsuite')->__('Regions'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit region - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function editAction()
    {
        $regionId    = $this->getRequest()->getParam('id');
        $region      = $this->_initRegion();
        if ($regionId && !$region->getId()) {
            $this->_getSession()->addError(
                Mage::helper('ibd_travelsuite')->__('This region no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getRegionData(true);
        if (!empty($data)) {
            $region->setData($data);
        }
        Mage::register('region_data', $region);
        $this->loadLayout();
        $this->_title(Mage::helper('ibd_travelsuite')->__('Travel Suite'))
             ->_title(Mage::helper('ibd_travelsuite')->__('Regions'));
        if ($region->getId()) {
            $this->_title($region->getName());
        } else {
            $this->_title(Mage::helper('ibd_travelsuite')->__('Add region'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new region action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save region - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('region')) {
            try {
                $region = $this->_initRegion();
                $region->addData($data);
                $coverName = $this->_uploadAndGetName(
                    'cover',
                    Mage::helper('ibd_travelsuite/region_image')->getImageBaseDir(),
                    $data
                );
                $region->setData('cover', $coverName);

                // save region title icon/logo
                $titleIconName = $this->_uploadAndGetName(
                    'banner_icon',
                    Mage::helper('ibd_travelsuite/title_image')->getImageBaseDir(),
                    $data
                );
                $region->setData('banner_icon', $titleIconName);

                $region->save();
                if(isset($data['adids'])) $region->upsertAdvertisementIds($data['adids']);
                else $region->upsertAdvertisementIds(array());
                if(isset($data['gallery'])) Mage::getSingleton('ibd_travelsuite/gallery')->updateImagePositions($data['gallery']);
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ibd_travelsuite')->__('Region was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $region->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                if (isset($data['cover']['value'])) {
                    $data['cover'] = $data['cover']['value'];
                }
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setRegionData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                if (isset($data['cover']['value'])) {
                    $data['cover'] = $data['cover']['value'];
                }
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_travelsuite')->__('There was a problem saving the region.')
                );
                Mage::getSingleton('adminhtml/session')->setRegionData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('ibd_travelsuite')->__('Unable to find region to save.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * delete region - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function deleteAction()
    {
        if ( $this->getRequest()->getParam('id') > 0) {
            try {
                $region = Mage::getModel('ibd_travelsuite/region');
                $region->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ibd_travelsuite')->__('Region was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_travelsuite')->__('There was an error deleting region.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('ibd_travelsuite')->__('Could not find region to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete region - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeleteAction()
    {
        $regionIds = $this->getRequest()->getParam('region');
        if (!is_array($regionIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('ibd_travelsuite')->__('Please select regions to delete.')
            );
        } else {
            try {
                foreach ($regionIds as $regionId) {
                    $region = Mage::getModel('ibd_travelsuite/region');
                    $region->setId($regionId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ibd_travelsuite')->__('Total of %d regions were successfully deleted.', count($regionIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_travelsuite')->__('There was an error deleting regions.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massStatusAction()
    {
        $regionIds = $this->getRequest()->getParam('region');
        if (!is_array($regionIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('ibd_travelsuite')->__('Please select regions.')
            );
        } else {
            try {
                foreach ($regionIds as $regionId) {
                $region = Mage::getSingleton('ibd_travelsuite/region')->load($regionId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d regions were successfully updated.', count($regionIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_travelsuite')->__('There was an error updating regions.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass country change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massCountryIdAction()
    {
        $regionIds = $this->getRequest()->getParam('region');
        if (!is_array($regionIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('ibd_travelsuite')->__('Please select regions.')
            );
        } else {
            try {
                foreach ($regionIds as $regionId) {
                $region = Mage::getSingleton('ibd_travelsuite/region')->load($regionId)
                    ->setCountryId($this->getRequest()->getParam('flag_country_id'))
                    ->setIsMassupdate(true)
                    ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d regions were successfully updated.', count($regionIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_travelsuite')->__('There was an error updating regions.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportCsvAction()
    {
        $fileName   = 'region.csv';
        $content    = $this->getLayout()->createBlock('ibd_travelsuite/adminhtml_region_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportExcelAction()
    {
        $fileName   = 'region.xls';
        $content    = $this->getLayout()->createBlock('ibd_travelsuite/adminhtml_region_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportXmlAction()
    {
        $fileName   = 'region.xml';
        $content    = $this->getLayout()->createBlock('ibd_travelsuite/adminhtml_region_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Ultimate Module Creator
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('ibd_travelsuite/region');
    }
}

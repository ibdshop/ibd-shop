<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Brand - product controller
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
require_once ("Mage/Adminhtml/controllers/Catalog/ProductController.php");
class IBD_TravelSuite_Adminhtml_Travelsuite_Brand_Catalog_ProductController extends Mage_Adminhtml_Catalog_ProductController
{
    /**
     * construct
     *
     * @access protected
     * @return void
     * @author Ultimate Module Creator
     */
    protected function _construct()
    {
        // Define module dependent translate
        $this->setUsedModuleName('IBD_TravelSuite');
    }

    /**
     * brands in the catalog page
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function brandsAction()
    {
        $this->_initProduct();
        $this->loadLayout();
        $this->getLayout()->getBlock('product.edit.tab.brand')
            ->setProductBrands($this->getRequest()->getPost('product_brands', null));
        $this->renderLayout();
    }

    /**
     * brands grid in the catalog page
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function brandsGridAction()
    {
        $this->_initProduct();
        $this->loadLayout();
        $this->getLayout()->getBlock('product.edit.tab.brand')
            ->setProductBrands($this->getRequest()->getPost('product_brands', null));
        $this->renderLayout();
    }
}

<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Travel Suite Widgets admin controller
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */

require_once('Mage/Widget/controllers/Adminhtml/Widget/InstanceController.php');

class IBD_TravelSuite_Adminhtml_TravelSuite_WidgetController extends Mage_Widget_Adminhtml_Widget_InstanceController
{
    public function brandsAction()
    {
        $selected = $this->getRequest()->getParam('selected', '');
        /* @var $chooser Envalo_Widget_Block_Cms_Page_Chooser */
        $chooser = $this->getLayout()
            ->createBlock('ibd_travelsuite/adminhtml_cms_widget_brand_chooser');

        $chooser->setName(Mage::helper('core')->uniqHash('products_grid_'))
            ->setUseMassaction(true)
            ->setSelectedProducts(explode(',', $selected));
        /* @var $serializer Mage_Adminhtml_Block_Widget_Grid_Serializer */
        $serializer = $this->getLayout()->createBlock('adminhtml/widget_grid_serializer');
        $serializer->initSerializerBlock($chooser, 'getSelectedProducts', 'selected_products', 'selected_products');
        $body = $chooser->toHtml().$serializer->toHtml();
        Mage::getSingleton('core/translate_inline')->processResponseBody($body);
        $this->getResponse()->setBody($body);
    }

    public function brandschooserAction()
    {
        $uniqId = $this->getRequest()->getParam('uniq_id');
        $massAction = $this->getRequest()->getParam('use_massaction', false);
        /* @var $brandsGrid Envalo_Widget_Block_Cms_Page_Chooser */
        $brandsGrid = $this->getLayout()->createBlock('ibd_travelsuite/adminhtml_cms_widget_brand_chooser', '', array(
            'id'                => $uniqId,
            'use_massaction'    => $massAction
        ));

        $html = $brandsGrid->toHtml();

        $this->getResponse()->setBody($html);
    }

    public function groupsAction()
    {
        $selected = $this->getRequest()->getParam('selected', '');
        /* @var $chooser Envalo_Widget_Block_Cms_Page_Chooser */
        $chooser = $this->getLayout()
            ->createBlock('ibd_travelsuite/adminhtml_cms_widget_group_chooser');

        $chooser->setName(Mage::helper('core')->uniqHash('products_grid_'))
            ->setUseMassaction(true)
            ->setSelectedGroups(explode(',', $selected));
        /* @var $serializer Mage_Adminhtml_Block_Widget_Grid_Serializer */
        $serializer = $this->getLayout()->createBlock('adminhtml/widget_grid_serializer');
        $serializer->initSerializerBlock($chooser, 'getSelectedProducts', 'selected_products', 'selected_products');
        $body = $chooser->toHtml().$serializer->toHtml();
        Mage::getSingleton('core/translate_inline')->processResponseBody($body);
        $this->getResponse()->setBody($body);
    }

    public function groupschooserAction()
    {
        $uniqId = $this->getRequest()->getParam('uniq_id');
        $massAction = $this->getRequest()->getParam('use_massaction', false);
        /* @var $groupsGrid Envalo_Widget_Block_Cms_Page_Chooser */
        $groupsGrid = $this->getLayout()->createBlock('ibd_travelsuite/adminhtml_cms_widget_group_chooser', '', array(
            'id'                => $uniqId,
            'use_massaction'    => $massAction
        ));

        $html = $groupsGrid->toHtml();

        $this->getResponse()->setBody($html);
    }

    public function regionsAction()
    {
        $selected = $this->getRequest()->getParam('selected', '');
        /* @var $chooser Envalo_Widget_Block_Cms_Page_Chooser */
        $chooser = $this->getLayout()
            ->createBlock('ibd_travelsuite/adminhtml_cms_widget_region_chooser');

        $chooser->setName(Mage::helper('core')->uniqHash('products_grid_'))
            ->setUseMassaction(true)
            ->setSelectedRegions(explode(',', $selected));
        /* @var $serializer Mage_Adminhtml_Block_Widget_Grid_Serializer */
        $serializer = $this->getLayout()->createBlock('adminhtml/widget_grid_serializer');
        $serializer->initSerializerBlock($chooser, 'getSelectedProducts', 'selected_products', 'selected_products');
        $body = $chooser->toHtml().$serializer->toHtml();
        Mage::getSingleton('core/translate_inline')->processResponseBody($body);
        $this->getResponse()->setBody($body);
    }

    public function regionschooserAction()
    {
        $uniqId = $this->getRequest()->getParam('uniq_id');
        $massAction = $this->getRequest()->getParam('use_massaction', false);
        /* @var $regionsGrid Envalo_Widget_Block_Cms_Page_Chooser */
        $regionsGrid = $this->getLayout()->createBlock('ibd_travelsuite/adminhtml_cms_widget_region_chooser', '', array(
            'id'                => $uniqId,
            'use_massaction'    => $massAction
        ));

        $html = $regionsGrid->toHtml();

        $this->getResponse()->setBody($html);
    }
}
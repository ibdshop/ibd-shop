<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Continent admin controller
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Adminhtml_Travelsuite_ContinentController extends IBD_TravelSuite_Controller_Adminhtml_TravelSuite
{
    /**
     * init the continent
     *
     * @access protected
     * @return IBD_TravelSuite_Model_Continent
     */
    protected function _initContinent()
    {
        $continentId  = (int) $this->getRequest()->getParam('id');
        $continent    = Mage::getModel('ibd_travelsuite/continent');
        if ($continentId) {
            $continent->load($continentId);
        }
        Mage::register('current_continent', $continent);
        return $continent;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('ibd_travelsuite')->__('Travel Suite'))
             ->_title(Mage::helper('ibd_travelsuite')->__('Continents'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit continent - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function editAction()
    {
        $continentId    = $this->getRequest()->getParam('id');
        $continent      = $this->_initContinent();
        if ($continentId && !$continent->getId()) {
            $this->_getSession()->addError(
                Mage::helper('ibd_travelsuite')->__('This continent no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getContinentData(true);
        if (!empty($data)) {
            $continent->setData($data);
        }
        Mage::register('continent_data', $continent);
        $this->loadLayout();
        $this->_title(Mage::helper('ibd_travelsuite')->__('Travel Suite'))
             ->_title(Mage::helper('ibd_travelsuite')->__('Continents'));
        if ($continent->getId()) {
            $this->_title($continent->getName());
        } else {
            $this->_title(Mage::helper('ibd_travelsuite')->__('Add continent'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new continent action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save continent - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('continent')) {
            try {
                $continent = $this->_initContinent();
                $continent->addData($data);
                $continent->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ibd_travelsuite')->__('Continent was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $continent->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setContinentData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_travelsuite')->__('There was a problem saving the continent.')
                );
                Mage::getSingleton('adminhtml/session')->setContinentData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('ibd_travelsuite')->__('Unable to find continent to save.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * delete continent - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function deleteAction()
    {
        if ( $this->getRequest()->getParam('id') > 0) {
            try {
                $continent = Mage::getModel('ibd_travelsuite/continent');
                $continent->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ibd_travelsuite')->__('Continent was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_travelsuite')->__('There was an error deleting continent.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('ibd_travelsuite')->__('Could not find continent to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete continent - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeleteAction()
    {
        $continentIds = $this->getRequest()->getParam('continent');
        if (!is_array($continentIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('ibd_travelsuite')->__('Please select continents to delete.')
            );
        } else {
            try {
                foreach ($continentIds as $continentId) {
                    $continent = Mage::getModel('ibd_travelsuite/continent');
                    $continent->setId($continentId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ibd_travelsuite')->__('Total of %d continents were successfully deleted.', count($continentIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_travelsuite')->__('There was an error deleting continents.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massStatusAction()
    {
        $continentIds = $this->getRequest()->getParam('continent');
        if (!is_array($continentIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('ibd_travelsuite')->__('Please select continents.')
            );
        } else {
            try {
                foreach ($continentIds as $continentId) {
                $continent = Mage::getSingleton('ibd_travelsuite/continent')->load($continentId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d continents were successfully updated.', count($continentIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_travelsuite')->__('There was an error updating continents.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportCsvAction()
    {
        $fileName   = 'continent.csv';
        $content    = $this->getLayout()->createBlock('ibd_travelsuite/adminhtml_continent_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportExcelAction()
    {
        $fileName   = 'continent.xls';
        $content    = $this->getLayout()->createBlock('ibd_travelsuite/adminhtml_continent_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportXmlAction()
    {
        $fileName   = 'continent.xml';
        $content    = $this->getLayout()->createBlock('ibd_travelsuite/adminhtml_continent_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Ultimate Module Creator
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('ibd_travelsuite/continent');
    }
}

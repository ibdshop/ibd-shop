<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Country admin controller
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Adminhtml_Travelsuite_CountryController extends IBD_TravelSuite_Controller_Adminhtml_TravelSuite
{
    /**
     * init the country
     *
     * @access protected
     * @return IBD_TravelSuite_Model_Country
     */
    protected function _initCountry()
    {
        $countryId  = (int) $this->getRequest()->getParam('id');
        $country    = Mage::getModel('ibd_travelsuite/country');
        if ($countryId) {
            $country->load($countryId);
        }
        Mage::register('current_country', $country);
        return $country;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('ibd_travelsuite')->__('Travel Suite'))
             ->_title(Mage::helper('ibd_travelsuite')->__('Countries'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit country - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function editAction()
    {
        $countryId    = $this->getRequest()->getParam('id');
        $country      = $this->_initCountry();
        if ($countryId && !$country->getId()) {
            $this->_getSession()->addError(
                Mage::helper('ibd_travelsuite')->__('This country no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getCountryData(true);
        if (!empty($data)) {
            $country->setData($data);
        }
        Mage::register('country_data', $country);
        $this->loadLayout();
        $this->_title(Mage::helper('ibd_travelsuite')->__('Travel Suite'))
             ->_title(Mage::helper('ibd_travelsuite')->__('Countries'));
        if ($country->getId()) {
            $this->_title($country->getName());
        } else {
            $this->_title(Mage::helper('ibd_travelsuite')->__('Add country'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new country action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save country - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('country')) {
            try {
                $country = $this->_initCountry();
                $country->addData($data);
                $country->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ibd_travelsuite')->__('Country was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $country->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setCountryData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_travelsuite')->__('There was a problem saving the country.')
                );
                Mage::getSingleton('adminhtml/session')->setCountryData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('ibd_travelsuite')->__('Unable to find country to save.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * delete country - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function deleteAction()
    {
        if ( $this->getRequest()->getParam('id') > 0) {
            try {
                $country = Mage::getModel('ibd_travelsuite/country');
                $country->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ibd_travelsuite')->__('Country was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_travelsuite')->__('There was an error deleting country.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('ibd_travelsuite')->__('Could not find country to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete country - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeleteAction()
    {
        $countryIds = $this->getRequest()->getParam('country');
        if (!is_array($countryIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('ibd_travelsuite')->__('Please select countries to delete.')
            );
        } else {
            try {
                foreach ($countryIds as $countryId) {
                    $country = Mage::getModel('ibd_travelsuite/country');
                    $country->setId($countryId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ibd_travelsuite')->__('Total of %d countries were successfully deleted.', count($countryIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_travelsuite')->__('There was an error deleting countries.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massStatusAction()
    {
        $countryIds = $this->getRequest()->getParam('country');
        if (!is_array($countryIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('ibd_travelsuite')->__('Please select countries.')
            );
        } else {
            try {
                foreach ($countryIds as $countryId) {
                $country = Mage::getSingleton('ibd_travelsuite/country')->load($countryId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d countries were successfully updated.', count($countryIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_travelsuite')->__('There was an error updating countries.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass continent change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massContinentIdAction()
    {
        $countryIds = $this->getRequest()->getParam('country');
        if (!is_array($countryIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('ibd_travelsuite')->__('Please select countries.')
            );
        } else {
            try {
                foreach ($countryIds as $countryId) {
                $country = Mage::getSingleton('ibd_travelsuite/country')->load($countryId)
                    ->setContinentId($this->getRequest()->getParam('flag_continent_id'))
                    ->setIsMassupdate(true)
                    ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d countries were successfully updated.', count($countryIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_travelsuite')->__('There was an error updating countries.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportCsvAction()
    {
        $fileName   = 'country.csv';
        $content    = $this->getLayout()->createBlock('ibd_travelsuite/adminhtml_country_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportExcelAction()
    {
        $fileName   = 'country.xls';
        $content    = $this->getLayout()->createBlock('ibd_travelsuite/adminhtml_country_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportXmlAction()
    {
        $fileName   = 'country.xml';
        $content    = $this->getLayout()->createBlock('ibd_travelsuite/adminhtml_country_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Ultimate Module Creator
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('ibd_travelsuite/country');
    }
}

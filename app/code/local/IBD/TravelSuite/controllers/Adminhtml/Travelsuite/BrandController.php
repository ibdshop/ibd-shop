<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Brand admin controller
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Adminhtml_Travelsuite_BrandController extends IBD_TravelSuite_Controller_Adminhtml_TravelSuite
{
    /**
     * init the brand
     *
     * @access protected
     * @return IBD_TravelSuite_Model_Brand
     */
    protected function _initBrand()
    {
        $brandId  = (int) $this->getRequest()->getParam('id');
        $brand    = Mage::getModel('ibd_travelsuite/brand');
        if ($brandId) {
            $brand->load($brandId);
        }
        Mage::register('current_brand', $brand);

        Mage::register('current_brand_groups', $brand->getSelectedGroupsCollection());

        return $brand;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('ibd_travelsuite')->__('Travel Suite'))
             ->_title(Mage::helper('ibd_travelsuite')->__('Brands'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit brand - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function editAction()
    {
        $brandId    = $this->getRequest()->getParam('id');
        $brand      = $this->_initBrand();
        if ($brandId && !$brand->getId()) {
            $this->_getSession()->addError(
                Mage::helper('ibd_travelsuite')->__('This brand no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getBrandData(true);
        if (!empty($data)) {
            $brand->setData($data);
        }
        Mage::register('brand_data', $brand);
        $this->loadLayout();
        $this->_title(Mage::helper('ibd_travelsuite')->__('Travel Suite'))
             ->_title(Mage::helper('ibd_travelsuite')->__('Brands'));
        if ($brand->getId()) {
            $this->_title($brand->getName());
        } else {
            $this->_title(Mage::helper('ibd_travelsuite')->__('Add brand'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new brand action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save brand - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('brand')) {
            try {
                $brand = $this->_initBrand();
                $brand->addData($data);

                $imageName = $this->_uploadAndGetName(
                    'image',
                    Mage::helper('ibd_travelsuite/brand_image')->getImageBaseDir(),
                    $data
                );
                $brand->setData('image', $imageName);

                $logoName = $this->_uploadAndGetName(
                    'logo',
                    Mage::helper('ibd_travelsuite/brand_image')->getImageBaseDir(),
                    $data
                );
                $brand->setData('logo', $logoName);

                $productImageName = $this->_uploadAndGetName(
                    'product_image',
                    Mage::helper('ibd_travelsuite/brand_image')->getImageBaseDir(),
                    $data
                );
                $brand->setData('product_image', $productImageName);

                $wechatQrName = $this->_uploadAndGetName(
                    'wechat_qr',
                    Mage::helper('ibd_travelsuite/brand_image')->getImageBaseDir(),
                    $data
                );
                $brand->setData('wechat_qr', $wechatQrName);

                $titleIconName = $this->_uploadAndGetName(
                    'banner_icon',
                    Mage::helper('ibd_travelsuite/title_image')->getImageBaseDir(),
                    $data
                );
                $brand->setData('banner_icon', $titleIconName);

                $products = $this->getRequest()->getPost('products', -1);
                if ($products != -1) {
                    $brand->setProductsData(Mage::helper('adminhtml/js')->decodeGridSerializedInput($products));
                }
                $brandData = $this->getRequest()->getPost('brand', array('group_ids' => array()));
                if(isset($brandData['group_ids'])) $brand->setGroupIds($brandData['group_ids']);
                else $brand->setGroupIds(array());
                if(isset($data['group'])){
                    // remove addresses marked for deletion and created the sorted key array
                    $sorted = array();
                    foreach ($data['group'] as $groupId => $group)
                    {
                        // this is what we're making, skip it
                        if($groupId == 'sorted') continue;

                        // address marked for deleltion, delete it
                        if( isset( $group['delete'] ) && $group['delete'] == 1 ){
                            unset($data['group'][$groupId]);
                            continue;
                        }

                        // add group IDs to their sort order arrays
                        $sortOrder = (int) $data['group'][$groupId]['sort'];
                        if( !array_key_exists($sortOrder, $sorted) )
                        {
                            $sorted[$sortOrder] = array();
                        }

                        $sorted[$sortOrder][] = (string) $groupId;
                    }

                    ksort($sorted); // sort groupId arrays by keys/sort order

                    // add the sorted array to the group data object and then encode and add to brand
                    $data['group']['sorted'] = $sorted;
                    $brand->setGroupData(json_encode($data['group']));
                }
                $brand->setData('social', Mage::helper('ibd_travelsuite/social')->encodeFormDataSocial($data));
                $brand->save();

                if(isset($data['adids'])) $brand->upsertAdvertisementIds($data['adids']);
                else $brand->upsertAdvertisementIds(array());

                // save brand tags (categories)
                //Mage::log('brand data: '.json_encode($data));
                if(isset($data['tags'])) $brand->saveTags($data['tags']);

                // upload gallery images (the data already has the ID and content type)
                if(isset($data['gallery'])) Mage::getSingleton('ibd_travelsuite/gallery')->updateImagePositions($data['gallery']);

                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ibd_travelsuite')->__('Brand was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $brand->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                if (isset($data['image']['value'])) {
                    $data['image'] = $data['image']['value'];
                }
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setBrandData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                if (isset($data['image']['value'])) {
                    $data['image'] = $data['image']['value'];
                }
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_travelsuite')->__('There was a problem saving the brand.')
                );
                Mage::getSingleton('adminhtml/session')->setBrandData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('ibd_travelsuite')->__('Unable to find brand to save.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * delete brand - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function deleteAction()
    {
        if ( $this->getRequest()->getParam('id') > 0) {
            try {
                $brand = Mage::getModel('ibd_travelsuite/brand');
                $brand->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ibd_travelsuite')->__('Brand was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_travelsuite')->__('There was an error deleting brand.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('ibd_travelsuite')->__('Could not find brand to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete brand - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeleteAction()
    {
        $brandIds = $this->getRequest()->getParam('brand');
        if (!is_array($brandIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('ibd_travelsuite')->__('Please select brands to delete.')
            );
        } else {
            try {
                foreach ($brandIds as $brandId) {
                    $brand = Mage::getModel('ibd_travelsuite/brand');
                    $brand->setId($brandId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ibd_travelsuite')->__('Total of %d brands were successfully deleted.', count($brandIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_travelsuite')->__('There was an error deleting brands.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massStatusAction()
    {
        $brandIds = $this->getRequest()->getParam('brand');
        if (!is_array($brandIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('ibd_travelsuite')->__('Please select brands.')
            );
        } else {
            try {
                foreach ($brandIds as $brandId) {
                $brand = Mage::getSingleton('ibd_travelsuite/brand')->load($brandId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d brands were successfully updated.', count($brandIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_travelsuite')->__('There was an error updating brands.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * get grid of products action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function productsAction()
    {
        $this->_initBrand();
        $this->loadLayout();
        $this->getLayout()->getBlock('brand.edit.tab.product')
            ->setBrandProducts($this->getRequest()->getPost('brand_products', null));
        $this->renderLayout();
    }

    /**
     * get grid of products action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function productsgridAction()
    {
        $this->_initBrand();
        $this->loadLayout();
        $this->getLayout()->getBlock('brand.edit.tab.product')
            ->setBrandProducts($this->getRequest()->getPost('brand_products', null));
        $this->renderLayout();
    }

    /**
     * get grid of groups action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function groupsAction()
    {
        $this->_initBrand();
        $this->loadLayout();
        $this->getLayout()->getBlock('brand.edit.tab.group')
            ->setBrandGroups($this->getRequest()->getPost('brand_groups', null));
        $this->renderLayout();
    }

    /**
     * get grid of groups action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function groupsgridAction()
    {
        $this->_initBrand();
        $this->loadLayout();
        $this->getLayout()->getBlock('brand.edit.tab.group')
            ->setBrandGroups($this->getRequest()->getPost('brand_groups', null));
        $this->renderLayout();
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportCsvAction()
    {
        $fileName   = 'brand.csv';
        $content    = $this->getLayout()->createBlock('ibd_travelsuite/adminhtml_brand_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportExcelAction()
    {
        $fileName   = 'brand.xls';
        $content    = $this->getLayout()->createBlock('ibd_travelsuite/adminhtml_brand_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportXmlAction()
    {
        $fileName   = 'brand.xml';
        $content    = $this->getLayout()->createBlock('ibd_travelsuite/adminhtml_brand_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Ultimate Module Creator
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('ibd_travelsuite/brand');
    }
}

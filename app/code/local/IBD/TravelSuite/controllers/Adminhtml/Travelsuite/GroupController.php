<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Group admin controller
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Adminhtml_Travelsuite_GroupController extends IBD_TravelSuite_Controller_Adminhtml_TravelSuite
{
    /**
     * init the group
     *
     * @access protected
     * @return IBD_TravelSuite_Model_Group
     */
    protected function _initGroup()
    {
        $groupId  = (int) $this->getRequest()->getParam('id');
        $group    = Mage::getModel('ibd_travelsuite/group');
        if ($groupId) {
            $group->load($groupId);
        }
        Mage::register('current_group', $group);
        return $group;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('ibd_travelsuite')->__('Travel Suite'))
             ->_title(Mage::helper('ibd_travelsuite')->__('Groups'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit group - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function editAction()
    {
        $groupId    = $this->getRequest()->getParam('id');
        $group      = $this->_initGroup();
        if ($groupId && !$group->getId()) {
            $this->_getSession()->addError(
                Mage::helper('ibd_travelsuite')->__('This group no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getGroupData(true);
        if (!empty($data)) {
            $group->setData($data);
        }
        Mage::register('group_data', $group);
        $this->loadLayout();
        $this->_title(Mage::helper('ibd_travelsuite')->__('Travel Suite'))
             ->_title(Mage::helper('ibd_travelsuite')->__('Groups'));
        if ($group->getId()) {
            $this->_title($group->getName());
        } else {
            $this->_title(Mage::helper('ibd_travelsuite')->__('Add group'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new group action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save group - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('group')) {
            try {
                $group = $this->_initGroup();
                $group->addData($data);

                // upload main image
                $imageName = $this->_uploadAndGetName(
                    'image',
                    Mage::helper('ibd_travelsuite/group_image')->getImageBaseDir(),
                    $data
                );
                $group->setData('image', $imageName);

                // upload badge icon
                $badgeIconName = $this->_uploadAndGetName(
                    'badge_icon',
                    Mage::helper('ibd_travelsuite/group_badgeicon')->getImageBaseDir(),
                    $data
                );
                $group->setData('badge_icon', $badgeIconName);

                // upload title icon
                $titleIconName = $this->_uploadAndGetName(
                    'banner_icon',
                    Mage::helper('ibd_travelsuite/title_image')->getImageBaseDir(),
                    $data
                );
                $group->setData('banner_icon', $titleIconName);

                // encode social data
                $group->setData('social', Mage::helper('ibd_travelsuite/social')->encodeFormDataSocial($data));

                // encode directions
                $group->setData('directions', json_encode(array(
                    'driving' => $data['driving'],
                    'walking' => $data['walking'],
                )));

                $group->save();

                if(isset($data['adids'])) $group->upsertAdvertisementIds($data['adids']);
                else $group->upsertAdvertisementIds(array());

                // upload gallery images
                if(isset($data['gallery'])) Mage::getSingleton('ibd_travelsuite/gallery')->updateImagePositions($data['gallery']);

                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ibd_travelsuite')->__('Group was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $group->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                if (isset($data['image']['value'])) {
                    $data['image'] = $data['image']['value'];
                }
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setGroupData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                if (isset($data['image']['value'])) {
                    $data['image'] = $data['image']['value'];
                }
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_travelsuite')->__('There was a problem saving the group.')
                );
                Mage::getSingleton('adminhtml/session')->setGroupData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('ibd_travelsuite')->__('Unable to find group to save.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * delete group - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function deleteAction()
    {
        if ( $this->getRequest()->getParam('id') > 0) {
            try {
                $group = Mage::getModel('ibd_travelsuite/group');
                $group->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ibd_travelsuite')->__('Group was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_travelsuite')->__('There was an error deleting group.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('ibd_travelsuite')->__('Could not find group to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete group - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeleteAction()
    {
        $groupIds = $this->getRequest()->getParam('group');
        if (!is_array($groupIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('ibd_travelsuite')->__('Please select groups to delete.')
            );
        } else {
            try {
                foreach ($groupIds as $groupId) {
                    $group = Mage::getModel('ibd_travelsuite/group');
                    $group->setId($groupId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ibd_travelsuite')->__('Total of %d groups were successfully deleted.', count($groupIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_travelsuite')->__('There was an error deleting groups.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massStatusAction()
    {
        $groupIds = $this->getRequest()->getParam('group');
        if (!is_array($groupIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('ibd_travelsuite')->__('Please select groups.')
            );
        } else {
            try {
                foreach ($groupIds as $groupId) {
                $group = Mage::getSingleton('ibd_travelsuite/group')->load($groupId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d groups were successfully updated.', count($groupIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_travelsuite')->__('There was an error updating groups.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass region change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massRegionIdAction()
    {
        $groupIds = $this->getRequest()->getParam('group');
        if (!is_array($groupIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('ibd_travelsuite')->__('Please select groups.')
            );
        } else {
            try {
                foreach ($groupIds as $groupId) {
                $group = Mage::getSingleton('ibd_travelsuite/group')->load($groupId)
                    ->setRegionId($this->getRequest()->getParam('flag_region_id'))
                    ->setIsMassupdate(true)
                    ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d groups were successfully updated.', count($groupIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ibd_travelsuite')->__('There was an error updating groups.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportCsvAction()
    {
        $fileName   = 'group.csv';
        $content    = $this->getLayout()->createBlock('ibd_travelsuite/adminhtml_group_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportExcelAction()
    {
        $fileName   = 'group.xls';
        $content    = $this->getLayout()->createBlock('ibd_travelsuite/adminhtml_group_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportXmlAction()
    {
        $fileName   = 'group.xml';
        $content    = $this->getLayout()->createBlock('ibd_travelsuite/adminhtml_group_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Ultimate Module Creator
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('ibd_travelsuite/group');
    }
}

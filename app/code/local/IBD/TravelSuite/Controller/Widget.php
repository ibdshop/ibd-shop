<?php
class IBD_TravelSuite_Controller_Widget
{
	public function addTravelSuiteHandle(Varien_Event_Observer $observer)
    {
        $event = $observer->getEvent();

        /* @var $page IBD_TravelSuite_Model_Brand */
        $brand = Mage::registry('current_brand');
        /* @var $page IBD_TravelSuite_Model_Group */
        $group = Mage::registry('current_group');
        /* @var $page IBD_TravelSuite_Model_Region */
        $region = Mage::registry('current_region');

        $layout = $event->getLayout();
        if( !$layout)
        {
            return;
        }

        $update = $layout->getUpdate();
        if( !$update)
        {
            return;
        }

        if($brand && $brand instanceof IBD_TravelSuite_Model_Brand)
        {
            $update->addHandle('ibd_brand_'.$brand->getId());
        }

        if($group && $group instanceof IBD_TravelSuite_Model_Group)
        {
            $update->addHandle('ibd_group_'.$group->getId());
        }

        if($region && $region instanceof IBD_TravelSuite_Model_Region)
        {
            $update->addHandle('ibd_region_'.$region->getId());
        }
    }
}
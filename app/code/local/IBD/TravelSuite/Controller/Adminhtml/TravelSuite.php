<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * module base admin controller
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Controller_Adminhtml_TravelSuite extends Mage_Adminhtml_Controller_Action
{
    /**
     * upload file and get the uploaded name (wrapper to get the helper)
     *
     * @access public
     * @param string $input
     * @param string $destinationFolder
     * @param array $data
     * @return string
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    protected function _uploadAndGetName($input, $destinationFolder, $data)
    {
        return Mage::helper('ibd_travelsuite')->uploadImageAndGetName($input, $destinationFolder, $data);
    }


    public function uploadAction()
    {
        if ($data = $this->getRequest()->getPost())
        {
            $response = array(
                'errorcode' => 2,
                'message' => 'unknown error'
            );

            if(!isset($data['relation_id']) || !isset($data['relation_type']))
            {
                // we do not support GET or other requests
                $this->getResponse()->setHeader('HTTP/1.0', '400', true);
                $this->getResponse()->setBody('Must specify relation_id and relation_type to upload to the gallery');
                return;
            }

            $imageHelper = Mage::helper('ibd_travelsuite/gallery_image');
            $destinationFolder = $imageHelper->getImageBaseDir();
            try
            {
                // setup uploader
                $uploader = new Varien_File_Uploader('Filedata');
                $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(true);
                $uploader->setAllowCreateFolders(true);

                // upload the file
                $result = $uploader->save($destinationFolder);

                // create the image DB object
                $image = Mage::getModel('ibd_travelsuite/gallery');
                $image->setRelationId($data['relation_id']);
                $image->setRelationType($data['relation_type']);
                $image->setImagePath($result['file']);
                $image->save();

                // respond with some JSON goodness
                $response['errorcode'] = 0;
                $response['message'] = 'success';
                $response['data'] = array(
                    'path' => $image->getImagePath(),
                    'id' => $image->getId(),
                    'thumb' => (string) $imageHelper->init($image, 'imagePath')->resize(200)
                );
            }
            catch (Exception $e)
            {
                Mage::logException($e);
                $this->getResponse()->setHeader('HTTP/1.0', '500', true);
                $response['message'] = $e->getMessage();
                $response['errorcode'] = 1;
            }

            $this->getResponse()->setBody(json_encode($response));
            return;
        }

        // we do not support GET or other requests
        $this->getResponse()->setHeader('HTTP/1.0', '404', true);
    }
}

<?php 
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Title image helper
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Helper_Title_Image extends IBD_TravelSuite_Helper_Image_Abstract
{
    /**
     * image placeholder
     * @var string
     */
    // protected $_placeholder = 'images/placeholder/title.jpg';
    protected $_placeholder = 'images/placeholder/title.gif';

    /**
     * image subdir
     * @var string
     */
    protected $_subdir      = 'title';
}

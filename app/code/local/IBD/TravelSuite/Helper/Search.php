<?php
/**
 * IBD_TravelSuite extension
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Search
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Helper_Search
{
    /**
     * generalized filter function. registers the results for use in blocks.
     *
     * @access public
     * @param string
     * @param Zend_Controller_Request_Http
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function filterResults($type, Zend_Controller_Request_Http $request, $limit=3, $offset=0)
    {
        if($type == 'brand')
        {
            $this->filterBrands($request, $limit, $offset);
        }
        elseif($type == 'group')
        {
            $this->filterGroups($request, $limit, $offset);
        }
        elseif($type == 'region')
        {
            $this->filterRegions($request, $limit, $offset);
        }
        else
        {
            // no valid type specified, let's query all the things!
            $this->filterBrands($request, $limit, $offset);
            $this->filterGroups($request, $limit, $offset);
            $this->filterRegions($request, $limit, $offset);
        }

        // register limit and offset for use in paginators
        Mage::register('search_limit', $limit);
        Mage::register('search_offset', $offset);
    }

    /**
     * brand specific filter. registers the results for use in blocks.
     *
     * @access public
     * @param Zend_Controller_Request_Http
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function filterBrands(Zend_Controller_Request_Http $request, $limit=20, $offset=0)
    {
        // grab relevant query parameters
        $mainQuery = $request->getParam('q');
        $status = $request->getParam('status', -1); // -1 (default) means all active, i.e., not 0
        $brandTag = $request->getParam('tag');
        $groupId = $request->getParam('group');
        $badge = $request->getParam('badge');
        $region = $request->getParam('region');
        $country = $request->getParam('country');
        $continent = $request->getParam('continent');
        $groupCategory = $request->getParam('category');
        $alpha = $request->getParam('alpha');

        // get the collection and SQL. we're going to be working with the SQl, but
        // the collection retains the reference for later use
        $collection = Mage::getSingleton('ibd_travelsuite/brand')->getCollection();
        $sql = $collection->getSelect();

        
        // deeper queries rely on shallower ones; continents are only on the country (etc)
        // so, if we only want to filter on continent, we still have to join group, region,
        // and country before joining the continent
        if($mainQuery || $groupId || $groupCategory || $region || $country || $continent)
        {
            $sql->joinLeft(
                array('brand_group' => $collection->getTable('ibd_travelsuite/brand_group')),
                'main_table.entity_id = brand_group.brand_id',
                array()
            );

            $sql->joinLeft(
                array('group' => $collection->getTable('ibd_travelsuite/group')),
                'brand_group.group_id = group.entity_id',
                array('group_name' => 'group.name')
            );

            if($groupId) $sql->where('group.entity_id = ?', $groupId);
            if($groupCategory) $sql->where('group.category = ?', $groupCategory);
        }

        // to link on group category and group badge, which can be on different groups,
        // we need to link the table twice if we're querying by badge
        if($badge)
        {
            $sql->joinLeft(
                array('brand_group_badge' => $collection->getTable('ibd_travelsuite/brand_group')),
                'main_table.entity_id = brand_group_badge.brand_id',
                array()
            );

            $sql->joinLeft(
                array('group_badge' => $collection->getTable('ibd_travelsuite/group')),
                'brand_group_badge.group_id = group_badge.entity_id',
                array()
            );

            $sql->where('group_badge.badge_name = ?', $badge);
        }

        // join region
        if($mainQuery || $region || $country || $continent)
        {
            $this->join($sql, 'group', $collection->getTable('ibd_travelsuite/region'), 'region');
            if($region) $sql->where('region.name = ?', $region);
        }

        // join country
        if($mainQuery || $country || $continent)
        {
            $this->join($sql, 'region', $collection->getTable('ibd_travelsuite/country'), 'country');
            if($country) $sql->where('country.name = ?', $country);
        }

        // join continent
        if($mainQuery || $continent)
        {
            $this->join($sql, 'country', $collection->getTable('ibd_travelsuite/continent'), 'continent');
            if($continent) $sql->where('continent.name = ?', $continent);
        }

        if($brandTag)
        {
            $sql->joinLeft(
                array('tag_relation' => $collection->getTable('ibd_travelsuite/tag_relation')),
                'main_table.entity_id = tag_relation.relation_id',
                array()
            );

            $sql->joinLeft(
                array('tag' => $collection->getTable('ibd_travelsuite/tag')),
                'tag_relation.tag_id = tag.entity_id',
                array()
            );

            $sql->where('tag_relation.relation_type = \'ibd_brand\'');
            $sql->where('tag.tag = ?', $brandTag);
        }

        // add status to query (-1 means any active status, i.e., greater than 0)
        if($status < 0) $sql->where('main_table.status > 0');
        else $sql->where('main_table.status = ?', $status);

        // add the name query if we have it
        if($mainQuery)
        {
            $sql->where(
                'main_table.name LIKE ? OR main_table.meta_keywords LIKE ? OR continent.name LIKE ?'.
                    ' OR country.name LIKE ? OR region.name LIKE ? OR group.name LIKE ?',
                "%$mainQuery%"
            );
        }
        elseif($alpha == '#')
        {
            $sql->where('main_table.name > \'zzzz\' OR main_table.name < \'a\'');
        }
        elseif($alpha)
        {
            // can't do a main query and an alpha query, really
            $sql->where('main_table.name >= ?', $alpha)->where('main_table.name < ?', ++$alpha);
        }


        // since brands can have multiple groups (and therefore, regions, countries
        // and continents), this ensures that we only get one brand, not duplicates
        // per group
        $sql->group('main_table.entity_id')
            ->order('main_table.status DESC')
            ->order('main_table.name ASC');

        // register the total count of the query
        Mage::register('brand_search_count', $collection->getSize());

        // now that we've counted, add the limit and then get the results
        $sql->limit($limit, $offset);

        // not a bad idea to log these queries, in case we ever want to do debugging
        Mage::log('brand search query: '.(string) $sql);
        Mage::register('brand_search_results', $this->collectionToArray($collection));
    }

    /**
     * group specific filter. registers the results for use in blocks.
     *
     * @access public
     * @param Zend_Controller_Request_Http
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function filterGroups(Zend_Controller_Request_Http $request, $limit=20, $offset=0)
    {
        // grab relevant query parameters
        $mainQuery = $request->getParam('q');
        $groupCategory = $request->getParam('category');
        $region = $request->getParam('region');
        $country = $request->getParam('country');
        $continent = $request->getParam('continent');

        // get the collection and SQL. we're going to be working with the SQl, but
        // the collection retains the reference for later use
        $collection = Mage::getSingleton('ibd_travelsuite/group')->getCollection();
        $sql = $collection->getSelect();

        // deeper queries rely on shallower ones; continents are only on the country (etc)
        // so, if we only want to filter on continent, we still have to join region
        // and country before joining the continent
        if($mainQuery || $region || $country || $continent)
        {
            $this->join($sql, 'main_table', $collection->getTable('ibd_travelsuite/region'), 'region');
            if($region) $sql->where('region.name = ?', $region);
        }

        if($mainQuery || $country || $continent)
        {
            $this->join($sql, 'region', $collection->getTable('ibd_travelsuite/country'), 'country');
            if($country) $sql->where('country.name = ?', $country);
        }

        if($mainQuery || $continent)
        {
            $this->join($sql, 'country', $collection->getTable('ibd_travelsuite/continent'), 'continent');
            if($continent) $sql->where('continent.name = ?', $continent);
        }

        // don't get disabled groups
        $sql->where('main_table.status > 0')
            ->order('main_table.name ASC');

        // add the name query if we have it
        if($mainQuery)
        {
            $sql->where(
                'main_table.name LIKE ? OR main_table.meta_keywords LIKE ? OR continent.name LIKE ?'.
                        ' OR country.name LIKE ? OR region.name LIKE ?',
                "%$mainQuery%"
            );
        }

        // register the total count of the query
        Mage::register('group_search_count', $collection->getSize());

        // now that we've counted, add the limit and then get the results
        $sql->limit($limit, $offset);

        // not a bad idea to log these queries, in case we ever want to do debugging
        Mage::log('group search query: '.(string) $sql);
        Mage::register('group_search_results', $this->collectionToArray($collection));
    }

    /**
     * region specific filter. registers the results for use in blocks.
     *
     * @access public
     * @param Zend_Controller_Request_Http
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function filterRegions(Zend_Controller_Request_Http $request, $limit=20, $offset=0)
    {
        // grab relevant query parameters
        $mainQuery = $request->getParam('q');
        $country = $request->getParam('country');
        $continent = $request->getParam('continent');

        // get the collection and SQL. we're going to be working with the SQl, but
        // the collection retains the reference for later use
        $collection = Mage::getSingleton('ibd_travelsuite/region')->getCollection();
        $sql = $collection->getSelect();

        // deeper queries rely on shallower ones; continents are only on the country
        // so, if we only want to filter on continent, we still have to join
        // the country before joining the continent
        if($mainQuery || $country || $continent)
        {
            $this->join($sql, 'main_table', $collection->getTable('ibd_travelsuite/country'), 'country');
            if($country) $sql->where('country.name = ?', $country);
        }

        if($mainQuery || $continent)
        {
            $this->join($sql, 'country', $collection->getTable('ibd_travelsuite/continent'), 'continent');
            if($continent) $sql->where('continent.name = ?', $continent);
        }

        // don't get disabled regions, set the order (by name)
        $sql->where('main_table.status > 0')
            ->order('main_table.name ASC');

        // add the name query if we have it
        if($mainQuery)
        {
            $sql->where(
                'main_table.name LIKE ? OR main_table.meta_keywords LIKE ? OR continent.name LIKE ? OR country.name LIKE ?',
                "%$mainQuery%"
            );
        }

        // register the total count of the query
        Mage::register('region_search_count', $collection->getSize());

        // now that we've counted, add the limit and then get the results
        $sql->limit($limit, $offset);

        // not a bad idea to log these queries, in case we ever want to do debugging
        Mage::log('region search query: '.(string) $sql);
        Mage::register('region_search_results', $this->collectionToArray($collection));
    }

    /**
     * helper method for vanilla joins
     *
     * @access protected
     * @param Zend_Db_Select
     * @param string
     * @param string
     * @param string
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    protected function join(Zend_Db_Select $sql, $existingTableAlias, $joinTable, $joinTableAlias)
    {
        $sql->joinLeft(
            array($joinTableAlias => $joinTable),
            "${existingTableAlias}.${joinTableAlias}_id = ${joinTableAlias}.entity_id",
            array($joinTableAlias.'_name' => "${joinTableAlias}.name")
        );
    }

    /**
     * helper method to put the DB results into a basic array
     *
     * @access protected
     * @param Magento Collection (?)
     * @return array
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    protected function collectionToArray($collection)
    {
        $results = array();
        foreach ($collection as $entity)
        {
            $results[] = $entity;
        }
        return $results;
    }
}
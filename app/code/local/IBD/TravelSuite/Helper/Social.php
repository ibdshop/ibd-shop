<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Social Form Helper
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Helper_Social
{

	/**
	 * Adds the social fields to the form
	 * @param array
	 * @author Kevin Sweeney (kevin@robotshanghai.com)
	 */
	public function addFormFields($form, $data, $fieldsetName)
	{
        $social = isset($data['social']) ? json_decode($data['social'], true) : null;

        $fieldset = $form->addFieldset(
            $fieldsetName,
            array('legend' => Mage::helper('ibd_travelsuite')->__('Social Media Links'))
        );
        $fieldset->addField(
            'twitter',
            'text',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Twitter'),
                'name' => 'twitter',
                'note' => $this->__('Twitter Handle Without @'),
                'value' => isset($social['twitter']) ? $social['twitter'] : '',
            )
        );
        $fieldset->addField(
            'facebook',
            'text',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Facebook'),
                'name' => 'facebook',
                'note' => $this->__('Facebook Handle'),
                'value' => isset($social['facebook']) ? $social['facebook'] : '',
            )
        );
        $fieldset->addField(
            'instagram',
            'text',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Instagram'),
                'name' => 'instagram',
                'note' => $this->__('Instagram Handle Without @'),
                'value' => isset($social['instagram']) ? $social['instagram'] : '',
            )
        );
        $fieldset->addField(
            'weibo',
            'text',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Weibo'),
                'name' => 'weibo',
                'note' => $this->__('Weibo Handle'),
                'value' => isset($social['weibo']) ? $social['weibo'] : '',
            )
        );

        $form->addValues($data);
	}

	/**
	 * Gets the fields from the form, returns a JSON encoded string
	 * @param array
	 * @return string
	 * @author Kevin Sweeney (kevin@robotshanghai.com)
	 */
	public function encodeFormDataSocial($data)
	{
		return json_encode(array(
            'twitter' => $data['twitter'],
            'facebook' => $data['facebook'],
            'instagram' => $data['instagram'],
            'weibo' => $data['weibo'],
        ));
	}

	/**
	 * Translates the text
	 * @param string
	 * @return string
	 * @author Kevin Sweeney (kevin@robotshanghai.com)
	 */
	protected function __($value)
	{
		Mage::helper('ibd_travelsuite')->__($value);
	}
}
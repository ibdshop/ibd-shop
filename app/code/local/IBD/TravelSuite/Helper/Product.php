<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Product helper
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Helper_Product extends IBD_TravelSuite_Helper_Data
{

    /**
     * get the selected brands for a product
     *
     * @access public
     * @param Mage_Catalog_Model_Product $product
     * @return array()
     * @author Ultimate Module Creator
     */
    public function getSelectedBrands(Mage_Catalog_Model_Product $product)
    {
        if (!$product->hasSelectedBrands()) {
            $brands = array();
            foreach ($this->getSelectedBrandsCollection($product) as $brand) {
                $brands[] = $brand;
            }
            $product->setSelectedBrands($brands);
        }
        return $product->getData('selected_brands');
    }

    /**
     * get brand collection for a product
     *
     * @access public
     * @param Mage_Catalog_Model_Product $product
     * @return IBD_TravelSuite_Model_Resource_Brand_Collection
     * @author Ultimate Module Creator
     */
    public function getSelectedBrandsCollection(Mage_Catalog_Model_Product $product)
    {
        $collection = Mage::getResourceModel('ibd_travelsuite/brand_collection')
            ->addProductFilter($product);
        return $collection;
    }
}

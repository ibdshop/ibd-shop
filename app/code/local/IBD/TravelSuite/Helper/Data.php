<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * TravelSuite default helper
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Helper_Data extends Mage_Core_Helper_Abstract
{
    const CACHE_TAG = 'IBD_TRAVEL_SUITE';

    /**
     * a way to tell if we think this server is a production server
     *
     * @access public
     * @return boolean
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function isProduction()
    {
        $isProd = !(strpos( $_SERVER['HTTP_HOST'], 'ibdshop.com' ) === false);
        Mage::log('HTTP_HOST: "'.$_SERVER['HTTP_HOST'].'", SERVER_NAME: "'.$_SERVER['SERVER_NAME'].'", Is Production: '.($isProd?'true':'false'));
        return $isProd;
    }

    /**
     * convert array to options
     *
     * @access public
     * @param $options
     * @return array
     * @author Ultimate Module Creator
     */
    public function convertOptions($options)
    {
        $converted = array();
        foreach ($options as $option) {
            if (isset($option['value']) && !is_array($option['value']) &&
                isset($option['label']) && !is_array($option['label'])) {
                $converted[$option['value']] = $option['label'];
            }
        }
        return $converted;
    }

    public function arrayToOptionArray($array)
    {
        $optionArray = array();
        foreach ($array as $value => $text)
        {
            $optionArray[] = array(
                'value' => $value,
                'label' => $text,
            );
        }
        return $optionArray;
    }

    public function getGeographyHierarchy()
    {
        $continents = Mage::getResourceModel('ibd_travelsuite/continent_collection');
        $countries = Mage::getResourceModel('ibd_travelsuite/country_collection');

        // setup _geographic continents
        $geoHier = array();
        foreach ($continents as $continent)
        {
            $geoHier[$continent->getId()] = array(
                'name' => $continent->getName(),
                'countries' => array()
            );
        }

        // setup _geographic countries underneath their contentint
        foreach ($countries as $country)
        {
            $geoHier[$country->getContinentId()]['countries'][$country->getId()] = $country->getName();
        }

        return $geoHier;
    }

    /**
     * turn new line characters into breaklines
     *
     * @access public
     * @param string
     * @return string
     * @author @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function carriageReturnsToBreaklines($text)
    {
        return str_replace("\r\n", '<br>', $text);
    }

    public function stringForJson($text)
    {
        return str_replace("'", "\\'", $text);
    }

    /**
     * Gets main image and gallery images in a sorted array with urls and thumbnail urls
     *
     * @access public
     * @param int (optional)
     * @return array
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function getAllImages($entity, $thumbSize, $imageField='image')
    {
        // check against null, not just if it's falsey. if it's an empty
        // array, we still want to return it.
        $images = array();

        // get the main image
        $imageHelper = Mage::helper('ibd_travelsuite/'.$entity->getIBDType().'_image');
        if($entity->getData($imageField))
        {
            $images[] = $this->getImagePathArray($entity, $imageHelper, 1200, $thumbSize, $imageField);
        }

        // get the gallery images
        $gallery = Mage::getModel('ibd_travelsuite/gallery')
                        ->getEntityImageCollection($entity->getId(), 'ibd_'.$entity->getIBDType());
        $imageHelper = Mage::helper('ibd_travelsuite/gallery_image');
        foreach ($gallery as $galleryImage)
        {
            $images[] = $this->getImagePathArray($galleryImage, $imageHelper, 1200, $thumbSize, 'image_path');
        }

        return $images;
    }

    /**
     * Gets gallery images in a sorted array with urls and thumbnail urls
     *
     * @access public
     * @param int (optional)
     * @return array
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function getGalleryImages($entity, $thumbSize, $carouselSize, $imageField='image')
    {
        $gallery = Mage::getModel('ibd_travelsuite/gallery')
                        ->getEntityImageCollection($entity->getId(), 'ibd_'.$entity->getIBDType());
        $imageHelper = Mage::helper('ibd_travelsuite/gallery_image');

        $images = array();
        foreach ($gallery as $galleryImage)
        {
            $images[] = $this->getImagePathArray($galleryImage, $imageHelper, $carouselSize, $thumbSize, 'image_path');
        }

        return $images;
    }

    /**
     * gets a associative array with different image sizes
     *
     * @access public
     * @param int (optional)
     * @return array
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function getImagePathArray($entity, $helper, $size, $thumbSize, $imageField)
    {
        $main = $size > 0 ? (string) $helper->init($entity, $imageField)->resize($size) : null;
        $thumb = $thumbSize > 0 ? (string) $helper->init($entity, $imageField)->resize($thumbSize) : null;
        return array(
            'full_url' => $helper->getImageBaseUrl() . $entity->getData($imageField),
            'main' => $main,
            'thumb' => $thumb,
        );
    }

    /**
     * Gets a css string from the entity
     *
     * @access public
     * @param Mage_Core_Model_Abstract
     * @param string optional
     * @param string optional
     * @return string
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function getStyleCss(Mage_Core_Model_Abstract $entity, $foreField='fore_color', $backField='color')
    {
        $ret = '';
        if($entity->hasData($foreField))
        {
            $ret .= 'color:#'.$entity->getData($foreField).';';
        }
        if($entity->hasData($backField))
        {
            $ret .= 'background-color:#'.$entity->getData($backField).';';
        }
        return $ret;
    }

    public function getStyleForeCss(Mage_Core_Model_Abstract $entity, $foreField='fore_color')
    {
        $ret = '';
        if($entity->hasData($foreField))
        {
            $ret .= 'color:#'.$entity->getData($foreField).';';
        }
        return $ret;
    }

    public function getStyleBackCss(Mage_Core_Model_Abstract $entity, $backField='color')
    {
        $ret = '';
        if($entity->hasData($backField))
        {
            $ret .= 'background-color:#'.$entity->getData($backField).';';
        }
        return $ret;
    }

    public function saveToTravelSuiteCache($item, $key, $lifespan = false)
    {
        Mage::app()->getCache()->save($item, $key, array('IBD_TRAVEL_SUITE'), $lifespan);
    }

    public function getBrandAlphaFilter($rebuild = false)
    {
        $cacheKey = 'IBD_BRAND_ALPHA_FILTERS';
        $magCache = Mage::app()->getCache();
        $filters = null;

        if($rebuild)
        {
            $magCache->remove($cacheKey);
        }
        else
        {
            $filters = $magCache->load($cacheKey);
        }
        
        if(!$filters)
        {
            Mage::log('building brand alphabet filters');
            $symbolStart = '#';
            $filters = array();
            $currentCount = 0;
            $lastStartLetter = $symbolStart; // initialized it to count symbols
            foreach (Mage::getModel('ibd_travelsuite/brand')->getCollection()->setOrder('name') as $brand)
            {
                // get the current first letter (lowercase)
                $firstLetter = strtolower(substr($brand->getName(), 0, 1));

                // if we're outside of the normal alphabet, we're still counting symbols
                if( $firstLetter < 'a' || $firstLetter > 'z' )
                {
                    $count++;
                    continue;
                }

                // alright, first letter is a letter, if current start is a symbol, let's save the symbol count
                if( $lastStartLetter == $symbolStart )
                {
                    $filters[$lastStartLetter] = $count;
                    $count = 0;
                    $lastStartLetter = $firstLetter;
                    continue;
                }

                // we've moved on to letters, if we're on the same letter, keep counting
                if( $lastStartLetter == $firstLetter )
                {
                    $count++;
                    continue;
                }

                // we're on letters, and we have a different one, save the count and move the current start
                $filters[$lastStartLetter] = $count;
                $count = 0;
                $lastStartLetter = $firstLetter;
            }

            $this->saveToTravelSuiteCache($filters, $cacheKey);
        }
        return $filters;
    }

    /**
     * upload file and get the uploaded name
     *
     * @access public
     * @param string $input
     * @param string $destinationFolder
     * @param array $data
     * @return string
     * @author Ultimate Module Creator
     */
    public function uploadImageAndGetName($input, $destinationFolder, $data)
    {
        try {
            if (isset($data[$input]['delete'])) {
                return '';
            } else {
                $uploader = new Varien_File_Uploader($input);
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(true);
                $uploader->setAllowCreateFolders(true);
                $result = $uploader->save($destinationFolder);
                return $result['file'];
            }
        } catch (Exception $e) {
            if ($e->getCode() != Varien_File_Uploader::TMP_NAME_EMPTY) {
                throw $e;
            } else {
                if (isset($data[$input]['value'])) {
                    return $data[$input]['value'];
                }
            }
        }
        return '';
    }
}

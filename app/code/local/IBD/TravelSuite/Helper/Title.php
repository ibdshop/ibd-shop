<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * TravelSuite title helper
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Helper_Title extends Mage_Core_Helper_Abstract
{
	const ALIGN_CENTER = 'center';
	const ALIGN_LEFT = 'left';
	const ALIGN_RIGHT = 'right';

	const ICON_LEFT = 'left';
	const ICON_ABOVE = 'top';
	const ICON_RIGHT = 'right';
	const ICON_BELOW = 'bottom';

    public function getTitleAlignmentArray()
    {
        return array(
            self::ALIGN_CENTER  => Mage::helper('ibd_travelsuite')->__('Center'),
            self::ALIGN_LEFT => Mage::helper('ibd_travelsuite')->__('Left'),
            self::ALIGN_RIGHT => Mage::helper('ibd_travelsuite')->__('Right'),
        );
    }

    public function getIconPositionArray()
    {
        return array(
            self::ICON_LEFT  => Mage::helper('ibd_travelsuite')->__('Left'),
            self::ICON_ABOVE => Mage::helper('ibd_travelsuite')->__('Above'),
            self::ICON_RIGHT => Mage::helper('ibd_travelsuite')->__('Right'),
            self::ICON_BELOW => Mage::helper('ibd_travelsuite')->__('Below'),
        );
    }

    public function getTitleAlignmentOptions()
    {
    	return Mage::helper('ibd_travelsuite')->arrayToOptionArray($this->getTitleAlignmentArray());
    }

    public function getIconPositionOptions()
    {
    	return Mage::helper('ibd_travelsuite')->arrayToOptionArray($this->getIconPositionArray());
    }
}
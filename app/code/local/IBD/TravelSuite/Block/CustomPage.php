<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Cms
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/**
 * IBD TravelSuite Page Header Block
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Block_CustomPage extends Mage_Core_Block_Template
{
	protected $_page = null;

	public function _construct()
	{
		parent::_construct();

		$pageId = $this->getRequest()->getParam('page_id', $this->getRequest()->getParam('id', false));

        $page = Mage::getSingleton('cms/page');
        if (!is_null($pageId) && $pageId!==$page->getId()) {
            $delimeterPosition = strrpos($pageId, '|');
            if ($delimeterPosition) {
                $pageId = substr($pageId, 0, $delimeterPosition);
            }

            $page->setStoreId(Mage::app()->getStore()->getId());
            if (!$page->load($pageId)) {
                return false;
            }
        }

        if (!$page->getId()) {
            return false;
        }

        $this->_page = $page;
	}

	public function getPage()
	{
		return $this->_page;
	}

	public function hasTopSection()
	{
		if(!$this->_page) return;
		
		if( $this->_page->getIbdTopsectionTitle() ||
			$this->_page->getIbdTopsectionImage() )
		{
			return true;
		}
		return false;
	}

	public function getIbdTopsectionImageUrl()
	{
		if(!$this->_page) return;

		return Mage::helper('ibd_travelsuite/cms_image')->getImageBaseUrl() . $this->_page->getIbdTopsectionImage();
	}

	public function getContainerStyle()
	{
		if(!$this->_page) return;

		if( $this->_page->getIbdHeaderIcon() )
		{
			return ' style="background-image:url('.Mage::helper('ibd_travelsuite/cms_image')->getImageBaseUrl() .$this->_page->getIbdHeaderIcon().');"';
		}
		return '';
	}

	// the name of this function makes me happy ~ Kevin
	public function getWrapperStyle()
	{
		if(!$this->_page) return;

		if( $this->_page->getIbdHeaderColor() )
		{
			return ' style="background-color:#'.$this->_page->getIbdHeaderColor().'"';
		}
		return '';
	}
}
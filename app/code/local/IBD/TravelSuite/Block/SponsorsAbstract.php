<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Abstract sponsors / ads (override with an entity type)
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Block_SponsorsAbstract extends IBD_TravelSuite_Block_Block_Abstract
{
    public function getAdCollection()
    {
        return Mage::registry('current_'.$this->_type)->getAdvertisementCollection();
    }

    public function getAdImageUrl($ad)
    {
        return (string) Mage::helper('ibd_homepagebanner/advertisement_image')->init($ad)->resize(250)->keepFrame(false);
    }
}
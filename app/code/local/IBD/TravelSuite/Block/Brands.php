<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Advertisement model
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Block_Brands extends IBD_TravelSuite_Block_Block_Abstract
{
	public function getBrands()
	{
		Mage::helper('ibd_travelsuite/search')->filterBrands($this->getRequest());
		return Mage::registry('brand_search_results');
	}

	public function getBrandShowcase()
	{
		return Mage::getModel('ibd_travelsuite/brand')->getBrands(true, 9, true);
	}

	public function i()
	{
		return Mage::helper('ibd_travelsuite/brand_image');
	}
}
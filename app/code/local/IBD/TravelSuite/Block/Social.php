<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Block for getting social links
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Block_Social extends Mage_Core_Block_Template
{
	protected $socialData;

    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('ibd_travelsuite/social.phtml');
    }

    public function setSocial($socialJSON)
    {
    	$this->socialData = json_decode($socialJSON, true);
    	return $this;
    }

    public function getSocial()
    {
        return $this->socialData;
    }
}
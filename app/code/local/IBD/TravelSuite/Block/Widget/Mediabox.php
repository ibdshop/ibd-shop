<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Media box widget block
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotjumpstart.com)
 */
class IBD_TravelSuite_Block_Widget_Mediabox extends IBD_TravelSuite_Block_Block_WidgetAbstract
{
    protected $_htmlTemplate = 'ibd_travelsuite/widget/mediabox.phtml';

    /**
     * Helper to check if we are left or right justified
     *
     * @access public
     * @return boolean
     * @author Kevin Sweeney (kevin@robotjumpstart.com)
     */
    public function isImageLeftAligned()
    {
        return IBD_TravelSuite_Model_Adminhtml_Source_Align::LEFT == $this->getData('main_image_align');
    }
}
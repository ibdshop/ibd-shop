<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Dual image widget block
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotjumpstart.com)
 */
class IBD_TravelSuite_Block_Widget_Dualimage extends IBD_TravelSuite_Block_Block_WidgetAbstract
{
    protected $_htmlTemplate = 'ibd_travelsuite/widget/dualimage.phtml';

    /**
     * Helper to get the row class
     *
     * @access public
     * @return string
     * @author Kevin Sweeney (kevin@robotjumpstart.com)
     */
    public function getRowClass()
    {
        if($this->hasData('left_image') && $this->hasData('right_image')){
            return 'double';
        }
        if($this->hasData('left_image'))
        {
            return 'single left';
        }
        return 'single right';
    }


    /**
     * Helper to get the image class
     *
     * @access public
     * @return string
     * @author Kevin Sweeney (kevin@robotjumpstart.com)
     */
    public function getImageClass()
    {
        if($this->hasData('left_image') && $this->hasData('right_image')){
            return '4';
        }
        return '5';
    }

    /**
     * Helper to get the main text class
     *
     * @access public
     * @return string
     * @author Kevin Sweeney (kevin@robotjumpstart.com)
     */
    public function getMainTextClass()
    {
        if($this->hasData('left_image')){
            // if we have a left or a left and a right
            return '4';
        }
        if($this->hasData('right_image'))
        {
            // no left, but a right, we want to offset a few (3 + 4 + 5 = 12)
            return '4 col-sm-offset-3';
        }
        return '8';
    }
}

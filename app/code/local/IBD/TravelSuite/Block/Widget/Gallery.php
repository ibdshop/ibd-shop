<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Gallery parent controller
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotjumpstart.com)
 */
class IBD_TravelSuite_Block_Widget_Gallery extends IBD_TravelSuite_Block_Block_WidgetAbstract
{
    protected $_galleryMeta = null;
    protected $_galleryField = 'gallery';
    protected $_carouselIdKey = 'ibd_travelsuite_carousel_id_counter';
    protected $_carouselId = null;

    /**
     * Reset the cached gallery. Only used when a widget has multiple galleries.
     *
     * @access public
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function resetGallery(){
        $this->_galleryMeta = null;
    }

    /**
     * helper function to get the JSON decoded gallery info
     *
     * @access public
     * @return object
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function getGallery($fieldName = null)
    {
        if( $this->_galleryMeta === null )
        {
            $this->_galleryMeta = array();
            if($fieldName === null) $fieldName = $this->_galleryField;
            foreach (json_decode($this->getData($fieldName), true) as $id => $data)
            {
                if($data['position'] == 0) continue;
                
                $this->_galleryMeta[$data['position']] = $data;
            }
            ksort($this->_galleryMeta);
        }
        return $this->_galleryMeta;
    }

    /**
     * helper to use the magento registry to support multiple carousels on a page
     *
     * @access public
     * @return string
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function getUniqueWidgetCarouselId()
    {
        // see if this gallery already has an id, return it if it does.
        if ($this->_carouselId) {
            return $this->_carouselId;
        }

        // no ID yet, try to get the next one from the registry
        $carouselId = Mage::registry($this->_carouselIdKey);

        // if we have one, unset the registry so we can increment it. otherwise, start at 0.
        if (!$carouselId) {
            $carouselId = 1;
        }
        else {
            Mage::unregister($this->_carouselIdKey);
        }

        // register the next ID, set this gallery's ID, then return it
        $this->_carouselId = $carouselId;
        Mage::register($this->_carouselIdKey, $carouselId + 1);
        return $this->_carouselId;
    }

    /**
     * helper to get the bootstrap column span class from the image span
     *
     * @access public
     * @return string
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function getImageClass($span)
    {
        switch ($span)
        {
            case 1:
                return 'col-sm-2';

            case 2:
                return 'col-sm-4';

            case 3:
                return 'col-sm-6';

            case 4:
                return 'col-sm-8';

            case 5:
                return 'col-sm-10';

            case 6:
                return 'col-sm-12';

            default:
                return 'no-span';
        }
    }
}
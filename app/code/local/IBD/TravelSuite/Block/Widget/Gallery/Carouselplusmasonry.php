<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Carouselplusmasonry widget block
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotjumpstart.com)
 */
class IBD_TravelSuite_Block_Widget_Gallery_Carouselplusmasonry extends IBD_TravelSuite_Block_Widget_Gallery
{
    protected $_htmlTemplate = 'ibd_travelsuite/widget/gallery/carouselplusmasonry.phtml';
}
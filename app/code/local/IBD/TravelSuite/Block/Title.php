<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Title helper block
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Block_Title extends IBD_TravelSuite_Block_Block_Abstract
{
	protected $_entity;
	protected $_title;

    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('ibd_travelsuite/title.phtml');
    }

	public function setInfo(Mage_Core_Model_Abstract $entity, $title)
	{
		$this->_entity = $entity;
		$this->_title = $title;
		return $this;
	}

	public function getTitle()
	{
		return $this->_title;
	}

	public function getEntity()
	{
		if(!$this->_entity)
		{
			throw $this->getBadMethodCallException();
		}
		return $this->_entity;
	}

	protected function getBadMethodCallException()
	{
		return new BadMethodCallException('Must call initialization method before toHtml: IBD_TravelSuite_Block_Title.setInfo(Mage_Core_Model_Abstract $entity, $title)');
	}
}
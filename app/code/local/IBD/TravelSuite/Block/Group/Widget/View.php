<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Group widget block
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Block_Group_Widget_View extends Mage_Core_Block_Template implements
    Mage_Widget_Block_Interface
{
    protected $_htmlTemplate = 'ibd_travelsuite/group/widget/view.phtml';

    /**
     * Prepare a for widget
     *
     * @access protected
     * @return IBD_TravelSuite_Block_Group_Widget_View
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml()
    {
        parent::_beforeToHtml();
        $groupId = $this->getData('group_id');
        if ($groupId) {
            $group = Mage::getModel('ibd_travelsuite/group')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($groupId);
            if ($group->getStatus()) {
                $this->setCurrentGroup($group);
                $this->setTemplate($this->_htmlTemplate);
            }
        }
        return $this;
    }
}

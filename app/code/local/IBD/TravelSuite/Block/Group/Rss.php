<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Group RSS block
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Block_Group_Rss extends Mage_Rss_Block_Abstract
{
    /**
     * Cache tag constant for feed reviews
     *
     * @var string
     */
    const CACHE_TAG = 'block_html_travelsuite_group_rss';

    /**
     * constructor
     *
     * @access protected
     * @return void
     * @author Ultimate Module Creator
     */
    protected function _construct()
    {
        $this->setCacheTags(array(self::CACHE_TAG));
        /*
         * setting cache to save the rss for 10 minutes
         */
        $this->setCacheKey('ibd_travelsuite_group_rss');
        $this->setCacheLifetime(600);
    }

    /**
     * toHtml method
     *
     * @access protected
     * @return string
     * @author Ultimate Module Creator
     */
    protected function _toHtml()
    {
        $url    = Mage::helper('ibd_travelsuite/group')->getGroupsUrl();
        $title  = Mage::helper('ibd_travelsuite')->__('Groups');
        $rssObj = Mage::getModel('rss/rss');
        $data  = array(
            'title'       => $title,
            'description' => $title,
            'link'        => $url,
            'charset'     => 'UTF-8',
        );
        $rssObj->_addHeader($data);
        $collection = Mage::getModel('ibd_travelsuite/group')->getCollection()
            ->addFieldToFilter('status', 1)
            ->addFieldToFilter('in_rss', 1)
            ->setOrder('created_at');
        $collection->load();
        foreach ($collection as $item) {
            $description = '<p>';
            $description .= '<div>'.
                Mage::helper('ibd_travelsuite')->__('Name').': 
                '.$item->getName().
                '</div>';
            $description .= '<div>'.
                Mage::helper('ibd_travelsuite')->__('Summary').': 
                '.$item->getSummary().
                '</div>';
            $description .= '<div>'.
                Mage::helper('ibd_travelsuite')->__('Detailed Description').': 
                '.$item->getDescription().
                '</div>';
            $description .= '<div>'.
                Mage::helper('ibd_travelsuite')->__('Extra HTML').': 
                '.$item->getHtml().
                '</div>';
            if ($item->getImage()) {
                $description .= '<div>';
                $description .= Mage::helper('ibd_travelsuite')->__('Main Image');
                $description .= '    <a href="'.Mage::helper('ibd_travelsuite/group')->getFileBaseUrl().$item->getImage().'">';
                $description .= '        <span>'. basename($item->getImage()).'</span>';
                $description .= '    </a>';
                $description .= '</div>';
            }
            $description .= '<div>'.
                Mage::helper('ibd_travelsuite')->__('Latitude').': 
                '.$item->getLat().
                '</div>';
            $description .= '<div>'.
                Mage::helper('ibd_travelsuite')->__('Longitude').': 
                '.$item->getLng().
                '</div>';
            $description .= '</p>';
            $data = array(
                'title'       => $item->getName(),
                'link'        => $item->getGroupUrl(),
                'description' => $description
            );
            $rssObj->_addEntry($data);
        }
        return $rssObj->createRssXml();
    }
}

<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Group view block
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Block_Group_View extends IBD_TravelSuite_Block_Block_Abstract
{
    /**
     * get the current group
     *
     * @access public
     * @return mixed (IBD_TravelSuite_Model_Group|null)
     * @author Ultimate Module Creator
     */
    public function getCurrentGroup()
    {
        return Mage::registry('current_group');
    }
}

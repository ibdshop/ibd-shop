<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Group list block
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author Ultimate Module Creator
 */
class IBD_TravelSuite_Block_Group_List extends Mage_Core_Block_Template
{
    /**
     * initialize
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $groups = Mage::getResourceModel('ibd_travelsuite/group_collection')
                         ->addFieldToFilter('status', array('gt' => 0));
        $groups->setOrder('name', 'asc');
        $this->setGroups($groups);
    }

    /**
     * prepare the layout
     *
     * @access protected
     * @return IBD_TravelSuite_Block_Group_List
     * @author Ultimate Module Creator
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $pager = $this->getLayout()->createBlock(
            'page/html_pager',
            'ibd_travelsuite.group.html.pager'
        )
        ->setCollection($this->getGroups());
        $this->setChild('pager', $pager);
        $this->getGroups()->load();
        return $this;
    }

    /**
     * get the pager html
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}

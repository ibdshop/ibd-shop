<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Abstract widget block
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotjumpstart.com)
 */
class IBD_TravelSuite_Block_Block_WidgetAbstract extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{
    /**
     * Prepare for the widget
     *
     * @access protected
     * @return IBD_TravelSuite_Block_Block_WidgetAbstract
     * @author Kevin Sweeney (kevin@robotjumpstart.com)
     */
    protected function _beforeToHtml()
    {
        parent::_beforeToHtml();
        $this->setTemplate($this->_htmlTemplate);
        return $this;
    }

    /**
     * Helper to get the color styles
     *
     * @access public
     * @return string
     * @author Kevin Sweeney (kevin@robotjumpstart.com)
     */
    public function getHeadingStyle()
    {
        $textColor = $this->getData('title_text_color');
        $backColor = $this->getData('title_background_color');

        if($textColor == '')
        {
            $textColor = '#000';
        }
        if($backColor == '')
        {
            $backColor = '#f6f6f6';
        }

        return "color:$textColor;background-color:$backColor";
    }

    /**
     * Helper to get the color styles
     *
     * @access public
     * @return string
     * @author Kevin Sweeney (kevin@robotjumpstart.com)
     */
    public function getBorderStyle()
    {
        $textColor = $this->getData('title_text_color');
        $borderColor = $this->getData('title_background_color');

        if($textColor == '')
        {
            $textColor = '#000';
        }
        if($borderColor == '')
        {
            $borderColor = '#f6f6f6';
        }

        return "color:$textColor;border-color:$borderColor";
    }

    /**
     * Helper to check if we are left or right justified
     *
     * @access public
     * @return boolean
     * @author Kevin Sweeney (kevin@robotjumpstart.com)
     */
    public function alignLeft($dataName)
    {
        return IBD_TravelSuite_Model_Adminhtml_Source_Align::LEFT == $this->getData($dataName);
    }
}

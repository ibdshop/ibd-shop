<?php
/**
 * IBD_TravelSuite extension
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Block template helper functions
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Block_Block_Abstract extends Mage_Core_Block_Template
{
	protected $_helper;

    /**
     * just a helper function to get the... helper
     *
     * @access public
     * @return Mage_Core_Block_Abstract
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function h()
    {
        if(!$this->_helper)
        {
            $this->_helper = Mage::helper('ibd_travelsuite');
        }
        return $this->_helper;
    }

    /**
     * shortcut for translations
     *
     * @access public
     * @param string
     * @return string
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function _e($text)
    {
    	echo $this->h()->__($text);
    }

    /**
     * shortcut to get the http request object
     *
     * @access public
     * @return Zend_Controller_Request_Http
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function getRequest()
    {
        return Mage::app()->getRequest();
    }

    /**
     * shortcut to get a request parameter
     *
     * @access public
     * @param string
     * @return string
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function getParam($value, $default=null)
    {
        return $this->getRequest()->getParam($value, $default);
    }

    /**
     * shortcut to get all request parameters
     *
     * @access public
     * @return array
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function getParams()
    {
        return $this->getRequest()->getParams();
    }
}
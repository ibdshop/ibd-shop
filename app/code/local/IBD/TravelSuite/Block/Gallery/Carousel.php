<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * entity gallery carousel block
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Block_Gallery_Carousel extends IBD_TravelSuite_Block_Block_Abstract
{
    protected $_type;
    protected $_fullWidth = false;

    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('ibd_travelsuite/gallery/carousel.phtml');
    }

    public function setType($type)
    {
        $this->_type = $type;
        return $this;
    }

    public function getType()
    {
        return $this->_type;
    }

    public function getEntity($page=false)
    {
        if($page || $this->_type == 'page')
        {
            return Mage::getSingleton('cms/page');
        }
        return Mage::registry('current_'.$this->_type);
    }

    public function getImages()
    {
        if($this->_type == 'page')
        {
            return $this->getCmsPageGalleryImages();
        }
        return $this->getEntity()->getGalleryImages(0); // 0 for thumb size skips resizing images
    }

    public function setFullWidth($fullWidth)
    {
        $this->_fullWidth = $fullWidth;
        return $this;
    }

    public function fullWidth()
    {
        return $this->_fullWidth;
    }

    public function getCmsPageGalleryImages()
    {
        $page = $this->getEntity(true);
        if($page)
        {
            //Mage::log('found page: '.$page->getPageId());
            $gallery = Mage::getModel('ibd_travelsuite/gallery')
                            ->getEntityImageCollection($page->getPageId(), 'ibd_page');

            $imageHelper = Mage::helper('ibd_travelsuite/gallery_image');
            $ibdHelper = Mage::helper('ibd_travelsuite');

            $images = array();
            foreach ($gallery as $galleryImage)
            {
                $images[] = $ibdHelper->getImagePathArray($galleryImage, $imageHelper, 1200, 0, 'image_path');
            }

            return $images;
        }
        Mage::log('no page');
        return array();
    }
}
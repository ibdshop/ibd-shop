<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Brand list block
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Block_Brand_Filter extends IBD_TravelSuite_Block_Region_Filter
{
	// Name Lists from the DB
    protected $_tags;
    protected $_groupCategories;
    protected $_groupBadges;

	/**
     * initialize
     *
     * @access public
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function __construct()
    {
    	parent::__construct();

        $this->_tags = array();
        foreach (Mage::getSingleton('ibd_travelsuite/tag')->getActiveTags('ibd_brand') as $tag)
        {
            $this->_tags[] = $tag;
        }

        $groupSingleton = Mage::getSingleton('ibd_travelsuite/group');
        $this->_groupBadges = $groupSingleton->getBadges();

        $this->_groupCategories = array();
        foreach ($groupSingleton->getActiveCategories() as $value)
        {
            $this->_groupCategories[] = $value->getCategory();
        }
    }

    /**
     * get the group badges
     *
     * @access public
     * @return array
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function getGroupBadges()
    {
    	return $this->_groupBadges;
    }

    /**
     * get the group categories
     *
     * @access public
     * @return array
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function getGroupCategories()
    {
        return $this->_groupCategories;
    }

    /**
     * get the tags
     *
     * @access public
     * @return array
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function getTags()
    {
    	return $this->_tags;
    }
}

<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Brand list block
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author Ultimate Module Creator
 */
class IBD_TravelSuite_Block_Brand_List extends IBD_TravelSuite_Block_Block_Abstract
{
    protected $limit;
    protected $offset;

    /**
     * initialize
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();

        $this->limit = $this->getParam('limit', 20);
        $this->offset = $this->getParam('offset', 0);
    }

    public function getBadges()
    {
        return Mage::getModel('ibd_travelsuite/group')->getBadges();
    }

    public function getBrandGroupIds($brand)
    {
        $groupIds = array();
        foreach (Mage::getSingleton('ibd_travelsuite/brand_group')->getGroupCollection($brand) as $group)
        {
            $groupIds[] = $group->getId();
        }
        return $groupIds;
    }

    public function getRegistryBrands()
    {
        $key = $this->getParam('key');
        if($key)
        {
            $brand = Mage::getSingleton('ibd_travelsuite/brand')
                    ->getCollection()
                    ->addFieldToFilter('url_key', $key)
                    ->getFirstItem();

            if($brand->getId()) return array($brand);
            return array();
        }

        Mage::helper('ibd_travelsuite/search')->filterBrands($this->getRequest(), $this->limit, $this->offset);
        return Mage::registry('brand_search_results');
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function getoffset()
    {
        return $this->offset;
    }

    public function getFullBrandDataJSON()
    {
        $cache = Mage::app()->getCache();
        $fullBrandDataCacheKey = 'ibd_full_brand_data';

        $json = $cache->load($fullBrandDataCacheKey);

        if( !$json )
        {
            $data = array();
            $data['brands'] = array();
            $data['brandGroups'] = array();
            $data['brandTags'] = array();
            $collection = Mage::getSingleton('ibd_travelsuite/brand')
                                    ->getCollection()
                                    ->addFieldToFilter('status', array('gt' => 0));
            $collection->getSelect()->order('name asc');
            foreach ($collection as $brand)
            {
                $data['brands'][] = array(
                    'name' => $brand->getName(),
                    'id' => $brand->getId(),
                    'thumb' => $brand->getStatus() == IBD_TravelSuite_Model_Brand::SHOWCASE && $brand->getImage() ?
                                    (string) Mage::helper('ibd_travelsuite/brand_image')->init($brand)->resize(330) :
                                    null,
                    'addresses' => json_decode($brand->getGroupData()),
                    'showcase' => $brand->getStatus() == IBD_TravelSuite_Model_Brand::SHOWCASE,
                    'url' => $brand->getBrandUrl(),
                    'featured' => $brand->getFeatured(),
                    'key' => $brand->getUrlKey(),
                );

                $data['brandTags'][$brand->getId()] = array();
                $data['brandGroups'][$brand->getId()] = array();
            }

            foreach (Mage::getSingleton('ibd_travelsuite/brand_group')->getCollection() as $relation)
            {
                $data['brandGroups'][$relation->getBrandId()][] = $relation->getGroupId();
            }

            $data['regions'] = array();
            foreach (Mage::getSingleton('ibd_travelsuite/region')
                    ->getCollection()->addFieldToFilter('status', array('gt' => 0)) as $region)
            {
                $data['regions'][$region->getId()] = array(
                    'name' => $region->getName(),
                    'groupIds' => array()
                );
            }

            $data['groups'] = array();
            foreach (Mage::getSingleton('ibd_travelsuite/group')
                    ->getCollection()->addFieldToFilter('status', array('gt' => 0)) as $group)
            {
                $data['regions'][$group->getRegionId()]['groupIds'][] = $group->getId();
                $data['groups'][$group->getId()] = array(
                    'name' => $group->getName(),
                    'regionId' => $group->getRegionId(),
                );
            }

            $data['tags'] = array();
            foreach (Mage::getSingleton('ibd_travelsuite/tag_relation')->getAllTags('ibd_brand') as $tag)
            {
                $data['tags'][$tag->getTagId()] = $tag->getTag();
                $data['brandTags'][$tag->getRelationId()][] = $tag->getTagId();
            }

            $json = json_encode($data);

             // cache for 10 minutes (10 minutes * 60 seconds)
            $cache->save($json, $fullBrandDataCacheKey, array(IBD_TravelSuite_Helper_Data::CACHE_TAG), 600);
        }

        return $json;
    }
}

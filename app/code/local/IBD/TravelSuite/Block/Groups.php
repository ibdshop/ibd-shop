<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Advertisement model
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Block_Groups extends IBD_TravelSuite_Block_Block_Abstract
{
	public function getGroups($status = -1, $limit = 0, $order = null)
	{
		return Mage::getModel('ibd_travelsuite/group')->getGroups($status, $limit, $order);
	}

	public function getFeaturedGroups()
	{
		return $this->getGroups(IBD_TravelSuite_Model_Group::FEATURED, 3);
	}
}
<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Cms_Page edit gallery tab
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Block_Adminhtml_Cms_Page_Edit_Tab_Gallery
	extends IBD_TravelSuite_Block_Adminhtml_Helper_Form_Gallery
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected $_type = 'page';

    protected function setRelId()
    {
        $this->_relId = Mage::registry('cms_page')->getPageId();
    }

    public function getGalleryUploadUrl()
    {
        return Mage::getUrl('adminhtml/travelsuite_page/upload');
    }

    public function getTabLabel()
    {
        return Mage::helper('cms')->__('Page Carousel');
    }

    public function getTabTitle()
    {
        return Mage::helper('cms')->__('Page Carousel');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }
}
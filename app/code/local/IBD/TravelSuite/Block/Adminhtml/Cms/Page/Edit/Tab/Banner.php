<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Brand edit gallery tab
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Block_Adminhtml_Cms_Page_Edit_Tab_Banner
	extends IBD_TravelSuite_Block_Adminhtml_Helper_BannerAbstract
	implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected $_type = 'brand';
    protected $_columnPrefix = 'ibd_';
    protected $_setFieldNameSuffix = false;
    protected $_addColorFields = true;

    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('ibd_shared/color_tab.phtml');
    }

    protected function getFormValues()
    {
        return Mage::registry('cms_page')->getData();
    }

    public function getTabLabel()
    {
        return Mage::helper('cms')->__('Banner Icon');
    }

    public function getTabTitle()
    {
        return Mage::helper('cms')->__('Banner Icon');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }
}

<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Country admin edit form
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Block_Adminhtml_Widget_Gallery extends Mage_Adminhtml_Block_Widget_Form_Renderer_Fieldset_Element
{
	const WIDGET_GALLERY_TYPE = 'widget_gallery';

	protected $_meta;

    /**
     * prepare the layout
     *
     * @access protected
     * @return void
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
	protected function _prepareLayout()
	{
		parent::_prepareLayout();
		$head = $this->getLayout()->getBlock('head');
		$head->setCanLoadTinyMce(true);
		$head->setCanLoadExtJs(true);
        $this->setTemplate('ibd_travelsuite/widget/gallery.phtml');
	}

	public function getMeta()
	{
		if(!$this->_meta)
		{
			$this->_meta = $this->getElement()->hasData('value')
                                ? json_decode($this->getElement()->getData('value'), true)
                                : array();
		}
		return $this->_meta;
	}

    /**
     * helper to determine if the gallery should show a column for the image column span
     *
     * @access public
     * @return boolean
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
	public function getShowColSpan()
	{
		return false;
	}

    /**
     * helper to get the related gallery images
     *
     * @access public
     * @return IBD_TravelSuite_Model_Resource_Gallery
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
	public function getGalleryImages()
	{
		if(sizeof($this->getMeta()) == 0)
		{
			return array();
		}

		return Mage::getSingleton('ibd_travelsuite/gallery')->getCollection()
					->addFieldToFilter('entity_id', array('in' => array_keys($this->getMeta())));
	}

    /**
     * helper to get the gallery relation (for widgets, i couldn't figure out how
     * to get the widget instance ID from here, so i'm using 0 for the relation ID
     * and using the element string as the relation type. it's still unique...)
     *
     * @access public
     * @return string
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
	public function getRelationType()
	{
		return self::WIDGET_GALLERY_TYPE;
	}

    /**
     * helper to get the gallery upload URL
     *
     * @access public
     * @return string
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
	public function getGalleryUploadUrl()
	{
		return Mage::getUrl('adminhtml/travelsuite_widget_gallery/upload');
	}
}
<?php
class IBD_TravelSuite_Block_Adminhtml_Widget_Image extends Mage_Adminhtml_Block_Widget_Form_Renderer_Fieldset_Element
{
	protected function _prepareLayout()
	{
		parent::_prepareLayout();
		$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
		$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
	}

	public function prepareElementHtml(Varien_Data_Form_Element_Abstract $element)
	{	
		$config = $this->getConfig();
		$chooseButton = $this->getLayout()->createBlock('adminhtml/widget_button')
			->setType('button')
			->setClass('scalable btn-chooser')
			->setLabel($config['button']['open'])
			->setOnclick('MediabrowserUtility.openDialog(\''.$this->getUrl('*/cms_wysiwyg_images/index', array('target_element_id' => $element->getName())).'\')')
			->setDisabled($element->getReadonly());

		$text = new Varien_Data_Form_Element_Text();
		$text->setForm($element->getForm())
			->setId($element->getName())
			->setName($element->getName())
			->setClass('widget-option input-text');

		if ($element->getRequired()) {
			$text->addClass('required-entry');
		}

		if ($element->getValue()) {
			$text->setValue($element->getValue());
		}

		$element->setData('after_element_html', $text->getElementHtml().$chooseButton->toHtml());
		return $element;
	}
}
<?php
class IBD_TravelSuite_Block_Adminhtml_Widget_Wyswyg extends Mage_Adminhtml_Block_Widget_Form_Renderer_Fieldset_Element
{
	public function render(Varien_Data_Form_Element_Abstract $element)
	{
		$editor = new Varien_Data_Form_Element_Editor($element->getData());
		$editor->setId($element->getId());
		$editor->setForm($element->getForm());
		$editor->setWysiwyg(true);
		$editor->setForceLoad(false);
		$editor->setConfig(Mage::getSingleton('cms/wysiwyg_config')->getConfig());
		return parent::render($editor);
	}
}
<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Country admin edit form
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Block_Adminhtml_Widget_ColSpanGallery extends IBD_TravelSuite_Block_Adminhtml_Widget_Gallery
{
    /**
     * helper to determine if the gallery should show a column for the image column span
     *
     * @access public
     * @return boolean
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
	public function getShowColSpan()
	{
		return true;
	}
}
<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Brand edit form tab
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Block_Adminhtml_Brand_Edit_Tab_Groups extends Mage_Adminhtml_Block_Widget_Form
{
    protected $groupData = '{}';

    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('ibd_travelsuite/brand/groups_tab.phtml');
    }

    /**
     * prepare the form
     *
     * @access protected
     * @return IBD_TravelSuite_Block_Adminhtml_Brand_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setFieldNameSuffix('brand');
        $this->setForm($form);

        $fieldset = $form->addFieldset(
            'brand_groups_form',
            array('legend' => Mage::helper('ibd_travelsuite')->__('Groups'))
        );

        $fieldset->addField(
            'group_ids',
            'multiselect',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Groups & Addresses'),
                'name' => 'group_ids',
                'note' => $this->__('To Select Multiple or Un-Select, Hold Command (Mac) or Ctrl (PC)'),
                'values' => Mage::getResourceModel('ibd_travelsuite/group_collection')->toOptionArray(),
            )
        );

        $groupIds = array();
        foreach (Mage::registry('current_brand_groups') as $group) {
            $groupIds[] = $group->getGroupId();
        }
        $formValues = array('group_ids' => $groupIds);
        $form->setValues($formValues);

        $this->groupData = Mage::registry('current_brand')->getGroupData();
        return parent::_prepareForm();
    }

    public function getGroupJSON()
    {
        if($this->groupData) return $this->groupData;
        return '{}';
    }
}

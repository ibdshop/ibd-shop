<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Brand edit form tab
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Block_Adminhtml_Brand_Edit_Tab_Color extends Mage_Adminhtml_Block_Widget_Form
{
    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('ibd_shared/color_tab.phtml');
    }

    /**
     * prepare the form
     *
     * @access protected
     * @return IBD_TravelSuite_Block_Adminhtml_Brand_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setFieldNameSuffix('brand');
        $this->setForm($form);

        $fieldset = $form->addFieldset(
            'brand_color_form',
            array('legend' => Mage::helper('ibd_travelsuite')->__('Colors'))
        );
        $fieldset->addField(
            'fore_color',
            'text',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Text Color'),
                'name'  => 'fore_color',
                'note'  => $this->__('If empty, default will be white or gray depending on the background color'),
           )
        );
        $fieldset->addField(
            'color',
            'text',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Background Color'),
                'name'  => 'color',
                'note'  => $this->__('The highlight color hex value, e.g., 981D19'),
           )
        );

        $form->addValues(Mage::registry('current_brand')->getData());
        return parent::_prepareForm();
    }
}

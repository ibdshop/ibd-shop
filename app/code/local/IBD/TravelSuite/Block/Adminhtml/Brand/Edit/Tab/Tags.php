<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Brand edit form tab
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Block_Adminhtml_Brand_Edit_Tab_Tags extends Mage_Adminhtml_Block_Widget_Form
{
    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('ibd_travelsuite/tags_tab.phtml');
    }

    public function getTitle()
    {
        return 'Brand Catagories';
    }

    public function getAllTagsJSON()
    {
        $tags = Mage::getSingleton('ibd_travelsuite/tag')->getAllTags();
        $simpleArray = array();
        foreach ($tags as $tag)
        {
            $simpleArray[] = $tag->getTag();
        }
        return json_encode($simpleArray);
    }

    public function getTagRelationsJSON()
    {
        $tags = Mage::registry('current_brand')->getSelectedTags();
        $tagValueArray = array();
        foreach ($tags as $tag)
        {
            $tagValueArray[$tag->getTagId()] = $tag->getTag();
        }
        return json_encode($tagValueArray);
    }

    public function getSelectName()
    {
        return 'brand[tags][]';
    }
}


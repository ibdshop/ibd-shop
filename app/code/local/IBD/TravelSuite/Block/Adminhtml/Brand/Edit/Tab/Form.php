<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Brand edit form tab
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Block_Adminhtml_Brand_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return IBD_TravelSuite_Block_Adminhtml_Brand_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $helper = Mage::helper('ibd_travelsuite');
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('brand_');
        $form->setFieldNameSuffix('brand');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'brand_form',
            array('legend' => $helper->__('Brand'))
        );
        $fieldset->addType(
            'image',
            Mage::getConfig()->getBlockClassName('ibd_travelsuite/adminhtml_brand_helper_image')
        );
        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig();

        $fieldset->addField(
            'featured',
            'select',
            array(
                'label' => $helper->__('Featured'),
                'name'  => 'featured',
                'note'  => $helper->__('Feature this brand in the brand showcase'),
                'values'=> array(
                    array(
                        'value' => 1,
                        'label' => $helper->__('Yes'),
                    ),
                    array(
                        'value' => 0,
                        'label' => $helper->__('No'),
                    ),
                ),
           )
        );

        $fieldset->addField(
            'status',
            'select',
            array(
                'label'  => $helper->__('Status'),
                'name'   => 'status',
                'values' => Mage::getSingleton('ibd_travelsuite/brand')->getStatusOptions(),
            )
        );

        $fieldset->addField(
            'name',
            'text',
            array(
                'label' => $helper->__('Name'),
                'name'  => 'name',
                'note'	=> $this->__('The brand name'),
                'required'  => true,
                'class' => 'required-entry',
           )
        );

        $fieldset->addField(
            'official_url',
            'text',
            array(
                'label' => $helper->__('Official URL'),
                'name'  => 'official_url',
            )
        );

        $fieldset->addField(
            'summary',
            'text',
            array(
                'label' => $helper->__('Summary'),
                'name'  => 'summary',
                'note'	=> $this->__('Brand summary'),
            )
        );

        $fieldset->addField(
            'description',
            'editor',
            array(
                'label' => $helper->__('Description'),
                'name'  => 'description',
                'config' => $wysiwygConfig,
                'note'	=> $this->__('The brand description'),
           )
        );

        $fieldset->addField(
            'url_key',
            'text',
            array(
                'label' => $helper->__('Url key'),
                'name'  => 'url_key',
                'note'  => $helper->__('Relative to Website Base URL')
            )
        );

        if (Mage::app()->isSingleStoreMode()) {
            $fieldset->addField(
                'store_id',
                'hidden',
                array(
                    'name'      => 'stores[]',
                    'value'     => Mage::app()->getStore(true)->getId()
                )
            );
            Mage::registry('current_brand')->setStoreId(Mage::app()->getStore(true)->getId());
        }
        $formValues = Mage::registry('current_brand')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getBrandData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getBrandData());
            Mage::getSingleton('adminhtml/session')->setBrandData(null);
        } elseif (Mage::registry('current_brand')) {
            $formValues = array_merge($formValues, Mage::registry('current_brand')->getData());
        }

        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}

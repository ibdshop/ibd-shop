<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Brand edit images tab
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Block_Adminhtml_Brand_Edit_Tab_Images extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return IBD_TravelSuite_Block_Adminhtml_Brand_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setFieldNameSuffix('brand');
        $this->setForm($form);

        $fieldset = $form->addFieldset(
            'brand_image_form',
            array('legend' => Mage::helper('ibd_travelsuite')->__('Images'))
        );
        $fieldset->addType(
            'image',
            Mage::getConfig()->getBlockClassName('ibd_travelsuite/adminhtml_brand_helper_image')
        );

        $fieldset->addField(
            'image',
            'image',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Store Image'),
                'name'  => 'image',
                'note'  => $this->__('Displays on the brand detail page'),
           )
        );
        $fieldset->addField(
            'logo',
            'image',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Logo'),
                'name'  => 'logo',
                'note'  => $this->__('Displays in the brand showcase'),
           )
        );
        $fieldset->addField(
            'product_image',
            'image',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Sample Product Image'),
                'name'  => 'product_image',
                'note'  => $this->__('Displays in the brand showcase'),
           )
        );
        $fieldset->addField(
            'wechat_qr',
            'image',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('WeChat QR Code'),
                'name'  => 'wechat_qr',
                'note'  => $this->__('Currently not displayed on the site'),
           )
        );

        $formValues = Mage::registry('current_brand')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getBrandData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getBrandData());
            Mage::getSingleton('adminhtml/session')->setBrandData(null);
        } elseif (Mage::registry('current_brand')) {
            $formValues = array_merge($formValues, Mage::registry('current_brand')->getData());
        }

        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}

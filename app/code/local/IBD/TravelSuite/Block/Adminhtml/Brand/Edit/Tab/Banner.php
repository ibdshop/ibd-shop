<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Brand edit gallery tab
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Block_Adminhtml_Brand_Edit_Tab_Banner extends IBD_TravelSuite_Block_Adminhtml_Helper_BannerAbstract
{
    protected $_type = 'brand';

    protected function getSessionData()
    {
        return Mage::getSingleton('adminhtml/session')->getBrandData();
    }

    protected function setSessionData($data)
    {
        Mage::getSingleton('adminhtml/session')->setBrandData(null);
    }
}

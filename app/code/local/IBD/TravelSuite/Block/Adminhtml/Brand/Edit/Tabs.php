<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Brand admin edit tabs
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Block_Adminhtml_Brand_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('brand_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('ibd_travelsuite')->__('Brand'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return IBD_TravelSuite_Block_Adminhtml_Brand_Edit_Tabs
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_brand',
            array(
                'label'   => Mage::helper('ibd_travelsuite')->__('Brand'),
                'title'   => Mage::helper('ibd_travelsuite')->__('Brand'),
                'content' => $this->getLayout()
                                  ->createBlock('ibd_travelsuite/adminhtml_brand_edit_tab_form')
                                  ->toHtml(),
            )
        );
        
        $this->addTab(
            'form_banner_brand',
            array(
                'label'   => Mage::helper('ibd_travelsuite')->__('Banner Icon & Position'),
                'title'   => Mage::helper('ibd_travelsuite')->__('Banner Icon & Position'),
                'content' => $this->getLayout()
                                  ->createBlock('ibd_travelsuite/adminhtml_brand_edit_tab_banner')
                                  ->toHtml(),
            )
        );

        $this->addTab(
            'form_images_brand',
            array(
                'label'   => Mage::helper('ibd_travelsuite')->__('Images'),
                'title'   => Mage::helper('ibd_travelsuite')->__('Images'),
                'content' => $this->getLayout()
                                  ->createBlock('ibd_travelsuite/adminhtml_brand_edit_tab_images')
                                  ->toHtml(),
            )
        );
        
        $this->addTab(
            'form_gallery_brand',
            array(
                'label'   => Mage::helper('ibd_travelsuite')->__('Carousel'),
                'title'   => Mage::helper('ibd_travelsuite')->__('Carousel'),
                'content' => $this->getLayout()
                                  ->createBlock('ibd_travelsuite/adminhtml_brand_edit_tab_gallery')
                                  ->toHtml(),
            )
        );
        $this->addTab(
            'form_color_brand',
            array(
                'label'   => Mage::helper('ibd_travelsuite')->__('Highlight Color'),
                'title'   => Mage::helper('ibd_travelsuite')->__('Highlight'),
                'content' => $this->getLayout()
                                  ->createBlock('ibd_travelsuite/adminhtml_brand_edit_tab_color')
                                  ->toHtml(),
            )
        );
        $this->addTab(
            'form_social_brand',
            array(
                'label'   => Mage::helper('ibd_travelsuite')->__('Social Media'),
                'title'   => Mage::helper('ibd_travelsuite')->__('Social Media'),
                'content' => $this->getLayout()
                                  ->createBlock('ibd_travelsuite/adminhtml_brand_edit_tab_social')
                                  ->toHtml(),
            )
        );
        $this->addTab(
            'form_groups_brand',
            array(
                'label'   => Mage::helper('ibd_travelsuite')->__('Groups & Addresses'),
                'title'   => Mage::helper('ibd_travelsuite')->__('Groups & Addresses'),
                'content' => $this->getLayout()
                                  ->createBlock('ibd_travelsuite/adminhtml_brand_edit_tab_groups')
                                  ->toHtml(),
            )
        );
        $this->addTab(
            'form_tags_brand',
            array(
                'label'   => Mage::helper('ibd_travelsuite')->__('Categories'),
                'title'   => Mage::helper('ibd_travelsuite')->__('Categories'),
                'content' => $this->getLayout()
                                  ->createBlock('ibd_travelsuite/adminhtml_brand_edit_tab_tags')
                                  ->toHtml(),
            )
        );
        $this->addTab(
            'form_sponsors_brand',
            array(
                'label'   => Mage::helper('ibd_travelsuite')->__('Content Images'),
                'title'   => Mage::helper('ibd_travelsuite')->__('Content Images'),
                'content' => $this->getLayout()->createBlock(
                    'ibd_travelsuite/adminhtml_brand_edit_tab_sponsors'
                )
                ->toHtml(),
            )
        );
        $this->addTab(
            'form_meta_brand',
            array(
                'label'   => Mage::helper('ibd_travelsuite')->__('Meta'),
                'title'   => Mage::helper('ibd_travelsuite')->__('Meta'),
                'content' => $this->getLayout()
                                  ->createBlock('ibd_travelsuite/adminhtml_brand_edit_tab_meta')
                                  ->toHtml(),
            )
        );
        if (!Mage::app()->isSingleStoreMode()) {
            $this->addTab(
                'form_store_brand',
                array(
                    'label'   => Mage::helper('ibd_travelsuite')->__('Store views'),
                    'title'   => Mage::helper('ibd_travelsuite')->__('Store views'),
                    'content' => $this->getLayout()
                                      ->createBlock('ibd_travelsuite/adminhtml_brand_edit_tab_stores')
                                      ->toHtml(),
                )
            );
        }
        $this->addTab(
            'products',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Associated products'),
                'url'   => $this->getUrl('*/*/products', array('_current' => true)),
                'class' => 'ajax'
            )
        );
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve brand entity
     *
     * @access public
     * @return IBD_TravelSuite_Model_Brand
     * @author Ultimate Module Creator
     */
    public function getBrand()
    {
        return Mage::registry('current_brand');
    }
}

<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Continent admin edit form
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Block_Adminhtml_Continent_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'ibd_travelsuite';
        $this->_controller = 'adminhtml_continent';
        $this->_updateButton(
            'save',
            'label',
            Mage::helper('ibd_travelsuite')->__('Save Continent')
        );
        $this->_updateButton(
            'delete',
            'label',
            Mage::helper('ibd_travelsuite')->__('Delete Continent')
        );
        $this->_addButton(
            'saveandcontinue',
            array(
                'label'   => Mage::helper('ibd_travelsuite')->__('Save And Continue Edit'),
                'onclick' => 'saveAndContinueEdit()',
                'class'   => 'save',
            ),
            -100
        );
        $this->_formScripts[] = "
            function saveAndContinueEdit() {
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * get the edit form header
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getHeaderText()
    {
        if (Mage::registry('current_continent') && Mage::registry('current_continent')->getId()) {
            return Mage::helper('ibd_travelsuite')->__(
                "Edit Continent '%s'",
                $this->escapeHtml(Mage::registry('current_continent')->getName())
            );
        } else {
            return Mage::helper('ibd_travelsuite')->__('Add Continent');
        }
    }
}

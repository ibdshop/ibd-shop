<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Continent admin edit tabs
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Block_Adminhtml_Continent_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('continent_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('ibd_travelsuite')->__('Continent'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return IBD_TravelSuite_Block_Adminhtml_Continent_Edit_Tabs
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_continent',
            array(
                'label'   => Mage::helper('ibd_travelsuite')->__('Continent'),
                'title'   => Mage::helper('ibd_travelsuite')->__('Continent'),
                'content' => $this->getLayout()->createBlock(
                    'ibd_travelsuite/adminhtml_continent_edit_tab_form'
                )
                ->toHtml(),
            )
        );
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve continent entity
     *
     * @access public
     * @return IBD_TravelSuite_Model_Continent
     * @author Ultimate Module Creator
     */
    public function getContinent()
    {
        return Mage::registry('current_continent');
    }
}

<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Group admin grid block
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Block_Adminhtml_Group_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * constructor
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('groupGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * prepare collection
     *
     * @access protected
     * @return IBD_TravelSuite_Block_Adminhtml_Group_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('ibd_travelsuite/group')
            ->getCollection();
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * prepare grid collection
     *
     * @access protected
     * @return IBD_TravelSuite_Block_Adminhtml_Group_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'entity_id',
            array(
                'header' => Mage::helper('ibd_travelsuite')->__('Id'),
                'index'  => 'entity_id',
                'type'   => 'number'
            )
        );
        $this->addColumn(
            'region_id',
            array(
                'header'    => Mage::helper('ibd_travelsuite')->__('Region'),
                'index'     => 'region_id',
                'type'      => 'options',
                'options'   => Mage::getResourceModel('ibd_travelsuite/region_collection')
                    ->toOptionHash(),
                'renderer'  => 'ibd_travelsuite/adminhtml_helper_column_renderer_parent',
                'params'    => array(
                    'id'    => 'getRegionId'
                ),
                'base_link' => 'adminhtml/travelsuite_region/edit'
            )
        );
        $this->addColumn(
            'name',
            array(
                'header'    => Mage::helper('ibd_travelsuite')->__('Name'),
                'align'     => 'left',
                'index'     => 'name',
            )
        );
        
        $this->addColumn(
            'status',
            array(
                'header'  => Mage::helper('ibd_travelsuite')->__('Status'),
                'index'   => 'status',
                'type'    => 'options',
                'options' => Mage::getSingleton('ibd_travelsuite/group')->getStatusArray(),
            )
        );
        $this->addColumn(
            'url_key',
            array(
                'header' => Mage::helper('ibd_travelsuite')->__('URL key'),
                'index'  => 'url_key',
            )
        );
        $this->addColumn(
            'action',
            array(
                'header'  =>  Mage::helper('ibd_travelsuite')->__('Action'),
                'width'   => '100',
                'type'    => 'action',
                'getter'  => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('ibd_travelsuite')->__('Edit'),
                        'url'     => array('base'=> '*/*/edit'),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'is_system' => true,
                'sortable'  => false,
            )
        );
        $this->addExportType('*/*/exportCsv', Mage::helper('ibd_travelsuite')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('ibd_travelsuite')->__('Excel'));
        $this->addExportType('*/*/exportXml', Mage::helper('ibd_travelsuite')->__('XML'));
        return parent::_prepareColumns();
    }

    /**
     * prepare mass action
     *
     * @access protected
     * @return IBD_TravelSuite_Block_Adminhtml_Group_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('group');
        $this->getMassactionBlock()->addItem(
            'delete',
            array(
                'label'=> Mage::helper('ibd_travelsuite')->__('Delete'),
                'url'  => $this->getUrl('*/*/massDelete'),
                'confirm'  => Mage::helper('ibd_travelsuite')->__('Are you sure?')
            )
        );
        $this->getMassactionBlock()->addItem(
            'status',
            array(
                'label'      => Mage::helper('ibd_travelsuite')->__('Change status'),
                'url'        => $this->getUrl('*/*/massStatus', array('_current'=>true)),
                'additional' => array(
                    'status' => array(
                        'name'   => 'status',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('ibd_travelsuite')->__('Status'),
                        'values' => Mage::getSingleton('ibd_travelsuite/group')->getStatusArray(),
                    )
                )
            )
        );
        $values = Mage::getResourceModel('ibd_travelsuite/region_collection')->toOptionHash();
        $values = array_reverse($values, true);
        $values[''] = '';
        $values = array_reverse($values, true);
        $this->getMassactionBlock()->addItem(
            'region_id',
            array(
                'label'      => Mage::helper('ibd_travelsuite')->__('Change Region'),
                'url'        => $this->getUrl('*/*/massRegionId', array('_current'=>true)),
                'additional' => array(
                    'flag_region_id' => array(
                        'name'   => 'flag_region_id',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('ibd_travelsuite')->__('Region'),
                        'values' => $values
                    )
                )
            )
        );
        return $this;
    }

    /**
     * get the row url
     *
     * @access public
     * @param IBD_TravelSuite_Model_Group
     * @return string
     * @author Ultimate Module Creator
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * get the grid url
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    /**
     * after collection load
     *
     * @access protected
     * @return IBD_TravelSuite_Block_Adminhtml_Group_Grid
     * @author Ultimate Module Creator
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }
}

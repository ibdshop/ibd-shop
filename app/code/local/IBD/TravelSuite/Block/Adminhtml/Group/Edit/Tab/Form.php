<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Group edit form tab
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Block_Adminhtml_Group_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return IBD_TravelSuite_Block_Adminhtml_Group_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('group_');
        $form->setFieldNameSuffix('group');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'group_form',
            array('legend' => Mage::helper('ibd_travelsuite')->__('Group'))
        );
        $fieldset->addType(
            'image',
            Mage::getConfig()->getBlockClassName('ibd_travelsuite/adminhtml_group_helper_image')
        );
        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig();
        $values = Mage::getResourceModel('ibd_travelsuite/region_collection')
            ->toOptionArray();
        array_unshift($values, array('label' => '', 'value' => ''));

        $html = '<a href="{#url}" id="group_region_id_link" target="_blank"></a>';
        $html .= '<script type="text/javascript">
            function changeRegionIdLink() {
                if ($(\'group_region_id\').value == \'\') {
                    $(\'group_region_id_link\').hide();
                } else {
                    $(\'group_region_id_link\').show();
                    var url = \''.$this->getUrl('adminhtml/travelsuite_region/edit', array('id'=>'{#id}', 'clear'=>1)).'\';
                    var text = \''.Mage::helper('core')->escapeHtml($this->__('View {#name}')).'\';
                    var realUrl = url.replace(\'{#id}\', $(\'group_region_id\').value);
                    $(\'group_region_id_link\').href = realUrl;
                    $(\'group_region_id_link\').innerHTML = text.replace(\'{#name}\', $(\'group_region_id\').options[$(\'group_region_id\').selectedIndex].innerHTML);
                }
            }
            $(\'group_region_id\').observe(\'change\', changeRegionIdLink);
            changeRegionIdLink();
            </script>';


        $fieldset->addField(
            'status',
            'select',
            array(
                'label'  => Mage::helper('ibd_travelsuite')->__('Status'),
                'name'   => 'status',
                'values' => Mage::getSingleton('ibd_travelsuite/group')->getStatusOptions(),
            )
        );
        $fieldset->addField(
            'region_id',
            'select',
            array(
                'label'     => Mage::helper('ibd_travelsuite')->__('Region'),
                'name'      => 'region_id',
                'required'  => false,
                'values'    => $values,
                'after_element_html' => $html
            )
        );

        $fieldset->addField(
            'name',
            'text',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Name'),
                'name'  => 'name',
                'required'  => true,
                'class' => 'required-entry',
            )
        );

        $fieldset->addField(
            'official_url',
            'text',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Official URL'),
                'name'  => 'official_url',
            )
        );

        $fieldset->addField(
            'summary',
            'text',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Summary'),
                'name'  => 'summary',
                'note'	=> $this->__('A short summary of the shopping area'),
            )
        );

        $fieldset->addField(
            'html',
            'editor',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Bullet Points'),
                'name'  => 'html',
                'config' => $wysiwygConfig,
                'note'	=> $this->__('Bullet point highlights of the shopping area'),
            )
        );

        $fieldset->addField(
            'description',
            'editor',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Detailed Description'),
                'name'  => 'description',
                'config' => $wysiwygConfig,
                'note'  => $this->__('A detailed description of the shopping area'),
            )
        );

        $fieldset->addField(
            'image',
            'image',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Main Image'),
                'name'  => 'image',
                'note'	=> $this->__('The main image for the group'),
            )
        );

        $fieldset->addField(
            'url_key',
            'text',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Url key'),
                'name'  => 'url_key',
                'note'  => Mage::helper('ibd_travelsuite')->__('Relative to Website Base URL')
            )
        );
        $fieldset->addField(
            'in_rss',
            'select',
            array(
                'label'  => Mage::helper('ibd_travelsuite')->__('Show in rss'),
                'name'   => 'in_rss',
                'values' => array(
                    array(
                        'value' => 1,
                        'label' => Mage::helper('ibd_travelsuite')->__('Yes'),
                    ),
                    array(
                        'value' => 0,
                        'label' => Mage::helper('ibd_travelsuite')->__('No'),
                    ),
                ),
            )
        );
        $formValues = Mage::registry('current_group')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getGroupData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getGroupData());
            Mage::getSingleton('adminhtml/session')->setGroupData(null);
        } elseif (Mage::registry('current_group')) {
            $formValues = array_merge($formValues, Mage::registry('current_group')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}

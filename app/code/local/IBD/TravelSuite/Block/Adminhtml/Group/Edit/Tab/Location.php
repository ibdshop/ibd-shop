<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Group edit form tab
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Block_Adminhtml_Group_Edit_Tab_Location extends Mage_Adminhtml_Block_Widget_Form
{
    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('ibd_travelsuite/location_tab.phtml');
    }

    /**
     * prepare the form
     *
     * @access protected
     * @return IBD_TravelSuite_Block_Adminhtml_Group_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setFieldNameSuffix('group');
        $this->setForm($form);

        $data = Mage::registry('current_group')->getData();
        $directions = isset($data['directions']) ? json_decode($data['directions'], true) : null;

        $fieldset = $form->addFieldset(
            'group_location_form',
            array('legend' => Mage::helper('ibd_travelsuite')->__('Location'))
        );
        $fieldset->addField(
            'lat',
            'hidden',
            array( 'name'  => 'lat' )
        );
        $fieldset->addField(
            'lng',
            'hidden',
            array( 'name'  => 'lng' )
        );
        $fieldset->addField(
            'address',
            'textarea',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Group Address'),
                'name'  => 'address',
            )
        );
        $fieldset->addField(
            'driving',
            'textarea',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Driving Directions'),
                'name'  => 'driving',
                'note'  => $this->__('Directions for driving'),
                'value' => isset($directions['driving']) ? $directions['driving'] : '',
            )
        );
        $fieldset->addField(
            'walking',
            'textarea',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Walking Directions'),
                'name'  => 'walking',
                'note'  => $this->__('Directions for walking'),
                'value' => isset($directions['walking']) ? $directions['walking'] : '',
            )
        );

        $form->addValues($data);
        return parent::_prepareForm();
    }
}

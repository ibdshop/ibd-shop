<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Group admin edit tabs
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Block_Adminhtml_Group_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('group_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('ibd_travelsuite')->__('Group'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return IBD_TravelSuite_Block_Adminhtml_Group_Edit_Tabs
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_group',
            array(
                'label'   => Mage::helper('ibd_travelsuite')->__('Group'),
                'title'   => Mage::helper('ibd_travelsuite')->__('Group'),
                'content' => $this->getLayout()->createBlock(
                    'ibd_travelsuite/adminhtml_group_edit_tab_form'
                )
                ->toHtml(),
            )
        );
        $this->addTab(
            'form_banner_group',
            array(
                'label'   => Mage::helper('ibd_travelsuite')->__('Banner Icon & Position'),
                'title'   => Mage::helper('ibd_travelsuite')->__('Banner Icon & Position'),
                'content' => $this->getLayout()
                                  ->createBlock('ibd_travelsuite/adminhtml_group_edit_tab_banner')
                                  ->toHtml(),
            )
        );
        $this->addTab(
            'form_gallery_group',
            array(
                'label'   => Mage::helper('ibd_travelsuite')->__('Carousel'),
                'title'   => Mage::helper('ibd_travelsuite')->__('Carousel'),
                'content' => $this->getLayout()->createBlock(
                    'ibd_travelsuite/adminhtml_group_edit_tab_gallery'
                )
                ->toHtml(),
            )
        );
        $this->addTab(
            'form_location_group',
            array(
                'label'   => Mage::helper('ibd_travelsuite')->__('Location'),
                'title'   => Mage::helper('ibd_travelsuite')->__('Location'),
                'content' => $this->getLayout()->createBlock(
                    'ibd_travelsuite/adminhtml_group_edit_tab_location'
                )
                ->toHtml(),
            )
        );
        $this->addTab(
            'form_social_group',
            array(
                'label'   => Mage::helper('ibd_travelsuite')->__('Social Media'),
                'title'   => Mage::helper('ibd_travelsuite')->__('Social Media'),
                'content' => $this->getLayout()->createBlock(
                    'ibd_travelsuite/adminhtml_group_edit_tab_social'
                )
                ->toHtml(),
            )
        );
        $this->addTab(
            'form_badcat_group',
            array(
                'label'   => Mage::helper('ibd_travelsuite')->__('Badge & Category'),
                'title'   => Mage::helper('ibd_travelsuite')->__('Badge & Category'),
                'content' => $this->getLayout()->createBlock(
                    'ibd_travelsuite/adminhtml_group_edit_tab_badcat'
                )
                ->toHtml(),
            )
        );
        $this->addTab(
            'form_color_group',
            array(
                'label'   => Mage::helper('ibd_travelsuite')->__('Highlight Color'),
                'title'   => Mage::helper('ibd_travelsuite')->__('Highlight'),
                'content' => $this->getLayout()->createBlock(
                    'ibd_travelsuite/adminhtml_group_edit_tab_color'
                )
                ->toHtml(),
            )
        );
        $this->addTab(
            'form_sponsors_group',
            array(
                'label'   => Mage::helper('ibd_travelsuite')->__('Content Images'),
                'title'   => Mage::helper('ibd_travelsuite')->__('Content Images'),
                'content' => $this->getLayout()->createBlock(
                    'ibd_travelsuite/adminhtml_group_edit_tab_sponsors'
                )
                ->toHtml(),
            )
        );
        $this->addTab(
            'form_meta_group',
            array(
                'label'   => Mage::helper('ibd_travelsuite')->__('Meta'),
                'title'   => Mage::helper('ibd_travelsuite')->__('Meta'),
                'content' => $this->getLayout()->createBlock(
                    'ibd_travelsuite/adminhtml_group_edit_tab_meta'
                )
                ->toHtml(),
            )
        );
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve group entity
     *
     * @access public
     * @return IBD_TravelSuite_Model_Group
     * @author Ultimate Module Creator
     */
    public function getGroup()
    {
        return Mage::registry('current_group');
    }
}

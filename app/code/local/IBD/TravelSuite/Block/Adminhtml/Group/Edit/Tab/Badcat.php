<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Group edit form tab
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Block_Adminhtml_Group_Edit_Tab_Badcat extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return IBD_TravelSuite_Block_Adminhtml_Group_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setFieldNameSuffix('group');
        $this->setForm($form);

        $fieldset = $form->addFieldset(
            'group_badcat_form',
            array('legend' => Mage::helper('ibd_travelsuite')->__('Badge & Category'))
        );
        $fieldset->addType(
            'image',
            Mage::getConfig()->getBlockClassName('ibd_travelsuite/adminhtml_group_helper_badgeicon')
        );
        $fieldset->addField(
            'category',
            'select',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Category'),
                'name' => 'category',
                'note' => $this->__('Group category'),
                'values' => Mage::getModel('ibd_travelsuite/group')->getCategoryOptions(),
            )
        );
        $fieldset->addField(
            'badge_name',
            'text',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Badge Name'),
                'name'  => 'badge_name',
                'note'  => $this->__('The title of the badge'),
           )
        );
        $fieldset->addField(
            'badge_icon',
            'image',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Badge Icon'),
                'name'  => 'badge_icon',
                'note'  => $this->__('The badge icon'),
           )
        );

        $form->addValues(Mage::registry('current_group')->getData());
        return parent::_prepareForm();
    }
}

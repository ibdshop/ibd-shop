<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Region edit form tab
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Block_Adminhtml_Helper_Form_Gallery extends Mage_Adminhtml_Block_Widget_Form
{
    protected $_relId;

    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('ibd_travelsuite/helper/gallery.phtml');
        $this->setRelId();
    }

    protected function setRelId()
    {
        $this->_relId = Mage::registry('current_'.$this->_type)->getId();
    }

    public function getGalleryImages()
    {
        return Mage::getSingleton('ibd_travelsuite/gallery')
                ->getAdminEntityImageCollection($this->_relId, 'ibd_'.$this->_type);
    }

    public function getGalleryUploadUrl()
    {
        return Mage::getUrl('*/*/upload');
    }

    public function getRelationId()
    {
        return $this->_relId;
    }
}

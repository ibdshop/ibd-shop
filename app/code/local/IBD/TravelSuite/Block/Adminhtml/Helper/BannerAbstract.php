<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Abstract banner edit placement
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Block_Adminhtml_Helper_BannerAbstract extends Mage_Adminhtml_Block_Widget_Form
{
    protected $_columnPrefix = '';
    protected $_setFieldNameSuffix = true;
    protected $_addColorFields = false;

    /**
     * prepare the form
     *
     * @access protected
     * @return IBD_TravelSuite_Block_Adminhtml_Helper_BannerAbstract
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        if(!isset($this->_type))
            throw new BadMethodCallException('Do not create IBD_TravelSuite_Block_Adminhtml_Helper_BannerAbstract, extend it and set a protected _type (see IBD_TravelSuite_Block_Adminhtml_Region_Edit_Tab_Banner, IBD_TravelSuite_Block_Adminhtml_Group_Edit_Tab_Banner, IBD_TravelSuite_Block_Adminhtml_Brand_Edit_Tab_Banner)');

        $form = new Varien_Data_Form();
        if($this->_setFieldNameSuffix) $form->setFieldNameSuffix($this->_type);
        $this->setForm($form);

        $fieldset = $form->addFieldset(
            $this->_type.'_banner_form',
            array('legend' => Mage::helper('ibd_travelsuite')->__('Banner Icon & Position'))
        );
        $fieldset->addType(
            'image',
            Mage::getConfig()->getBlockClassName('ibd_travelsuite/adminhtml_title_helper_image')
        );

        $fieldset->addField(
            $this->_columnPrefix.'banner_icon',
            'image',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Icon'),
                'name'  => $this->_columnPrefix.'banner_icon',
                'note'  => $this->__('The '.$this->_type.' logo or icon'),
           )
        );

        $fieldset->addField(
            $this->_columnPrefix.'banner_icon_position',
            'select',
            array(
                'label'  => Mage::helper('ibd_travelsuite')->__('Icon Position'),
                'name'   => $this->_columnPrefix.'banner_icon_position',
                'values' => Mage::helper('ibd_travelsuite/'.$this->_type)->getIconPositionOptions(),
                'note' => $this->__('Icon position in relation to the title')
            )
        );

        $fieldset->addField(
            $this->_columnPrefix.'banner_align',
            'select',
            array(
                'label'  => Mage::helper('ibd_travelsuite')->__('Alignment'),
                'name'   => $this->_columnPrefix.'banner_align',
                'values' => Mage::helper('ibd_travelsuite/'.$this->_type)->getTitleAlignmentOptions(),
                'note' => $this->__('Left/Right/Center justification for the title and icon')
            )
        );

        if($this->_addColorFields)
        {
            $fieldset->addField(
                'ibd_text_color',
                'text',
                array(
                    'label' => Mage::helper('ibd_travelsuite')->__('Text Color'),
                    'name'  => 'ibd_text_color',
                    'note'  => $this->__('The text color hex value, e.g., f6f6f6'),
                    'required' => true
               )
            );
            $fieldset->addField(
                'ibd_background_color',
                'text',
                array(
                    'label' => Mage::helper('ibd_travelsuite')->__('Background Color'),
                    'name'  => 'ibd_background_color',
                    'note'  => $this->__('The background color hex value, e.g., 333333'),
                    'required' => true
               )
            );
        }

        $formValues = $this->getFormValues();

        $form->setValues($formValues);
        return parent::_prepareForm();
    }

    protected function getFormValues()
    {
        $formValues = Mage::registry('current_'.$this->_type)->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if ($this->getSessionData()) {
            $formValues = array_merge($formValues, $this->getSessionData());
            $this->setSessionData(null);
        } elseif (Mage::registry('current_'.$this->_type)) {
            $formValues = array_merge($formValues, Mage::registry('current_'.$this->_type)->getData());
        }

        return $formValues;
    }
}

<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Region admin edit tabs
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Block_Adminhtml_Region_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('region_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('ibd_travelsuite')->__('Region'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return IBD_TravelSuite_Block_Adminhtml_Region_Edit_Tabs
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_region',
            array(
                'label'   => Mage::helper('ibd_travelsuite')->__('Region'),
                'title'   => Mage::helper('ibd_travelsuite')->__('Region'),
                'content' => $this->getLayout()->createBlock(
                    'ibd_travelsuite/adminhtml_region_edit_tab_form'
                )
                ->toHtml(),
            )
        );
        $this->addTab(
            'form_banner_region',
            array(
                'label'   => Mage::helper('ibd_travelsuite')->__('Banner Icon & Position'),
                'title'   => Mage::helper('ibd_travelsuite')->__('Banner Icon & Position'),
                'content' => $this->getLayout()
                                  ->createBlock('ibd_travelsuite/adminhtml_region_edit_tab_banner')
                                  ->toHtml(),
            )
        );
        $this->addTab(
            'form_gallery_region',
            array(
                'label'   => Mage::helper('ibd_travelsuite')->__('Carousel'),
                'title'   => Mage::helper('ibd_travelsuite')->__('Carousel'),
                'content' => $this->getLayout()->createBlock(
                    'ibd_travelsuite/adminhtml_region_edit_tab_gallery'
                )
                ->toHtml(),
            )
        );
        $this->addTab(
            'form_color_region',
            array(
                'label'   => Mage::helper('ibd_travelsuite')->__('Highlight Color'),
                'title'   => Mage::helper('ibd_travelsuite')->__('Highlight'),
                'content' => $this->getLayout()->createBlock(
                    'ibd_travelsuite/adminhtml_region_edit_tab_color'
                )
                ->toHtml(),
            )
        );
        $this->addTab(
            'form_sponsors_region',
            array(
                'label'   => Mage::helper('ibd_travelsuite')->__('Content Images'),
                'title'   => Mage::helper('ibd_travelsuite')->__('Content Images'),
                'content' => $this->getLayout()->createBlock(
                    'ibd_travelsuite/adminhtml_region_edit_tab_sponsors'
                )
                ->toHtml(),
            )
        );
        $this->addTab(
            'form_meta_region',
            array(
                'label'   => Mage::helper('ibd_travelsuite')->__('Meta'),
                'title'   => Mage::helper('ibd_travelsuite')->__('Meta'),
                'content' => $this->getLayout()->createBlock(
                    'ibd_travelsuite/adminhtml_region_edit_tab_meta'
                )
                ->toHtml(),
            )
        );
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve region entity
     *
     * @access public
     * @return IBD_TravelSuite_Model_Region
     * @author Ultimate Module Creator
     */
    public function getRegion()
    {
        return Mage::registry('current_region');
    }
}

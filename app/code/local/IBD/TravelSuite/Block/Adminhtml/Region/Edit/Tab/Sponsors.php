<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Sponsors form tab
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Block_Adminhtml_Region_Edit_Tab_Sponsors extends Mage_Adminhtml_Block_Widget_Form
{
    protected $_region;

    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('ibd_shared/sponsors_tab.phtml');
    }

    public function getSelectName()
    {
        return 'region';
    }

    /**
     * prepare the form
     *
     * @access protected
     * @return IBD_TravelSuite_Block_Adminhtml_Region_Edit_Tab_Form
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setFieldNameSuffix('region');
        $this->setForm($form);

        $fieldset = $form->addFieldset(
            'region_sponsors_form',
            array('legend' => Mage::helper('ibd_travelsuite')->__('Sponsors'))
        );

        $this->_region = Mage::registry('current_region');

        return parent::_prepareForm();
    }

    /**
     * gets a JSON encoded string of page advertisements for the tag data source
     *
     * @access public
     * @return string
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function getAllAdsJSON()
    {
        return $this->getTagJSON(Mage::getSingleton('ibd_homepagebanner/advertisement')
                                    ->getAds(0, IBD_HomepageBanner_Model_Advertisement::PAGE));
    }

    /**
     * gets a JSON encoded string of region advertisement IDs to initialize
     * the tag multiselect input
     *
     * @access public
     * @return string
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function getRelatedAdIdsJSON()
    {
        return $this->getTagJSON($this->_region->getAdvertisementCollection());
    }

    /**
     * helper to get some tag json from a advertisement collection
     *
     * @access protected
     * @param Traversable
     * @return string
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    protected function getTagJSON(Traversable $adCollection)
    {
        $preSerialized = array();
        foreach ($adCollection as $ad)
        {
            $preSerialized[] = array(
                'id' => $ad->getId(),
                'name' => $ad->getName(),
            );
        }
        return json_encode($preSerialized);
    }
}

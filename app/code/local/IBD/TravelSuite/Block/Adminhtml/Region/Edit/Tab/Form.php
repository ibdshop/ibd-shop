<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Region edit form tab
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Block_Adminhtml_Region_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return IBD_TravelSuite_Block_Adminhtml_Region_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('region_');
        $form->setFieldNameSuffix('region');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'region_form',
            array('legend' => Mage::helper('ibd_travelsuite')->__('Region'))
        );
        $fieldset->addType(
            'image',
            Mage::getConfig()->getBlockClassName('ibd_travelsuite/adminhtml_region_helper_image')
        );
        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig();
        $values = Mage::getResourceModel('ibd_travelsuite/country_collection')
            ->toOptionArray();
        array_unshift($values, array('label' => '', 'value' => ''));

        $html = '<a href="{#url}" id="region_country_id_link" target="_blank"></a>';
        $html .= '<script type="text/javascript">
            function changeCountryIdLink() {
                if ($(\'region_country_id\').value == \'\') {
                    $(\'region_country_id_link\').hide();
                } else {
                    $(\'region_country_id_link\').show();
                    var url = \''.$this->getUrl('adminhtml/travelsuite_country/edit', array('id'=>'{#id}', 'clear'=>1)).'\';
                    var text = \''.Mage::helper('core')->escapeHtml($this->__('View {#name}')).'\';
                    var realUrl = url.replace(\'{#id}\', $(\'region_country_id\').value);
                    $(\'region_country_id_link\').href = realUrl;
                    $(\'region_country_id_link\').innerHTML = text.replace(\'{#name}\', $(\'region_country_id\').options[$(\'region_country_id\').selectedIndex].innerHTML);
                }
            }
            $(\'region_country_id\').observe(\'change\', changeCountryIdLink);
            changeCountryIdLink();
            </script>';

        $fieldset->addField(
            'status',
            'select',
            array(
                'label'  => Mage::helper('ibd_travelsuite')->__('Status'),
                'name'   => 'status',
                'values' => array(
                    array(
                        'value' => 1,
                        'label' => Mage::helper('ibd_travelsuite')->__('Enabled'),
                    ),
                    array(
                        'value' => 0,
                        'label' => Mage::helper('ibd_travelsuite')->__('Disabled'),
                    ),
                ),
            )
        );
        $fieldset->addField(
            'country_id',
            'select',
            array(
                'label'     => Mage::helper('ibd_travelsuite')->__('Country'),
                'name'      => 'country_id',
                'required'  => false,
                'values'    => $values,
                'after_element_html' => $html
            )
        );

        $fieldset->addField(
            'name',
            'text',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Name'),
                'name'  => 'name',
            'note'	=> $this->__('The name of the region (city / region)'),
            'required'  => true,
            'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'summary',
            'text',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Summary'),
                'name'  => 'summary',
            'note'	=> $this->__('A short summary of the region'),

           )
        );

        $fieldset->addField(
            'description',
            'editor',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Description'),
                'name'  => 'description',
            'config' => $wysiwygConfig,

           )
        );

        $fieldset->addField(
            'cover',
            'image',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Main Image'),
                'name'  => 'cover',
            'note'	=> $this->__('Main image for the display page'),

           )
        );
        $fieldset->addField(
            'url_key',
            'text',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Url key'),
                'name'  => 'url_key',
                'note'  => Mage::helper('ibd_travelsuite')->__('Relative to Website Base URL')
            )
        );
        $fieldset->addField(
            'in_rss',
            'select',
            array(
                'label'  => Mage::helper('ibd_travelsuite')->__('Show in rss'),
                'name'   => 'in_rss',
                'values' => array(
                    array(
                        'value' => 1,
                        'label' => Mage::helper('ibd_travelsuite')->__('Yes'),
                    ),
                    array(
                        'value' => 0,
                        'label' => Mage::helper('ibd_travelsuite')->__('No'),
                    ),
                ),
            )
        );
        $formValues = Mage::registry('current_region')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getRegionData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getRegionData());
            Mage::getSingleton('adminhtml/session')->setRegionData(null);
        } elseif (Mage::registry('current_region')) {
            $formValues = array_merge($formValues, Mage::registry('current_region')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}

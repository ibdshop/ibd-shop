<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Country edit form tab
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Block_Adminhtml_Country_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return IBD_TravelSuite_Block_Adminhtml_Country_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('country_');
        $form->setFieldNameSuffix('country');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'country_form',
            array('legend' => Mage::helper('ibd_travelsuite')->__('Country'))
        );
        $values = Mage::getResourceModel('ibd_travelsuite/continent_collection')
            ->toOptionArray();
        array_unshift($values, array('label' => '', 'value' => ''));

        $html = '<a href="{#url}" id="country_continent_id_link" target="_blank"></a>';
        $html .= '<script type="text/javascript">
            function changeContinentIdLink() {
                if ($(\'country_continent_id\').value == \'\') {
                    $(\'country_continent_id_link\').hide();
                } else {
                    $(\'country_continent_id_link\').show();
                    var url = \''.$this->getUrl('adminhtml/travelsuite_continent/edit', array('id'=>'{#id}', 'clear'=>1)).'\';
                    var text = \''.Mage::helper('core')->escapeHtml($this->__('View {#name}')).'\';
                    var realUrl = url.replace(\'{#id}\', $(\'country_continent_id\').value);
                    $(\'country_continent_id_link\').href = realUrl;
                    $(\'country_continent_id_link\').innerHTML = text.replace(\'{#name}\', $(\'country_continent_id\').options[$(\'country_continent_id\').selectedIndex].innerHTML);
                }
            }
            $(\'country_continent_id\').observe(\'change\', changeContinentIdLink);
            changeContinentIdLink();
            </script>';

        $fieldset->addField(
            'continent_id',
            'select',
            array(
                'label'     => Mage::helper('ibd_travelsuite')->__('Continent'),
                'name'      => 'continent_id',
                'required'  => false,
                'values'    => $values,
                'after_element_html' => $html
            )
        );

        $fieldset->addField(
            'name',
            'text',
            array(
                'label' => Mage::helper('ibd_travelsuite')->__('Name'),
                'name'  => 'name',
            'note'	=> $this->__('The name of the country'),
            'required'  => true,
            'class' => 'required-entry',

           )
        );
        $fieldset->addField(
            'status',
            'select',
            array(
                'label'  => Mage::helper('ibd_travelsuite')->__('Status'),
                'name'   => 'status',
                'values' => array(
                    array(
                        'value' => 1,
                        'label' => Mage::helper('ibd_travelsuite')->__('Enabled'),
                    ),
                    array(
                        'value' => 0,
                        'label' => Mage::helper('ibd_travelsuite')->__('Disabled'),
                    ),
                ),
            )
        );
        $formValues = Mage::registry('current_country')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getCountryData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getCountryData());
            Mage::getSingleton('adminhtml/session')->setCountryData(null);
        } elseif (Mage::registry('current_country')) {
            $formValues = array_merge($formValues, Mage::registry('current_country')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}

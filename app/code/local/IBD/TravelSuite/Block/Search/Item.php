<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Search results block
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Block_Search_Item extends IBD_TravelSuite_Block_Block_Abstract
{
	// required properties in the setEntity function
	protected $entity,
			  $urlMethod;

	// default properties
	protected $titleField = 'name',
			  $imageField = 'image',
			  $summaryField = 'summary';

    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('ibd_travelsuite/search/item.phtml');
    }

    /**
     * Must be called before building the HTML; sets the item entity
     *
     * @access public
     * @param Mage_Core_Model_Abstract
     * @param string
     * @return IBD_TravelSuite_Block_Search_Item (this)
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
	public function setEntity(Mage_Core_Model_Abstract $entity, $urlMethod)
	{
		$this->entity = $entity;
		$this->urlMethod = $urlMethod;
		return $this;
	}

    /**
     * Sets the title field name if it differs from the default
     *
     * @access public
     * @param string
     * @return IBD_TravelSuite_Block_Search_Item (this)
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
	public function setTitleField($titleField)
	{
		$this->titleField = $titleField;
		return $this;
	}

    /**
     * Sets the image field name if it differs from the default
     *
     * @access public
     * @param string
     * @return IBD_TravelSuite_Block_Search_Item (this)
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
	public function setImageField($imageField)
	{
		$this->imageField = $imageField;
		return $this;
	}

    /**
     * Sets the summary field name if it differs from the default
     *
     * @access public
     * @param string
     * @return IBD_TravelSuite_Block_Search_Item (this)
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
	public function setSummaryField($summaryField)
	{
		$this->summaryField = $summaryField;
		return $this;
	}

    /**
     * Gets the title from the entity
     *
     * @access public
     * @return string
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
	public function getTitle()
	{
		return $this->entity->getData($this->titleField);
	}

    /**
     * Determines if the entity has an image
     *
     * @access public
     * @return boolean
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
	public function hasImage()
	{
		$image = $this->entity->getData($this->imageField);
		return ($image != null && $image != '');
	}

    /**
     * Gets the image from the entity
     *
     * @access public
     * @return string
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
	public function getImage($size=150)
	{
		return (string) Mage::helper('ibd_travelsuite/'.$this->entity->getIBDType().'_image')
                            ->init($this->entity, $this->imageField)
                            ->resize($size);
	}

    /**
     * Gets the summary from the entity
     *
     * @access public
     * @return string
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
	public function getSummary()
	{
		return $this->entity->getData($this->summaryField);
	}

    /**
     * Gets the entity URL
     *
     * @access public
     * @return string
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
	public function getEntityUrl()
	{
		return $this->entity->{$this->urlMethod}();
	}
}
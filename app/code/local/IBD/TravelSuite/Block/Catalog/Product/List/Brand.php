<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Brand list on product page block
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Block_Catalog_Product_List_Brand extends Mage_Catalog_Block_Product_Abstract
{
    /**
     * get the list of brands
     *
     * @access protected
     * @return IBD_TravelSuite_Model_Resource_Brand_Collection
     * @author Ultimate Module Creator
     */
    public function getBrandCollection()
    {
        if (!$this->hasData('brand_collection')) {
            $product = Mage::registry('product');
            $collection = Mage::getResourceSingleton('ibd_travelsuite/brand_collection')
                ->addStoreFilter(Mage::app()->getStore())
                ->addFieldToFilter('status', 1)
                ->addProductFilter($product);
            $collection->getSelect()->order('related_product.position', 'ASC');
            $this->setData('brand_collection', $collection);
        }
        return $this->getData('brand_collection');
    }
}

<?php
/**
 * IBD_TravelSuite extension
 *
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Html header block
 *
 * @category   IBD
 * @package    IBD_TravelSuite
 * @author     Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Block_Html_Header extends Mage_Page_Block_Html_Header
{
    /**
     * Retrieve page welcome message
     *
     * @deprecated after 1.7.0.2
     * @see IBD_TravelSuite_Block_Html_Welcome
     * @return mixed
     */
    public function getWelcome()
    {
        if (empty($this->_data['welcome'])) {
            $this->_data['welcome'] = Mage::getStoreConfig('design/header/welcome');
        }

        return $this->_data['welcome'];
    }
}
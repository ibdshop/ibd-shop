<?php
/**
 * IBD_TravelSuite extension
 *
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Html welcome block
 *
 * @category   IBD
 * @package    IBD_TravelSuite
 * @author     Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Block_Html_Welcome extends Mage_Page_Block_Html_Welcome
{
    /**
     * Get block messsage
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (empty($this->_data['welcome'])) {
            $this->_data['welcome'] = Mage::getStoreConfig('design/header/welcome');
        }
        $returnHtml =  $this->_data['welcome'];

        if (!empty($this->_data['additional_html'])) {
            $returnHtml .= ' ' . $this->_data['additional_html'];
        }

        return $returnHtml;
    }
}

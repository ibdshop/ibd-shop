<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Region list block
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Block_Region_Filter extends IBD_TravelSuite_Block_Block_Abstract
{
	// Name Lists from the DB
    protected $_geographic;

    // current entity
    protected $_brand = null;
    protected $_group = null;
    protected $_region = null;

    // query parameters
    protected $_queryCountry;
    protected $_queryContinent;

    // search base URL
    protected $_searchBaseUrl = '';

	/**
     * initialize
     *
     * @access public
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function __construct()
    {
    	parent::__construct();

        $this->_geographic = Mage::helper('ibd_travelsuite')->getGeographyHierarchy();
        //Mage::log('geoghier: '.json_encode($this->_geographic));

        if(Mage::registry('current_brand'))
        {
            $this->_brand = Mage::registry('current_brand');
        }

        if(Mage::registry('current_group'))
        {
            $this->_group = Mage::registry('current_group');
        }

        if(Mage::registry('current_region'))
        {
            $this->_region = Mage::registry('current_region');
        }

        $this->_queryContinent = $this->getParam('continent', null);
        $this->_queryCountry = $this->getParam('country', null);

        $this->_searchBaseUrl = Mage::getUrl('ibd_travelsuite/search');
    }

    /**
     * Sets the search base URL
     *
     * @param string
     * @return Mage_Core_Block_Template
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function setSearchBaseUrl($url)
    {
        $this->_searchBaseUrl = $url;
        return $this;
    }

    /**
     * Gets the search base URL
     *
     * @return string
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function getSearchBaseUrl()
    {
        return $this->_searchBaseUrl;
    }

    /**
     * Checks whether the current entity is the specified continent
     *
     * @param int
     * @return boolean
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function isContinent($continentName)
    {
        if($continentName == $this->_queryContinent)
        {
            return true;
        }

        if($this->_queryCountry && $this->getContinentNameFromCountryName($this->_queryCountry) == $continentName)
        {
            return true;
        }

        if($this->_group)
        {
            if($this->_group->getParentRegion() && $this->getContinentNameFromCountryName($this->_group->getParentRegion()->getParentCountry()->getName()) == $continentName)
            {
                return true;
            }
            return false;
        }
        if($this->_brand)
        {
            return false; // brands don't have locations, per se
        }
        if($this->_region)
        {
            if($this->getContinentNameFromCountryName($this->_region->getParentCountry()->getName()) == $continentName)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * Checks whether the current entity is the specified country
     *
     * @param int
     * @return boolean
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function isCountry($countryName)
    {
        if($countryName == $this->_queryCountry)
        {
            return true;
        }

        if($this->_group)
        {
            return $this->_group->getParentRegion() && $this->_group->getParentRegion()->getParentCountry()->getName() == $countryName;
        }
        if($this->_brand)
        {
            return false; // brands don't have locations, per se
        }
        if($this->_region)
        {
            return $countryName == $this->_region->getParentCountry()->getName();
        }
        return false;
    }

    /**
     * get the _geographic
     *
     * @access public
     * @return array
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function getGeographic()
    {
    	return $this->_geographic;
    }

    /**
     * Gets the continent ID from the country ID
     *
     * @param int
     * @return int
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function getContinentNameFromCountryName($searchName)
    {
        foreach ($this->_geographic as $continentData)
        {
            foreach ($continentData['countries'] as $countryName)
            {
                if($searchName == $countryName) return $continentData['name'];
            }
        }
        return null;
    }
}

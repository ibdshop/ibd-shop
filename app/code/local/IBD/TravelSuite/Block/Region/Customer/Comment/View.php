<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Region customer comments list
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Block_Region_Customer_Comment_View extends Mage_Customer_Block_Account_Dashboard
{
    /**
     * get current comment
     *
     * @access public
     * @return IBD_TravelSuite_Model_Region_Comment
     * @author Ultimate Module Creator
     */
    public function getComment()
    {
        return Mage::registry('current_comment');
    }

    /**
     * get current region
     *
     * @access public
     * @return IBD_TravelSuite_Model_Region
     * @author Ultimate Module Creator
     */
    public function getRegion()
    {
        return Mage::registry('current_region');
    }
}

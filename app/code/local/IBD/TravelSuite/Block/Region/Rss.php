<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Region RSS block
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Block_Region_Rss extends Mage_Rss_Block_Abstract
{
    /**
     * Cache tag constant for feed reviews
     *
     * @var string
     */
    const CACHE_TAG = 'block_html_travelsuite_region_rss';

    /**
     * constructor
     *
     * @access protected
     * @return void
     * @author Ultimate Module Creator
     */
    protected function _construct()
    {
        $this->setCacheTags(array(self::CACHE_TAG));
        /*
         * setting cache to save the rss for 10 minutes
         */
        $this->setCacheKey('ibd_travelsuite_region_rss');
        $this->setCacheLifetime(600);
    }

    /**
     * toHtml method
     *
     * @access protected
     * @return string
     * @author Ultimate Module Creator
     */
    protected function _toHtml()
    {
        $url    = Mage::helper('ibd_travelsuite/region')->getRegionsUrl();
        $title  = Mage::helper('ibd_travelsuite')->__('Regions');
        $rssObj = Mage::getModel('rss/rss');
        $data  = array(
            'title'       => $title,
            'description' => $title,
            'link'        => $url,
            'charset'     => 'UTF-8',
        );
        $rssObj->_addHeader($data);
        $collection = Mage::getModel('ibd_travelsuite/region')->getCollection()
            ->addFieldToFilter('status', 1)
            ->addFieldToFilter('in_rss', 1)
            ->setOrder('created_at');
        $collection->load();
        foreach ($collection as $item) {
            $description = '<p>';
            $description .= '<div>'.
                Mage::helper('ibd_travelsuite')->__('Name').': 
                '.$item->getName().
                '</div>';
            $description .= '<div>'.
                Mage::helper('ibd_travelsuite')->__('Summary').': 
                '.$item->getSummary().
                '</div>';
            $description .= '<div>'.
                Mage::helper('ibd_travelsuite')->__('Description').': 
                '.$item->getDescription().
                '</div>';
            if ($item->getCover()) {
                $description .= '<div>';
                $description .= Mage::helper('ibd_travelsuite')->__('Main Image');
                $description .= '<img src="'.Mage::helper('ibd_travelsuite/region_image')->init($item, 'cover')->resize(75).'" alt="'.$this->escapeHtml($item->getName()).'" />';
                $description .= '</div>';
            }
            $description .= '</p>';
            $data = array(
                'title'       => $item->getName(),
                'link'        => $item->getRegionUrl(),
                'description' => $description
            );
            $rssObj->_addEntry($data);
        }
        return $rssObj->createRssXml();
    }
}

<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Region list block
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author Ultimate Module Creator
 */
class IBD_TravelSuite_Block_Region_List extends Mage_Core_Block_Template
{
    /**
     * initialize
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $regions = Mage::getResourceModel('ibd_travelsuite/region_collection')
                         ->addFieldToFilter('status', 1);
        $regions->setOrder('name', 'asc');
        $this->setRegions($regions);
    }

    /**
     * prepare the layout
     *
     * @access protected
     * @return IBD_TravelSuite_Block_Region_List
     * @author Ultimate Module Creator
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $pager = $this->getLayout()->createBlock(
            'page/html_pager',
            'ibd_travelsuite.region.html.pager'
        )
        ->setCollection($this->getRegions());
        $this->setChild('pager', $pager);
        $this->getRegions()->load();
        return $this;
    }

    /**
     * get the pager html
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}

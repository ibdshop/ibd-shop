<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Tag model
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Model_Tag extends Mage_Core_Model_Abstract
{
    /**
     * Entity code.
     * Can be used as part of method name for entity processing
     */
    const ENTITY    = 'ibd_travelsuite_tag';
    const CACHE_TAG = 'ibd_travelsuite_tag';

    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('ibd_travelsuite/tag');
    }

    public function getName()
    {
        return $this->getTag();
    }

    public function getText()
    {
        return $this->getTag();
    }

    public function getAllTags()
    {
        return $this->getCollection();
    }

    public function getAll()
    {
        return $this->getAllTags();
    }

    public function getActiveTags($type)
    {
        $collection = $this->getCollection();
        $collection->getSelect()->join(
            array('relation' => $collection->getTable('ibd_travelsuite/tag_relation')),
            'main_table.entity_id = relation.tag_id',
            array()
        )
        // ->join(
        //     array('related' => $collection->getTable('ibd_travelsuite/'.substr($type, 4))),
        //     'relation.relation_id = related.entity_id',
        //     array()
        // )
        ->where('relation.relation_type = ?', $type)
        ->where('relation.relation_id IS NOT NULL')
        //->where('related.status > 0')
        ->group('main_table.tag');

        Mage::log('active tag select: '.$collection->getSelect());
        return $collection;
    }
}

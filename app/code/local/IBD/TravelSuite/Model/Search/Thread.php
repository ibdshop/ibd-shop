<?php
/**
 * IBD_TravelSuite extension
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Search Thread
 * DOES NOT WORK -- it was probably a bit too early for this kind of optimization anyway
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Model_Search_Thread extends Thread
{
	protected $_helper;
	protected $_type;
	protected $_request;
	protected $_results;
	protected $_limit;
	protected $_offset;

	public function setup(IBD_TravelSuite_Helper_Search $helper, $type, Zend_Controller_Request_Http $request, $limit, $offset)
	{
		if(!$this->_type || !$this->_request || !$this->_helper)
		{
			throw new Exception("Must specify type, helper, and request for threaded search.");
		}

		$this->_request = $request;
		$this->_type = $type;
		$this->_limit = $limit;
		$this->_offset = $offset;
		$this->_helper = $helper;
	}

	public function run()
	{
		if(!$this->_type || !$this->_request || !$this->_helper)
		{
			throw new Exception("Must run setup() before starting the thread.");
		}

		Mage::log('starting '.$this->_type.' thread');

		$helper->filterResults($this->_type, $this->_request, $this->_limit, $this->_offset);
	}
}
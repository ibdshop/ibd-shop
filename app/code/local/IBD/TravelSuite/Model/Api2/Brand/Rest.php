<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Brand abstract REST API handler model
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
abstract class IBD_TravelSuite_Model_Api2_Brand_Rest extends IBD_TravelSuite_Model_Api2_Brand
{
    /**
     * current brand
     */
    protected $_brand;

    /**
     * retrieve entity
     *
     * @access protected
     * @return array|mixed
     * @author Ultimate Module Creator
     */
    protected function _retrieve() {
        $brand = $this->_getBrand();
        $this->_prepareBrandForResponse($brand);
        return $brand->getData();
    }

    /**
     * get collection
     *
     * @access protected
     * @return array
     * @author Ultimate Module Creator
     */
    protected function _retrieveCollection() {
        $collection = Mage::getResourceModel('ibd_travelsuite/brand_collection');
        $entityOnlyAttributes = $this->getEntityOnlyAttributes(
            $this->getUserType(),
            Mage_Api2_Model_Resource::OPERATION_ATTRIBUTE_READ
        );
        $availableAttributes = array_keys($this->getAvailableAttributes(
            $this->getUserType(),
            Mage_Api2_Model_Resource::OPERATION_ATTRIBUTE_READ)
        );
        $collection->addFieldToFilter('status', array('eq' => 1));
        $store = $this->_getStore();
        $collection->addStoreFilter($store->getId());
        $this->_applyCollectionModifiers($collection);
        $brands = $collection->load();
        $brands->walk('afterLoad');
        foreach ($brands as $brand) {
            $this->_setBrand($brand);
            $this->_prepareBrandForResponse($brand);
        }
        $brandsArray = $brands->toArray();
        $brandsArray = $brandsArray['items'];

        return $brandsArray;
    }

    /**
     * prepare brand for response
     *
     * @access protected
     * @param IBD_TravelSuite_Model_Brand $brand
     * @author Ultimate Module Creator
     */
    protected function _prepareBrandForResponse(IBD_TravelSuite_Model_Brand $brand) {
        $brandData = $brand->getData();
        if ($this->getActionType() == self::ACTION_TYPE_ENTITY) {
            $brandData['url'] = $brand->getBrandUrl();
        }
    }

    /**
     * create brand
     *
     * @access protected
     * @param array $data
     * @return string|void
     * @author Ultimate Module Creator
     */
    protected function _create(array $data) {
        $this->_critical(self::RESOURCE_METHOD_NOT_ALLOWED);
    }

    /**
     * update brand
     *
     * @access protected
     * @param array $data
     * @author Ultimate Module Creator
     */
    protected function _update(array $data) {
        $this->_critical(self::RESOURCE_METHOD_NOT_ALLOWED);
    }

    /**
     * delete brand
     *
     * @access protected
     * @author Ultimate Module Creator
     */
    protected function _delete() {
        $this->_critical(self::RESOURCE_METHOD_NOT_ALLOWED);
    }

    /**
     * delete current brand
     *
     * @access protected
     * @param IBD_TravelSuite_Model_Brand $brand
     * @author Ultimate Module Creator
     */
    protected function _setBrand(IBD_TravelSuite_Model_Brand $brand) {
        $this->_brand = $brand;
    }

    /**
     * get current brand
     *
     * @access protected
     * @return IBD_TravelSuite_Model_Brand
     * @author Ultimate Module Creator
     */
    protected function _getBrand() {
        if (is_null($this->_brand)) {
            $brandId = $this->getRequest()->getParam('id');
            $brand = Mage::getModel('ibd_travelsuite/brand');
            $brand->load($brandId);
            if (!($brand->getId())) {
                $this->_critical(self::RESOURCE_NOT_FOUND);
            }
            if ($this->_getStore()->getId()) {
                $isValidStore = count(array_intersect(array(0, $this->_getStore()->getId()), $brand->getStoreId()));
                if (!$isValidStore) {
                    $this->_critical(self::RESOURCE_NOT_FOUND);
                }
            }
            $this->_brand = $brand;
        }
        return $this->_brand;
    }
}

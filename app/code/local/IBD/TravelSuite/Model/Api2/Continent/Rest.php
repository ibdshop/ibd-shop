<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Continent abstract REST API handler model
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
abstract class IBD_TravelSuite_Model_Api2_Continent_Rest extends IBD_TravelSuite_Model_Api2_Continent
{
    /**
     * current continent
     */
    protected $_continent;

    /**
     * retrieve entity
     *
     * @access protected
     * @return array|mixed
     * @author Ultimate Module Creator
     */
    protected function _retrieve() {
        $continent = $this->_getContinent();
        $this->_prepareContinentForResponse($continent);
        return $continent->getData();
    }

    /**
     * get collection
     *
     * @access protected
     * @return array
     * @author Ultimate Module Creator
     */
    protected function _retrieveCollection() {
        $collection = Mage::getResourceModel('ibd_travelsuite/continent_collection');
        $entityOnlyAttributes = $this->getEntityOnlyAttributes(
            $this->getUserType(),
            Mage_Api2_Model_Resource::OPERATION_ATTRIBUTE_READ
        );
        $availableAttributes = array_keys($this->getAvailableAttributes(
            $this->getUserType(),
            Mage_Api2_Model_Resource::OPERATION_ATTRIBUTE_READ)
        );
        $collection->addFieldToFilter('status', array('eq' => 1));
        $this->_applyCollectionModifiers($collection);
        $continents = $collection->load();
        $continents->walk('afterLoad');
        foreach ($continents as $continent) {
            $this->_setContinent($continent);
            $this->_prepareContinentForResponse($continent);
        }
        $continentsArray = $continents->toArray();
        $continentsArray = $continentsArray['items'];

        return $continentsArray;
    }

    /**
     * prepare continent for response
     *
     * @access protected
     * @param IBD_TravelSuite_Model_Continent $continent
     * @author Ultimate Module Creator
     */
    protected function _prepareContinentForResponse(IBD_TravelSuite_Model_Continent $continent) {
        $continentData = $continent->getData();
    }

    /**
     * create continent
     *
     * @access protected
     * @param array $data
     * @return string|void
     * @author Ultimate Module Creator
     */
    protected function _create(array $data) {
        $this->_critical(self::RESOURCE_METHOD_NOT_ALLOWED);
    }

    /**
     * update continent
     *
     * @access protected
     * @param array $data
     * @author Ultimate Module Creator
     */
    protected function _update(array $data) {
        $this->_critical(self::RESOURCE_METHOD_NOT_ALLOWED);
    }

    /**
     * delete continent
     *
     * @access protected
     * @author Ultimate Module Creator
     */
    protected function _delete() {
        $this->_critical(self::RESOURCE_METHOD_NOT_ALLOWED);
    }

    /**
     * delete current continent
     *
     * @access protected
     * @param IBD_TravelSuite_Model_Continent $continent
     * @author Ultimate Module Creator
     */
    protected function _setContinent(IBD_TravelSuite_Model_Continent $continent) {
        $this->_continent = $continent;
    }

    /**
     * get current continent
     *
     * @access protected
     * @return IBD_TravelSuite_Model_Continent
     * @author Ultimate Module Creator
     */
    protected function _getContinent() {
        if (is_null($this->_continent)) {
            $continentId = $this->getRequest()->getParam('id');
            $continent = Mage::getModel('ibd_travelsuite/continent');
            $continent->load($continentId);
            if (!($continent->getId())) {
                $this->_critical(self::RESOURCE_NOT_FOUND);
            }
            $this->_continent = $continent;
        }
        return $this->_continent;
    }
}

<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Group abstract REST API handler model
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
abstract class IBD_TravelSuite_Model_Api2_Group_Rest extends IBD_TravelSuite_Model_Api2_Group
{
    /**
     * current group
     */
    protected $_group;

    /**
     * retrieve entity
     *
     * @access protected
     * @return array|mixed
     * @author Ultimate Module Creator
     */
    protected function _retrieve() {
        $group = $this->_getGroup();
        $this->_prepareGroupForResponse($group);
        return $group->getData();
    }

    /**
     * get collection
     *
     * @access protected
     * @return array
     * @author Ultimate Module Creator
     */
    protected function _retrieveCollection() {
        $collection = Mage::getResourceModel('ibd_travelsuite/group_collection');
        $entityOnlyAttributes = $this->getEntityOnlyAttributes(
            $this->getUserType(),
            Mage_Api2_Model_Resource::OPERATION_ATTRIBUTE_READ
        );
        $availableAttributes = array_keys($this->getAvailableAttributes(
            $this->getUserType(),
            Mage_Api2_Model_Resource::OPERATION_ATTRIBUTE_READ)
        );
        $collection->addFieldToFilter('status', array('eq' => 1));
        $this->_applyCollectionModifiers($collection);
        $groups = $collection->load();
        $groups->walk('afterLoad');
        foreach ($groups as $group) {
            $this->_setGroup($group);
            $this->_prepareGroupForResponse($group);
        }
        $groupsArray = $groups->toArray();
        $groupsArray = $groupsArray['items'];

        return $groupsArray;
    }

    /**
     * prepare group for response
     *
     * @access protected
     * @param IBD_TravelSuite_Model_Group $group
     * @author Ultimate Module Creator
     */
    protected function _prepareGroupForResponse(IBD_TravelSuite_Model_Group $group) {
        $groupData = $group->getData();
        if ($this->getActionType() == self::ACTION_TYPE_ENTITY) {
            $groupData['url'] = $group->getGroupUrl();
        }
    }

    /**
     * create group
     *
     * @access protected
     * @param array $data
     * @return string|void
     * @author Ultimate Module Creator
     */
    protected function _create(array $data) {
        $this->_critical(self::RESOURCE_METHOD_NOT_ALLOWED);
    }

    /**
     * update group
     *
     * @access protected
     * @param array $data
     * @author Ultimate Module Creator
     */
    protected function _update(array $data) {
        $this->_critical(self::RESOURCE_METHOD_NOT_ALLOWED);
    }

    /**
     * delete group
     *
     * @access protected
     * @author Ultimate Module Creator
     */
    protected function _delete() {
        $this->_critical(self::RESOURCE_METHOD_NOT_ALLOWED);
    }

    /**
     * delete current group
     *
     * @access protected
     * @param IBD_TravelSuite_Model_Group $group
     * @author Ultimate Module Creator
     */
    protected function _setGroup(IBD_TravelSuite_Model_Group $group) {
        $this->_group = $group;
    }

    /**
     * get current group
     *
     * @access protected
     * @return IBD_TravelSuite_Model_Group
     * @author Ultimate Module Creator
     */
    protected function _getGroup() {
        if (is_null($this->_group)) {
            $groupId = $this->getRequest()->getParam('id');
            $group = Mage::getModel('ibd_travelsuite/group');
            $group->load($groupId);
            if (!($group->getId())) {
                $this->_critical(self::RESOURCE_NOT_FOUND);
            }
            $this->_group = $group;
        }
        return $this->_group;
    }
}

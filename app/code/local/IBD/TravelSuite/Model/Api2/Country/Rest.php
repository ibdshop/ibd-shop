<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Country abstract REST API handler model
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
abstract class IBD_TravelSuite_Model_Api2_Country_Rest extends IBD_TravelSuite_Model_Api2_Country
{
    /**
     * current country
     */
    protected $_country;

    /**
     * retrieve entity
     *
     * @access protected
     * @return array|mixed
     * @author Ultimate Module Creator
     */
    protected function _retrieve() {
        $country = $this->_getCountry();
        $this->_prepareCountryForResponse($country);
        return $country->getData();
    }

    /**
     * get collection
     *
     * @access protected
     * @return array
     * @author Ultimate Module Creator
     */
    protected function _retrieveCollection() {
        $collection = Mage::getResourceModel('ibd_travelsuite/country_collection');
        $entityOnlyAttributes = $this->getEntityOnlyAttributes(
            $this->getUserType(),
            Mage_Api2_Model_Resource::OPERATION_ATTRIBUTE_READ
        );
        $availableAttributes = array_keys($this->getAvailableAttributes(
            $this->getUserType(),
            Mage_Api2_Model_Resource::OPERATION_ATTRIBUTE_READ)
        );
        $collection->addFieldToFilter('status', array('eq' => 1));
        $this->_applyCollectionModifiers($collection);
        $countries = $collection->load();
        $countries->walk('afterLoad');
        foreach ($countries as $country) {
            $this->_setCountry($country);
            $this->_prepareCountryForResponse($country);
        }
        $countriesArray = $countries->toArray();
        $countriesArray = $countriesArray['items'];

        return $countriesArray;
    }

    /**
     * prepare country for response
     *
     * @access protected
     * @param IBD_TravelSuite_Model_Country $country
     * @author Ultimate Module Creator
     */
    protected function _prepareCountryForResponse(IBD_TravelSuite_Model_Country $country) {
        $countryData = $country->getData();
    }

    /**
     * create country
     *
     * @access protected
     * @param array $data
     * @return string|void
     * @author Ultimate Module Creator
     */
    protected function _create(array $data) {
        $this->_critical(self::RESOURCE_METHOD_NOT_ALLOWED);
    }

    /**
     * update country
     *
     * @access protected
     * @param array $data
     * @author Ultimate Module Creator
     */
    protected function _update(array $data) {
        $this->_critical(self::RESOURCE_METHOD_NOT_ALLOWED);
    }

    /**
     * delete country
     *
     * @access protected
     * @author Ultimate Module Creator
     */
    protected function _delete() {
        $this->_critical(self::RESOURCE_METHOD_NOT_ALLOWED);
    }

    /**
     * delete current country
     *
     * @access protected
     * @param IBD_TravelSuite_Model_Country $country
     * @author Ultimate Module Creator
     */
    protected function _setCountry(IBD_TravelSuite_Model_Country $country) {
        $this->_country = $country;
    }

    /**
     * get current country
     *
     * @access protected
     * @return IBD_TravelSuite_Model_Country
     * @author Ultimate Module Creator
     */
    protected function _getCountry() {
        if (is_null($this->_country)) {
            $countryId = $this->getRequest()->getParam('id');
            $country = Mage::getModel('ibd_travelsuite/country');
            $country->load($countryId);
            if (!($country->getId())) {
                $this->_critical(self::RESOURCE_NOT_FOUND);
            }
            $this->_country = $country;
        }
        return $this->_country;
    }
}

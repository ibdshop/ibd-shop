<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Region abstract REST API handler model
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
abstract class IBD_TravelSuite_Model_Api2_Region_Rest extends IBD_TravelSuite_Model_Api2_Region
{
    /**
     * current region
     */
    protected $_region;

    /**
     * retrieve entity
     *
     * @access protected
     * @return array|mixed
     * @author Ultimate Module Creator
     */
    protected function _retrieve() {
        $region = $this->_getRegion();
        $this->_prepareRegionForResponse($region);
        return $region->getData();
    }

    /**
     * get collection
     *
     * @access protected
     * @return array
     * @author Ultimate Module Creator
     */
    protected function _retrieveCollection() {
        $collection = Mage::getResourceModel('ibd_travelsuite/region_collection');
        $entityOnlyAttributes = $this->getEntityOnlyAttributes(
            $this->getUserType(),
            Mage_Api2_Model_Resource::OPERATION_ATTRIBUTE_READ
        );
        $availableAttributes = array_keys($this->getAvailableAttributes(
            $this->getUserType(),
            Mage_Api2_Model_Resource::OPERATION_ATTRIBUTE_READ)
        );
        $collection->addFieldToFilter('status', array('eq' => 1));
        $this->_applyCollectionModifiers($collection);
        $regions = $collection->load();
        $regions->walk('afterLoad');
        foreach ($regions as $region) {
            $this->_setRegion($region);
            $this->_prepareRegionForResponse($region);
        }
        $regionsArray = $regions->toArray();
        $regionsArray = $regionsArray['items'];

        return $regionsArray;
    }

    /**
     * prepare region for response
     *
     * @access protected
     * @param IBD_TravelSuite_Model_Region $region
     * @author Ultimate Module Creator
     */
    protected function _prepareRegionForResponse(IBD_TravelSuite_Model_Region $region) {
        $regionData = $region->getData();
        if ($this->getActionType() == self::ACTION_TYPE_ENTITY) {
            $regionData['url'] = $region->getRegionUrl();
        }
    }

    /**
     * create region
     *
     * @access protected
     * @param array $data
     * @return string|void
     * @author Ultimate Module Creator
     */
    protected function _create(array $data) {
        $this->_critical(self::RESOURCE_METHOD_NOT_ALLOWED);
    }

    /**
     * update region
     *
     * @access protected
     * @param array $data
     * @author Ultimate Module Creator
     */
    protected function _update(array $data) {
        $this->_critical(self::RESOURCE_METHOD_NOT_ALLOWED);
    }

    /**
     * delete region
     *
     * @access protected
     * @author Ultimate Module Creator
     */
    protected function _delete() {
        $this->_critical(self::RESOURCE_METHOD_NOT_ALLOWED);
    }

    /**
     * delete current region
     *
     * @access protected
     * @param IBD_TravelSuite_Model_Region $region
     * @author Ultimate Module Creator
     */
    protected function _setRegion(IBD_TravelSuite_Model_Region $region) {
        $this->_region = $region;
    }

    /**
     * get current region
     *
     * @access protected
     * @return IBD_TravelSuite_Model_Region
     * @author Ultimate Module Creator
     */
    protected function _getRegion() {
        if (is_null($this->_region)) {
            $regionId = $this->getRequest()->getParam('id');
            $region = Mage::getModel('ibd_travelsuite/region');
            $region->load($regionId);
            if (!($region->getId())) {
                $this->_critical(self::RESOURCE_NOT_FOUND);
            }
            $this->_region = $region;
        }
        return $this->_region;
    }
}

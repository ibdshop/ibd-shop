<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Region - advertisement relation resource model collection
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Model_Resource_Region_Advertisement_Collection
    extends IBD_HomepageBanner_Model_Resource_Advertisement_Collection
{
    /**
     * add region filter
     *
     * @access public
     * @param IBD_TravelSuite_Model_Region | int $region
     * @return IBD_TravelSuite_Model_Resource_Region_Advertisement_Collection
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function addRegionFilter($region)
    {
        if ($region instanceof IBD_TravelSuite_Model_Region) {
            $region = $region->getId();
        }
        return $this->addRelationFilter($region, 'ibd_region');
    }
}

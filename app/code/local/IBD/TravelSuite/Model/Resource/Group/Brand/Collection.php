<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Group - brand relation resource model collection
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Model_Resource_Group_Brand_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * remember if fields have been joined
     *
     * @var bool
     */
    protected $_joinedFields = false;

    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('ibd_travelsuite/brand');
    }

    /**
     * join the link table
     *
     * @access public
     * @return IBD_TravelSuite_Model_Resource_Group_Brand_Collection
     * @author Ultimate Module Creator
     */
    public function joinFields()
    {
        if (!$this->_joinedFields) {
            $this->getSelect()->join( array(
                'related' => $this->getTable('ibd_travelsuite/brand_group')
            ), 'related.brand_id = main_table.entity_id' );
            $this->_joinedFields = true;
        }
        return $this;
    }

    /**
     * add group filter. if status is -1, all enabled groups will be returned.
     *
     * @access public
     * @param IBD_TravelSuite_Model_Group | int $group
     * @param status int
     * @param excludeDisabled boolean
     * @return IBD_TravelSuite_Model_Resource_Group_Brand_Collection
     * @author Ultimate Module Creator
     */
    public function addGroupFilter($group, $status = -1, $excludeDisabled=true)
    {
        if ($group instanceof IBD_TravelSuite_Model_Group) {
            $group = $group->getId();
        }
        if (!$this->_joinedFields ) {
            $this->joinFields();
        }
        $this->getSelect()->where('related.group_id = ?', $group);
        if($status >= 0)
            $this->getSelect()->where('main_table.status = ?', $status);
        elseif($excludeDisabled)
            $this->getSelect()->where('main_table.status > 0');
        return $this;
    }

    /**
     * add group filter
     *
     * @access public
     * @param IBD_TravelSuite_Model_Group | int $group
     * @return IBD_TravelSuite_Model_Resource_Group_Brand_Collection
     * @author Ultimate Module Creator
     */
    public function addBrandFilter($brand)
    {
        if ($brand instanceof IBD_TravelSuite_Model_Brand) {
            $brand = $brand->getId();
        }
        if (!$this->_joinedFields ) {
            $this->joinGroupFields();
        }
        $this->getSelect()->where('related.brand_id = ?', $brand);
        return $this;
    }

    /**
     * get brands as array
     *
     * @access protected
     * @param string $valueField
     * @param string $labelField
     * @param array $additional
     * @return array
     * @author Ultimate Module Creator
     */
    protected function _toOptionArray($valueField='entity_id', $labelField='name', $additional=array())
    {
        return parent::_toOptionArray($valueField, $labelField, $additional);
    }

    /**
     * get options hash
     *
     * @access protected
     * @param string $valueField
     * @param string $labelField
     * @return array
     * @author Ultimate Module Creator
     */
    protected function _toOptionHash($valueField='entity_id', $labelField='name')
    {
        return parent::_toOptionHash($valueField, $labelField);
    }

    /**
     * Get SQL for get record count.
     * Extra GROUP BY strip added.
     *
     * @access public
     * @return Varien_Db_Select
     * @author Ultimate Module Creator
     */
    public function getSelectCountSql()
    {
        $countSelect = parent::getSelectCountSql();
        $countSelect->reset(Zend_Db_Select::GROUP);
        return $countSelect;
    }
}

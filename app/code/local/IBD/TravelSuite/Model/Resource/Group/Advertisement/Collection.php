<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Group - advertisement relation resource model collection
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Model_Resource_Group_Advertisement_Collection
    extends IBD_HomepageBanner_Model_Resource_Advertisement_Collection
{
    /**
     * add group filter
     *
     * @access public
     * @param IBD_TravelSuite_Model_Group | int $group
     * @return IBD_TravelSuite_Model_Resource_Group_Advertisement_Collection
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function addGroupFilter($group)
    {
        if ($group instanceof IBD_TravelSuite_Model_Group) {
            $group = $group->getId();
        }
        return $this->addRelationFilter($group, 'ibd_group');
    }
}

<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Brand - group relation model
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Model_Resource_Brand_Group extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * initialize resource model
     *
     * @access protected
     * @see Mage_Core_Model_Resource_Abstract::_construct()
     * @author Ultimate Module Creator
     */
    protected function  _construct()
    {
        $this->_init('ibd_travelsuite/brand_group', 'rel_id');
    }
    /**
     * Save brand - group relations
     *
     * @access public
     * @param IBD_TravelSuite_Model_Brand $brand
     * @param array $data
     * @return IBD_TravelSuite_Model_Resource_Brand_Group
     * @author Ultimate Module Creator
     */
    public function saveBrandRelation($brand, $data)
    {
        if (!is_array($data)) {
            $data = array();
        }
        $deleteCondition = $this->_getWriteAdapter()->quoteInto('brand_id=?', $brand->getId());
        $this->_getWriteAdapter()->delete($this->getMainTable(), $deleteCondition);

        foreach ($data as $groupId) {
            $this->_getWriteAdapter()->insert(
                $this->getMainTable(),
                array(
                    'brand_id' => $brand->getId(),
                    'group_id' => $groupId,
                )
            );
        }
        return $this;
    }

    /**
     * Save  group - brand relations
     *
     * @access public
     * @param Mage_Catalog_Model_Group $prooduct
     * @param array $data
     * @return IBD_TravelSuite_Model_Resource_Brand_Group
     * @@author Ultimate Module Creator
     */
    public function saveGroupRelation($group, $data)
    {
        if (!is_array($data)) {
            $data = array();
        }
        $deleteCondition = $this->_getWriteAdapter()->quoteInto('group_id=?', $group->getId());
        $this->_getWriteAdapter()->delete($this->getMainTable(), $deleteCondition);

        foreach ($data as $brandId) {
            $this->_getWriteAdapter()->insert(
                $this->getMainTable(),
                array(
                    'brand_id' => $brandId,
                    'group_id' => $group->getId(),
                )
            );
        }
        return $this;
    }
}

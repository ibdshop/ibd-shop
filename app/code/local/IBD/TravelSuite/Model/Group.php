<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Group model
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Model_Group extends Mage_Core_Model_Abstract
{
    const ENABLED = '1';
    const FEATURED = '2';
    const DISABLED = '0';

    /**
     * Entity code.
     * Can be used as part of method name for entity processing
     */
    const ENTITY    = 'ibd_travelsuite_group';
    const CACHE_TAG = 'ibd_travelsuite_group';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'ibd_travelsuite_group';

    /**
     * Parameter name in event
     *
     * @var string
     */
    protected $_eventObject = 'group';

    protected $_allImages = null;

    public function getStatusArray()
    {
        return array(
            self::ENABLED  => Mage::helper('ibd_travelsuite')->__('Enabled'),
            self::FEATURED => Mage::helper('ibd_travelsuite')->__('Featured'),
            self::DISABLED => Mage::helper('ibd_travelsuite')->__('Disabled'),
        );
    }

    public function getStatusOptions()
    {
        return Mage::helper('ibd_travelsuite')->arrayToOptionArray($this->getStatusArray());
    }

    public function getIBDType()
    {
        return $this->_eventObject;
    }

    public function getCategoryArray()
    {
        return array(
            '0'         => '-- None --',
            '百货公司'   => 'Department Store',
            '购物大道'   => 'Shopping Street',
            '购物街区'   => 'Shopping Area',
            '购物中心'   => 'Shopping Mall',
            '创意集市'   => 'Ideagoras',
            '跳蚤市场'   => 'Flea-Market',
            '奥特莱斯'   => 'Outlet',
            '精品店'     => 'Boutique',
            '免税购物'   => 'Tax-Free Shopping',
            '团体'       => 'Group',
        );
    }

    public function getCategoryOptions()
    {
        return Mage::helper('ibd_travelsuite')->arrayToOptionArray($this->getCategoryArray());
    }

    public function getActiveCategories()
    {
        $collection = $this->getCollection();
        $collection->getSelect()
            ->where('category != ?', '0')
            ->where('status > 0')
            ->columns('category')
            ->group('category');
        Mage::log('active category select: '.$collection->getSelect());
        return $collection;
    }

    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('ibd_travelsuite/group');
    }

    public function getGroups($status = -1, $limit = 0, $order = null)
    {
        $collection = $this->getCollection();
        $select = $collection->getSelect();

        if($status < 0) $select->where('main_table.status > 0');
        else $select->where('main_table.status = ?', $status);
        
        $select->join(
            array('region' => $collection->getTable('ibd_travelsuite/region')),
            'main_table.region_id = region.entity_id',
            array('region_name' => 'name')
        );
        $select->join(
            array('country' => $collection->getTable('ibd_travelsuite/country')),
            'region.country_id = country.entity_id',
            array('country_name' => 'name')
        );

        if($limit > 0) $select->limit($limit);

        if($order != null) $select->order($order);

        return $collection;
    }

    /**
     * before save group
     *
     * @access protected
     * @return IBD_TravelSuite_Model_Group
     * @author Ultimate Module Creator
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        $now = Mage::getSingleton('core/date')->gmtDate();
        if ($this->isObjectNew()) {
            $this->setCreatedAt($now);
        }
        $this->setUpdatedAt($now);
        return $this;
    }

    /**
     * get the url to the group details page
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getGroupUrl()
    {
        // this url key stuff isn't working at all...
        /*if ($this->getUrlKey()) {
            $urlKey = '';
            if ($prefix = Mage::getStoreConfig('ibd_travelsuite/group/url_prefix')) {
                $urlKey .= $prefix.'/';
            }
            $urlKey .= $this->getUrlKey();
            if ($suffix = Mage::getStoreConfig('ibd_travelsuite/group/url_suffix')) {
                $urlKey .= '.'.$suffix;
            }
            return Mage::getUrl('', array('_direct'=>$urlKey));
        }*/
        return Mage::getUrl('ibd_travelsuite/group/view', array('id'=>$this->getId()));
    }

    /**
     * check URL key
     *
     * @access public
     * @param string $urlKey
     * @param bool $active
     * @return mixed
     * @author Ultimate Module Creator
     */
    public function checkUrlKey($urlKey, $active = true)
    {
        return $this->_getResource()->checkUrlKey($urlKey, $active);
    }

    /**
     * get the group Detailed Description
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getDescription()
    {
        $description = $this->getData('description');
        $helper = Mage::helper('cms');
        $processor = $helper->getBlockTemplateProcessor();
        $html = $processor->filter($description);
        return $html;
    }

    /**
     * get the group Extra HTML
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getHtml()
    {
        $html = $this->getData('html');
        $helper = Mage::helper('cms');
        $processor = $helper->getBlockTemplateProcessor();
        $html = $processor->filter($html);
        return $html;
    }

    /**
     * save group relation
     *
     * @access public
     * @return IBD_TravelSuite_Model_Group
     * @author Ultimate Module Creator
     */
    protected function _afterSave()
    {
        return parent::_afterSave();
    }

    /**
     * Retrieve parent 
     *
     * @access public
     * @return null|IBD_TravelSuite_Model_Region
     * @author Ultimate Module Creator
     */
    public function getParentRegion()
    {
        if (!$this->hasData('_parent_region')) {
            if (!$this->getRegionId()) {
                return null;
            } else {
                $region = Mage::getModel('ibd_travelsuite/region')
                    ->load($this->getRegionId());
                if ($region->getId()) {
                    $this->setData('_parent_region', $region);
                } else {
                    $this->setData('_parent_region', null);
                }
            }
        }
        return $this->getData('_parent_region');
    }

    /**
     * get default values
     *
     * @access public
     * @return array
     * @author Ultimate Module Creator
     */
    public function getDefaultValues()
    {
        $values = array();
        $values['status'] = 1;
        $values['in_rss'] = 1;
        return $values;
    }

    /**
     * Get a list of all the badges. It should be cached for later use...
     */
    public function getBadges()
    {
        $registryKey = 'ibd_badges';
        $badges = Mage::registry($registryKey);
        if( !$badges )
        {
            $badgeCollection = $this->getCollection();
            $badgeCollection->getSelect()
                    ->reset(Zend_Db_Select::COLUMNS)
                    ->columns('main_table.entity_id')
                    ->columns('main_table.badge_name')
                    ->columns('main_table.badge_icon')
                    ->where('main_table.badge_name is not null OR main_table.badge_icon is not null');

            //Mage::log((string) $badgeCollection->getSelect());
            //Mage::log('count: '.$badgeCollection->count());
            $badges = array();
            foreach ($badgeCollection as $badge)
            {
                //Mage::log('found badge: '.$badge->getId().' - '.$badge->getBadgeName());
                $badges[$badge->getId()] = array(
                    'icon' => $badge->getBadgeIcon(),
                    'name' => $badge->getBadgeName(),
                );
            }
            Mage::unregister($registryKey);
            Mage::register($registryKey, $badges);
        }
        return $badges;
    }

    public function getDirections()
    {
        try
        {
            $dirs = json_decode($this->getData('directions'), true);
        }
        catch (Exception $e)
        {
            return array();
        }
        if($dirs) return $dirs;
        return array();
    }

    /**
     * get brand relation model
     *
     * @access public
     * @return IBD_TravelSuite_Model_Group_Brand
     * @author Ultimate Module Creator
     */
    public function getBrandInstance()
    {
        if (!$this->_brandInstance)
        {
            $this->_brandInstance = Mage::getSingleton('ibd_travelsuite/group_brand');
        }
        return $this->_brandInstance;
    }

    /**
     * get selected brands array
     *
     * @access public
     * @return array
     * @author Ultimate Module Creator
     */
    public function getSelectedBrands($order=null)
    {
        if (!$this->hasSelectedBrands()) {
            $brands = array();
            foreach ($this->getSelectedBrandsCollection($order) as $brand)
            {
                $brands[] = $brand;
            }
            $this->setSelectedBrands($brands);
        }
        return $this->getData('selected_brands');
    }

    /**
     * Retrieve collection selected brands
     *
     * @access public
     * @return IBD_TravelSuite_Resource_Group_Brand_Collection
     * @author Ultimate Module Creator
     */
    public function getSelectedBrandsCollection($order=null)
    {
        $collection = $this->getBrandInstance()->getBrandCollection($this);
        if($order)
        {
            $order = explode(' ', $order);
            $collection->setOrder($order[0], isset($order[1]) ? $order[1] : 'ASC');
        }
        //Mage::log('sql: '.((string)$collection->getSelect()));
        return $collection;
    }

    /**
     * Gets main image and gallery images in a sorted array with urls and thumbnail urls
     *
     * @access public
     * @param int (optional)
     * @return array
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function getGalleryImages($thumbSize=80)
    {
        if($this->_allImages === null)
        {
            $this->_allImages = Mage::helper('ibd_travelsuite')->getGalleryImages($this, $thumbSize, 1200);
        }
        return $this->_allImages;
    }

    /**
     * get default values
     *
     * @access public
     * @return IBD_TravelSuite_Model_Resource_Group_Advertisement_Collection
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function getAdvertisementCollection()
    {
        return Mage::getModel('ibd_travelsuite/group_advertisement')->getGroupAdCollection($this);
    }

    /**
     * saves group advertisement relations
     *
     * @access public
     * @param array (int)
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function upsertAdvertisementIds($ids)
    {
        Mage::getModel('ibd_homepagebanner/advertisement_relation')
                    ->upsertEntityRelations($this->getId(), 'ibd_group', $ids);
    }

    public function getGroupCount($status = -1)
    {
        return $this->getCollection()
                    ->addFieldToFilter('status', $status < 0 ? array('gt' => 0) : $status)
                    ->getSize();
    }
}

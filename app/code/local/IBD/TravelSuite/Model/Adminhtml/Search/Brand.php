<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Admin search model
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Model_Adminhtml_Search_Brand extends Varien_Object
{
    /**
     * Load search results
     *
     * @access public
     * @return IBD_TravelSuite_Model_Adminhtml_Search_Brand
     * @author Ultimate Module Creator
     */
    public function load()
    {
        $arr = array();
        if (!$this->hasStart() || !$this->hasLimit() || !$this->hasQuery()) {
            $this->setResults($arr);
            return $this;
        }
        $collection = Mage::getResourceModel('ibd_travelsuite/brand_collection')
            ->addFieldToFilter('name', array('like' => $this->getQuery().'%'))
            ->setCurPage($this->getStart())
            ->setPageSize($this->getLimit())
            ->load();
        foreach ($collection->getItems() as $brand) {
            $arr[] = array(
                'id'          => 'brand/1/'.$brand->getId(),
                'type'        => Mage::helper('ibd_travelsuite')->__('Brand'),
                'name'        => $brand->getName(),
                'description' => $brand->getName(),
                'url' => Mage::helper('adminhtml')->getUrl(
                    '*/travelsuite_brand/edit',
                    array('id'=>$brand->getId())
                ),
            );
        }
        $this->setResults($arr);
        return $this;
    }
}

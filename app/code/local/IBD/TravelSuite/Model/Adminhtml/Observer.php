<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Adminhtml observer
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Model_Adminhtml_Observer
{
    /**
     * check if tab can be added
     *
     * @access protected
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     * @author Ultimate Module Creator
     */
    protected function _canAddTab($product)
    {
        if ($product->getId()) {
            return true;
        }
        if (!$product->getAttributeSetId()) {
            return false;
        }
        $request = Mage::app()->getRequest();
        if ($request->getParam('type') == 'configurable') {
            if ($request->getParam('attributes')) {
                return true;
            }
        }
        return false;
    }

    /**
     * add the brand tab to products
     *
     * @access public
     * @param Varien_Event_Observer $observer
     * @return IBD_TravelSuite_Model_Adminhtml_Observer
     * @author Ultimate Module Creator
     */
    public function addProductBrandBlock($observer)
    {
        $block = $observer->getEvent()->getBlock();
        $product = Mage::registry('product');
        if ($block instanceof Mage_Adminhtml_Block_Catalog_Product_Edit_Tabs && $this->_canAddTab($product)) {
            $block->addTab(
                'brands',
                array(
                    'label' => Mage::helper('ibd_travelsuite')->__('Brands'),
                    'url'   => Mage::helper('adminhtml')->getUrl(
                        'adminhtml/travelsuite_brand_catalog_product/brands',
                        array('_current' => true)
                    ),
                    'class' => 'ajax',
                )
            );
        }
        return $this;
    }

    /**
     * save brand - product relation
     * @access public
     * @param Varien_Event_Observer $observer
     * @return IBD_TravelSuite_Model_Adminhtml_Observer
     * @author Ultimate Module Creator
     */
    public function saveProductBrandData($observer)
    {
        $post = Mage::app()->getRequest()->getPost('brands', -1);
        if ($post != '-1') {
            $post = Mage::helper('adminhtml/js')->decodeGridSerializedInput($post);
            $product = Mage::registry('product');
            $brandProduct = Mage::getResourceSingleton('ibd_travelsuite/brand_product')
                ->saveProductRelation($product, $post);
        }
        return $this;
    }}

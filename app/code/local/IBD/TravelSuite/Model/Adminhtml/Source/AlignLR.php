<?php
class IBD_TravelSuite_Model_Adminhtml_Source_AlignLR extends IBD_TravelSuite_Model_Adminhtml_Source_Align
{
	public function toOptionArray()
	{
		return array(
			array('value' => self::RIGHT, 'label' => 'Right'),
			array('value' => self::LEFT, 'label' => 'Left'),
		);
	}
}
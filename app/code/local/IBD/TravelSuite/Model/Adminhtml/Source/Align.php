<?php
class IBD_TravelSuite_Model_Adminhtml_Source_Align
{
	const TOP = 0;
	const RIGHT = 1;
	const BOTTOM = 2;
	const LEFT = 3;

	public function toOptionArray()
	{
		return array(
			array('value' => self::TOP, 'label' => 'Top'),
			array('value' => self::RIGHT, 'label' => 'Right'),
			array('value' => self::BOTTOM, 'label' => 'Bottom'),
			array('value' => self::LEFT, 'label' => 'Left'),
		);
	}
}
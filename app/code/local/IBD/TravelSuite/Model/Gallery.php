<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Gallery model
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Model_Gallery extends Mage_Core_Model_Abstract
{
    /**
     * Entity code.
     * Can be used as part of method name for entity processing
     */
    const ENTITY    = 'ibd_travelsuite_gallery';
    const CACHE_TAG = 'ibd_travelsuite_gallery';

    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('ibd_travelsuite/gallery');
    }

    public function upsertEntityGallery($relationId, $relationType, $imageUrlArray)
    {
        $galleryImages = $this->getAdminEntityImageCollection($relationId, $relationType);

        // overwrite existing gallery images with new URLs
        $index = 0;
        foreach ($galleryImages as $image)
        {
            if( isset($imageUrlArray[$index]) && $imageUrlArray[$index] )
            {
                // we have an image at this index
                $image->setImagePath($imageUrlArray[$index]);
                $image->save();
            }
            else
            {
                $image->delete();
            }
            $index++;
        }

        // if we have images left, create new gallery images
        while($index < sizeof($imageUrlArray) && $imageUrlArray[$index])
        {
            Mage::log('saving new: '.$imageUrlArray[$index]);
            $image = Mage::getModel('ibd_travelsuite/gallery');
            $image->setImagePath($imageUrlArray[$index]);
            $image->setRelationId($relationId);
            $image->setRelationType($relationType);
            $image->setOrder(++$index); // increment, then return
            $image->save();
        }
    }

    public function updateImagePositions($updateImages)
    {
        foreach (Mage::getSingleton('ibd_travelsuite/gallery')
                        ->getCollection()
                        ->addFieldToFilter('entity_id', array('in' => array_keys($updateImages))) as $galleryImage)
        {
            $updateImage = $updateImages[$galleryImage->getId()];
            if(isset($updateImage['remove']) && $updateImage['remove'] == '1')
            {
                $galleryImage->delete();
            }
            elseif(isset($updateImage['disable']) && $updateImage['disable'] == '1')
            {
                $galleryImage->setOrder(0);
                $galleryImage->save();
            }
            elseif(isset($updateImage['position']) && $galleryImage->getOrder() != $updateImage['position'])
            {
                $galleryImage->setOrder($updateImages[$galleryImage->getId()]['position']);
                $galleryImage->save();
            }
        }
    }

    public function getEntityImageCollection($relationId, $relationType)
    {
        return $this->getAdminEntityImageCollection($relationId, $relationType)
                    ->addFieldToFilter('main_table.order', array('gt' => 0));
    }

    public function getAdminEntityImageCollection($relationId, $relationType)
    {
        return $this->getCollection()
                            ->addFieldToFilter('main_table.relation_id', $relationId)
                            ->addFieldToFilter('main_table.relation_type', $relationType)
                            ->setOrder('main_table.order', 'ASC');
    }

    public function getImageUrl($size, $dims=null, $keepFrame=false)
    {
        if(!isset($this->_imageUrl[$size]))
        {
            $this->_imageUrl[$size] =
                (string) Mage::helper('ibd_travelsuite/gallery_image')
                            ->init($this, 'image_path')
                            ->keepFrame($keepFrame)
                            ->resize($dims, $dims);
        }
        return $this->_imageUrl[$size];
    }

    /**
     * before save gallery
     *
     * @access protected
     * @return IBD_TravelSuite_Model_Gallery
     * @author Ultimate Module Creator
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        $now = Mage::getSingleton('core/date')->gmtDate();
        if ($this->isObjectNew()) {
            $this->setCreatedAt($now);
        }
        $this->setUpdatedAt($now);
        return $this;
    }

    /**
     * before save gallery
     *
     * @access protected
     * @return IBD_TravelSuite_Model_Gallery
     * @author Ultimate Module Creator
     */
    public function delete()
    {
        $file = Mage::helper('ibd_travelsuite/gallery_image')->init($this, 'image_path')->delete();
        parent::delete();
    }
}

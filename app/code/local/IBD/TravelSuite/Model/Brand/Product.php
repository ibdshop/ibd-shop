<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Brand product model
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Model_Brand_Product extends Mage_Core_Model_Abstract
{
    /**
     * Initialize resource
     *
     * @access protected
     * @return void
     * @author Ultimate Module Creator
     */
    protected function _construct()
    {
        $this->_init('ibd_travelsuite/brand_product');
    }

    /**
     * Save data for brand-product relation
     * @access public
     * @param  IBD_TravelSuite_Model_Brand $brand
     * @return IBD_TravelSuite_Model_Brand_Product
     * @author Ultimate Module Creator
     */
    public function saveBrandRelation($brand)
    {
        $data = $brand->getProductsData();
        if (!is_null($data)) {
            $this->_getResource()->saveBrandRelation($brand, $data);
        }
        return $this;
    }

    /**
     * get products for brand
     *
     * @access public
     * @param IBD_TravelSuite_Model_Brand $brand
     * @return IBD_TravelSuite_Model_Resource_Brand_Product_Collection
     * @author Ultimate Module Creator
     */
    public function getProductCollection($brand)
    {
        $collection = Mage::getResourceModel('ibd_travelsuite/brand_product_collection')
            ->addBrandFilter($brand);
        return $collection;
    }
}

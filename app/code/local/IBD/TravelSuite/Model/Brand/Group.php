<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Brand group model
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Model_Brand_Group extends Mage_Core_Model_Abstract
{
    /**
     * Initialize resource
     *
     * @access protected
     * @return void
     * @author Ultimate Module Creator
     */
    protected function _construct()
    {
        $this->_init('ibd_travelsuite/brand_group');
    }

    /**
     * Save data for brand-group relation
     * @access public
     * @param  IBD_TravelSuite_Model_Brand $brand
     * @return IBD_TravelSuite_Model_Brand_Group
     * @author Ultimate Module Creator
     */
    public function saveBrandRelation($brand)
    {
        $groupIds = $brand->getGroupIds();
        if (!is_null($groupIds)) {
            $this->_getResource()->saveBrandRelation($brand, $groupIds);
        }
        return $this;
    }

    /**
     * get groups for brand
     *
     * @access public
     * @param IBD_TravelSuite_Model_Brand $brand
     * @return IBD_TravelSuite_Model_Resource_Brand_Group_Collection
     * @author Ultimate Module Creator
     */
    public function getGroupCollection($brand)
    {
        $collection = Mage::getResourceModel('ibd_travelsuite/brand_group_collection')->addBrandFilter($brand);
        return $collection;
    }
}

<?php
class IBD_TravelSuite_Model_Observer_Cms
{
    public function savePage(Varien_Event_Observer $observer)
    {
        $request = $observer->getEvent()->getRequest();

        /* SAVE PAGE BANNER ICON */
        // build an array with the banner icon info
        $data = array(
            'ibd_banner_icon' => isset($_FILES['ibd_banner_icon']) && $_FILES['ibd_banner_icon']['name'] != ""
                ? $_FILES['ibd_banner_icon']
                : $request->getPost('ibd_banner_icon'),
        );

        // upload the icon and get the name
        $iconName = Mage::helper('ibd_travelsuite')->uploadImageAndGetName(
            'ibd_banner_icon',
            Mage::helper('ibd_travelsuite/title_image')->getImageBaseDir(),
            $data
        );

        // set the icon field on the page
        $observer->getEvent()->getPage()->setIbdBannerIcon($iconName);

        /* SAVE PAGE GALLERY */
        // save the page gallery items
        $pageParam = $request->getParam('page');
        if( isset($pageParam['gallery']) )
        {
            Mage::getSingleton('ibd_travelsuite/gallery')->updateImagePositions($pageParam['gallery']);
        }
    }
}
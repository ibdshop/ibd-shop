<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Brand model
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Model_Brand extends Mage_Core_Model_Abstract
{
    protected $_productImageUrl = array();
    protected $_imageUrl = array();

    const DISABLED = '0';
    const ENABLED = '1';
    const SHOWCASE = '3';
    /**
     * Entity code.
     * Can be used as part of method name for entity processing
     */
    const ENTITY    = 'ibd_travelsuite_brand';
    const CACHE_TAG = 'ibd_travelsuite_brand';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'ibd_travelsuite_brand';

    /**
     * Parameter name in event
     *
     * @var string
     */
    protected $_eventObject = 'brand';
    protected $_productInstance = null;
    protected $_groupInstance = null;

    public function getStatusArray()
    {
        return array(
            self::ENABLED  => Mage::helper('ibd_travelsuite')->__('Enabled'),
            self::SHOWCASE => Mage::helper('ibd_travelsuite')->__('Showcased'),
            self::DISABLED => Mage::helper('ibd_travelsuite')->__('Disabled'),
        );
    }

    public function getIBDType()
    {
        return $this->_eventObject;
    }

    public function getStatusOptions()
    {
        $statusArray = $this->getStatusArray();
        $options = array();
        foreach ($statusArray as $value => $text)
        {
            $options[] = array(
                'value' => $value,
                'label' => $text,
            );
        }
        return $options;
    }

    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('ibd_travelsuite/brand');
    }

    public function delete()
    {
        Mage::getSingleton('ibd_travelsuite/tag_relation')->propogateRelationDelete('ibd_brand', $this->getId());
        return parent::delete();
    }

    public function getBrands($showcaseOnly, $limit = 0, $getProductThumbnail = false)
    {
        $collection = $this->getCollection();

        if($showcaseOnly) $collection->getSelect()->where('main_table.status = ?', self::SHOWCASE);
        else $collection->getSelect()->where('main_table.status > 0');

        if($getProductThumbnail)
        {
            $collection->getSelect()->joinLeft(
                array('brand_product' => $collection->getTable('ibd_travelsuite/brand_product')),
                'main_table.entity_id = brand_product.brand_id',
                array('product_id' => 'brand_product.product_id')
            );

            $collection->getSelect()->order('brand_product.position ASC');
            $collection->getSelect()->group('main_table.entity_id');
        }

        if($limit) $collection->getSelect()->limit($limit);

        $collection->getSelect()->order('main_table.name ASC');
        Mage::log('IBD_TravelSuite/Model/Brands.getBrands: ' . (string) $collection->getSelect());
        return $collection;
    }

    public function getLinkedProductImage($size, $dims=null, $keepFrame=true)
    {
        if(!isset($this->_productImageUrl[$size]))
        {
            $this->_productImageUrl[$size] =
                (string) Mage::helper('catalog/image')
                            ->init(Mage::getModel('catalog/product')->load($this->getProductId()), $size)
                            ->keepFrame($keepFrame)
                            ->resize($dims);
        }
        return $this->_productImageUrl[$size];
    }

    public function getImageUrl($size, $dims=null, $keepFrame=false)
    {
        if(!isset($this->_imageUrl[$size]))
        {
            $this->_imageUrl[$size] =
                (string) Mage::helper('ibd_travelsuite/brand_image')
                            ->init($this)
                            ->keepFrame($keepFrame)
                            ->resize($dims, $dims);
        }
        return $this->_imageUrl[$size];
    }

    public function isShowcased()
    {
        return $this->getStatus() == self::SHOWCASE;
    }

    /**
     * before save brand
     *
     * @access protected
     * @return IBD_TravelSuite_Model_Brand
     * @author Ultimate Module Creator
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        $now = Mage::getSingleton('core/date')->gmtDate();
        if ($this->isObjectNew()) {
            $this->setCreatedAt($now);
        }
        $this->setUpdatedAt($now);
        return $this;
    }

    /**
     * get the url to the brand details page
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getBrandUrl()
    {
        // this url key stuff isn't working at all...
        /*if ($this->getUrlKey()) {
            $urlKey = '';
            if ($prefix = Mage::getStoreConfig('ibd_travelsuite/brand/url_prefix')) {
                $urlKey .= $prefix.'/';
            }
            $urlKey .= $this->getUrlKey();
            if ($suffix = Mage::getStoreConfig('ibd_travelsuite/brand/url_suffix')) {
                $urlKey .= '.'.$suffix;
            }
            return Mage::getUrl('ibd_travelsuite/brand/view', array('_direct'=>$urlKey));
        }*/
        if($this->isShowcased())
        {
            return Mage::getUrl('ibd_travelsuite/brand/view', array('id'=>$this->getId()));
        }
        return '/brands?key='.$this->getUrlKey();
        
    }

    /**
     * check URL key
     *
     * @access public
     * @param string $urlKey
     * @param bool $active
     * @return mixed
     * @author Ultimate Module Creator
     */
    public function checkUrlKey($urlKey, $active = true)
    {
        return $this->_getResource()->checkUrlKey($urlKey, $active);
    }

    /**
     * get the brand Description
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getDescription()
    {
        $description = $this->getData('description');
        $helper = Mage::helper('cms');
        $processor = $helper->getBlockTemplateProcessor();
        $html = $processor->filter($description);
        return $html;
    }

    /**
     * save brand relation
     *
     * @access public
     * @return IBD_TravelSuite_Model_Brand
     * @author Ultimate Module Creator
     */
    protected function _afterSave()
    {
        $this->getProductInstance()->saveBrandRelation($this);
        $this->getGroupInstance()->saveBrandRelation($this);
        return parent::_afterSave();
    }

    /**
     * get product relation model
     *
     * @access public
     * @return IBD_TravelSuite_Model_Brand_Product
     * @author Ultimate Module Creator
     */
    public function getProductInstance()
    {
        if (!$this->_productInstance) {
            $this->_productInstance = Mage::getSingleton('ibd_travelsuite/brand_product');
        }
        return $this->_productInstance;
    }

    /**
     * get selected products array
     *
     * @access public
     * @return array
     * @author Ultimate Module Creator
     */
    public function getSelectedProducts()
    {
        if (!$this->hasSelectedProducts()) {
            $products = array();
            foreach ($this->getSelectedProductsCollection() as $product) {
                $products[] = $product;
            }
            $this->setSelectedProducts($products);
        }
        return $this->getData('selected_products');
    }

    /**
     * Retrieve collection selected products
     *
     * @access public
     * @return IBD_TravelSuite_Resource_Brand_Product_Collection
     * @author Ultimate Module Creator
     */
    public function getSelectedProductsCollection()
    {
        $collection = $this->getProductInstance()->getProductCollection($this);
        return $collection;
    }

    /**
     * get group relation model
     *
     * @access public
     * @return IBD_TravelSuite_Model_Brand_Group
     * @author Ultimate Module Creator
     */
    public function getGroupInstance()
    {
        if (!$this->_groupInstance) {
            $this->_groupInstance = Mage::getSingleton('ibd_travelsuite/brand_group');
        }
        return $this->_groupInstance;
    }

    /**
     * get selected groups array
     *
     * @access public
     * @return array
     * @author Ultimate Module Creator
     */
    public function getSelectedGroups()
    {
        if (!$this->hasSelectedGroups()) {
            $groups = array();
            foreach ($this->getSelectedGroupsCollection() as $group) {
                $groups[] = $group;
            }
            $this->setSelectedGroups($groups);
        }
        return $this->getData('selected_groups');
    }

    /**
     * Retrieve collection selected groups
     *
     * @access public
     * @return IBD_TravelSuite_Resource_Brand_Group_Collection
     * @author Ultimate Module Creator
     */
    public function getSelectedGroupsCollection()
    {
        $collection = $this->getGroupInstance()->getGroupCollection($this);
        return $collection;
    }

    /**
     * get default values
     *
     * @access public
     * @return array
     * @author Ultimate Module Creator
     */
    public function getDefaultValues()
    {
        $values = array();
        $values['status'] = 1;
        $values['in_rss'] = 1;
        return $values;
    }

    public function getSelectedTags()
    {
        $tags = Mage::registry('brand_tags');
        if(!$tags)
        {
            $tags = Mage::getModel('ibd_travelsuite/tag_relation')->getTags('ibd_brand', $this->getId());
            Mage::register('brand_tags', $tags);
        }
        return $tags;
    }

    /**
     * Merges, saves, and deletes tags and tag relations
     *
     * @access public
     * @param array
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function saveTags($tagNameArray)
    {
        $allExistingTags = Mage::getModel('ibd_travelsuite/tag')->getAll();
        $dbTagRelations = $this->getSelectedTags();

        // delete tag relations that are no longer in the list
        $existingTagNames = array();
        foreach ($dbTagRelations as $dbTagRelation)
        {
            $existingTagNames[] = $dbTagRelation->getTag();
            if(!in_array($dbTagRelation->getTag(), $tagNameArray))
            {
                $dbTagRelation->delete();
            }
        }

        // add missing tag relations
        foreach ($tagNameArray as $tagName)
        {
            if(!in_array($tagName, $existingTagNames))
            {
                // we'll need to create a new tag relation, but first
                // check if the tag exists
                $tagId = -1;
                foreach ($allExistingTags as $existingTag)
                {
                    if($existingTag->getTag() == $tagName)
                    {
                        $tagId = $existingTag->getId();
                        break;
                    }
                }

                // didn't find the tag, let's create the tag
                if($tagId < 0)
                {
                    // we didn't find an existing tag, create a new one
                    $tag = Mage::getModel('ibd_travelsuite/tag');
                    $tag->setTag($tagName);
                    $tag->save();
                    $tagId = $tag->getId();
                }

                // now that we have a tag ID, we can create the relation
                $newTag = Mage::getModel('ibd_travelsuite/tag_relation');
                $newTag->setRelationType('ibd_brand');
                $newTag->setRelationId($this->getId());
                $newTag->setTagId($tagId);
                $newTag->save();
            }
        }
    }

    /**
     * Gets main image and gallery images in a sorted array with urls and thumbnail urls
     *
     * @access public
     * @param int (optional)
     * @return array
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function getGalleryImages($thumbSize = 0)
    {
        if($this->_allImages === null)
        {
            $this->_allImages = Mage::helper('ibd_travelsuite')->getGalleryImages($this, $thumbSize, 1200);
        }
        return $this->_allImages;
    }

    /**
     * get default values
     *
     * @access public
     * @return IBD_TravelSuite_Model_Resource_Brand_Advertisement_Collection
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function getAdvertisementCollection()
    {
        return Mage::getModel('ibd_travelsuite/brand_advertisement')->getBrandAdCollection($this);
    }

    /**
     * saves brand advertisement relations
     *
     * @access public
     * @param array (int)
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function upsertAdvertisementIds($ids)
    {
        Mage::getModel('ibd_homepagebanner/advertisement_relation')
                    ->upsertEntityRelations($this->getId(), 'ibd_brand', $ids);
    }

    public function getBrandCount($status = -1)
    {
        return $this->getCollection()
                    ->addFieldToFilter('status', $status < 0 ? array('gt' => 0) : $status)
                    ->getSize();
    }
}

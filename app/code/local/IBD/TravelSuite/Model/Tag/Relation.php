<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * TagRelation model
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Model_Tag_Relation extends Mage_Core_Model_Abstract
{
    /**
     * Entity code.
     * Can be used as part of method name for entity processing
     */
    const ENTITY    = 'ibd_travelsuite_tag_relation';
    const CACHE_TAG = 'ibd_travelsuite_tag_relation';

    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('ibd_travelsuite/tag_relation');
    }

    public function getAllTags($relationType){

        $collection = $this->getCollection()
                            ->addFieldToFilter('main_table.relation_type', $relationType);

        $collection->getSelect()->join(
            array('tag' => $collection->getTable('ibd_travelsuite/tag')),
            'tag.entity_id = main_table.tag_id',
            array('tag', 'tag.tag')
        );

        return $collection;
    }

    public function getTags($relationType, $relationId, $getTagNames = true)
    {
        $collection = $this->getCollection()
                            ->addFieldToFilter('main_table.relation_id', $relationId)
                            ->addFieldToFilter('main_table.relation_type', $relationType);

        if($getTagNames)
        {
            $collection->getSelect()->join(
                array('tag' => $collection->getTable('ibd_travelsuite/tag')),
                'tag.entity_id = main_table.tag_id',
                array('tag', 'tag.tag')
            );
        }

        return $collection;
    }

    public function delete()
    {
        $resource = Mage::getSingleton('core/resource');
        $sql = 'DELETE FROM ' . $resource->getTableName('ibd_travelsuite/tag_relation') .
                ' WHERE tag_id = ' . $this->getTagId() .
                ' AND relation_type = \'' . $this->getRelationType() . '\'' .
                ' AND relation_id = ' . $this->getRelationId();
        Mage::log('delete SQL: '.$sql);
        $resource->getConnection('core_write')->query($sql);
    }

    public function propogateRelationDelete($relationType, $relationId)
    {
        $resource = Mage::getSingleton('core/resource');
        $connection = $resource->getConnection('core_write');
        $sql = 'DELETE FROM ' . $resource->getTableName('ibd_travelsuite/tag_relation') .
                ' WHERE relation_type = ' . $connection->quote($relationType) .
                ' AND relation_id = ' . (int) $relationId;
        Mage::log('delete SQL: '.$sql);
        $connection->query($sql);
    }
}

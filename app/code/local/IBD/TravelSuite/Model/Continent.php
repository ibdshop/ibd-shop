<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Continent model
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Model_Continent extends Mage_Core_Model_Abstract
{
    /**
     * Entity code.
     * Can be used as part of method name for entity processing
     */
    const ENTITY    = 'ibd_travelsuite_continent';
    const CACHE_TAG = 'ibd_travelsuite_continent';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'ibd_travelsuite_continent';

    /**
     * Parameter name in event
     *
     * @var string
     */
    protected $_eventObject = 'continent';

    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('ibd_travelsuite/continent');
    }

    /**
     * before save continent
     *
     * @access protected
     * @return IBD_TravelSuite_Model_Continent
     * @author Ultimate Module Creator
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        $now = Mage::getSingleton('core/date')->gmtDate();
        if ($this->isObjectNew()) {
            $this->setCreatedAt($now);
        }
        $this->setUpdatedAt($now);
        return $this;
    }

    /**
     * save continent relation
     *
     * @access public
     * @return IBD_TravelSuite_Model_Continent
     * @author Ultimate Module Creator
     */
    protected function _afterSave()
    {
        return parent::_afterSave();
    }

    /**
     * Retrieve  collection
     *
     * @access public
     * @return IBD_TravelSuite_Model_Country_Collection
     * @author Ultimate Module Creator
     */
    public function getSelectedCountriesCollection()
    {
        if (!$this->hasData('_country_collection')) {
            if (!$this->getId()) {
                return new Varien_Data_Collection();
            } else {
                $collection = Mage::getResourceModel('ibd_travelsuite/country_collection')
                        ->addFieldToFilter('continent_id', $this->getId());
                $this->setData('_country_collection', $collection);
            }
        }
        return $this->getData('_country_collection');
    }

    /**
     * get default values
     *
     * @access public
     * @return array
     * @author Ultimate Module Creator
     */
    public function getDefaultValues()
    {
        $values = array();
        $values['status'] = 1;
        return $values;
    }
    
}

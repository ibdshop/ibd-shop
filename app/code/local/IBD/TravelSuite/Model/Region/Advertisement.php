<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Region advertisement model
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Kevin Sweeney (kevin@robotshanghai.com)
 */
class IBD_TravelSuite_Model_Region_Advertisement extends Mage_Core_Model_Abstract
{
    /**
     * Initialize resource
     *
     * @access protected
     * @return void
     * @author Ultimate Module Creator
     */
    protected function _construct()
    {
        $this->_init('ibd_homepagebanner/advertisement');
    }

    /**
     * get advertisements for region
     *
     * @access public
     * @param IBD_TravelSuite_Model_Region $region
     * @return IBD_TravelSuite_Model_Resource_Region_Advertisement_Collection
     * @author Ultimate Module Creator
     */
    public function getRegionAdCollection($region)
    {
        $collection = Mage::getResourceModel('ibd_travelsuite/region_advertisement_collection')->addRegionFilter($region);
        return $collection;
    }
}

<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Country model
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Model_Country extends Mage_Core_Model_Abstract
{
    /**
     * Entity code.
     * Can be used as part of method name for entity processing
     */
    const ENTITY    = 'ibd_travelsuite_country';
    const CACHE_TAG = 'ibd_travelsuite_country';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'ibd_travelsuite_country';

    /**
     * Parameter name in event
     *
     * @var string
     */
    protected $_eventObject = 'country';

    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('ibd_travelsuite/country');
    }

    /**
     * before save country
     *
     * @access protected
     * @return IBD_TravelSuite_Model_Country
     * @author Ultimate Module Creator
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        $now = Mage::getSingleton('core/date')->gmtDate();
        if ($this->isObjectNew()) {
            $this->setCreatedAt($now);
        }
        $this->setUpdatedAt($now);
        return $this;
    }

    /**
     * save country relation
     *
     * @access public
     * @return IBD_TravelSuite_Model_Country
     * @author Ultimate Module Creator
     */
    protected function _afterSave()
    {
        return parent::_afterSave();
    }

    /**
     * Retrieve  collection
     *
     * @access public
     * @return IBD_TravelSuite_Model_Region_Collection
     * @author Ultimate Module Creator
     */
    public function getSelectedRegionsCollection()
    {
        if (!$this->hasData('_region_collection')) {
            if (!$this->getId()) {
                return new Varien_Data_Collection();
            } else {
                $collection = Mage::getResourceModel('ibd_travelsuite/region_collection')
                        ->addFieldToFilter('country_id', $this->getId());
                $this->setData('_region_collection', $collection);
            }
        }
        return $this->getData('_region_collection');
    }

    /**
     * Retrieve parent 
     *
     * @access public
     * @return null|IBD_TravelSuite_Model_Continent
     * @author Ultimate Module Creator
     */
    public function getParentContinent()
    {
        if (!$this->hasData('_parent_continent')) {
            if (!$this->getContinentId()) {
                return null;
            } else {
                $continent = Mage::getModel('ibd_travelsuite/continent')
                    ->load($this->getContinentId());
                if ($continent->getId()) {
                    $this->setData('_parent_continent', $continent);
                } else {
                    $this->setData('_parent_continent', null);
                }
            }
        }
        return $this->getData('_parent_continent');
    }

    /**
     * get default values
     *
     * @access public
     * @return array
     * @author Ultimate Module Creator
     */
    public function getDefaultValues()
    {
        $values = array();
        $values['status'] = 1;
        return $values;
    }
    
}

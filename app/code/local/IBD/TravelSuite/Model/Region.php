<?php
/**
 * IBD_TravelSuite extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       IBD
 * @package        IBD_TravelSuite
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Region model
 *
 * @category    IBD
 * @package     IBD_TravelSuite
 * @author      Ultimate Module Creator
 */
class IBD_TravelSuite_Model_Region extends Mage_Core_Model_Abstract
{
    /**
     * Entity code.
     * Can be used as part of method name for entity processing
     */
    const ENTITY    = 'ibd_travelsuite_region';
    const CACHE_TAG = 'ibd_travelsuite_region';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'ibd_travelsuite_region';

    /**
     * Parameter name in event
     *
     * @var string
     */
    protected $_eventObject = 'region';

    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('ibd_travelsuite/region');
    }

    public function getIBDType()
    {
        return $this->_eventObject;
    }

    /**
     * before save region
     *
     * @access protected
     * @return IBD_TravelSuite_Model_Region
     * @author Ultimate Module Creator
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        $now = Mage::getSingleton('core/date')->gmtDate();
        if ($this->isObjectNew()) {
            $this->setCreatedAt($now);
        }
        $this->setUpdatedAt($now);
        return $this;
    }

    /**
     * get the url to the region details page
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getRegionUrl()
    {
        /*if ($this->getUrlKey()) {
            $urlKey = '';
            if ($prefix = Mage::getStoreConfig('ibd_travelsuite/region/url_prefix')) {
                $urlKey .= $prefix.'/';
            }
            $urlKey .= $this->getUrlKey();
            if ($suffix = Mage::getStoreConfig('ibd_travelsuite/region/url_suffix')) {
                $urlKey .= '.'.$suffix;
            }
            return Mage::getUrl('', array('_direct'=>$urlKey));
        }*/
        return Mage::getUrl('ibd_travelsuite/region/view', array('id'=>$this->getId()));
    }

    /**
     * check URL key
     *
     * @access public
     * @param string $urlKey
     * @param bool $active
     * @return mixed
     * @author Ultimate Module Creator
     */
    public function checkUrlKey($urlKey, $active = true)
    {
        return $this->_getResource()->checkUrlKey($urlKey, $active);
    }

    /**
     * get the region Description
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getDescription()
    {
        $description = $this->getData('description');
        $helper = Mage::helper('cms');
        $processor = $helper->getBlockTemplateProcessor();
        $html = $processor->filter($description);
        return $html;
    }

    /**
     * save region relation
     *
     * @access public
     * @return IBD_TravelSuite_Model_Region
     * @author Ultimate Module Creator
     */
    protected function _afterSave()
    {
        return parent::_afterSave();
    }

    /**
     * Retrieve  collection
     *
     * @access public
     * @return IBD_TravelSuite_Model_Group_Collection
     * @author Ultimate Module Creator
     */
    public function getSelectedGroupsCollection()
    {
        if (!$this->hasData('_group_collection')) {
            if (!$this->getId()) {
                return new Varien_Data_Collection();
            } else {
                $collection = Mage::getResourceModel('ibd_travelsuite/group_collection')
                        ->addFieldToFilter('region_id', $this->getId());
                $this->setData('_group_collection', $collection);
            }
        }
        return $this->getData('_group_collection');
    }

    /**
     * Retrieve parent 
     *
     * @access public
     * @return null|IBD_TravelSuite_Model_Country
     * @author Ultimate Module Creator
     */
    public function getParentCountry()
    {
        if (!$this->hasData('_parent_country')) {
            if (!$this->getCountryId()) {
                return null;
            } else {
                $country = Mage::getModel('ibd_travelsuite/country')
                    ->load($this->getCountryId());
                if ($country->getId()) {
                    $this->setData('_parent_country', $country);
                } else {
                    $this->setData('_parent_country', null);
                }
            }
        }
        return $this->getData('_parent_country');
    }

    /**
     * get default values
     *
     * @access public
     * @return array
     * @author Ultimate Module Creator
     */
    public function getDefaultValues()
    {
        $values = array();
        $values['status'] = 1;
        $values['in_rss'] = 1;
        return $values;
    }

    /**
     * get default values
     *
     * @access public
     * @return IBD_TravelSuite_Model_Resource_Region_Advertisement_Collection
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function getAdvertisementCollection()
    {
        return Mage::getModel('ibd_travelsuite/region_advertisement')->getRegionAdCollection($this);
    }

    /**
     * saves region advertisement relations
     *
     * @access public
     * @param array (int)
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function upsertAdvertisementIds($ids)
    {
        Mage::getModel('ibd_homepagebanner/advertisement_relation')
                    ->upsertEntityRelations($this->getId(), 'ibd_region', $ids);
    }

    public function getRegionCount($status = -1)
    {
        return $this->getCollection()
                    ->addFieldToFilter('status', $status < 0 ? array('gt' => 0) : $status)
                    ->getSize();
    }

    /**
     * Gets main image and gallery images in a sorted array with urls and thumbnail urls
     *
     * @access public
     * @param int (optional)
     * @return array
     * @author Kevin Sweeney (kevin@robotshanghai.com)
     */
    public function getGalleryImages($thumbSize=80)
    {
        if($this->_allImages === null)
        {
            $this->_allImages = Mage::helper('ibd_travelsuite')->getGalleryImages($this, $thumbSize, 1200);
        }
        return $this->_allImages;
    }
}

<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Product_Filter-v1.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Productsfilter_Helper_Data extends Plumrocket_Productsfilter_Helper_Main
{
	/*
	$this->_selectedParams = [
		filtered array Plumrocket_Productsfilter_Model_Options::_optionsList
	]
	*/
	protected $_selectedParams = null;
	
	public function moduleEnabled($store = null)
	{
		return (bool)Mage::getStoreConfig('productsfilter/general/enable', $store);
	}

	public function disableExtension()
	{
		$resource = Mage::getSingleton('core/resource');
		$connection = $resource->getConnection('core_write');
		$connection->delete($resource->getTableName('core/config_data'), array($connection->quoteInto('path IN (?)', array('productsfilter/general/enable', 'productsfilter/attributes/enable', 'productsfilter/attributes/enable_custom',))));
		$config = Mage::getConfig();
		$config->reinit();
		Mage::app()->reinitStores();
	}

	public function customOptionsEnables($store = null)
	{
		return (bool)Mage::getStoreConfig('productsfilter/attributes/enable_custom', $store);
	}

	public function getSelectedParams()
	{
		if (is_null($this->_selectedParams)) {
			$this->_selectedParams = array();

			if ($this->moduleEnabled()) {
				$inputParams = Mage::app()->getRequest()->getParams();

				if ($inputParams) {
					// call if this method still not executed
					$options = Mage::getSingleton('productsfilter/options')->getItems();

					foreach ($inputParams as $code => $items) {
						$code = substr($code, 1);
						if ($code && array_key_exists($code, $options)) {
							if (! is_array($items)) {
								$items = array($items => 1);
							}
							
							$this->_selectedParams[ $code ] = array(
								'label' => $options[$code]['label'],
								'items' => array(),
							);

							foreach ($items as $option => $_) {
								if (array_key_exists($option, $options[$code]['items'])) {
									$this->_selectedParams[$code]['items'][ $option ] = $options[$code]['items'][ $option ];
								}
							}
						}
					}
				}
			}
		}
		return $this->_selectedParams;
	}
}
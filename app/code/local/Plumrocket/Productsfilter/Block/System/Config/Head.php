<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Product_Filter-v1.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Productsfilter_Block_System_Config_Head extends Mage_Adminhtml_Block_Template
{
	protected function _prepareLayout()
	{
		if (Mage::app()->getRequest()->getParam('section') == 'productsfilter') {
			$head = $this->getLayout()->getBlock('head');
			$head->addJs('plumrocket/jquery-1.9.1.min.js');
			$head->addItem('skin_js', 'js/plumrocket/productsfilter/system_config.js');
			$head->addItem('skin_js', 'js/plumrocket/productsfilter/jquery-ui-1.10.4.custom.min.js');
			$head->addItem('skin_js', 'js/plumrocket/productsfilter/jquery.ui.touch-punch.min.js');
			$head->addItem('skin_css', 'css/plumrocket/productsfilter/system_config.css');
		}
	    return parent::_prepareLayout();
	}
}

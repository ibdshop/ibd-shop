<?php

class Envalo_Widget_Model_Widget_Widget_Instance extends Mage_Widget_Model_Widget_Instance {

    const SPECIFIC_PAGE_LAYOUT_HANDLE     = 'cms_page_{{ID}}';
    const ALL_PAGE_LAYOUT_HANDLE          = 'cms_page_view';

    const SPECIFIC_BRAND_HANDLE		      = 'ibd_brand_{{ID}}';
    const ALL_BRAND_HANDLE			      = 'ibd_brand_view';

    const SPECIFIC_GROUP_HANDLE			  = 'ibd_group_{{ID}}';
    const ALL_GROUP_HANDLE			      = 'ibd_group_view';

    const SPECIFIC_REGION_HANDLE		  = 'ibd_region_{{ID}}';
    const ALL_REGION_HANDLE				  = 'ibd_region_view';


    protected function _construct()
    {
        parent::_construct();

        $this->_layoutHandles['specific_cms_page'] = self::ALL_PAGE_LAYOUT_HANDLE;
        $this->_specificEntitiesLayoutHandles['specific_cms_page'] = self::SPECIFIC_PAGE_LAYOUT_HANDLE;

        $this->_layoutHandles['specific_ibd_brand'] = self::ALL_BRAND_HANDLE;
        $this->_specificEntitiesLayoutHandles['specific_ibd_brand'] = self::SPECIFIC_BRAND_HANDLE;

        $this->_layoutHandles['specific_ibd_group'] = self::ALL_GROUP_HANDLE;
        $this->_specificEntitiesLayoutHandles['specific_ibd_group'] = self::SPECIFIC_GROUP_HANDLE;

        $this->_layoutHandles['specific_ibd_region'] = self::ALL_REGION_HANDLE;
        $this->_specificEntitiesLayoutHandles['specific_ibd_region'] = self::   SPECIFIC_REGION_HANDLE;
    }
    
    protected function _beforeSave()
    {
        //Mage::log('before save: '.json_encode($this->getData('widget_parameters')));
        if (is_array($this->getData('widget_parameters'))) {
            $params = $this->getData('widget_parameters');
            foreach($params as $k => $v){
                if( !is_string($v) )
                {
                    Mage::log('encoded: '.json_encode($v));
                    $params[$k] = json_encode($v);
                }
                elseif(strpos($v,'/cms_wysiwyg/directive/___directive/') !== false){
                    $parts = explode('/',parse_url($v, PHP_URL_PATH));
                    $key = array_search('___directive', $parts);
                    if($key !== false){
                        $directive = $parts[$key+1];
                        $src = Mage::getModel('core/email_template_filter')->filter(Mage::helper('core')->urlDecode($directive));
                        if(!empty($src)){
                            $params[$k] = parse_url($src, PHP_URL_PATH);
                        }
                    }
                }
                $params[$k] = html_entity_decode($params[$k]);
            }
            $this->setData('widget_parameters', $params);
        }
        //Mage::log('after save: '.json_encode($this->getData('widget_parameters')));
        return parent::_beforeSave();
    }
}
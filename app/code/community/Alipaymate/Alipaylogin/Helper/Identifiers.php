<?php
class Alipaymate_Alipaylogin_Helper_Identifiers extends Mage_Core_Helper_Abstract
{

    /**
     * Assigns a new identifier to a customer
     *
     * @param int $customer_id
     * @param string $identifier
     */
    public function saveIdentifier($data)
    {
        $user_id   =  isset($data['user_id']  ) ? $data['user_id']  : '';
        $real_name =  isset($data['real_name']) ? $data['real_name']: '';
        $token     =  isset($data['token']    ) ? $data['token']    : '';

        $_helper = Mage::helper('alipaylogin');
        $_helper->log('alipaylogin-return saveIdentifier', $data);

        try {
            $firstName  = mb_substr($real_name, 0, 1, 'UTF-8');
            $lastName   = mb_substr($real_name, 1, 100, 'UTF-8');
            $email      = isset($data['email']) ? $data['email'] : $user_id;

            if (strpos($email, '@') == false) {
                $email .= '@alipay.com';
            }

            $customer = Mage::getModel("customer/customer");
            $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
            $customer->loadByEmail($email);

            $customer_id = $customer->getId();

            // add new customer, if $email not exists
            if (empty($customer_id)) {
                $customer->setWebsiteId(Mage::app()->getWebsite()->getId())
                        ->setEmail($email)
                        ->setFirstname($firstName)
                        ->setLastname($lastName)
                        ->setAlipayToken($token)
                        ->setPassword($customer->generatePassword(10))
                        ->save();

                $customer->setConfirmation(null);
                $customer->save();
                $customer_id = $customer->getId();
            }

            if (empty($customer_id)) {
                return false;
            }

            // save identifier
            $identifier = Mage::getModel('alipaylogin/identifiers');
            $identifier_id = $identifier->getCollection()->addFieldToFilter('user_id', $user_id)->getFirstItem()->getId();

            if (empty($identifier_id)) {
                $identifier_id = 0;
            }

            $_helper->log('alipaylogin-return identifier_id', $identifier_id);

            $identifier->load($identifier_id)
                ->setUserId(       $user_id)
                ->setRealName(     $real_name)
                ->setToken(        $token)                    
                ->setCustomerId(   $customer_id)
                ->save();

            return $customer;
        } catch (Exception $e) {
            $_helper->log('alipaylogin-return saveIdentifier Exception', $e->getMessage());
        }

        return false;
    }

    /**
     * Gets a customer by identifier
     *
     * @param string $identifier
     * @return Mage_Customer_Model_Customer
     */
    public function getCustomer($identifier)
    {
        $collection = Mage::getModel('alipaylogin/identifiers')
            ->getCollection()
            ->addFieldToFilter('user_id', $identifier);

        if ($collection->getSize()) {
            $customer_id = $collection->getFirstItem()->getCustomerId();

            if (!empty($customer_id)) {
                $customer = Mage::getModel('customer/customer')
                    ->getCollection()
                    ->addFieldToFilter('entity_id', $customer_id)
                    ->getFirstItem();

                return $customer;
            }
        }

        return false;
    }


    public function getCustomerByEmail($email)
    {
        $customer = Mage::getModel("customer/customer");
        $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
        $customer->loadByEmail($email);

        return $customer;
    }

}

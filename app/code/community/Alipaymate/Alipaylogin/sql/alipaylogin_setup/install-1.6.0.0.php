<?php
$installer = $this;
$installer->startSetup();

$installer->run("
    create table if not exists {$this->getTable('social_login_alipay')} (
      id            int not null auto_increment,
      customer_id   int null,
      user_id       varchar(32)  not null default '',
      real_name     varchar(64)  not null default '',
      token         varchar(64)  not null default '',    
      primary key (id), unique(user_id)
    ) engine=innodb default charset=utf8;
");

$installer->endSetup();
<?php

class Alipaymate_Alipaylogin_ProcessingController extends Mage_Core_Controller_Front_Action
{
    protected function _getHelper()
    {
        return Mage::helper('alipaylogin');
    }

    /**
     * Redirect to alipay
     */
    public function redirectAction()
    {
        $session = Mage::getSingleton('core/session');

        try {
            $url = $this->_getRefererUrl();
            $session->setBeforeAlipayAuthUrl($url);

            $this->getResponse()->setBody($this->getLayout()->createBlock('alipaylogin/redirect')->toHtml());
            return;
        } catch (Exception $e) {
            Mage::logException($e);
            $session->addNotice($e->getMessage());
        }
    }


    /**
     * Alipay Return
     */
    public function returnAction()
    {
        $request = $this->getRequest()->getQuery();

        $_helper = $this->_getHelper()->setReturnLog();
        $_helper->log('alipaylogin-return', $request);

        try {
            $login   = Mage::getModel('alipaylogin/login');
            $config  = $login->prepareConfig();

            $alipay = Mage::getModel('alipaylogin/core');
            $alipay->setConfig($config);

            if (! isset($request['user_id'])) {
            	Mage::throwException($_helper->__('Sorry, Alipay login failed(Invalid user_id)!'));
            }

            /*
             * customer login
             */
            $identifierHelper = Mage::helper('alipaylogin/identifiers');
            $customer = $identifierHelper->getCustomer($request['user_id']);
            
            if (!$customer || ! $customer->getId()) {
                $customer = $identifierHelper->saveIdentifier($request);
            }

            Mage::getSingleton('customer/session')->setCustomerAsLoggedIn($customer);
        } catch (Exception $e) {
            $_helper->log('alipaylogin-return (error)', $e->getMessage());
            Mage::getSingleton('core/session')->addNotice($_helper->__('Sorry, Alipay login failed!'));
        }

        if ($url = Mage::getSingleton('core/session')->getBeforeAlipayAuthUrl()) {
            header('Location: ' . $url);
            exit();
        }
    }
}
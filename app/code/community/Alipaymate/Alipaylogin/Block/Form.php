<?php

class Alipaymate_Alipaylogin_Block_Form extends Mage_Payment_Block_Form
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('alipaylogin/form.phtml');
    }

    public function getPaymethod() {
		return Mage::getStoreConfig('payment/alipaylogin/paymethod');
    }
}
<?php

class Alipaymate_Alipaylogin_Model_Resource_Identifiers extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Resource initialization
     */
    protected function _construct()
    {
        $this->_init('alipaylogin/identifiers', 'id');
    }
}

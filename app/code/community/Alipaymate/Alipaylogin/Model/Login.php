<?php

class Alipaymate_Alipaylogin_Model_Login extends Mage_Core_Model_Abstract
{
    protected $_code  = 'alipaylogin';
    protected $_formBlockType = 'alipaylogin/form';

    public function prepareConfig()
    {
        $config = array(
            '_input_charset'    => 'utf-8',
            'service'           => 'alipay.auth.authorize',
            'target_service'    => 'user.auth.quick.login',
            'partner'           => $this->getConfigData('customer/alipaylogin/partner_id'),
            'sign_type'         => 'MD5',
            'sign'              => $this->getConfigData('customer/alipaylogin/security_key'),
            'return_url'        => $this->getReturnURL(),
        );

        return $config;
    }

	public function getRedirectURL()
	{
        return Mage::getUrl('alipaylogin/processing/redirect/', array('_secure' => true));
	}
	
	public function getReturnURL()
	{
        return Mage::getUrl('alipaylogin/processing/return/', array('_secure' => true));
	}
	
	private function getConfigData($key) {
	    return Mage::getStoreConfig($key);
	}
}
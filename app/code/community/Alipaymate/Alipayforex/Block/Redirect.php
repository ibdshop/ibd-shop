<?php

class Alipaymate_Alipayforex_Block_Redirect extends Mage_Core_Block_Abstract
{
    protected function _toHtml()
    {
        $payment = Mage::getModel('alipayforex/payment');
        $config  = $payment->prepareConfig();
        $params  = $payment->prepareBizData();

        $alipayforex = Mage::getModel('alipayforex/core');
        $alipayforex->setConfig($config);
        $alipayforex->setBizParams($params);

        $action        = $alipayforex->getAlipayUrl();
        $requestHtml   = $alipayforex->createRequestHtml();
        $redirectImg   = $this->getSkinUrl('images/redirect.gif');
        $redirectText  = $this->__('You will be redirected to the Alipay website in a few seconds ...');
        $redirectTitle = $this->__('Redirect to Alipay');

        $html = <<<EOT
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>{$redirectTitle}</title>
        <style>
            .container {
                font-family: Tahoma,Verdana,Arial;
                font-size: 13px;
                margin:0 auto;
                width: 100%;
            }

            .container p {
                border: 1px solid #efefef;
                width: 50%;
                vertical-align: middle;
                padding: 15px;
                text-align: center;
                margin:0 auto;
                margin-top: 45px;
            }

            .container img {
                vertical-align: middle;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <p>{$redirectText}</p>

            <form name="alipaysubmit" id="alipaysubmit" method="post" action="{$action}">
                {$requestHtml}
            </form>
        </div>

        <script type="text/javascript">
            document.getElementById("alipaysubmit").submit();
        </script>
    </body>
</html>
EOT;
        return $html;
    }
}

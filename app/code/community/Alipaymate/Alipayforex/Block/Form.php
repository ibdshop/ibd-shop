<?php

class Alipaymate_Alipayforex_Block_Form extends Mage_Payment_Block_Form
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('alipayforex/form.phtml');
    }

    public function getPaymethod() {
		return Mage::getStoreConfig('payment/alipayforex/paymethod');
    }
}
<?php

class Alipaymate_Alipayforex_ProcessingController extends Mage_Core_Controller_Front_Action
{
    /**
     * Get singleton of Checkout Session Model
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }

    protected function _getHelper()
    {
        return Mage::helper('alipayforex');
    }

    /**
     * when customer selects Alipayforex payment method
     */
    public function redirectAction()
    {
        try {
            $session = $this->_getCheckout();

            $order = Mage::getModel('sales/order');
            $order->loadByIncrementId($session->getLastRealOrderId());

            if (!$order->getId()) {
                Mage::throwException('No order for processing found');
            }

            $this->getResponse()->setBody($this->getLayout()->createBlock('alipayforex/redirect')->toHtml());

            return;
        } catch (Mage_Core_Exception $e) {
            $this->_getCheckout()->addError($e->getMessage());
        } catch(Exception $e) {
            Mage::logException($e);
        }

        $this->_redirect('checkout/cart');
    }

    /**
     * Alipayforex Return
     */
    public function returnAction()
    {
        $request = $this->getRequest()->getQuery();

        $this->_getHelper()->setReturnLog()->log('alipayforex-return', $request);

        try {
            $errorMessage = Mage::helper('alipayforex')->__('Sorry, Alipay payment failed, Please contact us!');

            if (empty($request['trade_status']) || $request['trade_status']!='TRADE_FINISHED') {
                Mage::throwException($errorMessage);
            }

            // check return
            $alipayforex = Mage::getModel('alipayforex/payment');
            $config = $alipayforex->prepareConfig();

            $alipayforex_core = Mage::getModel('alipayforex/core');
            $alipayforex_core->setConfig($config);
            $result = $alipayforex_core->verifyReturn($request);

            if (!$result) {
                Mage::throwException($errorMessage);
            }

            // update order status
            $trade_status = $request['trade_status'];
            // $orderId      = $request['out_trade_no'];

            $out_trade_no = $request['out_trade_no'];
            list($orderId,) = explode('-', $out_trade_no);

            $state   = '';
            $message = '';

            switch ($trade_status) {
                case 'TRADE_SUCCESS':
                    $state   = Mage_Sales_Model_Order::STATE_PROCESSING;
                    $message = 'Order status: success';
                    break;
                case 'TRADE_FINISHED':
                    $state   = Mage_Sales_Model_Order::STATE_PROCESSING;
                    $message = 'Order status: finished';
                    break;
                case 'TRADE_CLOSED':
                    $state   = Mage_Sales_Model_Order::STATE_CLOSED;
                    $message = 'Order status: closed';
                    break;
                default:
                    break;
            }

            $order = Mage::getModel('sales/order');
            $order->loadByIncrementId($orderId);

            if (empty($state)) {
                $state = $order->getStatus();
            }

            if ($state == Mage_Sales_Model_Order::STATE_PROCESSING) {
                if ($order->getStatus() != Mage_Sales_Model_Order::STATE_PROCESSING) {
                    // make invoice
                    if ($order->canInvoice()) {
                        $invoice = $order->prepareInvoice();
                        $invoice->register()->capture();
                        Mage::getModel('core/resource_transaction')
                            ->addObject($invoice)
                            ->addObject($invoice->getOrder())
                            ->save();
                    }

                    $order->addStatusToHistory($state, Mage::helper('alipayforex')->__('Order status: payment successful'));
                    $order->sendNewOrderEmail();
                    $order->setEmailSent(true);
                    $order->setIsCustomerNotified(true);
                }
            }

            $order->addStatusToHistory($state, Mage::helper('alipayforex')->__($message));
            $order->save();
            //

            header('Location: ' . Mage::getUrl('checkout/onepage/success', array('_secure' => true)));
            exit();
        } catch (Exception $e) {
            Mage::logException($e);
            header('Location: ' . Mage::getUrl('checkout/onepage/failure', array('_secure' => true)));
            exit();
        }
    }


    /**
     * Alipayforex notify
     */
    public function notifyAction()
    {
        $request = null;

        if ($this->getRequest()->isPost()) {
            $request = $this->getRequest()->getPost();
        }

        $this->_getHelper()->setNotifyLog()->log('alipayforex-notify', $request);

        if (empty($request) || !isset($request['trade_status'])) {
            $this->error();
        }

        // check notify
        $alipayforex = Mage::getModel('alipayforex/payment');
        $config = $alipayforex->prepareConfig();

        $alipayforex_core = Mage::getModel('alipayforex/core');
        $alipayforex_core->setConfig($config);
        $result = $alipayforex_core->verifyNotify($request);

        if (!$result) {
            $this->error();
        }

        // update order status
        $trade_status = $request['trade_status'];
        // $orderId      = $request['out_trade_no'];

        $out_trade_no = $request['out_trade_no'];
        list($orderId,) = explode('-', $out_trade_no);

        $state   = '';
        $message = '';

        switch ($trade_status) {
            case 'TRADE_SUCCESS':
                $state   = Mage_Sales_Model_Order::STATE_PROCESSING;
                $message = 'Order status: success';
                break;
            case 'TRADE_FINISHED':
                $state   = Mage_Sales_Model_Order::STATE_PROCESSING;
                $message = 'Order status: finished';
                break;
            case 'TRADE_CLOSED':
                $state   = Mage_Sales_Model_Order::STATE_CLOSED;
                $message = 'Order status: closed';
                break;
            default:
                break;
        }

        try {
            $order = Mage::getModel('sales/order');
            $order->loadByIncrementId($orderId);

            if (empty($state)) {
                $state = $order->getStatus();
            }

            if ($state == Mage_Sales_Model_Order::STATE_PROCESSING) {
                if ($order->getStatus() != Mage_Sales_Model_Order::STATE_PROCESSING) {
                    // make invoice
                    if ($order->canInvoice()) {
                        $invoice = $order->prepareInvoice();
                        $invoice->register()->capture();
                        Mage::getModel('core/resource_transaction')
                            ->addObject($invoice)
                            ->addObject($invoice->getOrder())
                            ->save();
                    }

                    $order->addStatusToHistory($state, Mage::helper('alipayforex')->__('Order status: payment successful'));
                    $order->sendNewOrderEmail();
                    $order->setEmailSent(true);
                    $order->setIsCustomerNotified(true);
                }
            }

            $order->addStatusToHistory($state, Mage::helper('alipayforex')->__($message));
            $order->save();
            $this->success();
        } catch (Exception $e) {
            Mage::logException($e);
            $this->error();
        }
    }


    private function success()
    {
        echo 'success';
        exit();
    }


    private function error()
    {
        echo 'fail';
        exit();
    }
}
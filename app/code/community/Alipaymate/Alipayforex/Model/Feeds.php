<?php

class Alipaymate_Alipayforex_Model_Feeds extends Mage_AdminNotification_Model_Feed
{
	public function getFeedUrl()
	{
	    $feedUrl = 'http://alipaymate.com/update/alipayforex/feed.rss';

	    return $feedUrl;
	}

	public function check()
	{
	    return Mage::getModel('alipayforex/feeds')->checkUpdate();
	}

    public function getFrequency()
    {
        return 3600 * 24;
    }

    public function getLastUpdate()
    {
        return Mage::app()->loadCache('alipaymate_alipayforex_notifications_lastcheck');
    }

    public function setLastUpdate()
    {
        Mage::app()->saveCache(time(), 'alipaymate_alipayforex_notifications_lastcheck');

        return $this;
    }
}
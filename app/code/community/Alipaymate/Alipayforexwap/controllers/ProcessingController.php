<?php

class Alipaymate_Alipayforexwap_ProcessingController extends Mage_Core_Controller_Front_Action
{
    /**
     * Get singleton of Checkout Session Model
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }

    protected function _getHelper()
    {
        return Mage::helper('alipayforexwap');
    }

    /**
     * when customer selects Alipayforexwap payment method
     */
    public function redirectAction()
    {
        try {
            $session = $this->_getCheckout();

            $order = Mage::getModel('sales/order');
            $order->loadByIncrementId($session->getLastRealOrderId());

            if (!$order->getId()) {
                Mage::throwException('No order for processing found');
            }

            $this->getResponse()->setBody($this->getLayout()->createBlock('alipayforexwap/redirect')->toHtml());

            return;
        } catch (Mage_Core_Exception $e) {
            $this->_getCheckout()->addError($e->getMessage());
        } catch(Exception $e) {
            Mage::logException($e);
        }

        $this->_redirect('checkout/cart');
    }

    /**
     * Alipayforexwap Return
     */
    public function returnAction()
    {
        $request = $this->getRequest()->getQuery();

        $this->_getHelper()->setReturnLog()->log('alipayforexwap-return', $request);

        try {
            $errorMessage = Mage::helper('alipayforexwap')->__('Sorry, Alipay payment failed, Please contact us!');

            if (empty($request['trade_status']) || $request['trade_status']!='TRADE_FINISHED') {
                Mage::throwException($errorMessage);
            }

            header('Location: ' . Mage::getUrl('checkout/onepage/success', array('_secure' => true)));
            exit();
        } catch (Exception $e) {
            Mage::logException($e);
            header('Location: ' . Mage::getUrl('checkout/onepage/failure', array('_secure' => true)));
            exit();
        }
    }

    /**
     * Alipayforexwap notify
     */
    public function notifyAction()
    {
        $request = null;

        if ($this->getRequest()->isPost()) {
            $request = $this->getRequest()->getPost();
        }

        $this->_getHelper()->setNotifyLog()->log('alipayforexwap-notify', $request);

        if (empty($request) || !isset($request['trade_status'])) {
            $this->error();
        }

        // check notify
        $alipayforexwap = Mage::getModel('alipayforexwap/payment');
        $config = $alipayforexwap->prepareConfig();

        $alipayforexwap_core = Mage::getModel('alipayforexwap/core');
        $alipayforexwap_core->setConfig($config);
        $result = $alipayforexwap_core->verifyResponse($request);

        if (!$result) {
            $this->error();
        }

        // update order status
        $trade_status = $request['trade_status'];
        // $orderId      = $request['out_trade_no'];

        $out_trade_no = $request['out_trade_no'];
        list($orderId,) = explode('-', $out_trade_no);
        
        $state   = '';
        $message = '';

        switch ($trade_status) {
            case 'TRADE_SUCCESS':
                $state   = Mage_Sales_Model_Order::STATE_PROCESSING;
                $message = 'Order status: success';
                break;
            case 'TRADE_FINISHED':
                $state   = Mage_Sales_Model_Order::STATE_PROCESSING;
                $message = 'Order status: finished';
                break;
            case 'TRADE_CLOSED':
                $state   = Mage_Sales_Model_Order::STATE_CLOSED;
                $message = 'Order status: closed';
                break;
            default:
                $state   = '';
                $message = $trade_status;
                break;
        }

        try {
            $order = Mage::getModel('sales/order');
            $order->loadByIncrementId($orderId);

            if (empty($state)) {
                $state = $order->getStatus();
            }

            if ($state == Mage_Sales_Model_Order::STATE_PROCESSING) {
                if ($order->getStatus() != Mage_Sales_Model_Order::STATE_PROCESSING) {
                    // make invoice
                    if ($order->canInvoice()) {
                        $invoice = $order->prepareInvoice();
                        $invoice->register()->capture();
                        Mage::getModel('core/resource_transaction')
                            ->addObject($invoice)
                            ->addObject($invoice->getOrder())
                            ->save();
                    }

                    $order->addStatusToHistory($state, Mage::helper('alipayforexwap')->__('Order status: payment successful'));
                    $order->sendNewOrderEmail();
                    $order->setEmailSent(true);
                    $order->setIsCustomerNotified(true);
                }
            }

            $order->addStatusToHistory($state, Mage::helper('alipayforexwap')->__($message));
            $order->save();
            $this->success();
        } catch (Exception $e) {
            Mage::logException($e);
            $this->error();
        }
    }


    private function success()
    {
        echo 'success';
        exit();
    }


    private function error()
    {
        echo 'fail';
        exit();
    }
}
<?php

class Alipaymate_Alipayforexwap_Model_Core
{
    const MODULE_PAYMENT_ALIPAY_GATEWAY           = 'https://mapi.alipay.com/gateway.do?';
    const MODULE_PAYMENT_ALIPAY_NOTIFY_VERIFY_URL = 'https://mapi.alipay.com/gateway.do?service=notify_verify&';

    private $_gateway    = self::MODULE_PAYMENT_ALIPAY_GATEWAY;
    private $_notify_verify_url = self::MODULE_PAYMENT_ALIPAY_NOTIFY_VERIFY_URL;

    private $_config     = array('service'           => ''
                                ,'partner'           => ''
                                ,'_input_charset'    => ''
                                ,'sign_type'         => ''
                                ,'sign'              => ''
                                ,'notify_url'        => ''
                                ,'return_url'        => ''
                         );

    private $_bizparam   = array('out_trade_no'        => ''
                                ,'subject'             => ''
                                ,'currency'            => ''
                                ,'total_fee'           => ''
                                ,'rmb_fee'             => ''
                                ,'merchant_url'        => ''
                                ,'body'                => ''
                         );


    private function getHelper()
    {
        return Mage::helper('alipayforexwap');
    }

    public function setConfig($config)
    {
        foreach ($config as $name => $value) {
            if (array_key_exists($name, $this->_config)) {
                $this->_config[$name] = trim($value);
            }
        }
    }

    public function getConfig()
    {
        return $this->_config;
    }

    public function setBizParams($params)
    {
        foreach ($params as $name => $value) {
            if (array_key_exists($name, $this->_bizparam)) {
                $this->_bizparam[$name] = trim($value);
            }
        }
    }

    public function getAlipayUrl($charset = 'utf-8') {
        $url = self::MODULE_PAYMENT_ALIPAY_GATEWAY;

        if (!empty($charset)) {
            $url .= '_input_charset=' . $charset;
        }

        return $url;
    }

    // request html
    public function createRequestHtml($method = 'post', $button_title = 'submit')
    {
        $request_data = $this->generateRequestData();
        $this->getHelper()->log('request data', $request_data);

        $action = $this->_gateway . '_input_charset=' . $this->_config['_input_charset'];

        $html = '';

        foreach ($request_data as $key => $val) {
            $html .= "<input type='hidden' name='" . $key . "' value='" . $val . "' />";
        }

        $this->getHelper()->log('request html', $html);

        return $html;
    }

    // verify response
    public function verifyResponse($data)
    {
        $this->getHelper()->log('org response data', $data);

        // check input
        if (empty($data)) {
            return false;
        }

        $sign      = isset($data['sign'])      ? $data['sign']      : '';
        $notify_id = isset($data['notify_id']) ? $data['notify_id'] : '';

        if (empty($sign) || empty($notify_id)) {
            return false;
        }

        // check sign
        $data   = $this->filterData($data);
        $data   = $this->sortData($data);
        $mysign = $this->generateMySign($data);

        $this->getHelper()->log('fixed response data', $data);
        $this->getHelper()->log('mysign', $mysign);

        if ($sign == $mysign && $this->isAlipayforexwapResponse($notify_id)) {
            return true;
        }

        return false;
    }

    private function isAlipayforexwapResponse($notify_id)
    {
        $verify_url =  $this->_notify_verify_url. 'partner=' . $this->_config['partner'] . '&notify_id=' . $notify_id;
        $response = $this->getHttpResponseBySocket($verify_url);

        $this->getHelper()->log('notify verify respons', $response);

        if (preg_match("/true$/i", $response)) {
            return true;
        }

        return false;
    }

    private function getHttpResponseBySocket($url, $charset = '', $timeout = 60)
    {
        $charset  = trim($charset);
        $url_arr  = parse_url($url);
        $hostname = $url_arr['host'];
        $port     = '';

        switch ($url_arr['scheme']) {
            case 'https':
                $hostname = 'ssl://' . $hostname;
                $port     = '443';
                break;
            case 'http':
                $hostname = 'tcp://' . $hostname;
                $port     = '80';
                break;
            default:
                break;
        }

        $response = '';
        $errno    = '';
        $errstr   = '';
        $data     = '';

        $fp = fsockopen($hostname, $port, $errno, $errstr, $timeout);

        if (!$fp) {
            $this->getHelper()->log('open socket failed', $errstr);
            return false;
        }

        if (empty($charset)) {
            $data .= 'POST ' . $url_arr['path'] . " HTTP/1.1\r\n";
        } else {
            $data .= 'POST ' . $url_arr['path'] . '?_input_charset=' . $charset . " HTTP/1.1\r\n";
        }

        $data .= "Host: " . $url_arr['host'] . "\r\n";
        $data .= "Content-type: application/x-www-form-urlencoded\r\n";
        $data .= "Content-length: " . strlen($url_arr['query']) . "\r\n";
        $data .= "Connection: close\r\n\r\n";
        $data .= $url_arr['query'] . "\r\n\r\n";

        fwrite($fp, $data);

        while (!feof($fp)) {
            $response .= fgets($fp, 1024);
        }

        fclose($fp);

        $response = trim(strstr($response, "\r\n\r\n"), "\r\n");

        return $response;
    }


    private function generateRequestData()
    {
        $data = array_merge($this->_config, $this->_bizparam);
        $data = $this->filterData($data);
        $data = $this->sortData($data);

        $mysign = $this->generateMySign($data);

        $data['sign']      = $mysign;
        $data['sign_type'] = $this->_config['sign_type'];

        return $data;
    }


    private function generateMySign($sort_params)
    {
        // convert params to string
        $str = '';

        foreach ($sort_params as $key => $val) {
            $str .= ($key . '=' . $val . '&');
        }

        $str = rtrim($str, '&');

        if (get_magic_quotes_gpc()) {
            $str = stripslashes($str);
        }

        // attach sign to string
        $str .= $this->_config['sign'];

        // generate my sign
        $mysign = '';

        switch ($this->_config['sign_type']) {
            case 'MD5':
                $mysign = md5($str);
                break;
        }

        return $mysign;
    }

    private function filterData($params)
    {
        foreach ($params as $key => $val) {
            if ($key == 'sign' || $key == 'sign_type' || $val == '') {
                unset($params[$key]);
            }
        }

        return $params;
    }

    private function sortData($params)
    {
        ksort($params);
        reset($params);
        return $params;
    }
}
<?php

class Alipaymate_Alipayforexwap_Model_Feeds extends Mage_AdminNotification_Model_Feed
{
	public function getFeedUrl()
	{
	    $feedUrl = 'http://alipaymate.com/update/alipayforexwap/feed.rss';

	    return $feedUrl;
	}

	public function check()
	{
	    return Mage::getModel('alipayforexwap/feeds')->checkUpdate();
	}

    public function getFrequency()
    {
        return 3600 * 24;
    }

    public function getLastUpdate()
    {
        return Mage::app()->loadCache('alipaymate_alipayforexwap_notifications_lastcheck');
    }

    public function setLastUpdate()
    {
        Mage::app()->saveCache(time(), 'alipaymate_alipayforexwap_notifications_lastcheck');

        return $this;
    }
}
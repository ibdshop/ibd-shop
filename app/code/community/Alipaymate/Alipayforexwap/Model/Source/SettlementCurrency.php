<?php

class Alipaymate_Alipayforexwap_Model_Source_SettlementCurrency
{
    public function toOptionArray()
    {       
        return array(
            array('value' =>'AUD',  'label' => 'AUD'),
            array('value' =>'CAD',  'label' => 'CAD'),
            array('value' =>'CHF',  'label' => 'CHF'),
            array('value' =>'DKK',  'label' => 'DKK'),
            array('value' =>'EUR',  'label' => 'EUR'),
            array('value' =>'GBP',  'label' => 'GBP'),
            array('value' =>'HKD',  'label' => 'HKD'),
            array('value' =>'JPY',  'label' => 'JPY'),
            array('value' =>'MOP',  'label' => 'MOP'),
            array('value' =>'NOK',  'label' => 'NOK'),
            array('value' =>'NZD',  'label' => 'NZD'),
            array('value' =>'SEK',  'label' => 'SEK'),
            array('value' =>'SGD',  'label' => 'SGD'),
            array('value' =>'THB',  'label' => 'THB'),
            array('value' =>'USD',  'label' => 'USD'),  
        );
    }
}

<?php

class Alipaymate_Alipayforexwap_Model_Payment extends Mage_Payment_Model_Method_Abstract
{
    protected $_code  = 'alipayforexwap';
    protected $_formBlockType = 'alipayforexwap/form';

    protected $_isGateway               = false;
    protected $_canAuthorize            = true;
    protected $_canCapture              = true;
    protected $_canVoid                 = false;
    protected $_canUseInternal          = false;
    protected $_canUseCheckout          = true;
    protected $_canUseForMultishipping  = false;
    protected $_canRefund               = false;

    protected $_order;


    /**
     * Get order model
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
		if (!$this->_order) {
			$orderIncrementId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
			$this->_order = Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
		}

		return $this->_order;
    }

    /**
     * Capture payment
     *
     * @param   Varien_Object $orderPayment
     * @return  Mage_Payment_Model_Abstract
     */
    public function capture(Varien_Object $payment, $amount)
    {
        $payment->setStatus(self::STATUS_APPROVED)
            ->setLastTransId($this->getTransactionId());

        return $this;
    }

    public function prepareConfig()
    {
        $config = array(
            '_input_charset'    => 'utf-8',
            'service'           => 'create_forex_trade_wap',
            'partner'           => $this->getConfigData('partner_id'),
            'sign_type'         => 'MD5',
            'sign'              => $this->getConfigData('security_key'),
            'notify_url'        => $this->getNotifyURL(),
            'return_url'        => $this->getReturnURL(),
        );

        return $config;
    }


    public function prepareBizData()
    {
        $settlement_currency = $this->getConfigData('settlement_currency');

        if (empty($settlement_currency)) {
            throw new Exception(Mage::helper('alipayforexwap')->__('Undefined Settlement Currency(alipayforexwap).'));
        }

        $order_currency = $this->_getCurrency();
        $order_total    = $this->_getTotalFee();

        $currency  = '';
        $total_fee = '';
        $rmb_fee   = '';

        if ($order_currency == $settlement_currency) {
            $currency  = $settlement_currency;
            $total_fee = $order_total;
        } else if ($order_currency == 'CNY') {
            $currency  = $settlement_currency;
            $rmb_fee   = $order_total;
            $total_fee = '';
        } else { // convert to settlement currency
            $base_currency    = $this->getOrder()->getBaseCurrencyCode();
            $base_grand_total = $this->getOrder()->getBaseGrandTotal();

            $currency  = $settlement_currency;
            $total_fee = Mage::getModel('directory/currency')->load($base_currency)->convert($base_grand_total, $currency);
            $total_fee = sprintf("%.2f", $total_fee);
        }

        // JPY value is int type
        if ($currency == 'JPY') {
            $total_fee = (int)$total_fee;
        }     
        
        $param = array(
            'out_trade_no'       => $this->_getOutTradeNo(),
            'subject'            => $this->_getSubject(),
            'currency'           => $currency,
            'total_fee'          => $total_fee,
            'rmb_fee'            => $rmb_fee,            
            'body'               => $this->_getBody(),
            'merchant_url'       => Mage::getBaseUrl(),
        );

        return $param;
    }

    protected function _getOutTradeNo()
    {
        $out_trade_no = $this->getOrder()->getRealOrderId() . '-' . rand(1000,9999);
        return $out_trade_no;
    }
    
    
    protected function _getTotalFee()
    {
        $total = $this->getOrder()->getGrandTotal();

        $total = sprintf("%.2f", $total);
        return $total;
    }

    protected function _getCurrency()
    {
        $currency = $this->getOrder()->getOrderCurrencyCode();
        return $currency;
    }

    protected function _getSubject()
    {
        return 'Order No: #' . $this->getOrder()->getRealOrderId();
    }

    protected function _getBody() {
        return 'Order No: #' . $this->getOrder()->getRealOrderId();
    }


    /**
     * Return Order place redirect url
     *
     * @return string
     */
    public function getOrderPlaceRedirectUrl()
    {
        return Mage::getUrl('alipayforexwap/processing/redirect', array('_secure' => true));
    }

	protected function getReturnURL()
	{
        return Mage::getUrl('alipayforexwap/processing/return/', array('_secure' => true));
	}

	protected function getNotifyURL()
	{
		return Mage::getUrl('alipayforexwap/processing/notify/', array('_secure' => true));
	}
}
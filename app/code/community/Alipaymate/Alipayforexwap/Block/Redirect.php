<?php

class Alipaymate_Alipayforexwap_Block_Redirect extends Mage_Core_Block_Abstract
{
    protected function _toHtml()
    {
        $payment = Mage::getModel('alipayforexwap/payment');
        $config  = $payment->prepareConfig();
        $params  = $payment->prepareBizData();

        $alipayforexwap = Mage::getModel('alipayforexwap/core');
        $alipayforexwap->setConfig($config);
        $alipayforexwap->setBizParams($params);

        $action        = $alipayforexwap->getAlipayUrl();
        $requestHtml   = $alipayforexwap->createRequestHtml();
        $redirectImg   = $this->getSkinUrl('images/redirect.gif');
        $redirectText  = $this->__('You will be redirected to the Alipay website in a few seconds ...');
        $redirectTitle = $this->__('Redirect to Alipay');

        $html = <<<EOT
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>{$redirectTitle}</title>
        <style>
            .container {
                font-family: Verdana,Arial;
                font-size: 14px;
                margin:0 auto;
                width: 100%;
            }

            .container p {
                vertical-align: middle;
                padding: 15px;
                text-align: center;
                margin:0 auto;
            }

            .container img {
                vertical-align: middle;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <p>{$redirectText}</p>

            <form name="alipaysubmit" id="alipaysubmit" method="post" action="{$action}">
                {$requestHtml}
            </form>
        </div>

        <script type="text/javascript">
            document.getElementById("alipaysubmit").submit();
        </script>
    </body>
</html>
EOT;
        return $html;
    }
}

<?php

class Alipaymate_Alipayforexwap_Block_Form extends Mage_Payment_Block_Form
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('alipayforexwap/form.phtml');
    }

    public function getPaymethod() {
		return Mage::getStoreConfig('payment/alipayforexwap/paymethod');
    }
}
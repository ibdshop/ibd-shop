jQuery.noConflict();
window.IBD_lodash=window.IBD_lodash || _.noConflict();

(function($,_,undefined){

	_.templateSettings = {
		interpolate: /\{\{(.+?)\}\}/g,
		evaluate: /\{\[(.+?)\]\}/g,
		escape: /\{\{-(.+?)\}\}/g
	};

	$(document).ready(function(){
		setupBrandGroup();
		setupGallery();
		setupLocation();
		setupTags();
		setupStyleGuideToggle();
	});

	function setupBrandGroup(){
		if(typeof window.ibdBrandGroups === 'undefined' || window.ibdBrandGroups === null){
			window.ibdBrandGroups = [];
		}

		var brandGroupTemplate = _.template($('#brand-group-info-template').html()),
			brandGroupInfoContainer = $('#brand-group-info-container'),
			brandGroupSelect = $('#group_ids'),
			orphanAddressKey = String.fromCharCode('a'.charCodeAt(0) - 1),
			defaultTemplateOptions = {
				address : '',
				hours : '',
				phone : '',
				driving : '',
				ptrans : '',
				url : '',
				targetblank : false,
				sort : 0
			};

		// if we haven't found the brand-group select element, we aren't on the correct page!
		if(brandGroupSelect.length < 1) return;

		// function for creating divs and setting visibility from multi-select
		var setupBrandGroupDivs = function(){
			var selectedValueArray = brandGroupSelect.val();
			_.forEach(brandGroupSelect[0].options, function(option){

				var groupAddressDiv = $('#brand-group-'+option.value);

				var groupSelected = (_.indexOf(selectedValueArray, option.value) >= 0);
				if(groupAddressDiv.length == 0 && groupSelected)
				{
					brandGroupInfoContainer.prepend(brandGroupTemplate(mergeDefaultOptions({
						addressType : 'group',
						groupName : option.innerHTML,
						targetblank: false,
						groupId : option.value
					})));
				}
				else if(groupAddressDiv.length > 0)
				{
					if(groupSelected) groupAddressDiv.show();
					else groupAddressDiv.hide();
				}
			});
		}

		var mergeDefaultOptions = function(options){
			_.forEach(defaultTemplateOptions, function(val,key){
				if(!(key in options)){
					options[key] = val;
				}
			});
			return options;
		};

		var initializeAddress = function(info, groupId){

			var nan = isNaN(groupId);
			if(nan && groupId.charCodeAt(0) >= orphanAddressKey.charCodeAt(0)){
				orphanAddressKey = groupId;
			}

			brandGroupInfoContainer.append(brandGroupTemplate({
				addressType : nan ? 'orphan' : 'group',
				groupName : 'groupName' in info ? info['groupName'] : '',
				groupId : groupId,
				address : 'address' in info ? info['address'] : '',
				hours : 'hours' in info ? info['hours'] : '',
				phone : 'phone' in info ? info['phone'] : '',
				driving : 'directions' in info ? info['directions']['driving'] : '',
				ptrans : 'directions' in info ? info['directions']['ptrans'] : '',
				url : 'url' in info ? info['url'] : '',
				targetblank : 'targetblank' in info ? info['targetblank'] : false,
				sort : 'sort' in info ? info['sort'] : 0
			}));
		};

		// initialize brand groups from the server-side JS object
		if("sorted" in ibdBrandGroups){
			// if we have the sorted object, let's display them in order
			_.forEach(ibdBrandGroups['sorted'], function(keys, order){
				if(order == 0) brandGroupInfoContainer.append('<div>Hidden from pages</div><hr>');

				_.forEach(keys, function(key){
					initializeAddress(ibdBrandGroups[key], key);
				});

				if(order == 0) brandGroupInfoContainer.append('<div>Active</div><hr>');
			});
		}
		else{
			// old initialization method before we had the sorted object
			_.forEach(ibdBrandGroups, function(info, groupId){
				initializeAddress(info, groupId);
			});
		}

		// update visibility / add any missing
		setupBrandGroupDivs();

		// recheck visible and existing when multi-select clicked
		brandGroupSelect.click(setupBrandGroupDivs);

		// start existing addresses in closed state
		$('.address-detail').toggle();

		// location collapse (use delegate since new ones can be added)
		brandGroupInfoContainer.on('click', '.collapse', function(e){
			$(e.currentTarget).closest('.box').children('.address-detail').slideToggle();
		});

		$('#brand_groups_form').find('td.value').append('<br><button id="add-non-group-address">Add Non-Group Address</button>');
		$('#add-non-group-address').click(function(e){
			e.preventDefault();
			orphanAddressKey = String.fromCharCode(orphanAddressKey.charCodeAt(0) + 1);
			brandGroupInfoContainer.prepend(brandGroupTemplate(mergeDefaultOptions({
				addressType : 'orphan',
				groupName : 'New Address Name',
				groupId : orphanAddressKey
			})));
		});

		// add delete class to orphans scheduled for deletion (use delegate since new ones can be added)
		brandGroupInfoContainer.on('change', 'input.delete-address', function(e){
			var box = $(e.currentTarget).closest('.box');
			if(e.currentTarget.checked){
				box.addClass('delete');
			}
			else{
				box.removeClass('delete');
			}
		});
	};

	function setupGallery(){
		if(window.IBD_Gallery === undefined || window.IBD_Gallery.length === 0) return;

		// this function allows us to isolate the table, notes, and gallery
		var setupFunction = function(item, tableBody, notesUlDiv, galleryInfo, notesToggle){

			var galleryImageCount = 0,
				locatorId = item.data('locatorId'),
				galleryRowTemplate = _.template($('#gallery-row-template-'+locatorId).html());

			notesUlDiv.hide();
			item.uploadify({
				swf : '/js/ibd/lib/uploadify.swf',
				uploader: galleryInfo['url'],
				multi: true,
				formData: {
					form_key: galleryInfo['form_key'],
					relation_type: galleryInfo['relation_type'],
					relation_id: galleryInfo['relation_id'],
				},
				onUploadSuccess: function(file, responseBody){
					var response = JSON.parse(responseBody);

					if(response.errorcode == 0)
						tableBody.append(galleryRowTemplate({
							imagename: response.data.path,
							inputname: galleryInfo.inputname,
							position: '0',
							imageid: response.data.id,
							thumb: response.data.thumb,
							link: '',
							target: 0,
							colSpan: ''
						}));
					else
						console.log(response.message);
				}
			});

			_.forEach(galleryInfo.images, function(image){
				if(image.position > 0) galleryImageCount++;
				tableBody.append(galleryRowTemplate({
					imagename: image.url,
					inputname: galleryInfo.inputname,
					position: image.position,
					imageid: image.id,
					thumb: image.thumb,
					link: image.link,
					target: image.target,
					colSpan: image.colSpan
				}));
			});

			if(galleryImageCount <= 10) notesUlDiv.siblings('.gt-ten-warning').hide();

			notesToggle.click(function(e){
				e.preventDefault();
				notesUlDiv.slideToggle();
			});
		};

		$('.uploadify').each(function(){
			item = $(this);
			var uploadifyRow = item.closest('.uploadify-row'),
				tableBody = uploadifyRow.find('tbody.ibd_travelsuite_gallery'),
				notesUlDiv = uploadifyRow.find('div.notes-ul'),
				galleryInfo = window.IBD_Gallery.shift(),
				notesToggle = uploadifyRow.find('a.notes-toggle');

				setupFunction(item, tableBody, notesUlDiv, galleryInfo, notesToggle);
		});

		window.ibdGalleryPositionChanged = function(input){
			var disableInput = $(input).closest('td').siblings('.cell-disable').children('input');
			var removeInput = $(input).closest('td').siblings('.cell-remove').children('input');
			if(input.value == 0){
				disableInput.prop('checked', true);
				removeInput.prop('checked', false);
				removeInput.closest('tr').removeClass('remove-background');
			}
			else if(input.value < 0){
				disableInput.prop('checked', false);
				removeInput.prop('checked', true);
				removeInput.closest('tr').addClass('remove-background');
			}
			else{
				disableInput.prop('checked', false);
				removeInput.prop('checked', false);
				removeInput.closest('tr').removeClass('remove-background');
			}
		};

		window.ibdGalleryExclude = function(exChkBx){
			var posNpt = $(exChkBx).closest('td').siblings('.cell-position').children();
			if(exChkBx.checked)
			{
				posNpt.val(0);
			}
			else
			{

				posNpt.val(posNpt.closest('tbody').children().length);
			}
		};

		window.ibdGalleryRemove = function(remChkBx){
			$(remChkBx).closest('tr').toggleClass('remove-background');
		};

		window.ibdToggleCell = function(e,td){
			if(e.target.nodeName == 'INPUT') return true;
			var chkBx = $(td).children('input');
			chkBx.prop('checked', !chkBx.prop('checked')).trigger('change');
		};

		window.ibdClickFocus = function(td){
			$(td).children('input').focus();
		};
	};

	function setupLocation(){
		var latInput = $('#lat'),
			lngInput = $('#lng'),
			searchBox = $('#map-search'),
			mapDiv = $('#map-holder'),
			tab = $('#group_tabs_form_location_group'),
			addressArea = $('#address'),
			markerMoved = function(){
				latInput.val(marker.getPosition().lat());
				lngInput.val(marker.getPosition().lng());
			},
			map, marker, search, markers;
		
		tab.click(function(){
			
			// setup the map when the tab is clicked, if it isn't already...
			if(map && marker) return;

			var center, zoom;
			if(latInput.val() == 0 && lngInput.val() == 0){
				center = new google.maps.LatLng(49, 12);
				zoom = 4;
			}
			else{
				center = new google.maps.LatLng(latInput.val(), lngInput.val());
				zoom = 16;
			}

			map = new google.maps.Map(mapDiv[0], {
				zoom : zoom,
				center: center,
				panControl: false,
				zoomControl: true,
				mapTypeControl: true,
				scaleControl: true,
				streetViewControl: false,
				overviewMapControl: false
			});

			marker = new google.maps.Marker({
				position: center,
				map: map,
				title: 'Drag me!',
				draggable:true,
				animation: google.maps.Animation.DROP
			});

			google.maps.event.addListener(marker, 'dragend', markerMoved);

			map.controls[google.maps.ControlPosition.TOP_LEFT].push(searchBox[0]);

			search = new google.maps.places.SearchBox(searchBox[0]);
			google.maps.event.addListener(search, 'places_changed', function(){
				var places = search.getPlaces();
				marker.setPosition(places[0].geometry.location);
				map.panTo(places[0].geometry.location);
				map.setZoom(14);
				markerMoved();

				addressArea.html(places[0].formatted_address);
			});
		});
	};

	function setupTags(){
		var tagsInputMultiSelect = $('.tagsInputMultiSelect');

		_.forEach(tagsInputMultiSelect, function(select){

			var seljct = $(select);
			var source = select.dataset['sourceName'];
			var options = select.dataset['optionsName'];
			var selected = select.dataset['selectedName'];

			if(!source || !selected) {
				console.warn('multiselect source or selected not defined in dataset');
				return;
			}

			// evaluate the names to get the data
			source = eval(source);
			selected = eval(selected);

			// options are optional...
			options = options ? eval(options) : {};
			options['typeahead'] = { source : source };

			seljct.tagsinput(options);

			_.forEach(selected, function(tag){
				seljct.tagsinput('add', tag);
			});
		});
	};

	function setupStyleGuideToggle(){
		$('.style-guide-toggle').click(function(e){
			e.preventDefault();
			$(this).siblings('.style-guide').slideToggle();
		});
	};

})(jQuery,window.IBD_lodash);